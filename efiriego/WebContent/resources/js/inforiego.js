
// create a dataSet with groups
var names = ['Intervalo óptimo - % PAW','VP-P1: 15 cm: %ADP', 'VP-P2: 30 cm: %ADP', 'VP-P3: 40 cm: %ADP', 'P4-ECRN-50: mm Precipitación'];
var groups = new vis.DataSet();

groups.add({
    id: 1,
    content: names[1],
    className: 'custom-linea1',
    options: {
        yAxisOrientation: 'left', // right, left
        drawPoints: false
    }});

groups.add({
    id: 2,
    content: names[2],
    className: 'custom-linea2',
    options: {
        yAxisOrientation: 'left', // right, left
        drawPoints: false
    }
});

groups.add({
    id: 3,
    content: names[3],
    className: 'custom-linea3',
    options: {
        yAxisOrientation: 'left', // right, left
        drawPoints: false
        
    }});

groups.add({
    id: 4,
    content: names[4],
    className: 'custom-barras',
    options: {
        style:'bar',
        yAxisOrientation: 'right', // right, left
        drawPoints: {style: 'square',
            size: 10
        }
        
    }});


var container = document.getElementById('visualization');
var container_week = document.getElementById('visualization_week');



var items = [];

lineahori = [[0],[100],[30]]; 
lineas = [lineahori];

$.getJSON( "resources/json/convertcsv.json", function( data ) {
    var json = data;
    additems(json);

function additems(resultado){
    for (i = 0; i< resultado.length; i ++){
    fechaHora  = resultado[i].FechaHora;
    fechaHora = fechaHora.split(" ");
    fecha = fechaHora[0];
    fecha = fecha.split("/");
    fecha_f = (fecha[2]+"-"+fecha[1]+"-"+fecha[0]);
    fecha_h = fechaHora[1];
    fecha_total = (fecha_f+" "+fecha_h);
    if (i == 0){
        fecha_inicio = fecha_total;
    }
    if (i == resultado.length-1){
    fecha_final = fecha_total;
    }
    port1 = resultado[i].Port1_15Cm;
    port2 = resultado[i].Port2_30Cm;
    port3 = resultado[i].Port3_45Cm;
    port4 = resultado[i].Port4_ECRN50;

    linea1 = resultado[i].AguaDisponible15Cm;
    linea2 = resultado[i].AguaDisponible30Cm;
    linea3 = resultado[i].AguaDisponible45Cm;

    items.push({x: fecha_total, y: linea1, group: 1});
    items.push({x: fecha_total, y: linea2, group: 2});
    items.push({x: fecha_total, y: linea3, group: 3});
    items.push({x: fecha_total, y: port4, group: 4});
    }

    groups.add({
        id: 0,
        content: names[0],
        className: 'custom-lineahori',
        options: {
        yAxisOrientation: 'left', // right, left
        drawPoints: false,
        shaded: {
            orientation: 30 // top, bottom
            }
            }
        });
    items.push({x: '2000-06-12', y: 100, group: 0 });
    items.push({x: '2050-06-12', y: 100, group: 0 });
}

var dataset = new vis.DataSet(items);
var options = {

    dataAxis: {
        showMinorLabels: false,
        right: {
            title: {text: 'Title (right axis)'}
        }
    },

    
    dataAxis: {showMinorLabels: false},
    dataAxis: { left: { title: {text: 'Agua disponible para planta (%)'}, range: {min:0, max:180} },
                right: { title: {text: 'Precipitación (mm)'}, range: {min:0, max:20} } },
    legend: {left:{position:"bottom-left"}},
    start: fecha_inicio,
    end: fecha_final
};
var options_week = {


    dataAxis: {
        showMinorLabels: false,
        right: {
            title: {text: 'Title (right axis)'}
        }
    },

    
    dataAxis: {showMinorLabels: false},
    dataAxis: { left: { title: {text: 'Agua disponible para planta (%)'}, range: {min:0, max:180} },
                right: { title: {text: 'Precipitación (mm)'}, range: {min:0, max:20} } },
    legend: {left:{position:"bottom-left"}},
    start: '2013-07-25', //CAMBIAR A UNA VARIABLE DE SEMANA!!!
    end: '2013-07-31'
};


var graph2d = new vis.Graph2d(container, dataset, groups, options);
var graph2d_week = new vis.Graph2d(container_week, dataset, groups, options_week);



    



function populateExternalLegend(legend_id) {
    var groupsData = groups.get();
    var legendDiv = document.getElementById(legend_id);
    legendDiv.innerHTML = "";

    // get for all groups:
    for (var i = 0; i < groupsData.length; i++) {
        // create divs
        var containerDiv = document.createElement("div");
        var iconDiv = document.createElement("div");
        var descriptionDiv = document.createElement("div");

        // give divs classes and Ids where necessary
        containerDiv.className = 'legend-element-container';
        containerDiv.id = groupsData[i].id + "_legendContainer"
        iconDiv.className = "icon-container";
        descriptionDiv.className = "description-container";

        // get the legend for this group.
        var legend = graph2d.getLegend(groupsData[i].id,30,30);

        // append class to icon. All styling classes from the vis.css have been copied over into the head here to be able to style the
        // icons with the same classes if they are using the default ones.
        legend.icon.setAttributeNS(null, "class", "legend-icon");

        // append the legend to the corresponding divs
        iconDiv.appendChild(legend.icon);
        descriptionDiv.innerHTML = legend.label;
        // determine the order for left and right orientation
        if (legend.orientation == 'left') {
        descriptionDiv.style.textAlign = "left";
        containerDiv.appendChild(iconDiv);
        containerDiv.appendChild(descriptionDiv);
        }
        else {
        descriptionDiv.style.textAlign = "right";
        containerDiv.appendChild(descriptionDiv);
        containerDiv.appendChild(iconDiv);
        }

        // append to the legend container div
        legendDiv.appendChild(containerDiv);

        // bind click event to this legend element.
        containerDiv.onclick = toggleGraph.bind(this,groupsData[i].id);
    }
    }

function toggleGraph(groupId) {
    // get the container that was clicked on.
    var container = document.getElementById(groupId + "_legendContainer")
    // if visible, hide
    if (graph2d.isGroupVisible(groupId) == true) {
        groups.update({id:groupId, visible:false});
        container.className = container.className + " hidden";
    }
    else { // if invisible, show
        groups.update({id:groupId, visible:true});
        container.className = container.className.replace("hidden","");
    }
    }
    var ficha_text = "Ficha de Riego nº ";
    var num_f = "22";
    var num_ficha = ficha_text + num_f;

    var finca_text = "Ficha: "
    var finca_nombre= "Chañe 150828";
    var finca = finca_text + finca_nombre;

    var fecha_p_inicio = "2013-01-04";
    var fecha_p_final = "2013-09-04";
    var descripcion = "Temporada de patatas";
    var periodo = "Periodo de " + fecha_p_inicio + " al " + fecha_p_final;

    var pluviometro_text = "Pluviómetro: ";
    var pluviometro_litros = "15 l/m2";
    var pluviometro = pluviometro_text + pluviometro_litros;
    
    var diagnostico_text = "Diagnóstico: "
    var diagnostico_orden = "NO REGAR";
    var diagnostico = diagnostico_text + diagnostico_orden;

    var div_num_ficha = document.getElementById('ficha_riego');
    div_num_ficha.innerHTML = num_ficha;

    var div_finca = document.getElementById('finca');
    div_finca.innerHTML = finca;

    var div_periodo = document.getElementById('periodo');
    div_periodo.innerHTML =  periodo;

    var div_descripcion = document.getElementById('descripcion');
    div_descripcion.innerHTML = descripcion;

    var div_pluviometro = document.getElementById('pluviometro');
    div_pluviometro.innerHTML =  pluviometro;

    var div_diagnostico = document.getElementById('diagnostico');
    div_diagnostico.innerHTML = diagnostico;

    //getElementById (lunes_fecha, lunes_valor....)


    populateExternalLegend("Legend")
    populateExternalLegend("Legend_week")
});

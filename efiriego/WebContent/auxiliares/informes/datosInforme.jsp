<%@page import="org.json.simple.JSONObject"%>
<%
	JSONObject datos_informe = null;
	Object obj_datos = request.getAttribute("datos_informe");
	if (obj_datos != null)
		datos_informe = (JSONObject) obj_datos;
%>

var datos_informe = <%=datos_informe%>	
var idinforme = datos_informe.idinforme;
var numinforme = datos_informe.numinforme;
var nombre_finca = datos_informe.nombre_finca;
var tipo_de_informe = datos_informe.tipo_de_informe;
var descripcion = datos_informe.nombre_temporada;
var temporada_inicio = datos_informe.temporada_inicio;
var temporada_fin = datos_informe.temporada_fin;
var fecha_p_inicio = datos_informe.fecha_inicio;
var fecha_p_final = datos_informe.fecha_fin;


var finca_text = "Finca: "
var finca = finca_text + nombre_finca.toString();

fecha_p_inicio = fecha_p_inicio.split(" ")[0];
fecha_p_final = fecha_p_final.split(" ")[0];
   
if(tipo_de_informe=="Semanal"){
	var periodo = "Periodo del " + moment(fecha_p_inicio).format('DD/MM/YYYY') + " al " + moment(fecha_p_final).format('DD/MM/YYYY');
} else {
	var periodo = "Informe diario " + moment(fecha_p_inicio).format('DD/MM/YYYY');
}
 

 var div_finca = document.getElementById('finca');
 div_finca.innerHTML = finca;

 var div_periodo = document.getElementById('periodo');
 div_periodo.innerHTML =  periodo;

 var div_descripcion = document.getElementById('descripcion');
 div_descripcion.innerHTML = descripcion;

</br>
<div class="loading" id="loading-popup-informe">
	<div>
		<h1>Cargando Informe</h1>
		<img class="loading-img" src="resources/img/loading.gif">
	</div>
</div>
<div id='cssmenu'>
	<ul>
		<li><a href="SrvInicial">Inicio</a></li>
		<li><a href="#" onclick="javascript:window.history.back()">Listado</a></li>
		<li><a href="j_spring_security_logout" >Desconexion</a></li>        
	</ul>
</div>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>	
	<sec:authorize var="loggedIn" access="isAuthenticated()" />
	<c:choose>
	    <c:when test="${loggedIn}">
	          <p><%=request.getUserPrincipal().getName()%></p>        
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
<hr>
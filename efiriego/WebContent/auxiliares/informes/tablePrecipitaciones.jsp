<h2 id="pluviometro" style="text-align:left">Pluvi�metro: <input id="campo_2" type="text" class="campoComentario"> </h2>
<h2 id="diagnostico" style="text-align:left">Diagn�stico: <input id="campo_3" type="text" class="campoComentario"> </h2>

<table class="table2 tableInfo">
    <tr>
        <th colspan="9" class="blueBox_1">Predicci�n de precipitaci�n (l/m2) hecha el lunes 01/02/2016:</th>
    </tr>

    <tr> 
        <td id="lunes_fecha">Lunes<br><input id="prediccion_fecha_1" class="campoTablaPrecipitacion" readOnly></td>
        <td id="martes_fecha">Martes<br><input id="prediccion_fecha_2" class="campoTablaPrecipitacion" readOnly></td>
        <td id="miercoles_fecha">Mi�rcoles<br><input id="prediccion_fecha_3" class="campoTablaPrecipitacion" readOnly></td>
        <td id="jueves_fecha">Jueves<br><input id="prediccion_fecha_4" class="campoTablaPrecipitacion" readOnly></td>
        <td id="viernes_fecha">Viernes<br><input id="prediccion_fecha_5" type="text" class="campoTablaPrecipitacion" readOnly></td>
        <td id="sabado_fecha" class="blueBox_2" >S�bado<br><input id="prediccion_fecha_6" class="campoTablaPrecipitacion" readOnly></td>
        <td id="domingo_fecha" class="blueBox_3">Domingo<br><input id="prediccion_fecha_7" class="campoTablaPrecipitacion" readOnly></td>
        <td id="lunes_2_fecha" class="blueBox_4" >Lunes<br><input id="prediccion_fecha_8" class="campoTablaPrecipitacion" readOnly></td>
    </tr>
    <tr> 
        <td id="lunes_valor"><input id="campo_4" type="text" class="campoTablaPrecipitacion"></td>
        <td id="martes_valor"><input id="campo_5" type="text" class="campoTablaPrecipitacion"></td>
        <td id="miercoles_valor"><input id="campo_6" type="text" class="campoTablaPrecipitacion"></td>
        <td id="jueves_valor"><input id="campo_7" type="text" class="campoTablaPrecipitacion"></td>
        <td id="viernes_valor"><input id="campo_8" type="text" class="campoTablaPrecipitacion"></td>
        <td id="sabado_valor" class="blueBox_2"><input id="campo_9" type="text" class="campoTablaPrecipitacion"></td>
        <td id="domingo_valor" class="blueBox_3"><input id="campo_10" type="text" class="campoTablaPrecipitacion"></td>
        <td id="lunes_2_valor" class="blueBox_4"><input id="campo_11" type="text" class="campoTablaPrecipitacion"></td>
    </tr>
    <tr>
        <th colspan="9" class="blueBox_1">Informaci�n suministrada por la Agencia Estatal de Meteorolog�a.</th>
    </tr>
    
</table>
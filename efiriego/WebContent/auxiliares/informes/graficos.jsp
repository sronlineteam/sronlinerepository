<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Arrays"%>

<%	
	ArrayList<JSONObject> datos_grafica_port = null;
	Object obj_5 = request.getAttribute("datos_grafica_port");
	if (obj_5 != null)
		datos_grafica_port = (ArrayList<JSONObject>) obj_5;
%>

var groups = new vis.DataSet();	
var options_class = [{
					yAxisOrientation: 'left', 
	  				drawPoints: false },
         	 		{
         	 		style:'bar',
         	 		yAxisOrientation: 'right',
	  				drawPoints: {style: 'square', size: 10
	  				}
	  				}];	


groups.add({
       id: 0,
       content: 'Intervalo óptimo - % PAW',
       className: 'custom-lineahori',
       options: {
       yAxisOrientation: 'left', // right, left
       drawPoints: false,
       shaded: {
           orientation: 30 // top, bottom
           }
           }
       });
   var items = [];
   items.push({x: '2000-06-12', y: 100, group: 0 });
   items.push({x: '2050-06-12', y: 100, group: 0 });
var fechas;
<%
	

	for (int i=0; i < datos_grafica_port.size() ; i++){
		JSONObject port = datos_grafica_port.get(i);
%>
		var option_port = ('<%=port.get("tipo")%>' == 'linea') ? options_class[0] : options_class[1];
		groups.add({
		    id: <%=i+1%>,
		    content: '<%=port.get("etiqueta")%>',
		    className: 'custom-<%=port.get("tipo")%><%=i+1%>',
		    options: option_port
		});
		
		<%
			JSONArray fechas = (JSONArray) port.get("fechas");
			JSONArray valores = (JSONArray) port.get("valores");
			
		
		%>   
			var fechas = JSON.parse('<%=fechas%>');
			var valores = JSON.parse('<%=valores%>');
			
			fecha_inicio = fechas[0];
			fecha_final = fechas[fechas.length-1];	
			
			for (var i = 0; i < fechas.length; i++) {
		 
		    	items.push({x: fechas[i], y: valores[i], group: <%=i+1%>});  
			}
	
<% } %>



var dataset = new vis.DataSet(items);
var options = {

    dataAxis: {
        showMinorLabels: false,
        right: {
            title: {text: 'Title (right axis)'}
        }
    },
  
    dataAxis: {showMinorLabels: false},
    dataAxis: { left: { title: {text: 'Agua disponible para planta (%)'}, range: {min:0, max:180} },
                right: { title: {text: 'Precipitación (mm)'}, range: {min:0, max:20} } },
    legend: {left:{position:"top-left"}},
    start: temporada_inicio,
    end: temporada_fin,
    clickToUse: true
};

var options_week = {
    dataAxis: {
        showMinorLabels: false,
        right: {
            title: {text: 'Title (right axis)'}
        }
    },

    
    dataAxis: {showMinorLabels: false},
    dataAxis: { left: { title: {text: 'Agua disponible para planta (%)'}, range: {min:0, max:180} },
                right: { title: {text: 'Precipitación (mm)'}, range: {min:0, max:20} } },
    legend: {left:{position:"bottom-left"}},
    start: fecha_p_inicio, //CAMBIAR A UNA VARIABLE DE SEMANA!!!
    end: fecha_p_final,
    clickToUse: true
    
};

function populateExternalLegend(legend_id) {
    var groupsData = groups.get();
    var legendDiv = document.getElementById(legend_id);
    legendDiv.innerHTML = "";

    // get for all groups:
    for (var i = 0; i < groupsData.length; i++) {
        // create divs
        var containerDiv = document.createElement("div");
        var iconDiv = document.createElement("div");
        var descriptionDiv = document.createElement("div");

        // give divs classes and Ids where necessary
        containerDiv.className = 'legend-element-container';
        containerDiv.id = groupsData[i].id + "_legendContainer"
        iconDiv.className = "icon-container";
        descriptionDiv.className = "description-container";

        // get the legend for this group.
        var legend = graph2d.getLegend(groupsData[i].id,30,30);

        // append class to icon. All styling classes from the vis.css have been copied over into the head here to be able to style the
        // icons with the same classes if they are using the default ones.
        legend.icon.setAttributeNS(null, "class", "legend-icon");

        // append the legend to the corresponding divs
        iconDiv.appendChild(legend.icon);
        descriptionDiv.innerHTML = legend.label;
        // determine the order for left and right orientation
        if (legend.orientation == 'left') {
        descriptionDiv.style.textAlign = "left";
        containerDiv.appendChild(iconDiv);
        containerDiv.appendChild(descriptionDiv);
        }
        else {
        descriptionDiv.style.textAlign = "right";
        containerDiv.appendChild(descriptionDiv);
        containerDiv.appendChild(iconDiv);
        }

        // append to the legend container div
        legendDiv.appendChild(containerDiv);

        // bind click event to this legend element.
        containerDiv.onclick = toggleGraph.bind(this,groupsData[i].id);
    }
    }

function toggleGraph(groupId) {
    // get the container that was clicked on.
    var container = document.getElementById(groupId + "_legendContainer")
    // if visible, hide
    if (graph2d.isGroupVisible(groupId) == true) {
        groups.update({id:groupId, visible:false});
        container.className = container.className + " hidden";
    }
    else { // if invisible, show
        groups.update({id:groupId, visible:true});
        container.className = container.className.replace("hidden","");
    }
}
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<script type="text/javascript" src="resources/js/bootstrap/js/transition.js"></script>
<script type="text/javascript" src="resources/js/bootstrap/js/collapse.js"></script>
<script type="text/javascript" src="resources/js/bootstrap/js/es.js"></script>
<script type="text/javascript" src="resources/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap/js/bootstrap-datetimepicker.js"></script>
<script src="resources/js/jquery.tablesorter.js"></script>
<link type="text/css"         href="resources/js/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet"  />
<link type="text/css"         href="resources/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<script src="resources/vis/docs/js/jquery.min.js"></script>
<link type="text/css" href="resources/vis/dist/vis.css" rel="stylesheet" /> 
<link rel="stylesheet" href="resources/cssmenu/styles.css">
<script src="resources/cssmenu/script.js"></script>
<script type="text/javascript" src="resources/vis/dist/vis.js"></script>
<script src="resources/js/jquery.js"></script>
<script type="text/javascript" src="resources/js/moment.js"></script>
<link type="text/css" href="resources/css/inforiego.css" rel="stylesheet"/>

<style>		
	.campoTabla{    
	    text-align: center;
	    max-width: 45px;
	}
	.campoComentario{    
	    text-align: left;
	    width: 90%;
	}
	.campoTablaPrecipitacion{    
	    text-align: center;
	    max-width: 85px;
	    background-color: inherit;
	}
	.bar-nav{
		text-align: left;
		padding:20px;
	}
	
	.btn_remove{
		margin-top:5px;
	}
	
	th.bigHeader{
		height: 1.8rem;
	}
		
	.loading{
		width:50%;
		height:28%;		
		background-color:rgba(255, 255, 255, 0.9);
		border-style: solid;
		border-radius:5px;
		border-width: 3px;
		border-color:black;
		display: none;
		padding: 10px;
		z-index:10;
		position: fixed;
		margin-left:25%;
		margin-top:7%;
		min-width: 230px;
		min-height: 180px;
	}
	.loading-img{
		width:50px;
		height:50px;
	}			
</style>

<title>Efi-Riego</title>

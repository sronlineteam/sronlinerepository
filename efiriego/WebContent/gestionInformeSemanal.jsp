<%@ include file="auxiliares/importsEfiriego.jsp" %>
<%@ include file="auxiliares/informes/getAttributes.jsp" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="auxiliares/informes/head.jsp" %>
	
<!-- 	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','http://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-61231638-1', 'auto');ga('send', 'pageview');</script> -->
</head>
<header>
<div id=foto>
	  <img href="SrvInicial" src="resources/img/logo.png">
	</div>
	<div id="tipo_informe">
	  <h2>Ficha de Riego n� <input id="campo_1" type="text" style="max-width: 45px;"></h2>
	  <h2 id="finca"></h2>
	  <p id="periodo"></p>
	  <p id="descripcion"></p>
	</div>
</div>
</header>

<body onload="actualizarTabla()" > 
	
	<%@ include file="auxiliares/informes/menu.jsp" %>
     
    <br/>
    <table onchange="actualizarTabla()" class="table1 tableInfo" >
        <tr>
            <th colspan="9" class="bigHeader">DATOS AGROCL�MATICOS</th>
        </tr>
        <tr>
            <th ></th>
            <td ><input id="fecha_1" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_2" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_3" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_4" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_5" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_6" type="text" readOnly class="campoTabla"></td>
            <td ><input id="fecha_7" type="text" readOnly class="campoTabla"></td>
            <td >Total</td>
        </tr>
        <form id="form_datos">
        <tr>
            <th >ET<sub>0</sub> (mm)</th>
            <td><input id="p_1" type="text" class="campoTabla"></td>
            <td><input id="p_2" type="text" class="campoTabla"></td>
            <td><input id="p_3" type="text" class="campoTabla"></td>
            <td><input id="p_4" type="text" class="campoTabla"></td>
            <td><input id="p_5" type="text" class="campoTabla"></td>
            <td><input id="p_6" type="text" class="campoTabla"></td>
            <td><input id="p_7" type="text" class="campoTabla"></td>
            <td><input id="total_1" type="text" readOnly class="campoTabla"></td>
        </tr>
        <tr>
            <th >K<sub>c</sub></th>
            <td><input id="kc_1" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_2" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_3" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_4" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_5" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_6" type="text" readOnly class="campoTabla"></td>
            <td><input id="kc_7" type="text" readOnly class="campoTabla"></td>
            <td><input id="total_kc" type="text" class="campoTabla"></td>
        </tr>
        <tr>
            <th>ET<sub>c</sub> (mm)</th>
            <td><input id="etc_1" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_2" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_3" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_4" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_5" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_6" type="text" readOnly class="campoTabla"></td>
            <td><input id="etc_7" type="text" readOnly class="campoTabla"></td>
            <td><input id="total_etc" type="text" readOnly class="campoTabla"></td>
        </tr>
        <tr>
            <th>Pe (mm)</th>
            <td><input id="p_8" type="text" class="campoTabla"></td>
            <td><input id="p_9" type="text" class="campoTabla"></td>
            <td><input id="p_10" type="text" class="campoTabla"></td>
            <td><input id="p_11" type="text" class="campoTabla"></td>
            <td><input id="p_12" type="text" class="campoTabla"></td>
            <td><input id="p_13" type="text" class="campoTabla"></td>
            <td><input id="p_14" type="text" class="campoTabla"></td>
            <td><input id="total_2" type="text" readOnly class="campoTabla"></td>
        </tr>
    </table>
    </form>
            
    <table onchange="actualizarTabla()" class="tableInfo" border="1">
        <tr>
            <th colspan="3" class="bigHeader">Recomendaciones Inforiego (ITACYL)</th>
        </tr>
        <tr>
            <th class="bigBox">Necesidades netas del cultivo ET<sub>0</sub> * Kc (mm � l/m2)</th>
            <th class="bigBox">Recomendaci�n de riego propuesta por Inforiego (ET<sub>0</sub>*K<sub>c</sub> - Pe) / Ef <br>(mm � l/m2)</th>
            <th class="bigBox">Coeficiente de Eficiencia</th>
            
        </tr>
        <tr></tr>
        <tr> 
            <td class="emptyBox"><input id="recomendacion_1" type="text" widht="100%" readOnly class="campoTabla"></td>
            <td class="emptyBox"> <input id="recomendacion_2" type="text" readOnly class="campoTabla"></td>
            <td class="emptyBox"> <input id="eficiencia" type="text" class="campoTabla"></td>
        </tr>
		
    </table>
    <form action="SrvGestionInforme" method="post" name="gestionInforme" id="frm" >
		<input type="hidden" id="idfinca" name="idfinca"></input>
		<input type="hidden" id="idtemporada" name="idtemporada"></input>
		<input type="hidden" id="idinforme" name="idinforme"></input>
		<input type="hidden" id="accion" name="accion"></input>
		<input type="hidden" id="datos" name="datos"></input>	
	</form>
	<button  class="btn_remove" onclick='actualizarInforme("6");'>Publicar</button>
	<button  class="btn_remove" onclick='actualizarInforme("7");'>Modificar datos</button>
	
            
    <br><p><b>ET<sub>0</sub>,</b> Evapotranspiraci�n de referencia. <b>K<sub>c</sub>,</b> coeficiente de cultivo para puerro en un cultivo FINAL.(110-140 d�as desde la siembra).</p>
    <p><b>ET<sub>c</sub>,</b> Evapotranspiraci�n del cultivo. <b>Pe, </b>precipitaci�n efectiva segun datos proporcionados por Inforiego (ITACYL). Estaci�n SG01 - Gomezserracin (Segovia).</p>

    <h2 id="grafico_1"> Gr�fica 1. HIST�RICO DE AGUA DISPONIBLE PARA LA PLANTA (ADP) (%)</h2>
    <div id="Legend" class="external-legend"></div>
    <div id="visualization"></div>
    <h2 id="grafico_2"> Gr�fica 2. AGUA DISPONIBLE PARA LA PLANTA (ADP) (%)</h2>
    <div id="Legend_week" class="external-legend"></div>
    <div id="visualization_week"></div>

    <%@ include file="auxiliares/informes/tablePrecipitaciones.jsp" %>
    
	<script type="text/javascript">
	$('#text-poup').text("Generando Informe pdf");
	$('#loading-popup-informe').css("display", "block");
	
	<%@ include file="auxiliares/informes/datosInforme.jsp" %>
		
	    var accion = <%=accion%>
		if (accion==="1"){
			var form = document.getElementById("form_datos");
			var elements = form.elements;
			for (var i = 0, len = elements.length; i < len; ++i) {
			    elements[i].readOnly = true;
			}
			$(".btn_remove").remove();
		}
		
		for (i = 0; i <= 7; i++) {
			var input = '#fecha_'+(i+1);		
			var input2 = '#prediccion_fecha_'+(i+1);
			var fecha_ini_moment = moment(fecha_p_inicio);
			
			var fecha_moment_add = fecha_ini_moment.add(i, 'days');
			
			$(input).val(moment(fecha_moment_add).format('DD/MM'));
			$(input2).val(moment(fecha_moment_add).format('DD/MM/YYYY'));
			
		}		
				
		var json = { "idinforme":idinforme,
				datos:datos_informe.datos_tabla.slice(1,datos_informe.datos_tabla.length-1).split(","),
				datos2:datos_informe.datos_campos.slice(1,datos_informe.datos_campos.length-1).split(",")
        };
		
		var datos = json.datos;
		var datos2 = json.datos2;
		for (i = 0; i < datos.length; i++) {
			valor = datos[i];
			valor = parseFloat(valor.slice(1,valor.length-1))
			if (i === 14){
				input = '#total_kc';
			}else if(i === 15){
				input = '#eficiencia';
			}else{
				input = '#p_'+(i+1);
			}	
			if (!valor){
				valor = ""
			}
			$(input).val(valor);
		}
		
		for (i = 0; i < datos2.length; i++) {
			valor = datos2[i];
			input = '#campo_'+(i+1);
				
			if (!valor){
				valor = ""
			}
			$(input).val(valor);
		}

		function callServlet() {
	    	
			var json = { "idinforme": idinforme,
						"idfinca": <%=idfinca%>,
						"idtemporada": <%=idtemporada%>,
						datos: [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "],
						datos2: [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "]
	            	};
			
			for (i = 0; i < json.datos.length-2; i++) {
				valor = datos[i];
				input = '#p_'+(i+1);
				json.datos[i] = $(input).val();
			}
			json.datos[14] = $('#total_kc').val();
			json.datos[15] = $('#eficiencia').val();
		    	          
	        $.ajax({
			    type: 'get', 
			    url: '/efiriego/SrvGestionParametrosInforme',
			    dataType: 'JSON',
			    data: { 			      
			      datos: JSON.stringify(json.datos),
			      idinforme: JSON.stringify(json.idinforme),
			      idfinca: JSON.stringify(json.idfinca),
			      idtemporada: JSON.stringify(json.idtemporada),
			    },
			    contentType: 'application/json',
		        mimeType: 'application/json',
			    success: function(data) {
					console.log("Entroo")
			    },
			    error: function(data) {
			    	console.log("Entroo error");
			    }
			});
	    }
		
	
		function actualizarInforme(accion){
			
			var json = {
						datos: [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "]
            	};
			var isPosPublicar = true;
			for (i = 0; i < json.datos.length-2; i++) {
				valor = datos[i];
				input = '#p_'+(i+1);
				json.datos[i] = $(input).val();
				if ($(input).val()=== ""){
					isPosPublicar = false;
				}
			}
			json.datos[14] = $('#total_kc').val();
			json.datos[15] = $('#eficiencia').val();
			if ($('#total_kc').val()=== "" || $('#eficiencia').val() === ""){
				isPosPublicar = false;
			}
			
			var idinforme = datos_informe.idinforme;
			console.log(accion)
			if (accion === "6" && !isPosPublicar){
				alert("No es posible publicar, rellene todo el informe");
				return;
			}
			document.getElementById("accion").value = accion;
			document.getElementById("idfinca").value = <%=idfinca%>;
			document.getElementById("idinforme").value = idinforme;
			document.getElementById("idtemporada").value = <%=idtemporada%>;
			document.getElementById("datos").value = JSON.stringify(json.datos);		
			document.forms.gestionInforme.submit();			
		}
		
		function actualizarTabla(){
			
			total_kc = parseFloat($('#total_kc').val());
			
			if (total_kc || total_kc===0){
				for (i = 1; i <= 8; i++) {
					input = '#kc_'+i;
					$(input).val(total_kc);	
				}					
			}else{	
				for (i = 1; i <= 8; i++) {
					input = '#kc_'+i;
					$(input).val("");	
				}
			}
			
			for (i = 1; i <= 8; i++) {		
				if (i===8){
					et0 = parseFloat($('#total_1').val());		
					input = '#total_etc';
				}else{
					et0 = parseFloat($('#p_'+(i)).val());
					input = '#etc_'+(i);
				}			
				kc = parseFloat($('#total_kc').val());
				if ((et0 || et0===0) && (kc || kc===0)){
					valor = et0 * kc;
					$(input).val(valor);
				}else{
					$(input).val("");
				}
			}
			
			for (i = 0; i <= 2; i++) {				
					valor1 = parseFloat($('#p_'+(1+i*7)).val());
					valor2 = parseFloat($('#p_'+(2+i*7)).val());
					valor3 = parseFloat($('#p_'+(3+i*7)).val());
					valor4 = parseFloat($('#p_'+(4+i*7)).val());
					valor5 = parseFloat($('#p_'+(5+i*7)).val());
					valor6 = parseFloat($('#p_'+(6+i*7)).val());
					valor7 = parseFloat($('#p_'+(7+i*7)).val());
					var input = '#total_'+(i+1);
										
					if ((valor1 || valor1===0) && (valor2 || valor2===0) && (valor3 || valor3===0) 
						&& (valor4 || valor4===0) && (valor5 || valor5===0) && (valor6 || valor6===0) && (valor7 || valor7===0) ){
						valor = valor1 + valor2 + valor3 + valor4 + valor5 + valor6 + valor7;
						$(input).val(valor);
					}else{
						$(input).val("");
					}
			}
						
			var eto = parseFloat($('#total_1').val());
			var kc = parseFloat($('#total_kc').val());
			var pe = parseFloat($('#total_2').val());
			var ef = parseFloat($('#eficiencia').val());
			
			if ((eto || eto===0) && (kc || kc===0)){
				valor = eto * kc;			
				$("#recomendacion_1").val(valor);
				$("#total_etc").val(valor);
			}else{		
				$("#recomendacion_1").val("");
				$("#total_etc").val("");
			}
			
			if ((eto || eto===0) && (kc || kc===0) && (pe || pe===0) && (ef || ef===0)){
				valor = (((eto * kc)-pe)/ef);
				$("#recomendacion_2").val(valor);
			}else{
				$("#recomendacion_2").val("");
			}			
		}

	</script>

	<script type="text/javascript">
	
		<%@ include file="auxiliares/informes/graficos.jsp" %>
		
		var container = document.getElementById('visualization');
		var container_week = document.getElementById('visualization_week');
	
		var graph2d = new vis.Graph2d(container, dataset, groups, options);
		var graph2d_week = new vis.Graph2d(container_week, dataset, groups, options_week);
	
	    populateExternalLegend("Legend")
	    populateExternalLegend("Legend_week")
	    $('#loading-popup-informe').css("display", "none");
	</script>

</body>
</html>
<%@ include file="auxiliares/importsEfiriego.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<META HTTP-EQUIV="REFRESH" CONTENT="1200;URL=SrvInicial">
	<link type="text/css" href="resources/css/inforiego.css" rel="stylesheet"/>
	<script src="resources/js/jquery-latest.js" type="text/javascript"></script>
   	<script src="resources/cssmenu/script.js"></script>
	<link rel="stylesheet" href="resources/cssmenu/styles.css">
	<title>Efi-Riego</title>
</head>
<body onload="cargarFincas()" onload="cargarTemporadas()" >
	<header>
		<div id=foto>
		  <img src="resources/img/logo.png">
		</div>
	</header>
	</br>
	<div id='cssmenu'>
		<ul>
          <li class='active'><a href="j_spring_security_logout" >Desconexion </a></li>        
		</ul>
	</div>
			<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>	
		<sec:authorize var="loggedIn" access="isAuthenticated()" />
		<c:choose>
		    <c:when test="${loggedIn}">
		          <p><%=request.getUserPrincipal().getName()%></p>        
		    </c:when>
		    <c:otherwise>
		    </c:otherwise>
		</c:choose>
	
	<hr>
	
	<form action="SrvListadoInforFincaTemp" name="frm">
		<select id="list_fincas" name="idfinca" onchange="changeTemporadas()">  
		</select>
		<select id="list_temporadas" name="idtemporada">
		</select>
		<button> Obtener Informes </button>
	</form>
	
	
	<script type="text/javascript">
	
		
		<%
			Collection<JSONObject> fincas = null;
			Object obj_0 = request.getAttribute("fincas_temporadas");
			if (obj_0 != null)
				fincas = (Collection<JSONObject>) obj_0;
		
		%>
		
		
		var fincas=[{
			id: 1,
			nombre: "che�o",
			temporadas: [{id:"1", nombre:"puerros"},{ id:"2", nombre:"tomates"}],
		},
		{
			id: 2,
			nombre: "che�o 2",
			temporadas: [{id:"1", nombre:"lechuga"},{ id:"2", nombre:"acelgas"}],
	  	},
	 
		{
	  		id: 3,
			nombre: "che�o 3",
			temporadas: [{id:"1", nombre:"apio"},{ id:"2", nombre:"zanahorias"}],
		}];
		
		var fincas=<%=fincas%>
	
		console.log(fincas)
	
		function cargarFincas(){
			
			var finca = document.getElementById("list_fincas");
			for (i = 0; i < fincas.length; i++) {
				var option = document.createElement("option");
				option.value = fincas[i].idFinca;
				option.text = fincas[i].nombreFinca;
				finca.add(option);
			}
			changeTemporadas();
		}
		
		function changeTemporadas(){
			document.getElementById("list_temporadas").options.length = 0;
			
			var temporada = document.getElementById("list_temporadas");
			var value = document.frm.list_fincas.selectedIndex;			
			var temporadas = fincas[value].temporadas;
			
			
			for (i = 0; i < temporadas.length; i++) {
				var option = document.createElement("option");
				option.value = temporadas[i].idTemporada;
				option.text = temporadas[i].nombreTemporada;
				temporada.add(option);
			} 
			
		}

	</script>
</body>
</html>
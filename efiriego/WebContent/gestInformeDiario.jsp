<%@ include file="auxiliares/importsEfiriego.jsp" %>
<%@ include file="auxiliares/informes/getAttributes.jsp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="auxiliares/informes/head.jsp" %>
	
<!-- 	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','http://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-61231638-1', 'auto');ga('send', 'pageview');</script> -->
</head>

<header>
	<%@ include file="auxiliares/informes/header.jsp" %>
</header>

<body> 
	<%@ include file="auxiliares/informes/menu.jsp" %>
	
    <br/>
    <form action="SrvGestionInforme" method="post" name="gestionInforme" id="frm" >
		<input type="hidden" id="idfinca" name="idfinca"></input>
		<input type="hidden" id="idtemporada" name="idtemporada"></input>
		<input type="hidden" id="idinforme" name="idinforme"></input>
		<input type="hidden" id="accion" name="accion"></input>
	    <button  class="btn_remove" onclick='actualizarInforme("6");' "style:margin=5px;">Publicar</button>	
		<button  class="btn_remove" onclick='callServlet();'>Guardar como borrador</button>
	</form>
            
    <h2 id="grafico_1"> Gr�fica 1. AGUA DISPONIBLE PARA LA PLANTA (ADP) (%)</h2>
    <div id="Legend" class="external-legend"></div>
    <div id="visualization"></div>
    
    <%@ include file="auxiliares/informes/tablePrecipitaciones.jsp" %>
    
	<script type="text/javascript">
		$('#text-poup').text("Generando Informe pdf");
		$('#loading-popup-informe').css("display", "block");
		
		<%@ include file="auxiliares/informes/datosInforme.jsp" %>
		
		if (accion==="1"){
			$(".btn_remove").remove();
		}
		
		var fecha_moment = moment(fecha_p_inicio);
		var dia = fecha_moment.format('dddd');
		
		while (dia!="Monday") {
			fecha_moment = fecha_moment.subtract(1, 'days');
			dia = fecha_moment.format('dddd');	
		}
		
		fecha_p_inicio = fecha_moment.format('YYYY-MM-DD');	
		fecha_p_final = fecha_moment.add(7, 'days').format('YYYY-MM-DD');
		
		for (i = 0; i <= 7; i++) {
			var input = '#fecha_'+(i+1);		
			var fecha_ini_moment = moment(fecha_p_inicio);
			
			var fecha_moment_add = fecha_ini_moment.add(i, 'days');
			
			$(input).val(moment(fecha_moment_add).format('DD/MM'));
			
		}		
				
		var json = { "idinforme":idinforme,
				datos:datos_informe.datos_tabla.slice(1,datos_informe.datos_tabla.length-1).split(","),
            	};
		
		var datos = json.datos
		for (i = 0; i < datos.length; i++) {
			valor = datos[i];
			input = '#p_'+(i+1);
			valor = parseFloat(valor.slice(1,valor.length-1))
			if (!valor){
				valor = ""
			}
			$(input).val(valor);
		}
		
		function callServlet() {
	    	
			var json = { "idinforme": idinforme,
					"idfinca": <%=idfinca%>,
					"idtemporada": <%=idtemporada%>,
					datos: [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "]
            	};
			
			for (i = 0; i < json.datos.length; i++) {
				valor = datos[i];
				input = '#p_'+(i+1);
				json.datos[i] = $(input).val();
			}
		    	          
	        $.ajax({
			    type: 'get', 
			    url: '/efiriego/SrvGestionParametrosInforme',
			    dataType: 'JSON',
			    data: { 			      
				      datos: JSON.stringify(json.datos),
				      idinforme: JSON.stringify(json.idinforme),
				      idfinca: JSON.stringify(json.idfinca),
				      idtemporada: JSON.stringify(json.idtemporada),
					},
			    contentType: 'application/json',
		        mimeType: 'application/json',
			    success: function(data) {
					console.log("Entroo")
			    },
			    error: function(data) {
			    	console.log("Entroo error");
			    }
			});
	    }
				
		function actualizarInforme(accion){	
			var idinforme = datos_informe.idinforme;
			document.getElementById("accion").value = accion;
			document.getElementById("idfinca").value = <%=idfinca%>;
			document.getElementById("idinforme").value = idinforme;
			document.getElementById("idtemporada").value = <%=idtemporada%>;
			document.forms.gestionInforme.submit();			
		}

	</script>
	
	<script type="text/javascript">	
		<%@ include file="auxiliares/informes/graficos.jsp" %>
		var container = document.getElementById('visualization');
		var graph2d = new vis.Graph2d(container, dataset, groups, options_week);
	    populateExternalLegend("Legend")
	    $('#loading-popup-informe').css("display", "none");
	</script>
</body>
</html>
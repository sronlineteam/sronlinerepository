<%@ include file="auxiliares/importsEfiriego.jsp" %>

<%
	Collection<JSONObject> informes = null;
	Object obj_0 = request.getAttribute("informes");
	if (obj_0 != null)
		informes = (Collection<JSONObject>) obj_0;
	
	String idfinca = null;
	Object obj_1 = request.getAttribute("idfinca");
	if (obj_1 != null)
		idfinca = (String) obj_1;
	
	String idtemporada = null;
	Object obj_2 = request.getAttribute("idtemporada");
	if (obj_2 != null)
		idtemporada = (String) obj_2;
		
%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<META HTTP-EQUIV="REFRESH" CONTENT="1200;URL=SrvListadoInforFincaTemp">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="resources/js/jquery.js"></script>
	<script type="text/javascript" src="resources/js/moment.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap/js/transition.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap/js/collapse.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap/js/es.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap/js/bootstrap-datetimepicker.js"></script>
	<script src="resources/js/jquery.tablesorter.js"></script>
	<script src="resources/js/jquery.dataTables.min.js"></script>
	<link type="text/css"         href="resources/js/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet"  />
	<link type="text/css"         href="resources/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="resources/cssmenu/styles.css">
	<script src="resources/cssmenu/script.js"></script>
	<link rel="stylesheet" type="text/css" href="resources/js/themes/blue/style.css"/>
	<link type="text/css" href="resources/css/inforiego.css" rel="stylesheet"/>
	<link type="text/css" href="resources/css/jquery.dataTables.min.css" rel="stylesheet"/>
	
	<title>Efi-Riego</title>
	
</head>
<body>
	<header>
		<div id=foto>
		  <img src="resources/img/logo.png">
		</div>
	</header>
	</br>
	<div class="loading" id="loading-popup-informe">
		<div>
			<h1 id="text-poup">f</h1>
			<img class="loading-img" src="resources/img/loading.gif">
		</div>
	</div>
	<div id='cssmenu'>
		<ul>
			<li><a href="SrvInicial">Inicio</a></li>
	        <li><a href="j_spring_security_logout" >Desconexion</a></li>        
		</ul>
	</div>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>	
	<sec:authorize var="loggedIn" access="isAuthenticated()" />
	<c:choose>
	    <c:when test="${loggedIn}">
	          <p><%=request.getUserPrincipal().getName()%></p>        
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<hr>
	<table id="table" class="tablesorter" >
		<thead> 
	        <tr>
	            <th>Nombre</th>
	            <th>Tipo</th>
	            <th>Fecha Inicio</th>
	            <th>Fecha Fin</th>
	            <th>Estado</th>
	            <th>Opciones</th>
	        </tr>
	    </thead>
	    <tbody> 
	    <% 
	    	Iterator itr = informes.iterator();
	   		while(itr.hasNext()) {   			
	   			JSONObject informe = null;
	   			Object Obj_informe = itr.next();
	   			if (Obj_informe != null)
	   				informe = (JSONObject) Obj_informe;
	   			String idInforme = (String) informe.get("idInforme");
	   			
	   			String fecha_inicio = (String) informe.get("fecha_inicio");
	   			String fecha_fin = (String) informe.get("fecha_fin");
	   			String dia_inicio = fecha_inicio.split(" ")[0];
	   			String dia_fin = fecha_fin.split(" ")[0];
	   			
	    %>
		    <tr>
		    	<td><%=informe.get("nombre_informe")%></td>
				<td><%=informe.get("tipo_de_informe")%></td>
		    	<td><%=dia_inicio%></td>
		    	<td><%=dia_fin%></td>
		    	<td><%=informe.get("estado")%></td>
				<td class="btnBox">
					<button type="button" class='btnVisualizar btnAccion' title='visualizar informe' onclick='gestionInf(<%=idfinca%>,<%=idtemporada%>,<%=idInforme%>,1);'></button>
					<button type="button" class='btnEditar btnAccion' title='editar informe' onclick='gestionInf(<%=idfinca%>,<%=idtemporada%>,<%=idInforme%>,2);'></button>
					<button type="button" class='btnBorrar btnAccion' title='borrar informe' onclick='gestionInf(<%=idfinca%>,<%=idtemporada%>,<%=idInforme%>,3);'></button>
				<%	
				     String tipo_informe = (String) informe.get("estado");
				     if(tipo_informe.equals("Publicado")){
				%>	
					<button class='btnEnviar btnAccion' title='descargar informe' onclick='gestionInf(<%=idfinca%>,<%=idtemporada%>,<%=idInforme%>,4);'></button>
				<%}%>
				</td>	
			</tr>	
	    <%}%>
	    </tbody> 
    </table>
    
    <div class="form-alta">
	    <div class="form-group">
	    <form action="SrvGestionInforme" method="post" name="gestionInforme" id="frm" >
			<input type="hidden" id="idfinca" name="idfinca">
			<input type="hidden" id="idtemporada" name="idtemporada">
			<input type="hidden" id="idinforme" name="idinforme">
			<input type="hidden" id="accion" name="accion">
			<input type="text" class="textoplano" value="Nombre Informe:" readOnly/>
			<input type="text" name="nombre-informe" class="inputNombre">
			Tipo: 
			<select id="tipo" name="tipo" class="inputTipoInfome">
			  <option value="Semanal">Semanal</option>
			  <option value="Diario">Diario</option>
			</select>
	        	<input type="text" class="textoplano center" value="Fecha inicio" readOnly/>
	            <div class='input-group date' id='datetimepicker1' >    	
	                <input type='text' class="form-control" name="fecha-inicio" id="fecha-inicio"/>
	                <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                </span>
	            </div>
	        
			<input type="hidden" type='text' id="fecha-fin" name="fecha-fin"/>
			<button class="btn" onclick='nuevoInforme(<%=idfinca%>,<%=idtemporada%>);'>Nuevo Informe</button>
		</form>
	    </div>
	</div>
    <script>
	    $('#datetimepicker1').datetimepicker({
	    	format: 'YYYY-MM-DD'
	    });
	    $(document).ready(function(){
	
		    $("#table").tablesorter({sortList:[[2,1]], widgets: ['zebra']});
	    	$("#table").DataTable({
	            "language": {
			    	"aria": {
		    	        "sortAscending":  ": activar para ordenar la columna ascendentemente",
		    	        "sortDescending": ": activar para ordenar la columna descendentemente"
		    	    },
	    			"loadingRecords": "Cargando...",
	                "lengthMenu": "Registros por pagina _MENU_",
	                "zeroRecords": "Sin registros",
	                "info": "Mostrando pagina _PAGE_ de _PAGES_",
	                "infoEmpty": "Sin registros disponibles",
	                "infoFiltered": "(  numero total de registros _MAX_)",
	                "search":         "Buscar:",
	                "zeroRecords":    "No se encuentran registros",
	                "paginate": {
	                    "first":      "Primero",
	                    "last":       "Ultimo",
	                    "next":       "Siguiente",
	                    "previous":   "Anterior"
	                }
	            }
	        } ); 
	    });
    	
		function CreateTable(){
			var table = document.getElementById("table");
			var informes = <%=informes%>
			
			for (i = 1; i < informes.length+1; i++) {
				var row = table.insertRow(i);
				
				row.insertCell(0).innerHTML = informes[i-1].nombre_informe;
				row.insertCell(1).innerHTML = informes[i-1].tipo_de_informe;
				row.insertCell(2).innerHTML = informes[i-1].fecha_inicio.split(" ")[0];
				row.insertCell(3).innerHTML = informes[i-1].fecha_fin.split(" ")[0];
				row.insertCell(4).innerHTML = informes[i-1].estado;
				var btns = "<button class='btnVisualizar btnAccion' title='visualizar informe' onclick='gestionInf(" + <%=idfinca%> + "," + <%=idtemporada%> + "," + informes[i-1].idInforme +",1);'></button>";
				btns+= "<button class='btnEditar btnAccion' title='editar informe' onclick='gestionInf("+<%=idfinca%>+","+<%=idtemporada%>+","+ informes[i-1].idInforme +",2);'></button>";
				btns+= "<button class='btnBorrar btnAccion' title='borrar informe' onclick='gestionInf("+<%=idfinca%>+","+<%=idtemporada%>+","+ informes[i-1].idInforme +",3);'> </button>";
				if (informes[i-1].estado=="Publicado" || informes[i-1].estado=="publicado" ){
					btns+= "<button class='btnEnviar btnAccion' title='publicar informe' onclick='gestionInf("+<%=idfinca%>+","+<%=idtemporada%>+","+ informes[i-1].idInforme +",4);'></button>";
				}
				row.insertCell(5).innerHTML = btns;	
			}
			$("#table").tablesorter();
		}
		
		function nuevoInforme(idfinca,idtemporada){
					
			var fechaInicial = $("#fecha-inicio").val();
			console.log(fechaInicial);
			var tipo = $("#tipo").val();
			var fechaFin;
			
			if (tipo=="Diario"){
				fechaFin = fechaInicial;
			} else {
				fechaFin = moment(fechaInicial).add(6,'days').format('YYYY-MM-DD');
			}
			
			document.getElementById("fecha-fin").value = fechaFin;
			document.getElementById("idtemporada").value = idtemporada;
			document.getElementById("accion").value = <%=Constantes.getNuevo_Informe()%>;
			document.getElementById("idfinca").value = idfinca;
			document.getElementById("idtemporada").value = idtemporada;
			document.forms.gestionInforme.submit();		
		}
		
		function gestionInf(idfinca,idtemporada,idinforme,accion){
			if(accion===4){
				$('#text-poup').text("Generando Informe pdf");
				$('#loading-popup-informe').css("display", "block");
			}else if(accion===1 || accion===2){
				console.log("entro")
				$('#text-poup').text("Cargando Informe");
				$('#loading-popup-informe').css("display", "block");
			}else if(accion===3){
				$('#text-poup').text("Eliminando Informe");
				$('#loading-popup-informe').css("display", "block");		
			}
			document.getElementById("accion").value = accion;
			document.getElementById("idfinca").value = idfinca;
			document.getElementById("idtemporada").value = idtemporada;
			document.getElementById("idinforme").value = idinforme;
			document.forms.gestionInforme.submit();		
		}
		
	</script>
	<style>
		.dataTables_wrapper {
		    width: 94%;
		    margin-left: 2%;
		}
		
		table.tablesorter {   
		    background-color: white;
		    border:none
		}
		td {
    		border: none;
    	}
    	table.tablesorter tbody tr.odd td {
		    background-color: #edf5f5;
		}
		table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    		background-color: #e6EEEE;
    	}
	</style>
</body>
</html>
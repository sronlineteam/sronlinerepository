package es.sronline.tareasQuartz;


import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.CronTrigger;
import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.sronline.informes.ConexionBBDD;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;


/**
 * This Example will demonstrate how to start and shutdown the Quartz scheduler and how to schedule a job to run in
 * Quartz.
 * 
 * @author Sergios
 */
public class QuartzTareas {
	
	static Scheduler sched;
	static SchedulerFactory sf = new StdSchedulerFactory();
	static ConexionBBDD conexion = new ConexionBBDD();
	
	public static Scheduler getSched(){
		return sched;
	}

	public void descargasPeriodicas() {
		Logger log = LoggerFactory.getLogger(QuartzTareas.class);
	
	    log.info("------- Initializing Job");
	    
	    try {
			sched = sf.getScheduler();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    JobDetail job = newJob(DescargaDatosPeriodicos.class)
	    	    .withIdentity("job", "group")
	    	    .build();

	    Trigger trigger = newTrigger()
	    	    .withIdentity("trigger", "group")
	    	    .withSchedule(simpleSchedule().withIntervalInMinutes(60*24).repeatForever())
	    	    .build();
	    try {
			sched.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    try {
			sched.start();	
		    log.info(job.getKey() + " Se ha iniciado la descarga periodica de datos ");
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	}

}

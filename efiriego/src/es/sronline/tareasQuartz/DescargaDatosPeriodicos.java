package es.sronline.tareasQuartz;


import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/* 
 * All content copyright Terracotta, Inc., unless otherwise indicated. All rights reserved. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
 
import java.util.Date;

import javax.script.ScriptException;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import es.sronline.informes.ConexionBBDD;
import es.sronline.servicio.ServicioDescargasDecagon;
import es.sronline.servicio.ServicioParserDecagonXml;

import org.json.simple.JSONObject;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

/**
 * 
 * @author Sergios
 */
public class DescargaDatosPeriodicos implements Job {

    private static Logger _log = LoggerFactory.getLogger(DescargaDatosPeriodicos.class);

    
    public DescargaDatosPeriodicos() {
    }

    public void execute(JobExecutionContext context)
        throws JobExecutionException {
    	
    	_log.info("Inicio Descargas de datos Decagon");
    	Date horaInicio = new Date();
  
		ConexionBBDD conexion = new ConexionBBDD();
    	ConexionBBDD.conectar();
    	
    	String respose;
    	
    	ArrayList<JSONObject> dispositivos = ConexionBBDD.getArrayDispositivosDecagon();
    	
    	for (int i = 0; i< dispositivos.size(); i++){
    		JSONObject dispositivo = dispositivos.get(i);
    		_log.info("Actualizando registros para dispositivo " + dispositivo.get("id_decagon") );
    		   		  
	        String horaUltimaPeticion = (String)dispositivo.get("hora_ultima_peticion");
	        String horaPeticion =  new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).format(new Date());			
	        
    		Boolean realizarPeticion = null;		
 			if (dispositivo.get("rid").equals("0")){
 				realizarPeticion = true;
 			} else {
 				try {
					realizarPeticion = realizarPeticion(horaPeticion, horaUltimaPeticion);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 
 			if (realizarPeticion){
	 			respose = ServicioDescargasDecagon.datosDecagonGET((String) dispositivo.get("id_decagon"), (String)dispositivo.get("password"), (String)dispositivo.get("rid"));
				ServicioParserDecagonXml.xmlString = respose;			
	        	ServicioParserDecagonXml.main(null);
 	  
		        if (ServicioParserDecagonXml.rid != null && Integer.parseInt(ServicioParserDecagonXml.rid)>Integer.parseInt((String) dispositivo.get("rid"))){	                	
	        		try {
						ConexionBBDD.insertDatosDecagon((String) dispositivo.get("iddispositivo_campo"), ServicioParserDecagonXml.dataString, String.valueOf(Integer.parseInt(ServicioParserDecagonXml.rid) + 1), (String)dispositivo.get("rid"), horaPeticion);
						_log.info("Actualizado dispositivo " + dispositivo.get("id_decagon") );
	        		} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ScriptException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}     					
		        } else {        	
					try {
						ConexionBBDD.updateHoraPeticionDecagon((String) dispositivo.get("iddispositivo_campo"),horaPeticion);
						_log.info("No existen nuevos registros para el dispositivo " + dispositivo.get("id_decagon"));				   
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}			        	
		        }
 			} else {
 				_log.info("No se puede realizar la peticion para el dispositivo" + dispositivo.get("id_decagon"));				   
				
 			}
    	}
    	
    	Date horaFin = new Date();
    	
    	int diferenciaHoras = (int) (horaFin.getTime() - horaInicio.getTime())/1000;
    	String horas = String.valueOf(diferenciaHoras/(60*60));
      	
    	int diferenciaMinutos = (diferenciaHoras%(60*60));
    	String minutos = String.valueOf(diferenciaMinutos/(60));
    	
    	int diferenciasegundos = (diferenciaMinutos%(60));
    	String segundos = String.valueOf(diferenciasegundos);
    	
    	_log.info("Finalizada descargas de datos con una duracion de " + horas + ":" + minutos +":"+ segundos);
		
    }
    
    public Boolean realizarPeticion(String horaPeticion, String horaUltimaPeticion) throws ParseException{
    	
    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date horaPeticionDate = formatter.parse(horaPeticion);
    	Date horaUltimaPeticionDate = formatter.parse(horaUltimaPeticion);
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(horaUltimaPeticionDate); // Configuramos la fecha que se recibe
    	calendar.add(Calendar.HOUR, 1);
    	Date minHora = calendar.getTime();
    	
    	if (minHora.compareTo(horaPeticionDate)>0){
    		return false;
    	} else {
    		return true;
    	} 
    }
    
}
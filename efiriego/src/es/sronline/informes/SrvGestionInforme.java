package es.sronline.informes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JFileChooser;

import java.lang.Integer;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.simple.JSONObject;

import com.mysql.jdbc.Connection;

import org.apache.log4j.Logger;
import es.sronline.informes.Constantes;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * Servlet implementation class SrvGestionInforme
 */
@WebServlet("/SrvGestionInforme")
public class SrvGestionInforme extends HttpServlet {
	static Logger log;   

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestionInforme() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idfinca = request.getParameter("idfinca");
		String idtemporada = request.getParameter("idtemporada");
		String accion = request.getParameter("accion");
		String paginaDestino = null;
		Boolean realizarResponse = true;
		
		if (Integer.parseInt(accion)==Constantes.VISUALIZAR_INFORME || Integer.parseInt(accion)==Constantes.EDITAR_INFORME || Integer.parseInt(accion)==Constantes.PUBLICAR_INFORME){
			String idinforme = request.getParameter("idinforme");
			JSONObject datos_informe = ConexionBBDD.datosInforme(idfinca,idtemporada,idinforme);			
			String idtipo_informe = (String) datos_informe.get("idtipo_informe");
			ArrayList<JSONObject> datos_grafica_port = ConexionBBDD.getPortsGrafica(idfinca,idtemporada,idtipo_informe);	
				
			request.setAttribute("datos_informe", datos_informe);
			request.setAttribute("accion", accion);
			request.setAttribute("idfinca", idfinca);
			request.setAttribute("idtemporada", idtemporada);
			request.setAttribute("datos_grafica_port", datos_grafica_port);
			
			if (Integer.parseInt(accion)==Constantes.PUBLICAR_INFORME){
				   			   
				   JSONObject infoPortsGrafica = ConexionBBDD.getInfoPortsGrafica(idtemporada ,idfinca ,idtipo_informe);
				   				   
				   String jrxmlFileName = Constantes.getRootJasper() + infoPortsGrafica.get("plantilla_jrxml");
				   String jasperFileName = Constantes.getRootJasper() + infoPortsGrafica.get("plantilla_jasper");
				   
				   Map<String, Object> hm = new HashMap<String,Object>();
				   				   
				   hm.put("id_grafico", infoPortsGrafica.get("idgrafico"));
			   
				   ArrayList<JSONObject> arrayPortsDis = (ArrayList<JSONObject>) infoPortsGrafica.get("arrayPortsDis");				   
				   for (int i = 0; i<arrayPortsDis.size();i++){
					   JSONObject port = arrayPortsDis.get(i);
					   String id = (String) port.get("idport");
					   String tipo = (String) port.get("tipo");
					   String etiqueta = (String) port.get("etiqueta");
					   String color = (String) port.get("color");
					   hm.put("id_dis_"+i, id);
					   hm.put("label_"+i, etiqueta);
					   
				   }
				   
				   hm.put("fecha_inicio", datos_informe.get("fecha_inicio"));
				   hm.put("fecha_fin", datos_informe.get("fecha_fin"));
				   hm.put("imagen", Constantes.getRootJasper() + "/logo.png");
				   hm.put("id_informe", idinforme);
				   hm.put("id_finca", idfinca);
				   hm.put("id_temporada", idtemporada);
				   
				   hm.put("value_shadow_up", "50");
				   hm.put("value_shadow_down", "25");
					   
				   String fecha = (String) datos_informe.get("fecha_inicio");
			       SimpleDateFormat dt = new SimpleDateFormat("yyyyy-mm-dd HH:MM:SS"); 
			       Date date = null;			  
				   try {
						date = (Date) dt.parse(fecha);
				   } catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				   }
				   
				   SimpleDateFormat dt1 = new SimpleDateFormat("dd-mm-yyyy");
			       String fecha_parse = dt1.format(date);
				   
				   Calendar cal = Calendar.getInstance();
				   SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd");
				   try {
						cal.setTime(dt2.parse(fecha));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			   
				   for (int i = 0; i<7;i++){
					   if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
						   String fecha_lunes = (cal.get(Calendar.YEAR)+"-"+((cal.get(Calendar.MONTH))+1)+"-"+cal.get(Calendar.DAY_OF_MONTH)+ " 0:00:00");
						   hm.put("fecha_lunes", fecha_lunes);
						   break;
					   }else{
						   cal.add(Calendar.DAY_OF_YEAR, -1);
					   }
				   }
				   
				   for (int j = 0; j<7;j++){
					   if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
						   String fecha_domingo = (cal.get(Calendar.YEAR)+"-"+((cal.get(Calendar.MONTH))+1)+"-"+cal.get(Calendar.DAY_OF_MONTH)+ " 0:00:00");
						   hm.put("fecha_domingo", fecha_domingo);
						   
						   break;
					   }else{
						   cal.add(Calendar.DAY_OF_YEAR, 1);
					   }
				   }	   
				   
				   try { 
					   JasperCompileManager.compileReportToFile(jrxmlFileName);
					   Connection conn = (Connection) DriverManager.getConnection(Constantes.getConexionBBDD(), Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
						   
					   JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperFileName, hm, conn);
					   
					   String pdfFileName = "Informe" + datos_informe.get("idinforme") + "_" + datos_informe.get("nombre_finca") +
							   "_" + datos_informe.get("nombre_temporada") +  "_" + fecha_parse + ".pdf";
					   
					   ByteArrayOutputStream baos = new ByteArrayOutputStream();
					   response.setHeader("Content-Disposition","inline; filename=\"" + pdfFileName + "\"");
					   response.setContentType("application/pdf; name=\"" + pdfFileName + "\"");
					   net.sf.jasperreports.engine.JasperExportManager.exportReportToPdfStream(jprint,baos);
					   response.setContentLength(baos.size());
					   ServletOutputStream out1 = response.getOutputStream();
					   baos.writeTo(out1);
					   
					   realizarResponse = false;
					   

				   } catch (Exception e) {
					   System.out.print("Exceptiion" + e);
				   }
				   
				    Collection<JSONObject> informes = null;
					try {
						informes = ConexionBBDD.tablaInformes(idfinca, idtemporada);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					request.setAttribute("informes", informes);
					request.setAttribute("idfinca", idfinca);
					request.setAttribute("idtemporada", idtemporada);
					paginaDestino = "/listaInformes.jsp";
				   
			}else{
				paginaDestino = (String) datos_informe.get("plantilla");		
			}
			
		}else if(Integer.parseInt(accion)==Constantes.BORRAR_INFORME){
			String idinforme = request.getParameter("idinforme");
			ConexionBBDD.borrarInforme(idfinca,idtemporada,idinforme);
			
			Collection<JSONObject> informes = null;
			try {
				informes = ConexionBBDD.tablaInformes(idfinca, idtemporada);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("informes", informes);
			request.setAttribute("idfinca", idfinca);
			request.setAttribute("idtemporada", idtemporada);
			paginaDestino = "/listaInformes.jsp";
		}else if(Integer.parseInt(accion)==Constantes.NUEVO_INFORME){
			String nom_informe = request.getParameter("nombre-informe");
			String tipo_informe = request.getParameter("tipo");
			String fecha_inicio = request.getParameter("fecha-inicio");
			String fecha_fin = request.getParameter("fecha-fin");
			
			ConexionBBDD.altaInforme(idfinca,idtemporada,tipo_informe,nom_informe,fecha_inicio,fecha_fin);
			
			Collection<JSONObject> informes = null;
			try {
				informes = ConexionBBDD.tablaInformes(idfinca, idtemporada);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			request.setAttribute("informes", informes);
			request.setAttribute("idfinca", idfinca);
			request.setAttribute("idtemporada", idtemporada);
			paginaDestino = "/listaInformes.jsp";
		}else if(Integer.parseInt(accion)==Constantes.ACTUALIZAR_ESTADO || Integer.parseInt(accion)==Constantes.ACTUALIZAR_DATOS){
			String idinforme = request.getParameter("idinforme");
			String datos = request.getParameter("datos");
			
			ConexionBBDD.modifTablaInforme(datos,idinforme);
			
			if (Integer.parseInt(accion)==Constantes.ACTUALIZAR_ESTADO){
				ConexionBBDD.actualizarEstado(idinforme);
			}
						
			request.setAttribute("idfinca", idfinca);
			request.setAttribute("idtemporada", idtemporada);
			paginaDestino = "/SrvListadoInforFincaTemp";
		}
		if(realizarResponse){
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		    response.setDateHeader("Expires", 0); // Proxies.
			getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);

	}

}
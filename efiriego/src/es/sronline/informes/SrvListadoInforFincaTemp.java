package es.sronline.informes;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class SrvListadoInforFincaTemp
 */
@WebServlet("/SrvListadoInforFincaTemp")
public class SrvListadoInforFincaTemp extends HttpServlet {
	static Logger log;   

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvListadoInforFincaTemp() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idfinca = request.getParameter("idfinca");
		String idtemporada = request.getParameter("idtemporada");
		Collection<JSONObject> informes = null;
		try {
			informes = ConexionBBDD.tablaInformes(idfinca, idtemporada);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("informes", informes);
		request.setAttribute("idfinca", idfinca);
		request.setAttribute("idtemporada", idtemporada);
		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
		
		getServletContext().getRequestDispatcher("/listaInformes.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);	
	}

}

package es.sronline.informes;


import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * @author julian.rodriguez
 *
 */
public class Constantes {

	/**
	* Propiedad que almacena los valores preconfigurados
	*/
	private static Properties properties;
	
	static final int VISUALIZAR_INFORME = 1;
	static final int EDITAR_INFORME = 2;
	static final int BORRAR_INFORME = 3;
	static final int PUBLICAR_INFORME = 4;
	static final int NUEVO_INFORME = 5;
	static final int ACTUALIZAR_ESTADO = 6;
	static final int ACTUALIZAR_DATOS = 7;

	public static int getVisualizar_Informe(){
		return VISUALIZAR_INFORME;
	}
	public static int getEditar_Informe(){
		return 	EDITAR_INFORME;
	}
	public static int getBorrar_Informe(){
		return BORRAR_INFORME;
	}
	public static int getPublicar_Informe(){
		return PUBLICAR_INFORME;
	}
	public static int getNuevo_Informe(){
		return NUEVO_INFORME;
	}
	public static int getActualizar_Estado(){
		return ACTUALIZAR_ESTADO;
	}
	public static int getActualizar_Datos(){
		return ACTUALIZAR_DATOS;
	}

	public static String getConexionBBDD() {
		return properties.getProperty("ConexionBBDD");
	}

	public static String getNombreBBDD() {
		return properties.getProperty("NombreBBDD");
	}
	public static String getUsuarioBBDD() {
		return properties.getProperty("UsuarioBBDD");
	}

	public static String getPasswordBBDD() {
		return properties.getProperty("PasswordBBDD");
	}
	
	public static String getEmailDecagon() {
		return properties.getProperty("emailDecagon");
	}
	
	public static String getUrlDecagon() {
		return properties.getProperty("urlDecagon");
	}
	
	public static String getPaswordDecagon() {
		return properties.getProperty("userpassDecagon");
	}
	
	public static String getTypeReportDecagon() {
		return properties.getProperty("typeReport");
	}
	
	public static String getRootJasper() {
		return properties.getProperty("rootJasper");
	}
	
	public static String getDescargasDecagon() {
		return properties.getProperty("descargasDecagon");
	}
	
	
	static {
		init();
	}

	public static void init() {
		try {
	
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if (cl == null)
		cl = ClassLoader.getSystemClassLoader();
	
		InputStream is = cl.getResourceAsStream("informes.properties");
		properties = new Properties();
		properties.load(is);
		is.close();
		
		} catch (Exception e) {
	
			Logger.getLogger("es.sronline.informes").error("Error al cargar las propiedades " + e.getMessage());;
	
		}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}

}


package es.sronline.informes;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.xml.sax.SAXException;

import es.sronline.servicio.ServicioDescargasDecagon;
import es.sronline.servicio.ServicioParserDecagonXml;
import es.sronline.tareasQuartz.QuartzTareas;


/**
 * Servlet implementation class SrvInforme
 */
@WebServlet("/SrvInicial")
public class SrvInicial extends HttpServlet {
	static Logger log;   

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvInicial() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConexionBBDD.conectar();
				
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String name = user.getUsername(); //get logged in username
	    Collection<JSONObject> Fincas =ConexionBBDD.fincaTemporada(name);
		request.setAttribute("fincas_temporadas", Fincas);
		getServletContext().getRequestDispatcher("/inicial.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		getServletContext().getRequestDispatcher("/gestionInforme.jsp").forward(request, response);
	}

}


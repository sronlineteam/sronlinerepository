package es.sronline.informes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;


import es.sronline.informes.Constantes;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.ScriptEngine;



//Datos Campo a�adir transformacion con su tabla
//Rellenar tabla parametros y cambiar transformcaiones a b c;

public class ConexionBBDD {
	static Connection conexion = null;
	static Logger log;   

	
	public static void conectar(){

		try {
			conexion = DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
	        log = Logger.getLogger("es.sronline.bbdd.ConexionBBDD");
			//valoresDispositivos();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject getInfoPortsGrafica(String idtempo , String idfinca , String idtipo_informe){
		JSONObject idPortsGrafica = new JSONObject();
		String idgrafico = new String();
		try{
			java.sql.Statement stm0 = conexion.createStatement();
			ResultSet rs0 = stm0.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".graficos_informe_temporada WHERE idtemporada = '"+idtempo+"' AND idfinca = '"+idfinca+"' AND idtipo_informe = '"+idtipo_informe+"'");
			while (rs0.next()) {			  
				  idgrafico = rs0.getString("idgraficos_informe_temporada");
				  idPortsGrafica.put("idgrafico", idgrafico);
			}
			java.sql.Statement stmt1 = conexion.createStatement();
			ResultSet rs1 = stmt1.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".graficos_informe_temporada WHERE idgraficos_informe_temporada = '"+idgrafico+"' AND idtipo_informe = '"+idtipo_informe+"'");
			while (rs1.next()) {	
				  idPortsGrafica.put("plantilla_jrxml", (String)rs1.getString("plantilla_jrxml"));
				  idPortsGrafica.put("plantilla_jasper", (String)rs1.getString("plantilla_jasper"));
			}
				
			java.sql.Statement stmt2 = conexion.createStatement();
			ResultSet rs2 = stmt2.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".definicion_datos_grafico WHERE idgrafico = '"+idgrafico+"'");
			ArrayList<JSONObject> arrayPortsDis = new ArrayList<JSONObject>();
			while (rs2.next()) {
				  JSONObject port = new JSONObject();
				  port.put("idport", (String)rs2.getString("idport_dispositivo"));
				  port.put("etiqueta", (String)rs2.getString("etiqueta"));
				  port.put("tipo", (String)rs2.getString("tipo"));
				  port.put("color", (String)rs2.getString("color"));
				  arrayPortsDis.add(port);			  
			}
			idPortsGrafica.put("arrayPortsDis", arrayPortsDis);
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
		return idPortsGrafica;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<JSONObject> getPortsGrafica(String idtempo , String idfinca , String idtipo_informe){
		ArrayList<JSONObject> portsGrafica = new ArrayList<JSONObject>();
		String idgrafico = null;
		try{
			java.sql.Statement stm = conexion.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".temporada WHERE idtemporada = '"+idtempo+"'");
			String inicioTemporada = new String();
			String finTemporada = new String();
			
			while (rs.next()) {			  
				inicioTemporada = rs.getString("inicio");
				finTemporada = rs.getString("fin");
			}
			
			java.sql.Statement stm0 = conexion.createStatement();
			ResultSet rs0 = stm0.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".graficos_informe_temporada WHERE idtemporada = '"+idtempo+"' AND idfinca = '"+idfinca+"' AND idtipo_informe = '"+idtipo_informe+"'");
			while (rs0.next()) {			  
				  idgrafico = rs0.getString("idgraficos_informe_temporada");
			}
			
			java.sql.Statement stmt1 = conexion.createStatement();
			ResultSet rs1 = stmt1.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".definicion_datos_grafico WHERE idgrafico = '"+idgrafico+"'");
			while (rs1.next()) {			  
				  JSONObject port = new JSONObject();
				  port.put("etiqueta", (String) rs1.getString("etiqueta"));
				  port.put("tipo", (String) rs1.getString("tipo"));
				  port.put("color", (String) rs1.getString("color"));
				  port.put("unidad", (String) rs1.getString("unidad_medida"));
				  String idport_dispositivo = rs1.getString("idport_dispositivo");

				  java.sql.Statement stmt2 = conexion.createStatement();
				  ResultSet rs2 = stmt2.executeQuery("SELECT `valor_tratado`, `fecha` FROM "+Constantes.getNombreBBDD()+".datos_grafico WHERE  idport_dispositivo = '"+idport_dispositivo+"' AND idgrafico='"+idgrafico+"'"); //"' AND fecha>'"+inicioTemporada+"' AND fecha<'"+finTemporada+"'");
				  JSONArray valor_array = new JSONArray();
				  JSONArray fecha_array = new JSONArray();
				  while (rs2.next()) {
					  String valor = rs2.getString("valor_tratado");
					  valor_array.put(valor);
					  String fecha = rs2.getString("fecha");
					  fecha_array.put(fecha);
				  }
				  port.put("valores", valor_array);
				  port.put("fechas", fecha_array);
				  
				  portsGrafica.add(port);
			}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
		return portsGrafica;
	}
	
	
	@SuppressWarnings("unchecked")
	public static Collection<JSONObject> fincaTemporada(String usuario){
		Collection<JSONObject> fincas_temporadas = new ArrayList<JSONObject>();
					
		try{
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet fincas = stmt.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".finca WHERE usuario = '"+usuario+"'");
		
			while (fincas.next()){
				String idfinca = fincas.getString("idfinca");
				String nombre_finca = fincas.getString("nombre_finca");
				JSONObject objFinca = new JSONObject();
				
				objFinca.put("idFinca", idfinca);
				objFinca.put("nombreFinca", nombre_finca);

				try{	
					java.sql.Statement stmt1 = conexion.createStatement();
					ResultSet temporadas = stmt1.executeQuery("SELECT y.idfinca, x.nombre_finca, y.idtemporada, y.nombre_temporada "
							+ "FROM "+Constantes.getNombreBBDD()+".finca x, "+Constantes.getNombreBBDD()+".temporada y "
								+ "WHERE x.idfinca = y.idfinca AND x.idfinca =  '"+idfinca+"'");
					
					Collection<JSONObject> array_temporada = new ArrayList<JSONObject>();
					
					while (temporadas.next()){
						JSONObject objTemporada = new JSONObject();
						int idtemporada = temporadas.getInt("idtemporada");
						String nombre_temporada = temporadas.getString("nombre_temporada");
						objTemporada.put("idTemporada",idtemporada);
						objTemporada.put("nombreTemporada", nombre_temporada);
						array_temporada.add(objTemporada);
						objFinca.put("temporadas", array_temporada);
					}
					fincas_temporadas.add(objFinca);
				}
				catch (SQLException e) {
					// TODO Auto-generated catch block
					log.debug(e.getMessage());			
				}
			}	
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
		return fincas_temporadas;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject datosInforme(String idFinca, String idTemporada, String idInforme){
		JSONObject objdatosInformes = new JSONObject();
		String nombre_finca;
		String nombre_temporada;
		String plantilla;
		String idinforme;
		String tipo_de_informe;
		String idtipo_informe;
		String fecha_inicio;
		String fecha_fin;
		String datos_tabla;
		String datos_campos;
		String temporada_inicio;
		String temporada_fin;
		try {
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet datos = stmt.executeQuery("SELECT x.nombre_finca, y.nombre_temporada, w.plantilla, "
					+ "z.idinforme, z.tipo_de_informe , z.idtipo_informe , z.fecha_inicio, z.fecha_fin, z.datos_tabla, z.datos_campos, y.inicio as temporada_inicio, y.Fin as temporada_fin "
					+ "FROM "+Constantes.getNombreBBDD()+".finca x, "+Constantes.getNombreBBDD()+".temporada y, "+Constantes.getNombreBBDD()+".informe z, "+Constantes.getNombreBBDD()+".tipo_informe w "
					+ "WHERE w.nombre = z.tipo_de_informe AND x.idfinca  = '"+idFinca+"' AND y.idtemporada =  '"+idTemporada+"' AND z.idinforme = '"+idInforme+"'");
			while (datos.next()){
				nombre_finca = datos.getString("nombre_finca");
				nombre_temporada = datos.getString("nombre_temporada");
				plantilla = datos.getString("plantilla");
				idinforme = datos.getString("idinforme");
				tipo_de_informe = datos.getString("tipo_de_informe");
				idtipo_informe = datos.getString("idtipo_informe");
				fecha_inicio = datos.getString("fecha_inicio");
				fecha_fin = datos.getString("fecha_fin");
				datos_tabla = datos.getString("datos_tabla");
				datos_campos = datos.getString("datos_campos");
				temporada_inicio = datos.getString("temporada_inicio");
				temporada_fin = datos.getString("temporada_fin");
				//Array datos_tabla = datos.getArray("datos_tabla");
			
				objdatosInformes.put("nombre_finca", nombre_finca);
				objdatosInformes.put("nombre_temporada", nombre_temporada);
				objdatosInformes.put("plantilla", plantilla);
				objdatosInformes.put("idinforme", idinforme);
				objdatosInformes.put("idtipo_informe", idtipo_informe);
				objdatosInformes.put("tipo_de_informe", tipo_de_informe);
				objdatosInformes.put("fecha_inicio", fecha_inicio);
				objdatosInformes.put("fecha_fin", fecha_fin);
				objdatosInformes.put("datos_tabla", datos_tabla);
				objdatosInformes.put("datos_campos", datos_campos);
				objdatosInformes.put("temporada_inicio", temporada_inicio);
				objdatosInformes.put("temporada_fin", temporada_fin);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objdatosInformes;
	}
	
	
	@SuppressWarnings("unchecked")
	public static Collection<JSONObject>  tablaInformes(String idFinca, String idTemporada) throws ParseException{
		Collection<JSONObject> tablas_informes = new ArrayList<JSONObject>();
		try {
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet informes = stmt.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".informe WHERE idfinca = '"+idFinca+"' AND idtemporada = '"+idTemporada+"'");
			while (informes.next()){
				String idinforme = informes.getString("idinforme");
				String idfinca = informes.getString("idfinca");
				String idtipo_informe = informes.getString("idtipo_informe");
				String idtemporada = informes.getString("idtemporada");
				String nombre_informe = informes.getString("nombre_informe");
				String tipo_de_informe = informes.getString("tipo_de_informe");
				String fecha_inicio = informes.getString("fecha_inicio");
				String fecha_fin = informes.getString("fecha_fin");
				String estado = informes.getString("estado");
				String visualizar = informes.getString("visualizar");
				String editar = informes.getString("editar");
				String eliminar = informes.getString("eliminar");
				String enviar = informes.getString("enviar");
				
				JSONObject objtablaInformes = new JSONObject();
				
				objtablaInformes.put("idInforme", idinforme);
				objtablaInformes.put("idFinca", idfinca);
				objtablaInformes.put("idTipo_Informe", idtipo_informe);
				objtablaInformes.put("idTemporada", idtemporada);
				objtablaInformes.put("nombre_informe", nombre_informe);
				objtablaInformes.put("tipo_de_informe", tipo_de_informe);			
				objtablaInformes.put("estado", estado);
				objtablaInformes.put("visualizar", visualizar);
				objtablaInformes.put("editar", editar);
				objtablaInformes.put("eliminar", eliminar);
				objtablaInformes.put("enviar", enviar);
				
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    	Date fecha_inicioDate = (Date) formatter.parse(fecha_inicio);
				objtablaInformes.put("fecha_inicio", new SimpleDateFormat( "dd-MM-yyyy " ).format(fecha_inicioDate));
				Date fecha_finDate = (Date) formatter.parse(fecha_fin);
				objtablaInformes.put("fecha_fin", new SimpleDateFormat( "dd-MM-yyyy " ).format(fecha_finDate));
				
				tablas_informes.add(objtablaInformes);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tablas_informes;	
	}
	
	public static void altaInforme(String idFinca, String idTemporada, String TipoDeInforme, String nombreInforme, String fechaInicio, String fechaFin){
		String idTipoInforme = null;
		List<String> datosTabla = Arrays.asList(" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ");
		List<String> datosInforme = Arrays.asList(" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ");
		
		if (TipoDeInforme.equals("Semanal")){
			idTipoInforme = "1";   
		}else{
		    idTipoInforme = "2";
		}
		   
		try {
			
			java.sql.Statement stmt1 = conexion.createStatement();
			
			stmt1.executeUpdate("INSERT INTO "+Constantes.getNombreBBDD()+".informe (idfinca, idtipo_informe, idtemporada, "
			     + "nombre_informe, tipo_de_informe, fecha_inicio, fecha_fin, "
			    + "estado, visualizar, editar, eliminar, enviar,datos_tabla, datos_informe) "
			     + "VALUES ('"+idFinca+"', '"+idTipoInforme+"', '"+idTemporada+"', '"+nombreInforme+"', '"+TipoDeInforme+"', "
			     + "'"+fechaInicio+"', '"+fechaFin+"', 'Borrador', 1, 1, 1, 1, '"+datosTabla+"', '"+datosInforme+"')");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
		
	}
	
	public static void actualizarEstado(String idInforme){
	  try{
	   java.sql.Statement stmt = conexion.createStatement();
	   stmt.executeUpdate("UPDATE "+Constantes.getNombreBBDD()+".informe SET estado = 'Publicado' WHERE idinforme = '"+idInforme+"'");
	    }catch (SQLException e) {
	      // TODO Auto-generated catch block
	      log.debug(e.getMessage()); 
	     }
	 }
	
	public static void modifTablaInforme(String datosTabla, String idInforme){
		
		try{	
			java.sql.Statement stmt1 = conexion.createStatement();
			stmt1.executeUpdate("UPDATE  "+Constantes.getNombreBBDD()+".informe SET datos_tabla = '"+datosTabla+"' WHERE idinforme = "+idInforme);
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }

	}
	public static void borrarInforme(String idFinca, String idTemporada, String idInforme){
		  try{//Con el ID de informe valdr�a
		   java.sql.Statement stmt = conexion.createStatement();
		   stmt.executeUpdate("DELETE FROM "+Constantes.getNombreBBDD()+".informe "
		     + "WHERE idinforme = '"+idInforme+"' "
		       + "AND idfinca = '"+idFinca+"' "
		         + "AND idtemporada = '"+idTemporada+"'");
		  }catch (SQLException e) {
		   // TODO Auto-generated catch block
		   log.debug(e.getMessage());
		   
		  }
	}
	
	@SuppressWarnings({ "unchecked", "null" })
	public static  void insertDatosDecagon(String iddispositivo, String dataString, String rid_new, String rid_past, String horaPeticion) throws SQLException, ScriptException{		
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    
	    updateHoraPeticionDecagon(iddispositivo,horaPeticion);
	    
	    ArrayList<String> raws_past_list = new ArrayList<String>();
	    raws_past_list.add("fecha");
	    	    
		ArrayList<JSONObject> ports_grafico = new ArrayList<JSONObject>();
		
		java.sql.Statement stmt = conexion.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".port_dispositivo where iddispositivo_campo = '" + iddispositivo +"'"); 	  	        
   	    while (rs.next()) {
   	    	  JSONObject port_grafico = new JSONObject();
   	    	  String idport = rs.getString("idport_dispositivo");
   	    	  port_grafico.put("idport_dispositivo", idport);
   	    	  
   	    	  ArrayList<JSONObject> array_transformaciones = new ArrayList<JSONObject>();
			  java.sql.Statement stmt0 = conexion.createStatement();
			  ResultSet rs0 = stmt0.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".port_dispositivos_transformaciones_raw where idport_dispositivo = '" + idport +"'"); 	  	    	      
			  while (rs0.next()) {
				  
				  java.sql.Statement stmt1 = conexion.createStatement();
				  ResultSet rs1 = stmt1.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".transformaciones_raw where idtransformaciones_raw = '" + (String) rs0.getString("idtransformaciones_raw") +"'"); 	  	    	      
				  while (rs1.next()) {		  
					  JSONObject transformacion = new JSONObject();
					  transformacion.put("condicion", (String) rs1.getString("condicion"));
					  transformacion.put("transformaciones", (String) rs1.getString("transformaciones"));
					  transformacion.put("tipo", (String) rs1.getString("tipo"));
					  array_transformaciones.add(transformacion);
				  }
				  
			  }
			  port_grafico.put("transformacion_raw", array_transformaciones);
			  
			  ArrayList<JSONObject> array_idgrafico_port = new ArrayList<JSONObject>();
			  
			  java.sql.Statement stmt2 = conexion.createStatement();
			  ResultSet rs2 = stmt2.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".definicion_datos_grafico where idport_dispositivo = '" + idport +"'"); 	  	    	      		  
			  while (rs2.next()) {
				  JSONObject transformaciones_grafico = new JSONObject();
				  transformaciones_grafico.put("idgrafico", (String) rs2.getString("idgrafico"));
				  transformaciones_grafico.put("transformacion", (String) rs2.getString("transformacion"));
				  
				  ArrayList<JSONObject> array_parametros = new ArrayList<JSONObject>();
				  
				  java.sql.Statement stmt3 = conexion.createStatement();
				  ResultSet rs3 = stmt3.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".parametros where idport_dispositivo = '" + idport +"'"); 	  	    	      		  
				  while (rs3.next()) {
					  JSONObject parametro = new JSONObject();
					  parametro.put("nombre", (String) rs3.getString("nombre"));
					  parametro.put("valor", (String) rs3.getString("valor"));
					  array_parametros.add(parametro);
				  }
				  
				  transformaciones_grafico.put("array_parametros", array_parametros);
				  array_idgrafico_port.add(transformaciones_grafico);
		      } 
			  port_grafico.put("array_idgrafico_port", array_idgrafico_port);		  
			  ports_grafico.add(port_grafico);
			  
			  if(!rid_past.equals("0")){
				  	 java.sql.Statement stmt4 = conexion.createStatement();	   	    	 
					 ResultSet rs4 = stmt4.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".datos_campo WHERE idport_dispositivo = '"+idport+"' ORDER BY iddato_campo DESC LIMIT 1"); 	  	    	      		  
					 while (rs4.next()) {
						 raws_past_list.add((String)rs4.getString("raw"));		 
					 }	
		   	  } else {
	   		  		raws_past_list.add("0");
		   	  }			  
   	    }
   	    
   	    String[] raws_past = new String[raws_past_list.size()];
   	    raws_past = raws_past_list.toArray(raws_past);
   	    
   	    java.sql.Statement stmt_datos_campo = conexion.createStatement();
        java.sql.Statement stmt_datos_grafico = conexion.createStatement();
        
   	    String[] datos = dataString.split("\\s+");	       
        for (int i=0; i < datos.length ; i++){
        	 String[] raws = datos[i].split(",");
        	 if (i!=0){
        		 raws_past = datos[i-1].split(","); 
        	 }
  	 
        	 Date date = new Date((Integer.parseInt(raws[0])*1000L + 946684800*1000L));
        	 String dateAsText = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        	 for (int j=1; j < (raws.length - 1) ; j++){
        		 String valor_transformado = new String();
        		 ArrayList<JSONObject> transformaciones_raw =  (ArrayList<JSONObject>) ports_grafico.get(j-1).get("transformacion_raw");
        		 for (int t=0; t < (transformaciones_raw.size()) ; t++){
        			 JSONObject transformacion_json = transformaciones_raw.get(t);
        			 String tipo = (String) transformacion_json.get("tipo");
        			 String condicion = (String) transformacion_json.get("condicion");
        			 condicion = condicion.replace("R",raws[j]);
        			 if (tipo.equals("1")){
        				 condicion = condicion.replace("P",(String)raws_past[j]);
        			 }
        			 
        			 String resultCondicion = String.valueOf(engine.eval(condicion));
        			 if (resultCondicion.equals("true")){
        				 String transformacion = (String) transformacion_json.get("transformaciones");
        				 transformacion = transformacion.replace("R",raws[j]);
            			 if (tipo.equals("1")){
            				 transformacion = transformacion.replace("P",(String)raws_past[j]);
            			 }
		        		 valor_transformado = String.valueOf(engine.eval(transformacion));
		        		 stmt_datos_campo.addBatch("INSERT INTO "+Constantes.getNombreBBDD()+".datos_campo (idport_dispositivo, fecha, raw, valor_transformado) "
			    			     + "VALUES ('"+(String) ports_grafico.get(j-1).get("idport_dispositivo")+"','"+dateAsText+"','"+raws[j]+"','"+ valor_transformado +"');"); 			        	 
        			 }
        		 } 		     
        	 }
        	 
        	 for (int j=1; j < (raws.length - 1) ; j++){
        		 ArrayList<JSONObject> array_idgrafico_port = (ArrayList<JSONObject>) ports_grafico.get(j-1).get("array_idgrafico_port");
        		 for (int l=0; l <  array_idgrafico_port.size() ; l++){
        			 JSONObject idgrafico_port = array_idgrafico_port.get(l);			 
        			 String valor_transformado = new String();         		 
        			 ArrayList<JSONObject> transformaciones_raw =  (ArrayList<JSONObject>) ports_grafico.get(j-1).get("transformacion_raw");
            		 for (int t=0; t < (transformaciones_raw.size()) ; t++){
            			 JSONObject transformacion_json = transformaciones_raw.get(t);
            			 String tipo = (String) transformacion_json.get("tipo");  
            			 
            			 String condicion = (String) transformacion_json.get("condicion");           			 
            			 condicion = condicion.replace("R",raws[j]);
            			 if (tipo.equals("1")){
            				 condicion = condicion.replace("P",(String)raws_past[j]);
            			 }    
            			 
            			 String resultCondicion = String.valueOf(engine.eval(condicion));
            			 if (resultCondicion.equals("true")){
            				 String transformacion = (String) transformacion_json.get("transformaciones");
            				 transformacion = transformacion.replace("R",raws[j]);
                			 if (tipo.equals("1")){
                				 transformacion = transformacion.replace("P",(String)raws_past[j]);
                			 }
    		        		 valor_transformado = String.valueOf(engine.eval(transformacion));
            			 }
            		 }
        			 
	    		     String transformacion = (String) idgrafico_port.get("transformacion");
	    		     String valor_tratado;
	    		     if (transformacion.equals("x")){
	    		    	 valor_tratado = valor_transformado;
	    		     }else{    		    	 
	        		     transformacion = transformacion.replace("x",valor_transformado);
	        		     ArrayList<JSONObject> arrayParametros = (ArrayList<JSONObject>) idgrafico_port.get("array_parametros");
	        		     for (int p=0; p < arrayParametros.size() ; p++){
	        		    	 JSONObject parametro = arrayParametros.get(p);
	        		    	 transformacion = transformacion.replace((String)parametro.get("nombre"),(String)parametro.get("valor"));
	        		     }
	        		     valor_tratado = String.valueOf(engine.eval(transformacion));
	    		     }
	    		     
	    		     stmt_datos_grafico.addBatch("INSERT INTO "+Constantes.getNombreBBDD()+".datos_grafico (idgrafico, idport_dispositivo, fecha, valor_tratado) "
		    			     + "VALUES ('"+(String) idgrafico_port.get("idgrafico")+"','"+(String) ports_grafico.get(j-1).get("idport_dispositivo")+"','"+dateAsText+"','"+valor_tratado+"');"); 			        	 
	        	 }
        	 } 
        }
        
        stmt_datos_campo.executeBatch(); 	
	   	stmt_datos_grafico.executeBatch();
	   	
	   	java.sql.Statement st = conexion.createStatement();	
		st.executeUpdate("UPDATE  "+Constantes.getNombreBBDD()+".dispositivo_campo"
							+ " SET rid = '"+rid_new+"' , hora_ultima_peticion ='"+horaPeticion+"' WHERE iddispositivo_campo = "+iddispositivo);
		
        
	}
	
	public static  void updateHoraPeticionDecagon(String iddispositivo, String horaPeticion) throws SQLException{		
	    
	    java.sql.Statement st = conexion.createStatement();	
		st.executeUpdate("UPDATE  "+Constantes.getNombreBBDD()+".dispositivo_campo"
							+ " SET hora_ultima_peticion ='"+horaPeticion+"' WHERE iddispositivo_campo = "+iddispositivo);		
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<JSONObject> getArrayDispositivosDecagon(){
		ArrayList<JSONObject> array_dispositivos = new ArrayList<JSONObject>();

		try{
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".dispositivo_campo");
			while (rs.next()) {
				  JSONObject dispositivo = new JSONObject();
				  dispositivo.put("iddispositivo_campo", (String) rs.getString("iddispositivo_campo"));
				  dispositivo.put("id_decagon", (String) rs.getString("id_decagon"));
				  dispositivo.put("password", (String) rs.getString("password"));
				  dispositivo.put("rid", (String) rs.getString("rid"));
				  dispositivo.put("hora_ultima_peticion", (String) rs.getString("hora_ultima_peticion"));
				  array_dispositivos.add(dispositivo);
			} 
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
		return array_dispositivos;
	}
	
}

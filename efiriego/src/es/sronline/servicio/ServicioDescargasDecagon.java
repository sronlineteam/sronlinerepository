package es.sronline.servicio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.io.*;
import javax.xml.parsers.*;
import org.json.XML;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


import es.sronline.informes.Constantes;
import es.sronline.tareasQuartz.DescargaDatosPeriodicos;



public class ServicioDescargasDecagon {
	private static org.slf4j.Logger _log = LoggerFactory.getLogger(ServicioDescargasDecagon.class);

	public static String datosDecagonGET(String device , String devicepass , String mrid) {

        String responce = "";
        BufferedReader rd = null;
        String request = Constantes.getUrlDecagon() + "/?email=" + Constantes.getEmailDecagon()+ 
        		"&userpass=" + Constantes.getPaswordDecagon() + "&deviceid=" + device 
        		+ "&devicepass=" + devicepass + "&report=" +  Constantes.getTypeReportDecagon() + "&mrid=" + mrid;
        try {
            URL url = new URL(request);
            URLConnection conn2 = url.openConnection();
            rd = new BufferedReader(new InputStreamReader(conn2.getInputStream()));           
            String line;
            while ((line = rd.readLine()) != null) {
                //Process line...
                responce += line;
            }
        } catch (Exception e) {
        	_log.info("Web request failed");
        // Web request failed
        } finally {
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException ex) {
                	_log.info("Problema al cerrar el objeto lector");
                }
            }
        }

        //_log.info("Responce decagon: " + responce);
		return responce;
	}
}

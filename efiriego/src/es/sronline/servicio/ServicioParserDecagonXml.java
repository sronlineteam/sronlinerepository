package es.sronline.servicio;
import java.io.File;
import java.io.StringReader;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ServicioParserDecagonXml {
	public static String xmlString = null;
	public static String dataString = null;
	public static String rid = null;
	
	public static void main(String args[]) {
	    try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler() {
				boolean data = false;
				StringBuilder stringBuffer = new StringBuilder();
		        public void startElement(String uri, String localName, String qName, Attributes attributes)
		            throws SAXException {
	        	
						stringBuffer.setLength(0);
		        		if (qName.equalsIgnoreCase("Data")) {
	        				data = true;
	        				int len = attributes.getLength();
		        		    // Loop through all attributes and save them as needed
		        		    for(int i = 0; i < len; i++){
		        		        String sAttrName = attributes.getLocalName(i);
		        		        if(sAttrName.compareTo("rid") == 0){
		        		          rid = attributes.getValue(i);
		        		        }
		        		    }
		        		}
		        	}

				public void characters(char ch[], int start, int length) throws SAXException {
					if (data) {
				        stringBuffer.append(new String(ch, start, length));
					}
				}
				
				public void endElement(String namespaceURI, String localName, String qName) throws SAXException {				
				    if (data) {
				    	dataString = stringBuffer.toString().trim();
				        data = false;
				    }
				}
				
			};

			saxParser.parse(new InputSource(new StringReader(xmlString)), handler);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
}


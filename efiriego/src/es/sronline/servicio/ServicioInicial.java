package es.sronline.servicio;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import es.sronline.informes.Constantes;
import es.sronline.tareasQuartz.QuartzTareas;

/**
 * Application Lifecycle Listener implementation class ServicioInicial
 *
 */
@WebListener
public class ServicioInicial implements ServletContextListener {
	Timer timer;
	Logger log ;

    /**
     * Default constructor. 
     */
	
    public ServicioInicial() {
		log = Logger.getLogger("eu.acebes.regadio.ServicioInicial");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
    */
    public void contextInitialized(ServletContextEvent arg0) {
		
		log.debug("Iniciado en contexto");
		
		if (Constantes.getDescargasDecagon().equals("true")){
			try {
				QuartzTareas start = new QuartzTareas();
			    start.descargasPeriodicas();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}

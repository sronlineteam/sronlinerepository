package es.bermejo.conteo.core;

import java.io.InputStream;
import java.util.Properties;


/**
 * @author julian.rodriguez
 *
 */
public class Constantes {

	/**
	* Propiedad que almacena los valores preconfigurados
	*/
	private static Properties properties;
	
	static {
	init();
	}

	public static void init() {

		try {
	
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if (cl == null)
		cl = ClassLoader.getSystemClassLoader();
	
		InputStream is = cl.getResourceAsStream("conteo.properties");
		properties = new Properties();
		properties.load(is);
		is.close();
		
		} 
		catch (Exception e) {
			System.out.println("Error al cargar las propiedades " + e.getMessage());
		}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}

	public static String getDriverBBDD() {
		return properties.getProperty("DriverBBDD");
	}

	public static String getConexionBBDD() {
		return properties.getProperty("ConexionBBDD");
	}

	public static String getNombreBBDD() {
		return properties.getProperty("NombreBBDD");
	}
	public static String getUsuarioBBDD() {
		return properties.getProperty("UsuarioBBDD");
	}

	public static String getPasswordBBDD() {
		return properties.getProperty("PasswordBBDD");
	}



}

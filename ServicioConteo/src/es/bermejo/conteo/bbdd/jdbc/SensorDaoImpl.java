package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import es.bermejo.conteo.bbdd.dao.SensorDao;
import es.bermejo.conteo.bbdd.dto.Sensor;
import es.bermejo.conteo.bbdd.dto.SensorPk;
import es.bermejo.conteo.bbdd.exceptions.SensorDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class SensorDaoImpl extends AbstractDAO implements SensorDao{
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `sensores`.`idSensor` FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`idSensor`) VALUES ( ?)";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET `idSensor` = ?, WHERE `idSensor` = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `idSensor` = ?";

	/** 
	 * Index of column idSensor
	 */
	protected static final int COLUMN_ID_SENSOR = 1;

	public SensorPk insert(Sensor dto) throws SensorDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setString( index++, dto.getIdSensor() );
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdSensor( rs.getString( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new SensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(SensorPk pk, Sensor dto) throws SensorDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setString( index++, dto.getIdSensor() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new SensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(SensorPk pk) throws SensorDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setString( 1, pk.getIdSensor() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new SensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Sensor findByPrimaryKey(SensorPk pk) throws SensorDaoException {
		return findByPrimaryKey( pk.getIdSensor() );
	}

	public Sensor findByPrimaryKey(String idSensor) throws SensorDaoException {
		Sensor ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idSensor = ?", new Object[] {  new String(idSensor) } );
		return ret.length==0 ? null : ret[0];
	}

	public Sensor[] findAll() throws SensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idSensor", null );
	}

	public Sensor[] findWhereIdSensorEquals(String idSensor) throws SensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE idSensor = ? ORDER BY idSensor", new Object[] {  new String(idSensor) } );
	}

	/**
	 * Method 'SensorDaoImpl'
	 * 
	 */
	public SensorDaoImpl()
	{
	}

	/**
	 * Method 'SensorDaoImpl'
	 * 
	 * @param userConn
	 */
	public SensorDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".sensores";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected Sensor fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Sensor dto = new Sensor();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Sensor[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Sensor dto = new Sensor();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Sensor ret[] = new Sensor[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Sensor dto, ResultSet rs) throws SQLException
	{
		dto.setIdSensor( rs.getString( COLUMN_ID_SENSOR) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Sensor dto)
	{
	}
	
	public Sensor[] findByDynamicSelect(String sql, Object[] sqlParams) throws SensorDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new SensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Sensor[] findByDynamicWhere(String sql, Object[] sqlParams) throws SensorDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new SensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

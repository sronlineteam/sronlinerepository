package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import es.bermejo.conteo.bbdd.dao.EmpleadoDao;
import es.bermejo.conteo.bbdd.dto.Empleado;
import es.bermejo.conteo.bbdd.dto.EmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.EmpleadoDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class EmpleadoDaoImpl extends AbstractDAO implements EmpleadoDao{
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `empleados`.`idempleado`,`empleados`.`nombre`, `empleados`.`alta`, `empleados`.`baja`, `empleados`.`funcion` FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`idempleado`, `nombre`, `alta`, `baja`, `funcion`) VALUES ( ?, ?, ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET `idempleado` = ?, `nombre` = ?, `alta` = ?, `baja` = ?, `funcion` = ? WHERE `idempleado` = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `idempleado` = ?";

	/** 
	 * Index of column idprogramacion
	 */
	protected static final int COLUMN_IDEMPLEADO = 1;

	/** 
	 * Index of column idprogramacion
	 */
	protected static final int COLUMN_NOMBRE = 2;

	/** 
	 * Index of column idprogramacion
	 */
	protected static final int COLUMN_ALTA = 3;

	/** 
	 * Index of column idprogramacion
	 */
	protected static final int COLUMN_BAJA = 4;


	/** 
	 * Index of column idprogramacion
	 */
	protected static final int COLUMN_FUNCION= 5;

	
	public EmpleadoPk insert(Empleado dto) throws EmpleadoDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdEmpleado() );
			stmt.setString( index++, dto.getNombre() );
			stmt.setTimestamp(index++, dto.getAlta()==null ? null : new java.sql.Timestamp( dto.getAlta().getTime() ) );
			stmt.setTimestamp(index++, dto.getBaja()==null ? null : new java.sql.Timestamp( dto.getBaja().getTime() ) );
			stmt.setInt( index++, dto.getFuncion());
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdEmpleado( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new EmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(EmpleadoPk pk, Empleado dto) throws EmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdEmpleado() );
			stmt.setString( index++, dto.getNombre() );
			stmt.setTimestamp(index++, dto.getAlta()==null ? null : new java.sql.Timestamp( dto.getAlta().getTime() ) );
			stmt.setTimestamp(index++, dto.getBaja()==null ? null : new java.sql.Timestamp( dto.getBaja().getTime() ) );
			stmt.setInt( index++, dto.getFuncion());
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new EmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(EmpleadoPk pk) throws EmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdEmpleado() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new EmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Empleado findByPrimaryKey(EmpleadoPk pk) throws EmpleadoDaoException {
		return findByPrimaryKey( pk.getIdEmpleado() );
	}

	public Empleado findByPrimaryKey(int idEmpleado) throws EmpleadoDaoException {
		Empleado ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idempleado = ?", new Object[] {  new Integer(idEmpleado) } );
		return ret.length==0 ? null : ret[0];
	}

	public Empleado[] findAll() throws EmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idempleado", null );
	}

	public Empleado[] findWhereIdEmpleadoEquals(int idEmpleado) throws EmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE idempleado = ? ORDER BY idempleado", new Object[] {  new Integer(idEmpleado) } );
	}

	/**
	 * Method 'ProgramacionDaoImpl'
	 * 
	 */
	public EmpleadoDaoImpl()
	{
	}

	/**
	 * Method 'ProgramacionDaoImpl'
	 * 
	 * @param userConn
	 */
	public EmpleadoDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".empleados";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected Empleado fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Empleado dto = new Empleado();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Empleado[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Empleado dto = new Empleado();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Empleado ret[] = new Empleado[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Empleado dto, ResultSet rs) throws SQLException
	{
		dto.setIdEmpleado( rs.getInt( COLUMN_IDEMPLEADO) );
		dto.setNombre( rs.getString( COLUMN_NOMBRE) );
		dto.setAlta( rs.getTimestamp(COLUMN_ALTA) );
		dto.setBaja( rs.getTimestamp(COLUMN_BAJA) );
		dto.setFuncion( rs.getInt( COLUMN_FUNCION ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Empleado dto)
	{
	}
	
	public Empleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws EmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new EmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Empleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws EmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new EmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

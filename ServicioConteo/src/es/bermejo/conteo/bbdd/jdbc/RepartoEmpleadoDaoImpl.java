package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import es.bermejo.conteo.bbdd.dao.RepartoEmpleadoDao;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleado;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.RepartoEmpleadoDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class RepartoEmpleadoDaoImpl extends AbstractDAO implements RepartoEmpleadoDao{
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `repartos_idrepartos`, `empleados_idempleados`, `peso`, `nombre` FROM " + Constantes.getNombreBBDD() + ".v_repartos_empleados";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`repartos_idrepartos`, `empleados_idempleados`, `peso`) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET peso = ? WHERE `repartos_idrepartos` = ? and `empleados_idempleados` = ? ";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `repartos_idrepartos` = ? and `empleados_idempleados` = ? ";

	/** 
	 * Index of column idReparto
	 */
	protected static final int COLUMN_ID_REPARTO = 1;

	/** 
	 * Index of column idEmpleado
	 */
	protected static final int COLUMN_ID_EMPLEADO = 2;

	/** 
	 * Index of column dia
	 */
	protected static final int COLUMN_PESO = 3;

	/** 
	 * Index of column idEmpleado
	 */
	protected static final int COLUMN_EMPLEADO = 4;

	public RepartoEmpleadoPk insert(RepartoEmpleado dto) throws RepartoEmpleadoDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdReparto() );
			stmt.setInt( index++, dto.getIdEmpleado() );
			stmt.setInt(index++, dto.getPeso());
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdReparto( rs.getInt( COLUMN_ID_REPARTO ) );
				dto.setIdEmpleado( rs.getInt( COLUMN_ID_EMPLEADO ) );
				dto.setPeso( rs.getInt( COLUMN_PESO ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new RepartoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(RepartoEmpleadoPk pk, RepartoEmpleado dto) throws RepartoEmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getPeso() );
			stmt.setInt( index++, dto.getIdReparto() );
			stmt.setInt( index++, dto.getIdEmpleado() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new RepartoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(RepartoEmpleadoPk pk) throws RepartoEmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdReparto() );
			stmt.setInt( 2, pk.getidEmpleado());
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new RepartoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public RepartoEmpleado findByPrimaryKey(RepartoEmpleadoPk pk) throws RepartoEmpleadoDaoException {
		return findByPrimaryKey( pk.getIdReparto(), pk.getidEmpleado());
	}

	public RepartoEmpleado findByPrimaryKey(int idReparto, int idEmpleado) throws RepartoEmpleadoDaoException {
		RepartoEmpleado ret[] = findByDynamicSelect( SQL_SELECT + " WHERE `repartos_idrepartos` = ? and `empleados_idempleados` = ? ", new Object[] {  new Integer(idReparto), new Integer(idEmpleado) } );
		return ret.length==0 ? null : ret[0];
	}

	public RepartoEmpleado[] findAll() throws RepartoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY repartos_idrepartos", null );
	}

	public RepartoEmpleado[] findWhereIdRepartoEquals(int idReparto) throws RepartoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE `repartos_idrepartos` = ? ORDER BY `repartos_idrepartos`", new Object[] {  new Integer(idReparto) } );
	}

	public RepartoEmpleado[] findWhereIdEmpleadoEquals(int idEmpleado) throws RepartoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE empleados_idempleados = ? ORDER BY empleados_idempleados", new Object[] {  new Integer(idEmpleado) } );
	}
	
	/**
	 * Method 'RepartoEmpleadoDaoImpl'
	 * 
	 */
	public RepartoEmpleadoDaoImpl()
	{
	}

	/**
	 * Method 'RepartoEmpleadoDaoImpl'
	 * 
	 * @param userConn
	 */
	public RepartoEmpleadoDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".repartos_empleados";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected RepartoEmpleado fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			RepartoEmpleado dto = new RepartoEmpleado();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected RepartoEmpleado[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			RepartoEmpleado dto = new RepartoEmpleado();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		RepartoEmpleado ret[] = new RepartoEmpleado[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(RepartoEmpleado dto, ResultSet rs) throws SQLException
	{
		dto.setIdReparto( rs.getInt( COLUMN_ID_REPARTO) );
		dto.setIdEmpleado( rs.getInt( COLUMN_ID_EMPLEADO) );
		dto.setPeso( rs.getInt(COLUMN_PESO) );
		dto.setEmpleado(rs.getString(COLUMN_EMPLEADO));
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(RepartoEmpleado dto)
	{
	}
	
	public RepartoEmpleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws RepartoEmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new RepartoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public RepartoEmpleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws RepartoEmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new RepartoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

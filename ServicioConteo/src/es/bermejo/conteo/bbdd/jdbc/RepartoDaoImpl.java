package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import es.bermejo.conteo.bbdd.dao.RepartoDao;
import es.bermejo.conteo.bbdd.dto.Reparto;
import es.bermejo.conteo.bbdd.dto.RepartoPk;
import es.bermejo.conteo.bbdd.exceptions.RepartoDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class RepartoDaoImpl extends AbstractDAO implements RepartoDao{
	protected java.sql.Connection userConn;

	protected static final LoggerContext context = new LoggerContext("Conteo");
	protected static final Logger logger = context.getLogger("RepartoDaoImpl");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `idrepartos`,`descripcion`, `desde`, `hasta` FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`descripcion`, `desde`, `hasta`) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET `descripcion` = ?, desde = ?, `hasta` = ? WHERE `idrepartos` = ? ";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `idrepartos` = ? ";

	/** 
	 * Index of column idPuesto
	 */
	protected static final int COLUMN_ID_REPARTO = 1;

	/** 
	 * Index of column idSensor
	 */
	protected static final int COLUMN_DESCRIPCION = 2;

	/** 
	 * Index of column desde
	 */
	protected static final int COLUMN_DESDE = 3;

	/** 
	 * Index of column hasta
	 */
	protected static final int COLUMN_HASTA = 4;

	public RepartoPk insert(Reparto dto) throws RepartoDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setTimestamp(index++, dto.getDesde()==null ? null : new java.sql.Timestamp( dto.getDesde().getTime() ) );
			stmt.setTimestamp(index++, dto.getHasta()==null ? null : new java.sql.Timestamp( dto.getHasta().getTime() ) );
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			ResultSet maxrows = stmt.getGeneratedKeys();
			if (maxrows.next()){
				setMaxRows(maxrows.getInt(1));
			}
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms) ");
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new RepartoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(RepartoPk pk, Reparto dto) throws RepartoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setTimestamp(index++, dto.getDesde()==null ? null : new java.sql.Timestamp( dto.getDesde().getTime() ) );
			stmt.setTimestamp(index++, dto.getHasta()==null ? null : new java.sql.Timestamp( dto.getHasta().getTime() ) );
			stmt.setInt( index++, dto.getIdReparto() );
			
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new RepartoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(RepartoPk pk) throws RepartoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdReparto() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new RepartoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Reparto findByPrimaryKey(RepartoPk pk) throws RepartoDaoException {
		return findByPrimaryKey( pk.getIdReparto());
	}

	public Reparto findByPrimaryKey(int idReparto) throws RepartoDaoException {
		Reparto ret[] = findByDynamicSelect( SQL_SELECT + " WHERE `idrepartos` = ? ", new Object[] {  new Integer(idReparto)} );
		return ret.length==0 ? null : ret[0];
	}

	public Reparto[] findAll() throws RepartoDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idrepartos", null );
	}

	public Reparto[] findWhereIdRepartoEquals(int idPuesto) throws RepartoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE idrepartos = ? ORDER BY idrepartos", new Object[] {  new Integer(idPuesto) } );
	}

	public Reparto[] findWhereDesdeEquals(Date desde) throws RepartoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE desde = ? ORDER BY idrepartos", new Object[] {  desde } );
	}
	
	/**
	 * Method 'RepartoDaoImpl'
	 * 
	 */
	public RepartoDaoImpl()
	{
	}

	/**
	 * Method 'RepartoDaoImpl'
	 * 
	 * @param userConn
	 */
	public RepartoDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".repartos";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected Reparto fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Reparto dto = new Reparto();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Reparto[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Reparto dto = new Reparto();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Reparto ret[] = new Reparto[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Reparto dto, ResultSet rs) throws SQLException
	{
		dto.setIdReparto( rs.getInt( COLUMN_ID_REPARTO) );
		dto.setDescripcion( rs.getString( COLUMN_DESCRIPCION) );
		dto.setDesde( rs.getTimestamp(COLUMN_DESDE) );
		dto.setHasta( rs.getTimestamp(COLUMN_HASTA) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Reparto dto)
	{
	}
	
	public Reparto[] findByDynamicSelect(String sql, Object[] sqlParams) throws RepartoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new RepartoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public Reparto[] findByDynamicWhere(String sql, Object[] sqlParams) throws RepartoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new RepartoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

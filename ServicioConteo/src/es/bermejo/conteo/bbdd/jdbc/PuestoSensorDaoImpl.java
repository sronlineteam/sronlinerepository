package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import es.bermejo.conteo.bbdd.dao.PuestoSensorDao;
import es.bermejo.conteo.bbdd.dto.PuestoSensor;
import es.bermejo.conteo.bbdd.dto.PuestoSensorPk;
import es.bermejo.conteo.bbdd.dto.PuestoSensor;
import es.bermejo.conteo.bbdd.exceptions.PuestoSensorDaoException;
import es.bermejo.conteo.bbdd.exceptions.PuestoSensorDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class PuestoSensorDaoImpl extends AbstractDAO implements PuestoSensorDao{
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `Puesto_idPuesto`, `sensores_idsensores`, `dia` FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`Puesto_idPuesto`, `sensores_idsensores`, `desde`, `hasta`) VALUES ( ?, ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET `Puesto_idPuesto` = ?, `sensores_idsensores` = ?, desde = ?, `hasta` = ? WHERE `Puesto_idPuesto` = ? and `sensores_idsensores` = ? and desde = ? and `hasta` = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `Puesto_idPuesto` = ? and `sensores_idsensores` = ? and desde = ? and hasta = ?";

	/** 
	 * Index of column idPuesto
	 */
	protected static final int COLUMN_ID_PUESTO = 1;

	/** 
	 * Index of column idSensor
	 */
	protected static final int COLUMN_ID_SENSOR = 2;

	/** 
	 * Index of column desde
	 */
	protected static final int COLUMN_DESDE = 3;

	/** 
	 * Index of column hasta
	 */
	protected static final int COLUMN_HASTA = 4;

	public PuestoSensorPk insert(PuestoSensor dto) throws PuestoSensorDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdPuesto() );
			stmt.setInt( index++, dto.getIdSensor() );
			stmt.setTimestamp(index++, dto.getDesde()==null ? null : new java.sql.Timestamp( dto.getDesde().getTime() ) );
			stmt.setTimestamp(index++, dto.getHasta()==null ? null : new java.sql.Timestamp( dto.getHasta().getTime() ) );
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdPuesto( rs.getInt( COLUMN_ID_PUESTO ) );
				dto.setIdSensor( rs.getInt( COLUMN_ID_SENSOR ) );
				dto.setDesde( rs.getTimestamp( COLUMN_DESDE ) );
				dto.setHasta( rs.getTimestamp( COLUMN_HASTA ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new PuestoSensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(PuestoSensorPk pk, PuestoSensor dto) throws PuestoSensorDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdPuesto() );
			stmt.setInt( index++, dto.getIdSensor() );
			stmt.setTimestamp(index++, dto.getDesde()==null ? null : new java.sql.Timestamp( dto.getDesde().getTime() ) );
			stmt.setTimestamp(index++, dto.getHasta()==null ? null : new java.sql.Timestamp( dto.getHasta().getTime() ) );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new PuestoSensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(PuestoSensorPk pk) throws PuestoSensorDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdPuesto() );
			stmt.setInt( 2, pk.getIdSensor());
			stmt.setTimestamp(3, pk.getDesde()==null ? null : new java.sql.Timestamp( pk.getDesde().getTime() ) );
			stmt.setTimestamp(4, pk.getHasta()==null ? null : new java.sql.Timestamp( pk.getHasta().getTime() ) );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new PuestoSensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public PuestoSensor findByPrimaryKey(PuestoSensorPk pk) throws PuestoSensorDaoException {
		return findByPrimaryKey( pk.getIdPuesto(), pk.getIdSensor(), pk.getDesde(), pk.getHasta() );
	}

	public PuestoSensor findByPrimaryKey(int idPuesto, int idSensor, Date desde, Date hasta) throws PuestoSensorDaoException {
		PuestoSensor ret[] = findByDynamicSelect( SQL_SELECT + " WHERE `Puesto_idPuesto` = ? and `sensores_idsensores` = ? and desde = ? and hasta = ? ", new Object[] {  new Integer(idPuesto), new Integer(idSensor), desde, hasta } );
		return ret.length==0 ? null : ret[0];
	}

	public PuestoSensor[] findAll() throws PuestoSensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY desde", null );
	}

	public PuestoSensor[] findWhereIdPuestoEquals(int idPuesto) throws PuestoSensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE idPuesto = ? ORDER BY Puesto_idPuesto", new Object[] {  new Integer(idPuesto) } );
	}

	public PuestoSensor[] findWhereIdSensorEquals(int idSensor) throws PuestoSensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE Sensors_idSensors = ? ORDER BY Sensors_idSensors", new Object[] {  new Integer(idSensor) } );
	}

	public PuestoSensor[] findWhereDesdeEquals(Date desde) throws PuestoSensorDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE desde = ? ORDER BY Puesto_idPuesto", new Object[] {  desde } );
	}
	
	/**
	 * Method 'PuestoSensorDaoImpl'
	 * 
	 */
	public PuestoSensorDaoImpl()
	{
	}

	/**
	 * Method 'PuestoSensorDaoImpl'
	 * 
	 * @param userConn
	 */
	public PuestoSensorDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".sensor_puesto";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected PuestoSensor fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			PuestoSensor dto = new PuestoSensor();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected PuestoSensor[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			PuestoSensor dto = new PuestoSensor();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		PuestoSensor ret[] = new PuestoSensor[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(PuestoSensor dto, ResultSet rs) throws SQLException
	{
		dto.setIdPuesto( rs.getInt( COLUMN_ID_PUESTO) );
		dto.setIdSensor( rs.getInt( COLUMN_ID_SENSOR) );
		dto.setDesde( rs.getTimestamp(COLUMN_DESDE) );
		dto.setHasta( rs.getTimestamp(COLUMN_HASTA) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(PuestoSensor dto)
	{
	}
	
	public PuestoSensor[] findByDynamicSelect(String sql, Object[] sqlParams) throws PuestoSensorDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new PuestoSensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public PuestoSensor[] findByDynamicWhere(String sql, Object[] sqlParams) throws PuestoSensorDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new PuestoSensorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

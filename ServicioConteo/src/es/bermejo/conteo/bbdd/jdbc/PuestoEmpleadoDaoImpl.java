package es.bermejo.conteo.bbdd.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import es.bermejo.conteo.bbdd.dao.PuestoEmpleadoDao;
import es.bermejo.conteo.bbdd.dto.PuestoEmpleado;
import es.bermejo.conteo.bbdd.dto.PuestoEmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.PuestoEmpleadoDaoException;
import es.bermejo.conteo.bbdd.jdbc.AbstractDAO;
import es.bermejo.conteo.bbdd.jdbc.ResourceManager;
import es.bermejo.conteo.core.Constantes;

public class PuestoEmpleadoDaoImpl extends AbstractDAO implements PuestoEmpleadoDao{
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT `Puesto_idPuesto`, `empleados_idempleados`, `dia` FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " (`Puesto_idPuesto`, `empleados_idempleados`, `dia`) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET `Puesto_idPuesto` = ?, `empleados_idempleados` = ?, dia = ? WHERE `Puesto_idPuesto` = ? and `empleados_idempleados` = ? and dia = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE `Puesto_idPuesto` = ? and `empleados_idempleados` = ? and dia = ?";

	/** 
	 * Index of column idPuesto
	 */
	protected static final int COLUMN_ID_PUESTO = 1;

	/** 
	 * Index of column idEmpleado
	 */
	protected static final int COLUMN_ID_EMPLEADO = 2;

	/** 
	 * Index of column dia
	 */
	protected static final int COLUMN_DIA = 3;

	public PuestoEmpleadoPk insert(PuestoEmpleado dto) throws PuestoEmpleadoDaoException {

		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdPuesto() );
			stmt.setInt( index++, dto.getIdEmpleado() );
			stmt.setTimestamp(index++, dto.getDia()==null ? null : new java.sql.Timestamp( dto.getDia().getTime() ) );
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdPuesto( rs.getInt( COLUMN_ID_PUESTO ) );
				dto.setIdEmpleado( rs.getInt( COLUMN_ID_EMPLEADO ) );
				dto.setDia( rs.getTimestamp( COLUMN_DIA ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			throw new PuestoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void update(PuestoEmpleadoPk pk, PuestoEmpleado dto) throws PuestoEmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdPuesto() );
			stmt.setInt( index++, dto.getIdEmpleado() );
			stmt.setTimestamp(index++, dto.getDia()==null ? null : new java.sql.Timestamp( dto.getDia().getTime() ) );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new PuestoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	public void delete(PuestoEmpleadoPk pk) throws PuestoEmpleadoDaoException {
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdPuesto() );
			stmt.setInt( 2, pk.getidEmpleado());
			stmt.setTimestamp(3, pk.getDia()==null ? null : new java.sql.Timestamp( pk.getDia().getTime() ) );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
		
		}
		catch (Exception _e) {
			throw new PuestoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public PuestoEmpleado findByPrimaryKey(PuestoEmpleadoPk pk) throws PuestoEmpleadoDaoException {
		return findByPrimaryKey( pk.getIdPuesto(), pk.getidEmpleado(), pk.getDia() );
	}

	public PuestoEmpleado findByPrimaryKey(int idPuesto, int idEmpleado, Date dia) throws PuestoEmpleadoDaoException {
		PuestoEmpleado ret[] = findByDynamicSelect( SQL_SELECT + " WHERE `Puesto_idPuesto` = ? and `empleados_idempleados` = ? and dia = ?", new Object[] {  new Integer(idPuesto), new Integer(idEmpleado), dia } );
		return ret.length==0 ? null : ret[0];
	}

	public PuestoEmpleado[] findAll() throws PuestoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " ORDER BY dia", null );
	}

	public PuestoEmpleado[] findWhereIdPuestoEquals(int idPuesto) throws PuestoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE idPuesto = ? ORDER BY Puesto_idPuesto", new Object[] {  new Integer(idPuesto) } );
	}

	public PuestoEmpleado[] findWhereIdEmpleadoEquals(int idEmpleado) throws PuestoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE empleados_idempleados = ? ORDER BY empleados_idempleados", new Object[] {  new Integer(idEmpleado) } );
	}

	public PuestoEmpleado[] findWhereDiaEquals(Date dia) throws PuestoEmpleadoDaoException {
		return findByDynamicSelect( SQL_SELECT + " WHERE dia = ? ORDER BY Puesto_idPuesto", new Object[] {  dia } );
	}
	
	/**
	 * Method 'PuestoEmpleadoDaoImpl'
	 * 
	 */
	public PuestoEmpleadoDaoImpl()
	{
	}

	/**
	 * Method 'PuestoEmpleadoDaoImpl'
	 * 
	 * @param userConn
	 */
	public PuestoEmpleadoDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".puesto_empleados";
	}
	/** 
	 * Fetches a single row from the result set
	 */
	protected PuestoEmpleado fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			PuestoEmpleado dto = new PuestoEmpleado();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected PuestoEmpleado[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			PuestoEmpleado dto = new PuestoEmpleado();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		PuestoEmpleado ret[] = new PuestoEmpleado[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(PuestoEmpleado dto, ResultSet rs) throws SQLException
	{
		dto.setIdPuesto( rs.getInt( COLUMN_ID_PUESTO) );
		dto.setIdEmpleado( rs.getInt( COLUMN_ID_EMPLEADO) );
		dto.setDia( rs.getTimestamp(COLUMN_DIA) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(PuestoEmpleado dto)
	{
	}
	
	public PuestoEmpleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws PuestoEmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new PuestoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

	public PuestoEmpleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws PuestoEmpleadoDaoException {
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			throw new PuestoEmpleadoDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
	}

}

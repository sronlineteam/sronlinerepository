package es.bermejo.conteo.bbdd;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import es.bermejo.conteo.bbdd.dto.CuentasDia;
import es.bermejo.conteo.core.Constantes;

public class ConexionBBDD {
	BasicDataSource basicDataSource = null;
	Connection conexion = null;
    private static final Logger log = LogManager.getLogger(ConexionBBDD.class.getName());

	public ConexionBBDD() {
        	basicDataSource = new BasicDataSource();
        	basicDataSource.setDriverClassName(Constantes.getDriverBBDD());
        	basicDataSource.setUsername(Constantes.getUsuarioBBDD());
        	basicDataSource.setPassword(Constantes.getPasswordBBDD());
        	basicDataSource.setUrl(Constantes.getConexionBBDD());
	}
	public List<CuentasDia> consultarDia(String dia) {
		
		List<CuentasDia> cuentasDia = new ArrayList<CuentasDia>();

		StringBuffer sqlSB = new StringBuffer("SELECT sp.Puesto_idPuesto as puesto, nombre, count(cuando) as cuenta");
		sqlSB.append(" FROM conteo.cuenta c left join conteo.sensor_puesto sp on c.id_sensor = sp.sensores_idsensores ");
		sqlSB.append(" left join conteo.puesto_empleados pe on sp.Puesto_idPuesto = pe.Puesto_idPuesto ");
		sqlSB.append(" left join conteo.empleados e on pe.empleados_idEmpleados = e.idempleado ");
		sqlSB.append(" where date(cuando) = date('").append(dia).append("')"); 
		sqlSB.append(" and sp.desde <= date('").append(dia).append("')");
		sqlSB.append(" and (sp.hasta >= date('").append(dia).append("') or sp.hasta is null)");
		sqlSB.append(" and date(pe.dia) = date('").append(dia).append("')");
		sqlSB.append(" group by id_sensor ");
		sqlSB.append(" order by puesto;");
		
		try {
			conexion = basicDataSource.getConnection();
			
			Statement s = conexion.createStatement();
			ResultSet rs= null;
			CuentasDia auxCuentaDias;
			
			rs = s.executeQuery(sqlSB.toString());
			while (rs.next()){
				auxCuentaDias = new CuentasDia();
				auxCuentaDias.setIdPuesto(rs.getInt(1));
				auxCuentaDias.setPersona(rs.getString(2));
				auxCuentaDias.setCuenta(rs.getInt(3));
				cuentasDia.add(auxCuentaDias);
			}
		    
		    log.debug("consultado dia " + dia);
		   

	    }catch (SQLException e) {
	    	// TODO Auto-generated catch block
	    	log.error(e.toString());
	    }		
		finally {
			   try {
			      if (null != conexion) {
			         conexion.close();
			      }
			   } catch (SQLException e) {
			      e.printStackTrace();
			}
		}
		return cuentasDia;
	}	
	public boolean insertarCuenta(String dirMac){

		String sql = "INSERT INTO "+ Constantes.getNombreBBDD()+".cuenta (`id_sensor`,`cuando`) "
		  					+ "VALUES ('"+dirMac+"', current_timestamp(6) )";
	    boolean resul = false;
	    
		try {
			conexion = basicDataSource.getConnection();

			Statement s = conexion.createStatement();
		    s.executeUpdate (sql);
		    log.debug("guardado " + dirMac);
		   
		    resul = true;
	    }catch (SQLException e) {
	    	// TODO Auto-generated catch block
	    	log.error(e.toString());
	    }
		finally {
			   try {
			      if (null != conexion) {
			         conexion.close();
			      }
			   } catch (SQLException e) {
			      e.printStackTrace();
			}
		}
    	return resul;
	}
}

package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;


/** 
 * This class represents the primary key of the puestos table.
 */
public class PuestoPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int idPuesto;

	/** 
	 * This attribute represents whether the primitive attribute idpuesto is null.
	 */
	protected boolean idPuestoNull;

	/** 
	 * Sets the value of idpuesto
	 */
	public void setIdPuesto(int idpuesto)
	{
		this.idPuesto = idpuesto;
	}

	/** 
	 * Gets the value of idpuesto
	 */
	public int getIdPuesto()
	{
		return idPuesto;
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 */
	public PuestoPk()
	{
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 * @param idpuesto
	 */
	public PuestoPk(final int idpuesto)
	{
		this.idPuesto = idpuesto;
	}

	/** 
	 * Sets the value of idpuestoNull
	 */
	public void setidpuestoNull(boolean idpuestoNull)
	{
		this.idPuestoNull = idpuestoNull;
	}

	/** 
	 * Gets the value of idpuestoNull
	 */
	public boolean isidpuestoNull()
	{
		return idPuestoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PuestoPk)) {
			return false;
		}
		
		final PuestoPk _cast = (PuestoPk) _other;
		if (idPuesto != _cast.idPuesto) {
			return false;
		}
		
		if (idPuestoNull != _cast.idPuestoNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idPuesto;
		_hashCode = 29 * _hashCode + (idPuestoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoPk: " );
		ret.append( "idpuesto=" + idPuesto );
		return ret.toString();
	}

}

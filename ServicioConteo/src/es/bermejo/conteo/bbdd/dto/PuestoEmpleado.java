package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;

public class PuestoEmpleado implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idpuesto in the Puestos table.
	 */
	protected int idPuesto;
	protected int idEmpleado;
	protected Date dia;

	/**
	 * Method 'Puesto'
	 * 
	 */
	public PuestoEmpleado()
	{
	}


	public int getIdPuesto() {
		return idPuesto;
	}


	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}


	public int getIdEmpleado() {
		return idEmpleado;
	}


	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}


	public Date getDia() {
		return dia;
	}


	public void setDia(Date dia) {
		this.dia = dia;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PuestoEmpleado)) {
			return false;
		}
		
		final PuestoEmpleado _cast = (PuestoEmpleado) _other;
		if (idPuesto != _cast.idPuesto) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idPuesto;

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return PuestosPk
	 */
	public PuestoEmpleadoPk createPk()
	{
		return new PuestoEmpleadoPk(idPuesto, idEmpleado, dia);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoEmpleadoPk: " );
		ret.append( "idpuesto=" + idPuesto );
		ret.append( "idEmpleado=" + idEmpleado );
		ret.append( "dia=" + dia );
		return ret.toString();
	}

}

package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;

public class Puesto implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 
	 * This attribute maps to the column idpuesto in the Puestos table.
	 */
	protected int idpuesto;

	/** 
	 * This attribute maps to the column idpuesto in the Puestos table.
	 */
	protected String puesto;

	/**
	 * Method 'Puesto'
	 * 
	 */
	public Puesto()
	{
	}


	public int getIdPuesto() {
		return idpuesto;
	}


	public void setIdPuesto(int idpuesto) {
		this.idpuesto = idpuesto;
	}


	public String getPuesto() {
		return puesto;
	}


	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Puesto)) {
			return false;
		}
		
		final Puesto _cast = (Puesto) _other;
		if (idpuesto != _cast.idpuesto) {
			return false;
		}
		
		if (puesto == null ? _cast.puesto != puesto : !puesto.equals( _cast.puesto )) {
			return false;
		}
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idpuesto;
		if (puesto != null) {
			_hashCode = 29 * _hashCode + puesto.hashCode();
		}

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return PuestosPk
	 */
	public PuestoPk createPk()
	{
		return new PuestoPk(idpuesto);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.Puesto: " );
		ret.append( "idpuesto=" + idpuesto );
		ret.append( ", puesto=" + puesto );
		return ret.toString();
	}

}

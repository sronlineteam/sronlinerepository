package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;


/** 
 * This class represents the primary key of the empleados table.
 */
public class EmpleadoPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int idEmpleado;

	/** 
	 * This attribute represents whether the primitive attribute idempleado is null.
	 */
	protected boolean idEmpleadoNull;

	/** 
	 * Sets the value of idempleado
	 */
	public void setIdEmpleado(int idempleado)
	{
		this.idEmpleado = idempleado;
	}

	/** 
	 * Gets the value of idempleado
	 */
	public int getIdEmpleado()
	{
		return idEmpleado;
	}

	/**
	 * Method 'EmpleadoPk'
	 * 
	 */
	public EmpleadoPk()
	{
	}

	/**
	 * Method 'EmpleadoPk'
	 * 
	 * @param idempleado
	 */
	public EmpleadoPk(final int idempleado)
	{
		this.idEmpleado = idempleado;
	}

	/** 
	 * Sets the value of idempleadoNull
	 */
	public void setidempleadoNull(boolean idempleadoNull)
	{
		this.idEmpleadoNull = idempleadoNull;
	}

	/** 
	 * Gets the value of idempleadoNull
	 */
	public boolean isidempleadoNull()
	{
		return idEmpleadoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof EmpleadoPk)) {
			return false;
		}
		
		final EmpleadoPk _cast = (EmpleadoPk) _other;
		if (idEmpleado != _cast.idEmpleado) {
			return false;
		}
		
		if (idEmpleadoNull != _cast.idEmpleadoNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idEmpleado;
		_hashCode = 29 * _hashCode + (idEmpleadoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.EmpleadoPk: " );
		ret.append( "idempleado=" + idEmpleado );
		return ret.toString();
	}

}

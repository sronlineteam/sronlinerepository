package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;

public class PuestoSensor implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idpuesto in the Puestos table.
	 */
	protected int idPuesto;
	protected int idSensor;
	protected Date desde;
	protected Date hasta;

	/**
	 * Method 'Puesto'
	 * 
	 */
	public PuestoSensor()
	{
	}


	public int getIdPuesto() {
		return idPuesto;
	}


	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}


	public int getIdSensor() {
		return idSensor;
	}


	public void setIdSensor(int idSensor) {
		this.idSensor = idSensor;
	}


	public Date getDesde() {
		return desde;
	}


	public void setDesde(Date desde) {
		this.desde = desde;
	}


	public Date getHasta() {
		return hasta;
	}


	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PuestoSensor)) {
			return false;
		}
		
		final PuestoSensor _cast = (PuestoSensor) _other;
		if (idPuesto != _cast.idPuesto) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idPuesto;

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return PuestosPk
	 */
	public PuestoSensorPk createPk()
	{
		return new PuestoSensorPk(idPuesto, idSensor, desde, hasta);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoSensorPk: " );
		ret.append( "idpuesto=" + idPuesto );
		ret.append( "idSensor=" + idSensor );
		ret.append( "desde=" + desde );
		ret.append( "hasta=" + hasta );
		return ret.toString();
	}

}

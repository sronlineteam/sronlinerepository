package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;

public class RepartoEmpleado implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idReparto in the `repartos_empleados` table.
	 */
	protected int idReparto;
	protected int idEmpleado;
	protected String empleado;
	protected int peso;

	/**
	 * Method 'Reparto'
	 * 
	 */
	public RepartoEmpleado()
	{
	}


	public int getIdReparto() {
		return idReparto;
	}


	public void setIdReparto(int idReparto) {
		this.idReparto = idReparto;
	}


	public int getIdEmpleado() {
		return idEmpleado;
	}


	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}



	public String getEmpleado() {
		return empleado;
	}


	public void setEmpleado(String empleado) {
		this.empleado = empleado;
	}


	public int getPeso() {
		return peso;
	}


	public void setPeso(int peso) {
		this.peso = peso;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof RepartoEmpleado)) {
			return false;
		}
		
		final RepartoEmpleado _cast = (RepartoEmpleado) _other;
		if (idReparto != _cast.idReparto) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idReparto;

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return RepartosPk
	 */
	public RepartoEmpleadoPk createPk()
	{
		return new RepartoEmpleadoPk(idReparto, idEmpleado);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk: " );
		ret.append( "idReparto=" + idReparto );
		ret.append( "idEmpleado=" + idEmpleado );
		ret.append( "peso=" + peso );
		return ret.toString();
	}

}

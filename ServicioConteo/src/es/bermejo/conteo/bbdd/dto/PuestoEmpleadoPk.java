package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;


/** 
 * This class represents the primary key of the puestos table.
 */
public class PuestoEmpleadoPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int idPuesto;
	protected int idEmpleado;
	protected Date dia;

	/** 
	 * This attribute represents whether the primitive attribute idpuesto is null.
	 */
	protected boolean idPuestoNull;
	protected boolean idEmpleadoNull;
	protected boolean diaNull;

	/** 
	 * Sets the value of idpuesto
	 */
	public void setIdPuesto(int idPuesto)
	{
		this.idPuesto = idPuesto;
	}

	/** 
	 * Gets the value of idpuesto
	 */
	public int getIdPuesto()
	{
		return idPuesto;
	}

	/** 
	 * Sets the value of idEmpleado
	 */
	public void setidEmpleado(int idEmpleado)
	{
		this.idEmpleado = idEmpleado;
	}

	/** 
	 * Gets the value of idEmpleado
	 */
	public int getidEmpleado()
	{
		return idEmpleado;
	}

	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 */
	public PuestoEmpleadoPk()
	{
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 * @param idpuesto
	 */
	public PuestoEmpleadoPk(final int idPuesto, final int idEmpleado, final Date dia)
	{
		this.idPuesto = idPuesto;
		this.idEmpleado = idEmpleado;
		this.dia = dia;
	}

	/** 
	 * Sets the value of idpuestoNull
	 */
	public void setidpuestoNull(boolean idpuestoNull)
	{
		this.idPuestoNull = idpuestoNull;
	}

	/** 
	 * Gets the value of idpuestoNull
	 */
	public boolean isidpuestoNull()
	{
		return idPuestoNull;
	}

	/** 
	 * Sets the value of idpuestoNull
	 */
	public void setidEmpleadoNull(boolean idEmpleadoNull)
	{
		this.idEmpleadoNull = idEmpleadoNull;
	}

	/** 
	 * Gets the value of idpuestoNull
	 */
	public boolean isidEmpleadoNull()
	{
		return idEmpleadoNull;
	}

	public boolean isDiaNull() {
		return diaNull;
	}

	public void setDiaNull(boolean diaNull) {
		this.diaNull = diaNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PuestoEmpleadoPk)) {
			return false;
		}
		
		final PuestoEmpleadoPk _cast = (PuestoEmpleadoPk) _other;
		if (idPuesto != _cast.idPuesto) {
			return false;
		}
		if (idEmpleado != _cast.idEmpleado) {
			return false;
		}
		
		if (idPuestoNull != _cast.idPuestoNull) {
			return false;
		}

		if (idEmpleadoNull != _cast.idEmpleadoNull) {
			return false;
		}

		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idPuesto;
		_hashCode = 29 * _hashCode + (idPuestoNull ? 1 : 0);
		_hashCode = 29 * _hashCode + idEmpleado;
		_hashCode = 29 * _hashCode + (idEmpleadoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoEmpleadoPk: " );
		ret.append( "idpuesto=" + idPuesto );
		ret.append( "idEmpleado=" + idEmpleado );
		ret.append( "dia=" + dia );
		return ret.toString();
	}

}

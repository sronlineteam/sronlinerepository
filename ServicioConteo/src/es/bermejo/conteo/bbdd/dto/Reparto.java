package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;

public class Reparto implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the columns in the Puestos table.
	 */
	protected int idReparto;
	protected String descripcion;
	protected Date desde;
	protected Date hasta;

	/**
	 * Method 'Puesto'
	 * 
	 */
	public Reparto()
	{
	}


	public int getIdReparto() {
		return idReparto;
	}


	public void setIdReparto(int idReparto) {
		this.idReparto = idReparto;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Date getDesde() {
		return desde;
	}


	public void setDesde(Date desde) {
		this.desde = desde;
	}


	public Date getHasta() {
		return hasta;
	}


	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Reparto)) {
			return false;
		}
		
		final Reparto _cast = (Reparto) _other;
		if (idReparto != _cast.idReparto) {
			return false;
		}
		
		if (descripcion == null ? _cast.descripcion!= descripcion : !descripcion.equals( _cast.descripcion )) {
			return false;
		}
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idReparto;
		if (descripcion != null) {
			_hashCode = 29 * _hashCode + descripcion.hashCode();
		}

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return PuestosPk
	 */
	public RepartoPk createPk()
	{
		return new RepartoPk(idReparto);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.Puesto: " );
		ret.append( "idReparto=" + idReparto );
		ret.append( ", descripcion=" + descripcion);
		ret.append( "desde=" + desde );
		ret.append( "hasta=" + hasta );
		return ret.toString();
	}

}

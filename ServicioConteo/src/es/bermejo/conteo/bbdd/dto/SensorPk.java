package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;


/** 
 * This class represents the primary key of the Sensors table.
 */
public class SensorPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String idSensor;

	/** 
	 * This attribute represents whether the primitive attribute idSensor is null.
	 */
	protected boolean idSensorNull;

	/** 
	 * Sets the value of idSensor
	 */
	public void setIdPuesto(String idSensor)
	{
		this.idSensor = idSensor;
	}

	/** 
	 * Gets the value of idSensor
	 */
	public String getIdSensor()
	{
		return idSensor;
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 */
	public SensorPk()
	{
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 * @param idsensor
	 */
	public SensorPk(final String idSensor)
	{
		this.idSensor = idSensor;
	}

	/** 
	 * Sets the value of idsensorNull
	 */
	public void setidSensorNull(boolean idSensorNull)
	{
		this.idSensorNull = idSensorNull;
	}

	/** 
	 * Gets the value of idSensorNull
	 */
	public boolean isidSensorNull()
	{
		return idSensorNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof SensorPk)) {
			return false;
		}
		
		final SensorPk _cast = (SensorPk) _other;
		if (idSensor != _cast.idSensor) {
			return false;
		}
		
		if (idSensorNull != _cast.idSensorNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (idSensor != null) {
			_hashCode = 29 * _hashCode + idSensor.hashCode();
		}
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoPk: " );
		ret.append( "idSensor=" + idSensor );
		return ret.toString();
	}

}

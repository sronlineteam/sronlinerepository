package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;

public class Sensor implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idsensor in the Sensors table.
	 */
	protected String idSensor;

	/**
	 * Method 'Sensor'
	 * 
	 */
	public Sensor()
	{
	}


	public String getIdSensor() {
		return idSensor;
	}


	public void setIdSensor(String idSensor) {
		this.idSensor = idSensor;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Sensor)) {
			return false;
		}
		
		final Sensor _cast = (Sensor) _other;
		if (idSensor != _cast.idSensor) {
			return false;
		}
		if (idSensor == null ? _cast.idSensor != idSensor : !idSensor.equals( _cast.idSensor )) {
			return false;
		}
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (idSensor != null) {
			_hashCode = 29 * _hashCode + idSensor.hashCode();
		}

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return SensorsPk
	 */
	public SensorPk createPk()
	{
		return new SensorPk(idSensor);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.Sensor: " );
		ret.append( "idSensor=" + idSensor );
		return ret.toString();
	}

}

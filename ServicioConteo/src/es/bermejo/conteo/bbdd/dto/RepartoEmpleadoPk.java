package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;


/** 
 * This class represents the primary key of the repartos_empleados table.
 */
public class RepartoEmpleadoPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int idReparto;
	protected int idEmpleado;

	/** 
	 * This attribute represents whether the primitive attribute idReparto is null.
	 */
	protected boolean idRepartoNull;
	protected boolean idEmpleadoNull;

	/** 
	 * Sets the value of idReparto
	 */
	public void setIdReparto(int idReparto)
	{
		this.idReparto = idReparto;
	}

	/** 
	 * Gets the value of idReparto
	 */
	public int getIdReparto()
	{
		return idReparto;
	}

	/** 
	 * Sets the value of idEmpleado
	 */
	public void setidEmpleado(int idEmpleado)
	{
		this.idEmpleado = idEmpleado;
	}

	/** 
	 * Gets the value of idEmpleado
	 */
	public int getidEmpleado()
	{
		return idEmpleado;
	}

	/**
	 * Method 'RepartoPk'
	 * 
	 */
	public RepartoEmpleadoPk()
	{
	}

	/**
	 * Method 'RepartoPk'
	 * 
	 * @param idReparto
	 */
	public RepartoEmpleadoPk(final int idReparto, final int idEmpleado)
	{
		this.idReparto = idReparto;
		this.idEmpleado = idEmpleado;
	}

	/** 
	 * Sets the value of idRepartoNull
	 */
	public void setidRepartoNull(boolean idRepartoNull)
	{
		this.idRepartoNull = idRepartoNull;
	}

	/** 
	 * Gets the value of idRepartoNull
	 */
	public boolean isidRepartoNull()
	{
		return idRepartoNull;
	}

	/** 
	 * Sets the value of idRepartoNull
	 */
	public void setidEmpleadoNull(boolean idEmpleadoNull)
	{
		this.idEmpleadoNull = idEmpleadoNull;
	}

	/** 
	 * Gets the value of idRepartoNull
	 */
	public boolean isidEmpleadoNull()
	{
		return idEmpleadoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof RepartoEmpleadoPk)) {
			return false;
		}
		
		final RepartoEmpleadoPk _cast = (RepartoEmpleadoPk) _other;
		if (idReparto != _cast.idReparto) {
			return false;
		}
		if (idEmpleado != _cast.idEmpleado) {
			return false;
		}
		
		if (idRepartoNull != _cast.idRepartoNull) {
			return false;
		}

		if (idEmpleadoNull != _cast.idEmpleadoNull) {
			return false;
		}

		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idReparto;
		_hashCode = 29 * _hashCode + (idRepartoNull ? 1 : 0);
		_hashCode = 29 * _hashCode + idEmpleado;
		_hashCode = 29 * _hashCode + (idEmpleadoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk: " );
		ret.append( "idReparto=" + idReparto );
		ret.append( "idEmpleado=" + idEmpleado );
		return ret.toString();
	}

}

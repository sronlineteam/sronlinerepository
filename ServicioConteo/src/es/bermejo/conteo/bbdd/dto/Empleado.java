package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;

public class Empleado implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 
	 * This attribute maps to the column idempleado in the Empleados table.
	 */
	protected int idempleado;

	/** 
	 * This attribute maps to the column idempleado in the Empleados table.
	 */
	protected String nombre;

	/** 
	 * This attribute maps to the column alta in the Empleados table.
	 */
	protected Date alta;

	/** 
	 * This attribute maps to the column baja in the Empleados table.
	 */
	protected Date baja;
	
	/** 
	 * This attribute maps to the column funcion in the Empleados table.
	 */
	protected int funcion;
	
		
	/**
	 * Method 'Empleado'
	 * 
	 */
	public Empleado()
	{
	}


	public int getIdEmpleado() {
		return idempleado;
	}


	public void setIdEmpleado(int idempleado) {
		this.idempleado = idempleado;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Date getAlta() {
		return alta;
	}


	public void setAlta(Date alta) {
		this.alta = alta;
	}


	public Date getBaja() {
		return baja;
	}


	public void setBaja(Date baja) {
		this.baja = baja;
	}


	public int getFuncion() {
		return funcion;
	}


	public void setFuncion(int funcion) {
		this.funcion = funcion;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Empleado)) {
			return false;
		}
		
		final Empleado _cast = (Empleado) _other;
		if (idempleado != _cast.idempleado) {
			return false;
		}
		
		if (nombre == null ? _cast.nombre != nombre : !nombre.equals( _cast.nombre )) {
			return false;
		}
				
		if (alta == null ? _cast.alta != alta : !alta.equals( _cast.alta )) {
			return false;
		}
		
		if (baja == null ? _cast.baja != baja : !baja.equals( _cast.baja )) {
			return false;
		}
		
		if (funcion != _cast.funcion) {
			return false;
		}


		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idempleado;
		if (nombre != null) {
			_hashCode = 29 * _hashCode + nombre.hashCode();
		}
		if (alta != null) {
			_hashCode = 29 * _hashCode + alta.hashCode();
		}
		if (baja != null) {
			_hashCode = 29 * _hashCode + baja.hashCode();
		}
		_hashCode = 29 * _hashCode + funcion;

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return EmpleadosPk
	 */
	public EmpleadoPk createPk()
	{
		return new EmpleadoPk(idempleado);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.Empleado: " );
		ret.append( "idempleado=" + idempleado );
		ret.append( ", nombre=" + nombre );
		ret.append( ", alta=" + alta );
		ret.append( ", baja=" + baja );
		ret.append( ", funcion=" + funcion );
		return ret.toString();
	}

}

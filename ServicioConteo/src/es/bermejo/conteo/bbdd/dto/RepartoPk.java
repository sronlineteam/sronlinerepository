package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;


/** 
 * This class represents the primary key of the Sensors table.
 */
public class RepartoPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int idReparto;

	/** 
	 * This attribute represents whether the primitive attribute idReparto is null.
	 */
	protected boolean idRepartoNull;

	/** 
	 * Sets the value of idReparto
	 */
	public void setIdReparto(int idReparto)
	{
		this.idReparto = idReparto;
	}

	/** 
	 * Gets the value of idReparto
	 */
	public int getIdReparto()
	{
		return idReparto;
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 */
	public RepartoPk()
	{
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 * @param idReparto
	 */
	public RepartoPk(final int idReparto)
	{
		this.idReparto = idReparto;
	}

	/** 
	 * Sets the value of idRepartoNull
	 */
	public void setIdRepartoNull(boolean idRepartoNull)
	{
		this.idRepartoNull = idRepartoNull;
	}

	/** 
	 * Gets the value of idRepartoNull
	 */
	public boolean isIdRepartoNull()
	{
		return idRepartoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof RepartoPk)) {
			return false;
		}
		
		final RepartoPk _cast = (RepartoPk) _other;
		if (idReparto != _cast.idReparto) {
			return false;
		}
		
		if (idRepartoNull != _cast.idRepartoNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idReparto;
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.RepartoPk: " );
		ret.append( "idReparto=" + idReparto );
		return ret.toString();
	}

}

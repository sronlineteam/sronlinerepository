package es.bermejo.conteo.bbdd.dto;

import java.io.Serializable;
import java.util.Date;


/** 
 * This class represents the primary key of the puestos table.
 */
public class PuestoSensorPk implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int idPuesto;
	protected int idSensor;
	protected Date desde;
	protected Date hasta;

	/** 
	 * This attribute represents whether the primitive attribute idpuesto is null.
	 */
	protected boolean idPuestoNull;
	protected boolean idSensorNull;

	/** 
	 * Sets the value of idpuesto
	 */
	public void setIdPuesto(int idPuesto)
	{
		this.idPuesto = idPuesto;
	}

	/** 
	 * Gets the value of idpuesto
	 */
	public int getIdPuesto()
	{
		return idPuesto;
	}

	/** 
	 * Sets the value of idSensor
	 */
	public void setIdSensor(int idSensor)
	{
		this.idSensor = idSensor;
	}

	/** 
	 * Gets the value of idSensor
	 */
	public int getIdSensor()
	{
		return idSensor;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 */
	public PuestoSensorPk()
	{
	}

	/**
	 * Method 'PuestoPk'
	 * 
	 * @param idpuesto
	 */
	public PuestoSensorPk(final int idPuesto, final int idSensor, final Date desde, final Date hasta)
	{
		this.idPuesto = idPuesto;
		this.idSensor = idSensor;
		this.desde = desde;
		this.hasta = hasta;
	}

	/** 
	 * Sets the value of idpuestoNull
	 */
	public void setIdpuestoNull(boolean idpuestoNull)
	{
		this.idPuestoNull = idpuestoNull;
	}

	/** 
	 * Gets the value of idpuestoNull
	 */
	public boolean isIdpuestoNull()
	{
		return idPuestoNull;
	}

	/** 
	 * Sets the value of idpuestoNull
	 */
	public void setIdSensorNull(boolean idSensorNull)
	{
		this.idSensorNull = idSensorNull;
	}

	/** 
	 * Gets the value of idpuestoNull
	 */
	public boolean isIdSensorNull()
	{
		return idSensorNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PuestoSensorPk)) {
			return false;
		}
		
		final PuestoSensorPk _cast = (PuestoSensorPk) _other;
		if (idPuesto != _cast.idPuesto) {
			return false;
		}
		if (idSensor != _cast.idSensor) {
			return false;
		}
		
		if (idPuestoNull != _cast.idPuestoNull) {
			return false;
		}

		if (idSensorNull != _cast.idSensorNull) {
			return false;
		}

		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idPuesto;
		_hashCode = 29 * _hashCode + (idPuestoNull ? 1 : 0);
		_hashCode = 29 * _hashCode + idSensor;
		_hashCode = 29 * _hashCode + (idSensorNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.bermejo.conteo.bbdd.dto.PuestoSensorPk: " );
		ret.append( "idpuesto=" + idPuesto );
		ret.append( "idSensor=" + idSensor );
		ret.append( "desde=" + desde );
		ret.append( "hasta=" + hasta );
		return ret.toString();
	}

}

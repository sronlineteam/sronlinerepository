package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.SensorDao;
import es.bermejo.conteo.bbdd.jdbc.SensorDaoImpl;

public class SensorDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static SensorDao create()
	{
		return new SensorDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static SensorDao create(Connection conn)
	{
		return new SensorDaoImpl( conn );
	}


}

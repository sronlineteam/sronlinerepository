package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.PuestoSensorDao;
import es.bermejo.conteo.bbdd.jdbc.PuestoSensorDaoImpl;

public class PuestoSensorDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static PuestoSensorDao create()
	{
		return new PuestoSensorDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static PuestoSensorDao create(Connection conn)
	{
		return new PuestoSensorDaoImpl( conn );
	}


}

package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.EmpleadoDao;
import es.bermejo.conteo.bbdd.jdbc.EmpleadoDaoImpl;

public class EmpleadoDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return EmpleadoDao
	 */
	public static EmpleadoDao create()
	{
		return new EmpleadoDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static EmpleadoDao create(Connection conn)
	{
		return new EmpleadoDaoImpl( conn );
	}


}

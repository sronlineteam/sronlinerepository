package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.PuestoDao;
import es.bermejo.conteo.bbdd.jdbc.PuestoDaoImpl;

public class PuestoDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static PuestoDao create()
	{
		return new PuestoDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static PuestoDao create(Connection conn)
	{
		return new PuestoDaoImpl( conn );
	}


}

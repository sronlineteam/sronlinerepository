package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.RepartoEmpleadoDao;
import es.bermejo.conteo.bbdd.jdbc.RepartoEmpleadoDaoImpl;

public class RepartoEmpleadoDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static RepartoEmpleadoDao create()
	{
		return new RepartoEmpleadoDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static RepartoEmpleadoDao create(Connection conn)
	{
		return new RepartoEmpleadoDaoImpl( conn );
	}


}

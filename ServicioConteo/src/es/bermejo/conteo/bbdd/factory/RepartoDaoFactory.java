package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.RepartoDao;
import es.bermejo.conteo.bbdd.jdbc.RepartoDaoImpl;

public class RepartoDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return RepartoDao
	 */
	public static RepartoDao create()
	{
		return new RepartoDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static RepartoDao create(Connection conn)
	{
		return new RepartoDaoImpl( conn );
	}


}

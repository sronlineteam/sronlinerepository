package es.bermejo.conteo.bbdd.factory;

import java.sql.Connection;

import es.bermejo.conteo.bbdd.dao.PuestoEmpleadoDao;
import es.bermejo.conteo.bbdd.jdbc.PuestoEmpleadoDaoImpl;

public class PuestoEmpleadoDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static PuestoEmpleadoDao create()
	{
		return new PuestoEmpleadoDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static PuestoEmpleadoDao create(Connection conn)
	{
		return new PuestoEmpleadoDaoImpl( conn );
	}


}

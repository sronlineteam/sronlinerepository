package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.Reparto;
import es.bermejo.conteo.bbdd.dto.RepartoPk;
import es.bermejo.conteo.bbdd.exceptions.RepartoDaoException;
import java.util.Date;

public interface RepartoDao {
	/** 
	 * Inserts a new row in the Repartos table.
	 */
	public RepartoPk insert(Reparto dto) throws RepartoDaoException;

	/** 
	 * Updates a single row in the Repartos table.
	 */
	public void update(RepartoPk pk, Reparto dto) throws RepartoDaoException;

	/** 
	 * Deletes a single row in the Repartos table.
	 */
	public void delete(RepartoPk pk) throws RepartoDaoException;

	/** 
	 * Returns the rows from the Repartos table that matches the specified primary-key value.
	 */
	public Reparto findByPrimaryKey(RepartoPk pk) throws RepartoDaoException;

	/** 
	 * Returns all rows from the Repartos table that match the criteria 'IdReparto = :IdReparto'.
	 */
	public Reparto findByPrimaryKey(int IdReparto) throws RepartoDaoException;

	/** 
	 * Returns all rows from the Puestos table that match the criteria ''.
	 */
	public Reparto[] findAll() throws RepartoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'IdReparto = :IdReparto'.
	 */
	public Reparto[] findWhereIdRepartoEquals(int IdReparto) throws RepartoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'IdReparto = :IdReparto'.
	 */
	public Reparto[] findWhereDesdeEquals(Date dia) throws RepartoDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public Reparto[] findByDynamicSelect(String sql, Object[] sqlParams) throws RepartoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public Reparto[] findByDynamicWhere(String sql, Object[] sqlParams) throws RepartoDaoException;


}

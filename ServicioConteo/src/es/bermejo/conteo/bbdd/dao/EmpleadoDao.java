package es.bermejo.conteo.bbdd.dao;

import java.util.Date;

import es.bermejo.conteo.bbdd.dto.Empleado;
import es.bermejo.conteo.bbdd.dto.EmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.EmpleadoDaoException;

public interface EmpleadoDao {
	/** 
	 * Inserts a new row in the Empleado table.
	 */
	public EmpleadoPk insert(Empleado dto) throws EmpleadoDaoException;

	/** 
	 * Updates a single row in the Empleado table.
	 */
	public void update(EmpleadoPk pk, Empleado dto) throws EmpleadoDaoException;

	/** 
	 * Deletes a single row in the Empleado table.
	 */
	public void delete(EmpleadoPk pk) throws EmpleadoDaoException;

	/** 
	 * Returns the rows from the Empleado table that matches the specified primary-key value.
	 */
	public Empleado findByPrimaryKey(EmpleadoPk pk) throws EmpleadoDaoException;

	/** 
	 * Returns all rows from the Empleado table that match the criteria 'idEmpleado = :idEmpleado'.
	 */
	public Empleado findByPrimaryKey(int idEmpleado) throws EmpleadoDaoException;

	/** 
	 * Returns all rows from the Empleado table that match the criteria ''.
	 */
	public Empleado[] findAll() throws EmpleadoDaoException;

	/** 
	 * Returns all rows from the Empleado table that match the criteria 'idEmpleado = :idEmpleado'.
	 */
	public Empleado[] findWhereIdEmpleadoEquals(int idEmpleado) throws EmpleadoDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Empleado table that match the specified arbitrary SQL statement
	 */
	public Empleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws EmpleadoDaoException;

	/** 
	 * Returns all rows from the Empleado table that match the specified arbitrary SQL statement
	 */
	public Empleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws EmpleadoDaoException;


}

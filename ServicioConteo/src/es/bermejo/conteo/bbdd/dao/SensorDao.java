package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.Sensor;
import es.bermejo.conteo.bbdd.dto.SensorPk;
import es.bermejo.conteo.bbdd.exceptions.SensorDaoException;

public interface SensorDao {
	/** 
	 * Inserts a new row in the Sensor table.
	 */
	public SensorPk insert(Sensor dto) throws SensorDaoException;

	/** 
	 * Updates a single row in the Sensor table.
	 */
	public void update(SensorPk pk, Sensor dto) throws SensorDaoException;

	/** 
	 * Deletes a single row in the Sensor table.
	 */
	public void delete(SensorPk pk) throws SensorDaoException;

	/** 
	 * Returns the rows from the Sensor table that matches the specified primary-key value.
	 */
	public Sensor findByPrimaryKey(SensorPk pk) throws SensorDaoException;

	/** 
	 * Returns all rows from the Sensor table that match the criteria 'idSensor = :idSensor'.
	 */
	public Sensor findByPrimaryKey(String idSensor) throws SensorDaoException;

	/** 
	 * Returns all rows from the Sensor table that match the criteria ''.
	 */
	public Sensor[] findAll() throws SensorDaoException;

	/** 
	 * Returns all rows from the Sensor table that match the criteria 'idSensor = :idSensor'.
	 */
	public Sensor[] findWhereIdSensorEquals(String idSensor) throws SensorDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Sensor table that match the specified arbitrary SQL statement
	 */
	public Sensor[] findByDynamicSelect(String sql, Object[] sqlParams) throws SensorDaoException;

	/** 
	 * Returns all rows from the Sensor table that match the specified arbitrary SQL statement
	 */
	public Sensor[] findByDynamicWhere(String sql, Object[] sqlParams) throws SensorDaoException;


}

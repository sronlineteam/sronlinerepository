package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.PuestoSensor;
import es.bermejo.conteo.bbdd.dto.PuestoSensorPk;
import es.bermejo.conteo.bbdd.exceptions.PuestoSensorDaoException;
import java.util.Date;

public interface PuestoSensorDao {
	/** 
	 * Inserts a new row in the PuestoSensor table.
	 */
	public PuestoSensorPk insert(PuestoSensor dto) throws PuestoSensorDaoException;

	/** 
	 * Updates a single row in the PuestoSensor table.
	 */
	public void update(PuestoSensorPk pk, PuestoSensor dto) throws PuestoSensorDaoException;

	/** 
	 * Deletes a single row in the PuestoSensor table.
	 */
	public void delete(PuestoSensorPk pk) throws PuestoSensorDaoException;

	/** 
	 * Returns the rows from the PuestoSensor table that matches the specified primary-key value.
	 */
	public PuestoSensor findByPrimaryKey(PuestoSensorPk pk) throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the PuestoSensor table that match the criteria 'Puesto_idPuesto = :idPuesto'.
	 */
	public PuestoSensor findByPrimaryKey(int idPuesto, int idSensor, Date desde, Date hasta) throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria ''.
	 */
	public PuestoSensor[] findAll() throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoSensor[] findWhereIdPuestoEquals(int idPuesto) throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoSensor[] findWhereIdSensorEquals(int idSensor) throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoSensor[] findWhereDesdeEquals(Date dia) throws PuestoSensorDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public PuestoSensor[] findByDynamicSelect(String sql, Object[] sqlParams) throws PuestoSensorDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public PuestoSensor[] findByDynamicWhere(String sql, Object[] sqlParams) throws PuestoSensorDaoException;


}

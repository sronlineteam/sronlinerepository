package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.PuestoEmpleado;
import es.bermejo.conteo.bbdd.dto.PuestoEmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.PuestoEmpleadoDaoException;
import java.util.Date;

public interface PuestoEmpleadoDao {
	/** 
	 * Inserts a new row in the PuestoEmpleado table.
	 */
	public PuestoEmpleadoPk insert(PuestoEmpleado dto) throws PuestoEmpleadoDaoException;

	/** 
	 * Updates a single row in the PuestoEmpleado table.
	 */
	public void update(PuestoEmpleadoPk pk, PuestoEmpleado dto) throws PuestoEmpleadoDaoException;

	/** 
	 * Deletes a single row in the PuestoEmpleado table.
	 */
	public void delete(PuestoEmpleadoPk pk) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns the rows from the PuestoEmpleado table that matches the specified primary-key value.
	 */
	public PuestoEmpleado findByPrimaryKey(PuestoEmpleadoPk pk) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the PuestoEmpleado table that match the criteria 'Puesto_idPuesto = :idPuesto'.
	 */
	public PuestoEmpleado findByPrimaryKey(int idPuesto, int idEmpleado, Date dia) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria ''.
	 */
	public PuestoEmpleado[] findAll() throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoEmpleado[] findWhereIdPuestoEquals(int idPuesto) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoEmpleado[] findWhereIdEmpleadoEquals(int idEmpleado) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public PuestoEmpleado[] findWhereDiaEquals(Date dia) throws PuestoEmpleadoDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public PuestoEmpleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws PuestoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public PuestoEmpleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws PuestoEmpleadoDaoException;


}

package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.RepartoEmpleado;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk;
import es.bermejo.conteo.bbdd.exceptions.RepartoEmpleadoDaoException;
import java.util.Date;

public interface RepartoEmpleadoDao {
	/** 
	 * Inserts a new row in the Repartos table.
	 */
	public RepartoEmpleadoPk insert(RepartoEmpleado dto) throws RepartoEmpleadoDaoException;

	/** 
	 * Updates a single row in the Repartos table.
	 */
	public void update(RepartoEmpleadoPk pk, RepartoEmpleado dto) throws RepartoEmpleadoDaoException;

	/** 
	 * Deletes a single row in the Repartos table.
	 */
	public void delete(RepartoEmpleadoPk pk) throws RepartoEmpleadoDaoException;

	/** 
	 * Returns the rows from the Repartos table that matches the specified primary-key value.
	 */
	public RepartoEmpleado findByPrimaryKey(RepartoEmpleadoPk pk) throws RepartoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Repartos table that match the criteria 'IdReparto = :IdReparto'.
	 */
	public RepartoEmpleado findByPrimaryKey(int IdReparto, int idEmpleado) throws RepartoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puestos table that match the criteria ''.
	 */
	public RepartoEmpleado[] findAll() throws RepartoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'IdReparto = :IdReparto'.
	 */
	public RepartoEmpleado[] findWhereIdRepartoEquals(int IdReparto) throws RepartoEmpleadoDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public RepartoEmpleado[] findByDynamicSelect(String sql, Object[] sqlParams) throws RepartoEmpleadoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public RepartoEmpleado[] findByDynamicWhere(String sql, Object[] sqlParams) throws RepartoEmpleadoDaoException;


}

package es.bermejo.conteo.bbdd.dao;

import es.bermejo.conteo.bbdd.dto.Puesto;
import es.bermejo.conteo.bbdd.dto.PuestoPk;
import es.bermejo.conteo.bbdd.exceptions.PuestoDaoException;

public interface PuestoDao {
	/** 
	 * Inserts a new row in the Puesto table.
	 */
	public PuestoPk insert(Puesto dto) throws PuestoDaoException;

	/** 
	 * Updates a single row in the Puesto table.
	 */
	public void update(PuestoPk pk, Puesto dto) throws PuestoDaoException;

	/** 
	 * Deletes a single row in the Puesto table.
	 */
	public void delete(PuestoPk pk) throws PuestoDaoException;

	/** 
	 * Returns the rows from the Puesto table that matches the specified primary-key value.
	 */
	public Puesto findByPrimaryKey(PuestoPk pk) throws PuestoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public Puesto findByPrimaryKey(int idPuesto) throws PuestoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria ''.
	 */
	public Puesto[] findAll() throws PuestoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the criteria 'idPuesto = :idPuesto'.
	 */
	public Puesto[] findWhereIdPuestoEquals(int idPuesto) throws PuestoDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public Puesto[] findByDynamicSelect(String sql, Object[] sqlParams) throws PuestoDaoException;

	/** 
	 * Returns all rows from the Puesto table that match the specified arbitrary SQL statement
	 */
	public Puesto[] findByDynamicWhere(String sql, Object[] sqlParams) throws PuestoDaoException;


}

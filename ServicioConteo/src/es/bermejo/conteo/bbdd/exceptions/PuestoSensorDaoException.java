package es.bermejo.conteo.bbdd.exceptions;

public class PuestoSensorDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * */
	 
	public PuestoSensorDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public PuestoSensorDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
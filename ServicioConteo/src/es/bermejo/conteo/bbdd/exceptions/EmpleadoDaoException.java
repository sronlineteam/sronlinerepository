package es.bermejo.conteo.bbdd.exceptions;

public class EmpleadoDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'EmpleadoDaoException'
	 * 
	 * @param message
	 * */
	 
	public EmpleadoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'EmpleadoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public EmpleadoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
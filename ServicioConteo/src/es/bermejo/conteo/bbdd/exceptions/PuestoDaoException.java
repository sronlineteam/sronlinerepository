package es.bermejo.conteo.bbdd.exceptions;

public class PuestoDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * */
	 
	public PuestoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public PuestoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
package es.bermejo.conteo.bbdd.exceptions;

public class RepartoDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'RepartoDaoException'
	 * 
	 * @param message
	 * */
	 
	public RepartoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'RepartoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public RepartoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
package es.bermejo.conteo.bbdd.exceptions;

public class SensorDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'SensorDaoException'
	 * 
	 * @param message
	 * */
	 
	public SensorDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'SensorDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public SensorDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
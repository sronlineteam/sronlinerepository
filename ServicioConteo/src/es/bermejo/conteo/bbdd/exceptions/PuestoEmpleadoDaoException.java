package es.bermejo.conteo.bbdd.exceptions;

public class PuestoEmpleadoDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * */
	 
	public PuestoEmpleadoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public PuestoEmpleadoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
package es.bermejo.conteo.bbdd.exceptions;

public class RepartoEmpleadoDaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * */
	 
	public RepartoEmpleadoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'PuestoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public RepartoEmpleadoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
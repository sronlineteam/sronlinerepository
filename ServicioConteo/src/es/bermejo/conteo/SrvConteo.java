package es.bermejo.conteo;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileLock;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.bermejo.conteo.bbdd.ConexionBBDD;

/**
 * Servlet implementation class SrvConteo
 */
@WebServlet("/SrvConteo")
public class SrvConteo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvConteo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String simput = (String) request.getParameter("id");
		System.out.println("tengo: " + simput);
		ConexionBBDD conexion = new ConexionBBDD();
		conexion.insertarCuenta(simput);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

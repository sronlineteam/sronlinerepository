package es.bermejo.conteo.controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.bermejo.conteo.bbdd.dao.EmpleadoDao;
import es.bermejo.conteo.bbdd.dao.RepartoDao;
import es.bermejo.conteo.bbdd.dao.RepartoEmpleadoDao;
import es.bermejo.conteo.bbdd.dto.Empleado;
import es.bermejo.conteo.bbdd.dto.Reparto;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleado;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk;
import es.bermejo.conteo.bbdd.dto.RepartoPk;
import es.bermejo.conteo.bbdd.exceptions.EmpleadoDaoException;
import es.bermejo.conteo.bbdd.exceptions.RepartoDaoException;
import es.bermejo.conteo.bbdd.exceptions.RepartoEmpleadoDaoException;
import es.bermejo.conteo.bbdd.factory.EmpleadoDaoFactory;
import es.bermejo.conteo.bbdd.factory.RepartoDaoFactory;
import es.bermejo.conteo.bbdd.factory.RepartoEmpleadoDaoFactory;

/**
 * Servlet implementation class SrvGstRepartoEmpleados
 */
@WebServlet("/SrvGstRepartoEmpleados")
public class SrvGstRepartoEmpleados extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGstRepartoEmpleados() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/SrvGstRepartos";
		int idReparto = 0;
		int idEmpleado;
		int peso;
		String accionEmpleado = "editar";
		String accion = "editar";
		
		Reparto reparto = new Reparto();
		RepartoEmpleado empleado = new RepartoEmpleado();
		RepartoEmpleadoPk pk = new RepartoEmpleadoPk();
		Empleado[] lstEmpleados = null;
		
		
		if(request.getParameter("idReparto") != null ){
			idReparto = Integer.parseInt((String) request.getParameter("idReparto"));
			if(request.getParameter("accionEmpleado") != null)
				accionEmpleado = request.getParameter("accionEmpleado");
	
			RepartoEmpleadoDao empleadosRepartoDao = RepartoEmpleadoDaoFactory.create();
			EmpleadoDao empleado_Dao = EmpleadoDaoFactory.create();
			RepartoDao repartoDao = RepartoDaoFactory.create();

			//alta muestra el formulario de alta
			if(accionEmpleado.equals("alta")){
				try {
					reparto = repartoDao.findByPrimaryKey(idReparto);
					Date[] parametros = {reparto.getHasta(), reparto.getDesde()};
					lstEmpleados = empleado_Dao.findByDynamicWhere("alta <= ? and (baja >= ? or baja is null) ", parametros);
					request.setAttribute("lstEmpleados", lstEmpleados);

				} catch (EmpleadoDaoException | RepartoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				paginaDestino = "/gestionRepartoEmpleados.jsp";
			}
			//editar edita los datos del empleado
			else if(accionEmpleado.equals("editar")){
				try {
					idEmpleado = Integer.parseInt((String) request.getParameter("idEmpleado"));
					pk.setidEmpleado(idEmpleado);
					pk.setIdReparto(idReparto);
					
					empleado = empleadosRepartoDao.findByPrimaryKey(pk);

					reparto = repartoDao.findByPrimaryKey(idReparto);
					Date[] parametros = {reparto.getHasta(), reparto.getDesde()};
					lstEmpleados = empleado_Dao.findByDynamicWhere("alta <= ? and (baja >= ? or baja is null) ", parametros);
					request.setAttribute("lstEmpleados", lstEmpleados);
					request.setAttribute("idEmpleado", idEmpleado);
					
					
				} catch (RepartoEmpleadoDaoException | EmpleadoDaoException | RepartoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paginaDestino = "/gestionRepartoEmpleados.jsp";
			}
			else if (accionEmpleado.equals("eliminar")){
				try {
					
					idEmpleado = Integer.parseInt((String) request.getParameter("idEmpleado"));
					pk.setidEmpleado(idEmpleado);
					pk.setIdReparto(idReparto);
					empleadosRepartoDao.delete(pk);
					//accionEmpleado = "editar";
					}
				catch (RepartoEmpleadoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//paginaDestino = "/SrvGstRepartos";
			}
			//guardar guarda los cambios
			else if (accionEmpleado.equals("guardar")){
				try {
					idEmpleado = Integer.parseInt((String) request.getParameter("idEmpleado"));
					peso = Integer.parseInt((String) request.getParameter("peso"));
					pk.setidEmpleado(idEmpleado);
					pk.setIdReparto(idReparto);
					
					empleado.setIdEmpleado(idEmpleado);
					empleado.setIdReparto(idReparto);
					empleado.setPeso(peso);
					
					empleadosRepartoDao.update(pk, empleado);

					//accionEmpleado = "editar";
					//paginaDestino = "/SrvGstRepartos";
				} catch (RepartoEmpleadoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//nuevo crea uno nuevo
			else if (accionEmpleado.equals("nuevo")){
				try {
					idEmpleado = Integer.parseInt((String) request.getParameter("idEmpleado"));
					peso = Integer.parseInt((String) request.getParameter("peso"));
					pk.setidEmpleado(idEmpleado);
					pk.setIdReparto(idReparto);
					
					empleado.setIdEmpleado(idEmpleado);
					empleado.setIdReparto(idReparto);
					empleado.setPeso(peso);
					
					empleadosRepartoDao.insert(empleado);
					
					//paginaDestino = "/SrvGstRepartos";
					//accionEmpleado = "editar";
				} catch (RepartoEmpleadoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}


		}
		
//		request.setAttribute("reparto", reparto);
		request.setAttribute("accion", accion);
		request.setAttribute("accionEmpleado", accionEmpleado);
		request.setAttribute("idReparto", idReparto);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

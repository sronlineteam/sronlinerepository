package es.bermejo.conteo.controlador;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import es.bermejo.conteo.bbdd.ConexionBBDD;
import es.bermejo.conteo.bbdd.dto.CuentasDia;

/**
 * Servlet implementation class SrvConsultaCuentas
 */
@WebServlet("/SrvConsultaCuentas")
public class SrvConsultaCuentas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
	LoggerContext context = new LoggerContext("Conteo");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvConsultaCuentas() {
        super();
		log = context.getLogger(getClass().getName());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/cuenta.jsp";
		String jornada = request.getParameter("jornada");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date();
				
		jornada = dateFormat.format(date);
		log.debug(jornada);

		List<CuentasDia> cuentasDia = new ArrayList<CuentasDia>();
		ConexionBBDD conexion = new ConexionBBDD();
		
		cuentasDia = conexion.consultarDia(jornada);
				
		request.setAttribute("jornada", jornada);
		request.setAttribute("cuentasDia", cuentasDia);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

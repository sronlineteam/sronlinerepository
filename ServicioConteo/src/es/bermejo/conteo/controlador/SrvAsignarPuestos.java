package es.bermejo.conteo.controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import es.bermejo.conteo.bbdd.dao.EmpleadoDao;
import es.bermejo.conteo.bbdd.dao.PuestoEmpleadoDao;
import es.bermejo.conteo.bbdd.dto.Empleado;
import es.bermejo.conteo.bbdd.dto.PuestoEmpleado;
import es.bermejo.conteo.bbdd.exceptions.PuestoEmpleadoDaoException;
import es.bermejo.conteo.bbdd.factory.EmpleadoDaoFactory;
import es.bermejo.conteo.bbdd.factory.PuestoEmpleadoDaoFactory;

/**
 * Servlet implementation class SrvAsignarPuestos
 */
@WebServlet("/SrvAsignarPuestos")
public class SrvAsignarPuestos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
	LoggerContext context = new LoggerContext("Conteo");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvAsignarPuestos() {
        super();
		log = context.getLogger(getClass().getName());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/confirmarJornada.jsp";
		String jornada = request.getParameter("jornada");
		Date dia = null;
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
		String empleadosPuestos = request.getParameter("empleadosPuestos");	
		String[] puestos = empleadosPuestos.split(",");
		Empleado[] lstEmpleado;
		try {
			EmpleadoDao _dao = EmpleadoDaoFactory.create();
			int funcion = 1;
			Integer[] parametros = {new Integer(funcion)};
			
			lstEmpleado = _dao.findByDynamicWhere("(baja >= STR_to_DATE('" + jornada + "','%d-%m-%Y')  or baja is null) and alta <= STR_to_DATE('" + jornada + "','%d-%m-%Y') and funcion = ?", parametros);
			request.setAttribute("lstEmpleado", lstEmpleado);
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}

		if(jornada != null && empleadosPuestos != null){
			try {
				dia = dateformat.parse(jornada);
			} catch (ParseException e) {
				log.error(e.getMessage());
			}
	
			for (int i = 0; i < puestos.length; i++)
				asignar(puestos[i], request.getParameter("empleadoPuesto" + puestos[i]), dia);
		}
		
		request.setAttribute("asignaciones", asignaciones(dia));
		request.setAttribute("jornada", jornada);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
		
		
	}

	private PuestoEmpleado[] asignaciones(Date dia) {
		PuestoEmpleadoDao _dao = PuestoEmpleadoDaoFactory.create();

		PuestoEmpleado[] resultado = null;

		try {
			resultado = _dao.findWhereDiaEquals(dia);
		} catch (PuestoEmpleadoDaoException e) {
			log.error(e.getMessage());
		}
		return resultado;
	}

	private void asignar(String idPuesto, String idEmpleado, Date dia) {
		PuestoEmpleadoDao _dao = PuestoEmpleadoDaoFactory.create();
		PuestoEmpleado dto = new PuestoEmpleado();
		dto.setIdPuesto(Integer.parseInt(idPuesto));
		dto.setIdEmpleado(Integer.parseInt(idEmpleado));
		dto.setDia(dia);
		try {
			_dao.insert(dto);
		} catch (PuestoEmpleadoDaoException e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package es.bermejo.conteo.controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import es.bermejo.conteo.bbdd.dao.RepartoDao;
import es.bermejo.conteo.bbdd.dao.RepartoEmpleadoDao;
import es.bermejo.conteo.bbdd.dto.Reparto;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleado;
import es.bermejo.conteo.bbdd.dto.RepartoEmpleadoPk;
import es.bermejo.conteo.bbdd.dto.RepartoPk;
import es.bermejo.conteo.bbdd.exceptions.RepartoDaoException;
import es.bermejo.conteo.bbdd.exceptions.RepartoEmpleadoDaoException;
import es.bermejo.conteo.bbdd.factory.RepartoDaoFactory;
import es.bermejo.conteo.bbdd.factory.RepartoEmpleadoDaoFactory;

/**
 * Servlet implementation class SrvGstRepartos
 */
@WebServlet("/SrvGstRepartos")
public class SrvGstRepartos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
	LoggerContext context = new LoggerContext("Conteo");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGstRepartos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/gestionRepartos.jsp";
		int idReparto;
		String accion = "editar";
		Reparto reparto = new Reparto();
		RepartoEmpleado[] empleados = null;
		String auxIdReparto = (String) request.getParameter("idReparto");

		if(auxIdReparto != null && !auxIdReparto.equals("")){
			idReparto = Integer.parseInt(auxIdReparto);
			if(request.getParameter("accion") != null)
				accion = request.getParameter("accion");
	
			RepartoDao repartoDao = RepartoDaoFactory.create();
			RepartoEmpleadoDao empleadosDao = RepartoEmpleadoDaoFactory.create();
			
			if(accion.equals("editar")){
				try {
					reparto = repartoDao.findByPrimaryKey(idReparto);
					empleados = empleadosDao.findWhereIdRepartoEquals(idReparto);
				} catch (RepartoDaoException | RepartoEmpleadoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (accion.equals("eliminar")){
				try {
					RepartoPk pk = new RepartoPk(idReparto);
					RepartoEmpleadoPk pk2 = new RepartoEmpleadoPk();
					pk2.setIdReparto(idReparto);
					empleados = empleadosDao.findWhereIdRepartoEquals(idReparto);
					if(empleados.length>0){
						for(RepartoEmpleado empleado: empleados){
							pk2.setidEmpleado(empleado.getIdEmpleado());
							empleadosDao.delete(pk2);
						}
					}
					repartoDao.delete(pk);
					paginaDestino = "/SrvLstRepartos";
				} catch (RepartoDaoException | RepartoEmpleadoDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (accion.equals("guardar")){
				try {
					RepartoPk pk = new RepartoPk(idReparto);
					String descripcion = request.getParameter("descripcion");
					SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
					
					Date desde = dateformat.parse(request.getParameter("desde"));
					Date hasta = dateformat.parse(request.getParameter("hasta"));
										
					reparto.setIdReparto(idReparto);
					reparto.setDescripcion(descripcion);
					reparto.setDesde(desde);
					reparto.setHasta(hasta);
					
					repartoDao.update(pk, reparto);
					
					paginaDestino = "/SrvLstRepartos";
				} catch (RepartoDaoException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (accion.equals("nuevo")){
				try {
					String descripcion = request.getParameter("descripcion");
					SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
					
					Date desde = dateformat.parse(request.getParameter("desde"));
					Date hasta = dateformat.parse(request.getParameter("hasta"));
					
					reparto.setDescripcion(descripcion);
					reparto.setDesde(desde);
					reparto.setHasta(hasta);
					
					repartoDao.insert(reparto);
					
					paginaDestino = "/SrvLstRepartos";
				} catch (RepartoDaoException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}


		}
		else
			accion = "nuevo";
		
		if(empleados != null)
			request.setAttribute("empleados", empleados);

		request.setAttribute("reparto", reparto);
		request.setAttribute("accion", accion);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

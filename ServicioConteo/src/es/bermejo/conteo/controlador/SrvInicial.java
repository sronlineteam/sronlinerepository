package es.bermejo.conteo.controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import es.bermejo.conteo.bbdd.dao.EmpleadoDao;
import es.bermejo.conteo.bbdd.dto.Empleado;
import es.bermejo.conteo.bbdd.factory.EmpleadoDaoFactory;

/**
 * Servlet implementation class SrvIncial
 */
@WebServlet("/SrvInicial")
public class SrvInicial extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
	LoggerContext context = new LoggerContext("Conteo");
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvInicial() {
        super();
		log = context.getLogger(getClass().getName());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/configurarJornada.jsp";
		String jornada = request.getParameter("jornada");
		Empleado[] lstEmpleado;
		
		
		if(jornada != null && !jornada.isEmpty()){
			try {
				EmpleadoDao _dao = EmpleadoDaoFactory.create();
				int funcion = 1;
				Integer[] parametros = {new Integer(funcion)};
				String puestos = request.getParameter("puestosJornada");

				lstEmpleado = _dao.findByDynamicWhere("(baja >= STR_to_DATE('" + jornada + "','%d-%m-%Y')  or baja is null) and alta <= STR_to_DATE('" + jornada + "','%d-%m-%Y') and funcion = ?", parametros);
				request.setAttribute("lstEmpleado", lstEmpleado);
				request.setAttribute("puestosJornada", puestos);
			}
			catch (Exception e) {
				log.error(e.getMessage());
			}

		}
		
		
		request.setAttribute("jornada", jornada);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

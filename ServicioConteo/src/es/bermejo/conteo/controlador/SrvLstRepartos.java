package es.bermejo.conteo.controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.bermejo.conteo.bbdd.dao.RepartoDao;
import es.bermejo.conteo.bbdd.dto.Reparto;
import es.bermejo.conteo.bbdd.exceptions.RepartoDaoException;
import es.bermejo.conteo.bbdd.factory.RepartoDaoFactory;

/**
 * Servlet implementation class SrvLstRepartos
 */
@WebServlet("/SrvLstRepartos")
public class SrvLstRepartos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvLstRepartos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paginaDestino = "/listaRepartos.jsp";
		Reparto[] lstRepartos = null;
		RepartoDao repartoDao = RepartoDaoFactory.create();
		
		try {
			lstRepartos = repartoDao.findAll();
		} catch (RepartoDaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("lstRepartos", lstRepartos);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

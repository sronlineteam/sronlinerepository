<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="es.bermejo.conteo.bbdd.dto.CuentasDia"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="auxiliares/header.html" %>
<%
String jornada = null;
List<CuentasDia> cuentasDia = null;

Object obj = request.getAttribute("jornada");
if (obj != null){
	jornada = (String) obj; 
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat miDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	jornada = miDateFormat.format(dateFormat.parse(jornada));
}

obj = request.getAttribute("cuentasDia");
if (obj != null)
	cuentasDia = (List<CuentasDia>) obj; 

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="REFRESH" CONTENT="60">
<script type="text/javascript">

function consultarJornada(){
	this.document.forms.formJornada.submit();
}

</script>

<title>Consulta de la Jornada</title>
</head>
<body>
	<div class="cabecera">
		<h1>Sistema de conteo de Puerros</h1>
		<%@ include file="auxiliares/menu.jsp" %>
	</div>
	<div class="contenido" id="contenido">
		<h1>Consultar Jornada <%=jornada %></h1>
	<%
	int nAnterior = 0;
	for(int i = 0; i < cuentasDia.size(); i++){%>
					<p align="center"><font size="5">Puesto <%=cuentasDia.get(i).getIdPuesto()%>: &nbsp;&nbsp;&nbsp;<b><%=cuentasDia.get(i).getCuenta() - nAnterior%></b></font></p>
	<%
	nAnterior = cuentasDia.get(i).getCuenta();
	} %>
		<h1></h1>
			<form action="SrvConsultaCuentas" name="formJornada" method="post" >
				<div class="row">
				  	<div class="col-md-6">
						<label for="tipo_conexion" class="col-sm-4 control-label">Indique d�a a consultar:</label>
						<div class="container">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker3'>
					                    <input type='text' class="form-control" name="jornada"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
						        <script type="text/javascript">
									$(function () {
									    $('#datetimepicker3').datetimepicker({
									    	format: 'DD-MM-YYYY',
									    	defaultDate: new Date(),
									    	maxDate: 'now'
									    });
									    
									});
								</script>
					        <div class='col-sm-1'>
								<input type="button" class="btn" value="Consultar" alt="Iniciar" onclick="consultarJornada()" />
							</div>
						</div>
					</div>
				</div>
			</form>

</body>
</html>
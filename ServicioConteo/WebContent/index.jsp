<html>
<head>
<%@ include file="auxiliares/header.html" %>
<meta charset="ISO-8859-1">
<script type="text/javascript">
var arrayPuestos = [];

function iniciarJornada(){
	
	if(this.document.forms.formJornada.jornada.value == "")
		alert("Indique la fecha de la jornada!");
	else{
		recogerPuestos();
		if(arrayPuestos==""){
			alert("Seleccione al menos un puesto");
		}
		else{
			this.document.forms.formJornada.puestosJornada.value=arrayPuestos;
			this.document.forms.formJornada.submit();
		}
	}
}

function cambiarTodos(){
	if ($("#todosDias").is( ":checked")){
		$('#1, #2, #3, #4, #5, #6').prop('checked', true);
	} else {
		$('#1, #2, #3, #4, #5, #6').prop('checked', false);
	}
}
function recogerPuestos(){
    var cboxes = document.getElementsByName('puesto');
    var len = cboxes.length;
    for (var i=0; i<len; i++) {
    	if(cboxes[i].checked)
    		arrayPuestos.push(cboxes[i].value);
    }
    
    return arrayPuestos;
}


</script>
<title>Inicio Jornada</title>
</head>
<body>
	<div class="cabecera">
		<h1>Sistema de conteo de Puerros</h1>
		<%@ include file="auxiliares/menu.jsp" %>
	</div>
	<div class="contenido" id="contenido">
		<h1>Iniciar Jornada</h1>
			<form action="SrvInicial" name="formJornada" method="post" >
				<div class="row">
				  	<div class="col-md-6">
						<label for="tipo_conexion" class="col-sm-3 control-label">Indique d�a:</label>
						<div class="container">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker3'>
					                    <input type='text' class="form-control" name="jornada"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
						        <script type="text/javascript">
									$(function () {
									    $('#datetimepicker3').datetimepicker({
									    	format: 'DD-MM-YYYY',
									    	defaultDate: new Date()
									    });
									    
									});
								</script>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label for="tipo_conexion" class="col-sm-3 control-label">Puestos del d�a:</label>
						<div class="container">
					        <div class='col-sm-3'>
								1 <input type="checkbox" id="1" name="puesto" value="1"> 
								2 <input type="checkbox" id="2" name="puesto" value="2"> 
								3 <input type="checkbox" id="3" name="puesto" value="3"> 
								4 <input type="checkbox" id="4" name="puesto" value="4"> 
								5 <input type="checkbox" id="5" name="puesto" value="5"> 
								6 <input type="checkbox" id="6" name="puesto" value="6"> 
								Todos <input type="checkbox" id="todosDias" onclick="cambiarTodos()"><br/>
							    <input type="text" name="puestosJornada" hidden />
						    </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
				        <div class='col-sm-3'>
				        </div>
				        <div class='col-sm-3'>
							<input type="button" class="btn" value="Iniciar" alt="Iniciar" onclick="iniciarJornada()" />
						</div>
					</div>
				</div>
			</form>
</body>
</html>
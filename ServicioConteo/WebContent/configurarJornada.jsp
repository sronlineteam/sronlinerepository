<%@page import="es.bermejo.conteo.bbdd.dto.Empleado"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="auxiliares/header.html" %>
    
<%
String jornada = null;
Empleado[]  lstEmpleado = null;
String listaPuestos = null;
String[] puestos = null;
StringBuffer opcionesEmpleados = new StringBuffer();
StringBuffer variablesOpciones = new StringBuffer("[");
int nPuestos = 0;

Object obj = request.getAttribute("jornada");
if (obj != null)
	jornada = (String) obj; 

obj = request.getAttribute("lstEmpleado");
if (obj != null){
	lstEmpleado = (Empleado[]) obj;
	nPuestos = lstEmpleado.length;
	
	opcionesEmpleados.append("<option value='0'>- - - - - -</option>)");
	
	for (int i = 0; i < lstEmpleado.length; i++){
		opcionesEmpleados.append("<option value='").append(lstEmpleado[i].getIdEmpleado()).append("'>").append(lstEmpleado[i].getNombre()).append("</option>)");
	}
}

obj = request.getAttribute("puestosJornada");
if (obj != null)
	listaPuestos = (String) obj; 
	puestos = listaPuestos.split(","); 

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">

function asignar(){
	this.document.forms.asignarPuestos.submit();
}

</script>

<title>Configurar Jornada</title>
</head>
<body>
	<div class="cabecera">
		<h1>Sistema de conteo de Puerros</h1>
		<%@ include file="auxiliares/menu.jsp" %>
	</div>
	<div class="contenido" id="contenido">
		<h1>Jornada del <%=jornada %></h1>
		<form name="asignarPuestos" action="SrvAsignarPuestos" method="post">
			<input type="hidden" name="jornada" value="<%=jornada%>">
			<input type="hidden" name="npuestos" value="<%=nPuestos%>">
			<input type="hidden" name="empleadosPuestos" value="<%=listaPuestos%>">
			<div class="row">
			  	<div class="col-md-6">
	<%for(int i = 0; i < puestos.length; i++){ %>
					<label for="Puesto" class="col-sm-2 control-label">Puesto <%=puestos[i]%>:</label>
					<div class="container">
				        <div class='col-sm-4'>
				            <div class="form-group">
					        	<select name="empleadoPuesto<%=puestos[i]%>">
					        		<%=opcionesEmpleados %>
					        	</select>
				        	</div>
				       	</div>
					</div>
	<%} %>
				</div>
			</div>
		    <div class="row">
				<div class="col-sm-6">
					<div class='col-sm-1'>
				    </div>
				    <div class='col-sm-5'>
					    <input type="button" class="btn" value="Aceptar" alt="Iniciar" onclick="asignar()" />
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="cancelar()">
					</div>
				</div>
			</div>
	
	
		</form>
	</div>
</body>
</html>
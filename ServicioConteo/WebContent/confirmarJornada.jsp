<%@page import="es.bermejo.conteo.bbdd.dto.PuestoEmpleado"%>
<%@page import="es.bermejo.conteo.bbdd.dto.Empleado"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="auxiliares/header.html" %>
    
<%
String jornada = null;
PuestoEmpleado[]  lstAsignaciones = null;
Empleado[]  lstEmpleado = null;

Object obj = request.getAttribute("jornada");
if (obj != null)
	jornada = (String) obj; 

obj = request.getAttribute("asignaciones");
if (obj != null)
	lstAsignaciones = (PuestoEmpleado[]) obj; 

obj = request.getAttribute("lstEmpleado");
if (obj != null)
	lstEmpleado = (Empleado[]) obj;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">

function asignar(){
	alert("seguro");
	this.document.forms.asignarPuestos.submit();
	this.document.forms.formJornada.submit();
}

</script>

<title>Configurar Jornada</title>
</head>
<body>
	<div class="cabecera">
		<h1>Sistema de conteo de Puerros</h1>
		<%@ include file="auxiliares/menu.jsp" %>
	</div>
	<div class="contenido" id="contenido">
		<h1>Jornada del <%=jornada %></h1>
	<%
	int idEmpleado = 0;
	if(lstEmpleado != null){
	for(int i = 0; i < lstAsignaciones.length; i++){ 
		idEmpleado = lstAsignaciones[i].getIdEmpleado();
	%>
					<p><b>Puesto <%=lstAsignaciones[i].getIdPuesto()%></b>: <%=lstEmpleado[idEmpleado].getNombre()%>  </p>
	<%}
	}%>
	</div>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Gestion de Valvulas</title>
		<META HTTP-EQUIV="REFRESH" CONTENT="1200;URL=SrvGestValvulas">
			<style type="text/css">
			
			#valvulas_panel ul {
			    list-style-type: none;
			    margin: 0px;
			    padding: 0px;
			    width: 100%;
			    font-family: Arial, sans-serif;
			    font-size: 8pt;
			}
			
			
			#valvulas_panel ul li {
			    border-bottom: 1px solid white;
			}
			
			#valvulas_panel ul li a {
			    color: #ccc;
			    text-decoration: none;
			    text-transform: uppercase;
			    display: block;
			    padding: 6px;
			}
			
			
			#valvulas_panel ul li a:hover {
			    background: #000;
			    color: #fff;
			}
			
			</style>
		<%@ include file="auxiliares/header.html" %>
	</head>
	<body onload="initialize()" style="margin: 0px; overflow: hidden;">
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		
		<div class="contenido" id="contenido" style="height: 90%">
			<h1>Mapa de Valvulas</h1>
			<%@ include file="auxiliares/gstValvulasMapa.jsp" %>
		</div>
	</body>
</html>
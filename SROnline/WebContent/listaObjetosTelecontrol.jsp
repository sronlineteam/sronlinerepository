<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Objetos de Telecontrol</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<style>
		.progListadoBloque {
    		height: 372px;
    	}
    	.formularioModalSmall {
		    width: 60%;
		    margin-left: 20%;
		}
		UL.col3{
			PADDING-LEFT: 15px;
		}
	</style>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de acciones sobre elementos telecontrolables</h1>
			<%@ include file="auxiliares/lstObjetosTelecontrol.jsp" %>
		</div>
	</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Gestion de Objetos Telecontrol</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
<%
	String idAEMET = Constantes.getIdAEMET();
%>		
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
<%
	if(idAEMET.length()>5){
%>
		<div class="Movile">
			<script type='text/javascript' src='http://www.aemet.es/es/eltiempo/prediccion/municipios/launchwidget/<%=idAEMET%>?w=g3p01100000ohmffffffw308z94x4f86d9t95b6e9r0s1n1'></script>
			<noscript>
				<a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/es/eltiempo/prediccion/municipios/<%=idAEMET%>'>
					El Tiempo. Consulte la predicción de la AEMET
				</a>
			</noscript>
		</div>
		<div class="noMovile" style="margin-top: 360px;position: absolute;" >
				<script type='text/javascript' src='http://www.aemet.es/es/eltiempo/prediccion/municipios/launchwidget/<%=idAEMET%>?w=g4p01111111ohmffffffw580z248x4f86d9t95b6e9r1s3n2'></script>
				<noscript>
					<a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/es/eltiempo/prediccion/municipios/<%=idAEMET%>'>
						El Tiempo. Consulte la predicción de la AEMET
					</a>
				</noscript>
		</div>
<%
	}
%>	
		<div class="contenido" id="contenido">
			<h1>Gestion manual de Objetos Telecontrol</h1>
			<%@ include file="auxiliares/gstObjetosTelecontrol.jsp" %>
		</div>
	
	</body>
</html>

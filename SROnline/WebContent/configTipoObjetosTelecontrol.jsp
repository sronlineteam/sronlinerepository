<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Gestion de Objetos Telecontrol</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Gestion de Tipos de Objetos Telecontrol</h1>
			<%@ include file="auxiliares/confTipoObjetosTC.jsp" %>
		</div>
	
	</body>
</html>

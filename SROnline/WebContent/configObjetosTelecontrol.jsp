<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Objetos de Telecontrol</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Edici�n de elemento telecontrolable</h1>
			<%@ include file="auxiliares/confObjetosTC.jsp" %>
		</div>
	</body>
</html>

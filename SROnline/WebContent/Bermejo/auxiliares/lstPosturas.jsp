<%@page import="es.sronline.bbdd.dto.Parcelas"%>
<%@page import="es.sronline.bbdd.dto.Abonos"%>
<%@page import="es.sronline.bbdd.dto.Posturas"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.Sectores"%>
<%@page import="es.sronline.bermejo.ProgramacionPosturasFormateada"%>
<%@page import="es.sronline.bbdd.dto.ProgramacionPosturas"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
Posturas[] lstPosturas = null;
Object objVal = request.getAttribute("lstPosturas");
int nPosturas = 0;
int nTratamientos = 6;

Parcelas[] lstParcelas = null;
Object objValParcelas = request.getAttribute("lstParcelas");

ProgramacionPosturas progmPosturas = null;
Object objProg = request.getAttribute("programaSector");

String auxDia;
String auxHora;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdfhora=new SimpleDateFormat("HH:mm");
boolean isCambio = false;

if(request.getAttribute("isCambio")!=null)
	isCambio = "true".equals(request.getAttribute("isCambio"));

boolean isPlantilla = false;

if(request.getAttribute("isPlantilla")!=null)
	isPlantilla = "true".equals(request.getAttribute("isPlantilla"));

if (objVal != null){
	lstPosturas = (Posturas[]) objVal;
	nPosturas = lstPosturas.length;
}

if (objValParcelas != null)
	lstParcelas = (Parcelas[]) objValParcelas;


if (objProg != null)
	progmPosturas = (ProgramacionPosturas) objProg; 


Abonos[][] abonos = new Abonos[nPosturas][nTratamientos];
Object objAbonos = request.getAttribute("abonos");

if(objAbonos != null)
	abonos = (Abonos[][]) objAbonos;

int idProgramacion = ((Integer) request.getAttribute("idProgramacion")).intValue();


String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 }
%>
 
<script type="text/javascript">
var idPosturas = [];
var tiempoPosturas = [];
var totalTiempo = <%=progmPosturas.getminutos() %>;


function configPosturas(nPosturas){
	
	var auxresultado;
	var errores = "";
	var ok = "si";
	var conAbonoChecked = this.document.getElementById("conAbono").checked;
	var conAcidoChecked = this.document.getElementById("conAcido").checked;
	var conSoplanteChecked = this.document.getElementById("conSoplante").checked;
	var conDisolventeChecked = this.document.getElementById("conDisolvente").checked;
	var conLavadoAbonoChecked = this.document.getElementById("conLavadoAbono").checked;
	var conLavadoAcidoChecked = this.document.getElementById("conLavadoAcido").checked;
	var conLavadoDisolventeChecked = this.document.getElementById("conLavadoDisolvente").checked;
	
	this.document.forms.configurarPosturas.hayAbono = conAbonoChecked;
	this.document.forms.configurarPosturas.hayAcido = conAcidoChecked;
	this.document.forms.configurarPosturas.haySoplante = conSoplanteChecked;
	this.document.forms.configurarPosturas.hayDisolvente = conDisolventeChecked;
	this.document.forms.configurarPosturas.hayLavadoAbono = conLavadoAbonoChecked;
	this.document.forms.configurarPosturas.hayLavadoAcido = conLavadoAcidoChecked;
	this.document.forms.configurarPosturas.hayLavadoDisolvente = conLavadoDisolventeChecked;
	
	for(var p = 0; p < nPosturas; p++){
		var resultado = "";
		var pivot;
		var auxDuracionPostura = eval(this.document.getElementById("postura" + idPosturas[p]).value);
		var auxIdParcelas =  eval(this.document.getElementById("parcela" + idPosturas[p]).value);
		var auxInicioAbono = 0;
		var auxDuracionAbono = 0;
		var auxInicioAcido = 0;
		var auxDuracionAcido = 0;
		var auxInicioSoplante = 0;
		var auxDuracionSoplante = 0;
		var auxInicioLavadoAbono = 0;
		var auxDuracionLavadoAbono = 0;
		var auxInicioLavadoAcido = 0;
		var auxDuracionLavadoAcido = 0;
		var auxInicioDisolvente = 0;
		var auxDuracionDisolvente = 0;
		var auxInicioLavadoDisolvente = 0;
		var auxDuracionLavadoDisolvente = 0;
		var auxComentarios = this.document['tablaPosturas']['observacion' + idPosturas[p]].value;

		
		if(conAbonoChecked){
			auxInicioAbono = eval(this.document.getElementById("abonoIni" + idPosturas[p]).value);
			auxDuracionAbono = eval(this.document.getElementById("abonoTiempo" + idPosturas[p]).value);

			if(eval(this.document.getElementById("abono" + idPosturas[p]).value) == 2){
				pivot = auxDuracionPostura - (auxInicioAbono + auxDuracionAbono);
				auxInicioAbono = pivot;
				}

			if (auxInicioAbono < 0 || auxInicioAbono > auxDuracionPostura){
				errores += "Revise incio Abono Planificación " + (p + 1) + ".\n";
				ok = "no";
			}

			if ( auxInicioAbono + auxDuracionAbono > auxDuracionPostura){
				errores += "Revise duración Abono Planificación " + (p + 1) + ".\n";
				ok = "no";
			}
			if(conLavadoAbonoChecked){
				auxInicioLavadoAbono = eval(this.document.getElementById("abonoIni" + idPosturas[p]).value) + eval(this.document.getElementById("abonoTiempo" + idPosturas[p]).value) + 1;
				auxDuracionLavadoAbono = eval(this.document.getElementById("lavadoAbonoTiempo" + idPosturas[p]).value);
			}
			else
				auxDuracionLavadoAbono = 0;

			if (auxInicioSoplante < 0 || auxInicioSoplante > auxDuracionPostura){
				errores += "Revise incio Soplante Planificación " + (p + 1) + ".\n";
				ok = "no";
			}

			if ( auxInicioSoplante + auxDuracionSoplante > auxDuracionPostura){
				errores += "Revise duración Soplante Planificación " + (p + 1) + ".\n";
				ok = "no";
			}
			
		}
		
			
		if(conAcidoChecked){
			auxInicioAcido = eval(this.document.getElementById("acidoIni" + idPosturas[p]).value);
			auxDuracionAcido = eval(this.document.getElementById("acidoTiempo" + idPosturas[p]).value);
			
			if(eval(this.document.getElementById("acido" + idPosturas[p]).value) == 2){
				pivot = auxDuracionPostura - (auxInicioAcido + auxDuracionAcido);
				auxInicioAcido = pivot;
			}
			
			if (auxInicioAcido < 0 || auxInicioAcido > auxDuracionPostura){
				errores += "Revise inicio Acido Planificación " + (p + 1) + ".\n";
				ok = "no";
			}

			if (auxInicioAcido + auxDuracionAcido > auxDuracionPostura){
				errores += "Revise duración Acido Planificación " + (p + 1) + ".\n";
				ok = "no";
			}
			if(conLavadoAcidoChecked){
				auxInicioLavadoAcido = auxInicioAcido + auxDuracionAcido + 1;
				auxDuracionLavadoAcido = eval(this.document.getElementById("lavadoAcidoTiempo" + idPosturas[p]).value);
			}
			else
				auxDuracionLavadoAcido = 0;

		}
		if(conSoplanteChecked){
			auxInicioSoplante = eval(this.document.getElementById("soplanteIni" + idPosturas[p]).value);
			auxDuracionSoplante = eval(this.document.getElementById("soplanteTiempo" + idPosturas[p]).value);

			if(eval(this.document.getElementById("soplante" + idPosturas[p]).value) == 2){
				pivot = auxDuracionPostura - (auxInicioSoplante + auxDuracionSoplante);
				auxInicioSoplante = pivot;
			}
			
			if (auxInicioSoplante < 0 || auxInicioSoplante > auxDuracionPostura){
				errores += "Revise incio Soplante Planificación " + (p + 1) + ".\n";
				ok = "no";
			}

			if ( auxInicioSoplante + auxDuracionSoplante > auxDuracionPostura){
				errores += "Revise duración Soplante Planificación " + (p + 1) + ".\n";
				ok = "no";
			}
		}

		if(conDisolventeChecked){
			auxInicioDisolvente = eval(this.document.getElementById("disolventeIni" + idPosturas[p]).value);
			auxDuracionDisolvente = eval(this.document.getElementById("disolventeTiempo" + idPosturas[p]).value);

			if(eval(this.document.getElementById("disolvente" + idPosturas[p]).value) == 2){
				pivot = auxDuracionPostura - (auxInicioDisolvente + auxDuracionDisolvente);
				auxInicioDisolvente = pivot;
			}

			if (auxInicioDisolvente < 0 || auxInicioDisolvente > auxDuracionPostura){
				errores += "Revise incio Disolvente Planificación " + (p + 1) + ".\n";
				ok = "no";
			}

			if ( auxInicioDisolvente + auxDuracionDisolvente > auxDuracionPostura){
				errores += "Revise duración Disolvente Planificación " + (p + 1) + ".\n";
				ok = "no";
			}
			if(conLavadoDisolventeChecked){
				auxInicioLavadoDisolvente = auxInicioDisolvente + auxDuracionDisolvente + 1;
				auxDuracionLavadoDisolvente = eval(this.document.getElementById("lavadoDisolventeTiempo" + idPosturas[p]).value);

				if (auxInicioLavadoDisolvente + auxDuracionLavadoDisolvente > auxDuracionPostura){
					errores += "Revise duración lavado Disolvente Planificación " + (p + 1) + ".\n";
					ok = "no";
				}
			}
			else
				auxDuracionLavadoDisolvente = 0;
		}
		
/* 		if(conAbonoChecked && conAcidoChecked){
			if(auxInicioAcido < auxInicioAbono + auxDuracionAbono){
				errores += "En la Planificación " + (p + 1) + " el acido se iniciará antes de terminar el abono" + ".\n";
				ok = "no";
			}
		}
 */
 		//duracionPostura | idPostura | idparcelas | comentarios |inicio abono | duración Abono | inicio Acido | duración acido | inicio Soplante | duración Soplante 
 		// | inicio Inyector | duración Inyector | inicio Inyector Acido | duración Inyector Acido | inicio Disolvente | duracion Disolvente | inicio Lavado Disolvente | duracion Lavado Disolvente | comentarios
		resultado += auxDuracionPostura;
		resultado += ";" + idPosturas[p];
		resultado += ";" + auxIdParcelas;
		resultado += ";" + auxComentarios;
		resultado += ";" + auxInicioAbono;
		resultado += ";" + auxDuracionAbono;
		resultado += ";" + auxInicioAcido;
		resultado += ";" + auxDuracionAcido;
		resultado += ";" + auxInicioSoplante;
		resultado += ";" + auxDuracionSoplante;
		resultado += ";" + auxInicioLavadoAbono;
		resultado += ";" + auxDuracionLavadoAbono;
		resultado += ";" + auxInicioLavadoAcido;
		resultado += ";" + auxDuracionLavadoAcido;
		resultado += ";" + auxInicioDisolvente;
		resultado += ";" + auxDuracionDisolvente;
		resultado += ";" + auxInicioLavadoDisolvente;
		resultado += ";" + auxDuracionLavadoDisolvente;
		auxresultado = eval(this.document.getElementById("resultado" + p)); 
		auxresultado.value = resultado;
		
	}
 	if(ok == "si")
 		this.document.forms.configurarPosturas.submit();
 	else
 		alert(errores);
	
}

function CancelarNueva(){
	document.location = "./SrvSectores";
}

function activarAbonos(nPosturas){
	var isActivo = this.document.getElementById("conAbono");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	
	
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("abonoIni" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("abonoTiempo" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("abono" + idPosturas[p]).disabled = valor);
	}
		
	
}

function activarLavadoAbono(nPosturas){
	var isActivo = this.document.getElementById("conLavadoAbono");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("lavadoAbonoTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function activarAcidos(nPosturas){
	var isActivo = this.document.getElementById("conAcido");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("acido" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("acidoIni" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("acidoTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function activarLavadoAcido(nPosturas){
	var isActivo = this.document.getElementById("conLavadoAcido");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("lavadoAcidoTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function activarSoplantes(nPosturas){
	var isActivo = this.document.getElementById("conSoplante");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("soplante" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("soplanteIni" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("soplanteTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function activarDisolvente(nPosturas){
	var isActivo = this.document.getElementById("conDisolvente");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("disolvente" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("disolventeIni" + idPosturas[p]).disabled = valor);
		eval(this.document.getElementById("disolventeTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function activarLavadoDisolvente(nPosturas){
	var isActivo = this.document.getElementById("conLavadoDisolvente");
	var valor;
	if(isActivo.checked)
		valor = false;
	else
		valor = true;
	for(var p = 0; p < nPosturas; p++){
		eval(this.document.getElementById("lavadoDisolventeTiempo" + idPosturas[p]).disabled = valor);
	}
	
}

function verificarDuracion(){
	var resultado = 0;
	for(var p = 0; p < idPosturas.length; p++){
		var duracion = this.document['tablaPosturas']['postura' + idPosturas[p]].value;
		this.document['tablaPosturas']['horaminuto' + idPosturas[p]].value = formateaHoraMinutos(duracion);
		resultado += parseInt(duracion);
	}
	if(resultado != <%=progmPosturas.getminutos() %>)
		{
			this.document.getElementById("tiempo").style.backgroundColor = "red";		
		}
	else
		this.document.getElementById("tiempo").style.backgroundColor = "";		

}

function formateaHoraMinutos(duracion){
	var minutos = duracion%60;
	var hora = (duracion - minutos)/60;
	
	var formateado = hora + ":";
	if(minutos<10)
		formateado += "0" + minutos;
	else
		formateado += minutos;
	return formateado;
}

function Cancelar(){
		this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModal").style.display ="none";
}

function abrirObservacion(idPosturaObservacion) {
	this.document.getElementById("textoObserv").value = this.document['tablaPosturas']['observacion' + idPosturaObservacion].value;
	this.document.getElementById("idObservacion").value = idPosturaObservacion;
	abrir_dialog();
  }
  
 function actualizarComentario(){
	var idPosturaObservacion = this.document.getElementById("idObservacion").value;
	var textonuevo = this.document.getElementById("textoObserv").value;
	//var auxiliarEval = "this.document.getElementById('observacion" + idPosturaObservacion + "').value = '" + textonuevo + "'";
	this.document['tablaPosturas']['observacion' + idPosturaObservacion].value = textonuevo;
	 
 }

  function abrir_dialog() {
	    $( "#dialogObservaciones" ).dialog({
	        modal: true
	    });
	  };


</script>

<div id="dialogObservaciones" title="Observaciones" style="display:none;">
    <p><textarea id="textoObserv"textoObserv rows="8" cols="35" maxlength ="295"></textarea> </p>
    <p>
    	<input type="hidden" id="idObservacion" />
    	<input type="button" class="btn" value="Actualizar" alt="Actualizar comentario"  onclick="actualizarComentario()"/>
    </p>
</div>

<h2>Parcela el <%=progmPosturas.getfecha() %>, con duracion <%=progmPosturas.getminutos() %> minutos</h2>
<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<form action="SrvPosturas" name="configurarPosturas" method="post" >
	<input type="hidden" name="numeroPosturas" value="<%=lstPosturas.length %>" />
	<input type="hidden" name="accion" value="<%=Constantes.getAccionActualizar() %>" />
	<input type="hidden" name="idProgramacion" value="<%=idProgramacion %>" />
	<input type="hidden" name="hayAbono"  />
	<input type="hidden" name="hayAcido"  />
	<input type="hidden" name="haySoplante"  />
	<input type="hidden" name="hayDisolvente"  />
	<input type="hidden" name="hayLavadoAbono"  />
	<input type="hidden" name="hayLavadoAcido"  />
	<input type="hidden" name="hayLavadoDisolvente"  />
<%
	if(isPlantilla){
%>
	<input type="hidden" name="isPlantilla" value="true"/>
<%
	}
%>
<%
	for (int l = 0; l < lstPosturas.length;l++){

%>
	<input type="hidden" 
		name="postura<%=lstPosturas[l].getIdposturas() %>" 
		value="<%=lstPosturas[l].getDuracion()%>" />
	<input type="hidden" 
		name="resultado<%=l %>" 
		id="resultado<%=l %>"
		value="" />

<%
	}
%>
</form>
<form name="tablaPosturas" >
<table class="listadoPosturas tablesorter" id="listado">
<%
	boolean isAbono = false;
	boolean isAcido = false;
	boolean isSoplante = false;
	boolean isDisolvente = false;
	boolean isLavadoAbono = false;
	boolean isLavadoAcido = false;
	boolean isLavadoDisolvente = false;
	
	for (int l = 0; l < nPosturas;l++){
		int auxInicioAbono = 0;
		int auxDuracionAbono = 0;
		int auxInicioLavadoAbono = 0;
		int auxDuracionLavadoAbono = 10;
		int auxInicioAcido = 0;
		int auxDuracionAcido = 0;
		int auxInicioLavadoAcido = 0;
		int auxDuracionLavadoAcido = 0;
		int auxInicioSoplante = 0;
		int auxDuracionSoplante = 0;
		int auxInicioDisolvente = 0;
		int auxDuracionDisolvente = 0;
		int auxInicioLavadoDisolvente = 0;
		int auxDuracionLavadoDisolvente = 0;

		if(abonos[l][0] != null){
			auxInicioAbono = abonos[l][0].getInicio();
			auxDuracionAbono = abonos[l][0].getDuracion();
			if(auxDuracionAbono > 0 && isCambio)
				isAbono = true;
		}
		if(abonos[l][1] != null){
			auxInicioAcido = abonos[l][1].getInicio();
			auxDuracionAcido = abonos[l][1].getDuracion();
			if(auxDuracionAcido > 0 && isCambio)
				isAcido = true;
		}
		if(abonos[l][2] != null){
			auxInicioSoplante = abonos[l][2].getInicio();
			auxDuracionSoplante = abonos[l][2].getDuracion();
			if(auxDuracionSoplante > 0 && isCambio)
				isSoplante = true;
		}
		if(abonos[l][3] != null){
			auxInicioLavadoAbono = abonos[l][3].getInicio();
			auxDuracionLavadoAbono = abonos[l][3].getDuracion();
			if(auxDuracionLavadoAbono > 0 && isCambio)
				isLavadoAbono = true;
		}
		if(abonos[l][4] != null){
			auxInicioLavadoAcido = abonos[l][4].getInicio();
			auxDuracionLavadoAcido = abonos[l][4].getDuracion();
			if(auxDuracionLavadoAcido > 0 && isCambio)
				isLavadoAcido = true;
		}
		if(abonos[l][5] != null){
			auxInicioDisolvente = abonos[l][5].getInicio();
			auxDuracionDisolvente = abonos[l][5].getDuracion();
			if(auxDuracionDisolvente > 0 && isCambio)
				isDisolvente = true;
		}

		if(abonos[l][6] != null){
			auxInicioLavadoDisolvente = abonos[l][6].getInicio();
			auxDuracionLavadoDisolvente = abonos[l][6].getDuracion();
			if(auxDuracionLavadoDisolvente > 0 && isCambio)
				isLavadoDisolvente = true;
		}

		int auxIdPosturas = lstPosturas[l].getIdposturas();
		int idParcelaSelect = lstPosturas[l].getIdParcelas();
		String auxParcelaSelecionada = "";
		int auxDuracion = lstPosturas[l].getDuracion();
		int auxHoras = auxDuracion/60;
		int auxMinutos = auxDuracion%60;
		String textoMinutos = (auxMinutos < 10)?"0"+auxMinutos:""+auxMinutos;
		String textoDuracion = auxHoras + ":" + (textoMinutos);
%>

	<thead> 
	<tr>
		<th width="25%">Parcela</th>
		<th width="25%" id="tiempo">Duración</th>
		<th width="25%">Postura</th>
		<th width="25%">Observaciones</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>Planificación <%=l + 1%></td>
		<td>
			<input type="text" id="postura<%=auxIdPosturas%>" size="1" value="<%=lstPosturas[l].getDuracion()%>" onchange="verificarDuracion()"/> 
			<input type="text" disabled="disabled" name="horaminuto<%=auxIdPosturas%>" value="<%=textoDuracion %>" class= "textoplano" style="width:35px" /> 
		
		</td>
		<td>
			<select id="parcela<%=auxIdPosturas%>" name="parcela">
<%
		for(int par = 0; par < lstParcelas.length; par++ ){
			if(lstParcelas[par].getIdparcela() == idParcelaSelect)
				auxParcelaSelecionada = "selected";
			else
				auxParcelaSelecionada = "";
%>
				<option value="<%=lstParcelas[par].getIdparcela() %>" <%=auxParcelaSelecionada %> ><%=lstParcelas[par].getDescripcion() %></option>
<%
		}

		String auxAbonoDisabled = (!isAbono)?"disabled=\"disabled\"":"";
		String auxAcidoDisabled = (!isAcido)?"disabled=\"disabled\"":"";
		String auxSoplanteDisabled = (!isSoplante)?"disabled=\"disabled\"":"";
		String auxDisolventeDisabled = (!isDisolvente)?"disabled=\"disabled\"":"";
		String auxLavadoAbonoDisabled = (!isLavadoAbono)?"disabled=\"disabled\"":"";
		String auxLavadoAcidoDisabled = (!isLavadoAcido)?"disabled=\"disabled\"":"";
		String auxLavadoDisolventeDisabled = (!isLavadoDisolvente)?"disabled=\"disabled\"":"";

%>
			</select>
			</td>
		<td>
			<input readonly type="text" id="observacion<%=auxIdPosturas%>"  value="<%=lstPosturas[l].getObservaciones() %>" 
			name="observacion<%=auxIdPosturas%>"  value="<%=lstPosturas[l].getObservaciones() %>" onclick="abrirObservacion(<%=auxIdPosturas%>)"/>
		</td>		
	</tr>
	</tbody>
	<thead> 
	<tr>
		<th>Abono <input type="checkbox" id="conAbono" onclick="activarAbonos(<%=lstPosturas.length%>)"/></th>
		<th>Soplante <input type="checkbox" id="conSoplante" onclick="activarSoplantes(<%=lstPosturas.length%>)"/></th>
		<th>Acido <input type="checkbox" id="conAcido" onclick="activarAcidos(<%=lstPosturas.length%>)"/></th>
		<th>Disolvente <input type="checkbox" id="conDisolvente" onclick="activarDisolvente(<%=lstPosturas.length%>)"/></th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>
			<select id="abono<%=auxIdPosturas%>" name="inicio" <%=auxAbonoDisabled %>>
					<option value="1" selected="selected">Iniciar despues de:</option>
					<option value="2">Terminar antes de:</option>
			</select>
			<input type="text" id="abonoIni<%=auxIdPosturas%>" value="<%=auxInicioAbono %>" size="1" <%=auxAbonoDisabled %>> min. - 
			Duración: <input type="text" id="abonoTiempo<%=auxIdPosturas%>" value="<%=auxDuracionAbono %>" size="1" <%=auxAbonoDisabled %>> min.
			
		</td>
		<td>
			<select id="soplante<%=auxIdPosturas%>" name="inicio" <%=auxSoplanteDisabled %> >
					<option value="1" selected="selected">Iniciar despues de:</option>
					<option value="2">Terminar antes de:</option>
			</select>
			<input type="text" id="soplanteIni<%=auxIdPosturas%>" value="<%=auxInicioSoplante %>" size="1" <%=auxSoplanteDisabled %>> min. - 
			Duración: <input type="text" id="soplanteTiempo<%=auxIdPosturas%>" value="<%=auxDuracionSoplante %>" size="1" <%=auxSoplanteDisabled %>> min.
		</td>
		<td>
			<select id="acido<%=auxIdPosturas%>" name="inicio" <%=auxAcidoDisabled %> >
					<option value="1" selected="selected">Iniciar despues de:</option>
					<option value="2">Terminar antes de:</option>
			</select>
			<input type="text" id="acidoIni<%=auxIdPosturas%>" value="<%=auxInicioAcido %>" size="1" <%=auxAcidoDisabled %>> min. - 
			Duración: <input type="text" id="acidoTiempo<%=auxIdPosturas%>" value="<%=auxDuracionAcido %>" size="1" <%=auxAcidoDisabled %>> min.
		</td>
		<td>
			<select id="disolvente<%=auxIdPosturas%>" name="inicio" <%=auxDisolventeDisabled %> >
					<option value="1" selected="selected">Iniciar despues de:</option>
					<option value="2">Terminar antes de:</option>
			</select>
			<input type="text" id="disolventeIni<%=auxIdPosturas%>" value="<%=auxInicioDisolvente %>" size="1" <%=auxDisolventeDisabled %>> min. - 
			Duración: <input type="text" id="disolventeTiempo<%=auxIdPosturas%>" value="<%=auxDuracionDisolvente %>" size="1" <%=auxDisolventeDisabled %>> min.
		</td>
	</tr>
	</tbody>
	<thead> 
	<tr>
		<th>Lavado Abono <input type="checkbox" id="conLavadoAbono" onclick="activarLavadoAbono(<%=lstPosturas.length%>)"/></th>
		<th>&nbsp;</th>
		<th>Lavado Acido <input type="checkbox" id="conLavadoAcido" onclick="activarLavadoAcido(<%=lstPosturas.length%>)"/></th>
		<th>Lavado Disolvente <input type="checkbox" id="conLavadoDisolvente" onclick="activarLavadoDisolvente(<%=lstPosturas.length%>)"/></th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>
			<input type="hidden" id="lavadoAbonoIni<%=auxIdPosturas%>" value="<%=auxInicioLavadoAbono %>" size="1" <%=auxLavadoAbonoDisabled %>> 
			Duración: <input type="text" id="lavadoAbonoTiempo<%=auxIdPosturas%>" value="<%=auxDuracionLavadoAbono %>" size="1" <%=auxLavadoAbonoDisabled %>> min.
		</td>
		<td>
			&nbsp;
		</td>
		<td>
			<input type="hidden" id="labadoAcidoIni<%=auxIdPosturas%>" value="<%=auxInicioLavadoAcido %>" size="1" <%=auxLavadoAcidoDisabled %>> 
			Duración: <input type="text" id="lavadoAcidoTiempo<%=auxIdPosturas%>" value="<%=auxDuracionLavadoAcido %>" size="1" <%=auxLavadoAcidoDisabled %>> min.
		</td>
		<td>
			<input type="hidden" id="labadoDisolventeIni<%=auxIdPosturas%>" value="<%=auxInicioLavadoDisolvente %>" size="1" <%=auxLavadoDisolventeDisabled %>> 
			Duración: <input type="text" id="lavadoDisolventeTiempo<%=auxIdPosturas%>" value="<%=auxDuracionLavadoDisolvente %>" size="1" <%=auxLavadoDisolventeDisabled %>> min.
		</td>
	<tr>
	</tbody>
	<script type="text/javascript">
		idPosturas[<%=l%>] = <%=auxIdPosturas%>;
		if(<%=isAbono%>)
			this.document.getElementById("conAbono").checked = true;
		if(<%=isAcido%>)
			this.document.getElementById("conAcido").checked = true;
		if(<%=isSoplante%>)
			this.document.getElementById("conSoplante").checked = true;
		if(<%=isLavadoAbono%>)
			this.document.getElementById("conLavadoAbono").checked = true;
		if(<%=isLavadoAcido%>)
			this.document.getElementById("conLavadoAcido").checked = true;
		if(<%=isDisolvente%>)
			this.document.getElementById("conDisolvente").checked = true;
		if(<%=isLavadoDisolvente%>)
			this.document.getElementById("conLavadoDisolvente").checked = true;
			
	</script>
<%		
	}
%>
</table>
</form>
<p>
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Guardar" alt="Guardar"  onclick="configPosturas(<%=lstPosturas.length%>)"/>
				</li>
				<li>
					<input type="button" class="btn" value="Cerrar" alt="Cerrar" onclick="CancelarNueva()" />
				</li>
			</ul>
</p>

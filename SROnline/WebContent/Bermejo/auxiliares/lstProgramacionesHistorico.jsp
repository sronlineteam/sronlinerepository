<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.Sectores"%>
<%@page import="es.sronline.bermejo.ProgramacionPosturasFormateada"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
List<ProgramacionPosturasFormateada> lstProgramaciones;
Object obj = request.getAttribute("lstProgramacionesPosturas");

String auxDia;
String auxHora;
// SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
// SimpleDateFormat sdfhora=new SimpleDateFormat("hh:mm");
 Sectores[] lstSectores = null;
 Object objVal = request.getAttribute("lstSectores");
 if (objVal != null)
	 lstSectores = (Sectores[]) objVal; 

int auxIdProgramacion;

if (obj != null){
	lstProgramaciones = (List<ProgramacionPosturasFormateada>) obj; 

String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 }
 
 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
 String fHoy =  format.format(now);

%>
<script type="text/javascript">

	function CancelarConsultas(){
		document.location.href = 'SrvSectores';
	}

 function Consultar(){
		this.document.getElementById("formularioModalConsulta").style.display ="block";
	}

	function cancelarConsulta(){
		this.document.getElementById("formularioModalConsulta").style.display ="none";
	}

	function enviarConsulta(){
		this.document.forms.consultarProg.ConsultaDesde.value = this.document.getElementById("datetimepickerConsultaDesde").value;
		this.document.forms.consultarProg.ConsultaHasta.value = this.document.getElementById("datetimepickerConsultaHasta").value;
		this.document.forms.consultarProg.submit();
	}

</script>
<%
	if(lstProgramaciones.size()>0){
%>
<script type="text/javascript">

	$(function() {
		$("#listado").tablesorter({sortList:[[2,0],[3,0]], widgets: ['zebra']});
		$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	});


</script>
<%
	}
%>
<div class="modal" id="formularioModalConsulta" style="display:none" >
	<div class="formularioModal" >
		<h2>Consultar programaciones</h2>
		<form action="SrvSectores" name="consultarProg" method="post" >
			<input type="hidden" name="accion" value="<%=Constantes.getAccionConsultar()%>" />
			<p>
				Indique d�a desde 
			</p>
			<p>
				<input type="hidden" name="ConsultaDesde"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker30'>
					                    <input type='text' class="form-control" id="datetimepickerConsultaDesde" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker30').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			<p>
				Indique d�a hasta 
			</p>
			<p>
				<input type="hidden" name="ConsultaHasta"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker31'>
					                    <input type='text' class="form-control" id="datetimepickerConsultaHasta" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker31').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Consultar" alt="Consultar" onclick="enviarConsulta()" />
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="cancelarConsulta()" />
				</li>
			</ul>
		</form>
		
	</div>

</div>

<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<table class="tablesorter" id="listado">
	<thead> 
	<tr>
		<th>Parcela</th>
		<th>Postura</th>
		<th>D�a</th>
		<th>Hora Inicio</th>
		<th>Hora Fin</th>
		<th>Duracion</th>
		<th>Inicio Abono</th>
		<th>Fin Abono</th>
		<th>D. Abono</th>
		<th>Inicio Acido</th>
		<th>Fin Acido</th>
		<th>D. Acido</th>
		<th>Observaciones</th>
	</tr>
	</thead>
	<tbody> 
<%
	String auxInicioAbono;
	String auxInicioAcido;
	String auxFinAbono;
	String auxFinAcido;
	String auxSaltoLinea = "";
	List<String> auxPosturas = new ArrayList<String>(); 
	for (int l = 0; l < lstProgramaciones.size();l++){
		auxDia = lstProgramaciones.get(l).getFechaFormateada();
		auxHora = lstProgramaciones.get(l).getHoraFormateada();
		auxIdProgramacion = lstProgramaciones.get(l).getIdprogramacion_Postura();
		if(lstProgramaciones.get(l).getDuracionAbono() > 0 ){ 
			auxInicioAbono = lstProgramaciones.get(l).getInicioAbono();
			auxFinAbono = lstProgramaciones.get(l).getFinAbono();
		}
		else{ 
			auxInicioAbono = "-";
			auxFinAbono = "-";
		}
		if(lstProgramaciones.get(l).getDuracionAcido() > 0 ){ 
		 	auxInicioAcido = lstProgramaciones.get(l).getInicioAcido();
			auxFinAcido = lstProgramaciones.get(l).getFinAcido();
		}
		else{ 
			auxInicioAcido = "-";
			auxFinAcido = "-";
		}
		
%>
	<tr >
		<td><%=lstProgramaciones.get(l).getNombreSector()%></td>
		<td>
<%
	auxPosturas = lstProgramaciones.get(l).getPosturas();
	for (int p = 0; p < auxPosturas.size(); p++){
%>
		<%=auxPosturas.get(p) %>
		<%=auxSaltoLinea %>		
<%
	if(p==0)
		auxSaltoLinea = "<br/>";
	}
%>
		</td>
		<td><%=auxDia%></td>
		<td><%=auxHora%></td>
		<td><%=lstProgramaciones.get(l).getHoraFinFormateada()%></td>
		<td><%=lstProgramaciones.get(l).getDuracion()%></td>
		<td><%=auxInicioAbono%></td>
		<td><%=auxFinAbono%></td>
		<td><%=lstProgramaciones.get(l).getDuracionAbono()%></td>
		<td><%=auxInicioAcido%></td>
		<td><%=auxFinAcido%></td>
		<td><%=lstProgramaciones.get(l).getDuracionAcido()%></td>
		<td><%=lstProgramaciones.get(l).getObservaciones()%></td>
<%
	}
%>
		
	</tr>

</table>
<p>
	<input type="button"  class="btn" value="Consulta" alt="Consulta" onclick="Consultar()"/>
</p>

<%
}
else{
%>
<h2>No hay ninguna programaci�n</h2>
<%} %>


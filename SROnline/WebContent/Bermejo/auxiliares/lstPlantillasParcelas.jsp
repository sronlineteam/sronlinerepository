<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.Sectores"%>
<%@page import="es.sronline.bermejo.ProgramacionPosturasFormateada"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
List<ProgramacionPosturasFormateada> lstProgramaciones;
Object obj = request.getAttribute("lstProgramacionesPosturas");

String auxDia;
String auxHora;
// SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
// SimpleDateFormat sdfhora=new SimpleDateFormat("hh:mm");
 Sectores[] lstSectores = null;
 Object objVal = request.getAttribute("lstSectores");
 if (objVal != null)
	 lstSectores = (Sectores[]) objVal; 

int auxIdProgramacion;

if (obj != null){
	lstProgramaciones = (List<ProgramacionPosturasFormateada>) obj; 

String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 }
 
 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
 String fHoy =  format.format(now);

%>
 
<script type="text/javascript">
function cancelarCambio(){
		this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModalCambio").style.display ="none";
}

function CambiarProg(idProgramacion, dia, hora){
	this.document.forms.cambiarProg.idProgramacion.value = idProgramacion;
	this.document.forms.cambiarProg.datetimepickerDiaCambio.value = dia;
	this.document.forms.cambiarProg.datetimepickerHoraCambio.value = hora;
	this.document.getElementById("formularioModalCambio").style.display ="block";
}


function NuevaProg(){
	this.document.getElementById("formularioModalAlta").style.display ="block";
}

function CancelarPlantillas(){
	document.location.href = 'SrvSectores';
}

function CancelarNueva(){
// 	this.document.forms.altaPosturas.idProgramacion.value = "vacio";
	this.document.getElementById("formularioModalAlta").style.display ="none";
}

function eliminar(idProgramacion){
	if(confirm("Eliminar la programaci�n")){
		this.document.forms.eliminarProg.idProgramacion.value = idProgramacion;
		this.document.forms.eliminarProg.submit();
	}
}
function nuevasPosturas(){
	var ok = "si";
	var errores = "Indique: \n";
	var aux = this.document.getElementById("datetimepickerDia").value;
	if(aux == null || aux == ""){
		errores += " - D�a.\n";
		ok = "no";
	}
	this.document.forms.altaPosturas.cuando.value = aux;
	this.document.forms.altaPosturas.cuando.value += " ";

	aux = this.document.getElementById("datetimepickerHora").value;
	if(aux == null || aux == ""){
		errores += " - Hora.\n";
		ok = "no";
	}
	this.document.forms.altaPosturas.cuando.value += aux;
	
	aux = this.document.getElementById("datetimepickerDuracion").value;
	if(aux == null || aux == ""){
		errores += " - Duracion";
		ok = "no";
	}
	this.document.forms.altaPosturas.duracion.value = aux;
 	if(ok == "si")
		this.document.forms.altaPosturas.submit();
 	else
 		alert(errores);
	
}

function enviarCambio(){
	this.document.forms.cambiarProg.cuando.value = this.document.getElementById("datetimepickerDiaCambio").value;
	this.document.forms.cambiarProg.cuando.value += " ";
	this.document.forms.cambiarProg.cuando.value += this.document.getElementById("datetimepickerHoraCambio").value;
 	this.document.forms.cambiarProg.submit();
	
}

</script>

<%
	if(lstProgramaciones.size()>0){
%>
<script type="text/javascript">

	$(function() {
		$("#listado").tablesorter({sortList:[[0,0],[2,0]], widgets: ['zebra']});
		$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	});


</script>
<%
	}
%>

<div class="modal" id="formularioModalCambio" style="display:none" >
	<div class="formularioModal" >
		<h2>Copiar programaci�n</h2>
		<form action="SrvSectores" name="eliminarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="0" />
			<input type="hidden" name="isPlantilla" value="true"/>
			<input type="hidden" name="accion" value="<%=Constantes.getAccionEliminar()%>" />
		</form>
		<form action="SrvSectores" name="cambiarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="vacio" />
			<input type="hidden" name="accion" value="<%=Constantes.getAccionCopiar()%>" />
			<p>
				Indique el d�a 
			</p>
			<p>
				<input type="hidden" name="cuando"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker10'>
					                    <input type='text' class="form-control" id="datetimepickerDiaCambio" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker10').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			 	Indique hora
 				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker20'>
						            <input type='text' class="form-control" id='datetimepickerHoraCambio'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker20').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 
 			</p>
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Copiar" alt="Copiar" onclick="enviarCambio()" />
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="cancelarCambio()" />
				</li>
			</ul>
		</form>
		
	</div>

</div>

<div class="modal" id="formularioModalAlta" style="display:none" >
	<div class="formularioModal" >
		<h2>Plantilla</h2>
		<form action="SrvPosturas" name="altaPosturas" method="post" >
			<input type="hidden" name="accion" value="<%=Constantes.getAccionNuevaProgramacion()%>" />
			<input type="hidden" name="valvulas" value="vacio" />
			<input type="hidden" name="isPlantilla" value="true"/>
			<p>
				<select name="sector" id="sector">
<%
	String valChecked = "checked";
	for(int v = 0; v < lstSectores.length; v++){
%>
					<option value="<%=lstSectores[v].getIdSector()%>" <%=valChecked %> ><%=lstSectores[v].getNombre()%></option>
<%
	if(v==0)
		valChecked = "";
	} 
%>
				</select>
				<input type="hidden" name="cuando" />
				<input type='hidden' id="datetimepickerDia" value="<%=Constantes.getFechaParaPlantilla()%>"/>
			</p>
			<p>
				Indique hora de la acci�n
 				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" id='datetimepickerHora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 
			</p>
			<p>
				Indique duraci�n 
				<input type="hidden" id="inputField" name="duracion" value="06:00"/>
				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker3'>
				                    <input type='text' class="form-control" id="datetimepickerDuracion" value="01:00"/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker3').datetimepicker({
				                    format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			
				<input type="hidden" name="posturas" value="1"/>
			</p>			
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Aceptar" alt="Aceptar"  onclick="nuevasPosturas()"/>
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="CancelarNueva()" />
				</li>
			</ul>
		</form>
		
	</div>

</div>


<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<table class="tablesorter" id="listado">
	<thead> 
	<tr>
		<th>Parcela</th>
		<th>Postura</th>
		<th>D�a</th>
		<th>Hora Inicio</th>
		<th>Hora Fin</th>
		<th>Duracion</th>
		<th>Inicio Abono</th>
		<th>Fin Abono</th>
		<th>D. Abono</th>
		<th>Inicio Acido</th>
		<th>Fin Acido</th>
		<th>D. Acido</th>
		<th>Acci�n</th>
	</tr>
	</thead>
	<tbody> 
<%
	String auxInicioAbono;
	String auxInicioAcido;
	String auxFinAbono;
	String auxFinAcido;
	String auxSaltoLinea = "";
	List<String> auxPosturas = new ArrayList<String>(); 
	for (int l = 0; l < lstProgramaciones.size();l++){
		auxDia = lstProgramaciones.get(l).getFechaFormateada();
		auxHora = lstProgramaciones.get(l).getHoraFormateada();
		auxIdProgramacion = lstProgramaciones.get(l).getIdprogramacion_Postura();
		if(lstProgramaciones.get(l).getDuracionAbono() > 0 ){ 
			auxInicioAbono = lstProgramaciones.get(l).getInicioAbono();
			auxFinAbono = lstProgramaciones.get(l).getFinAbono();
		}
		else{ 
			auxInicioAbono = "-";
			auxFinAbono = "-";
		}
		if(lstProgramaciones.get(l).getDuracionAcido() > 0 ){ 
		 	auxInicioAcido = lstProgramaciones.get(l).getInicioAcido();
			auxFinAcido = lstProgramaciones.get(l).getFinAcido();
		}
		else{ 
			auxInicioAcido = "-";
			auxFinAcido = "-";
		}
		
%>
	<tr >
		<td><%=lstProgramaciones.get(l).getNombreSector()%></td>
		<td>
<%
	auxPosturas = lstProgramaciones.get(l).getPosturas();
	for (int p = 0; p < auxPosturas.size(); p++){
%>
		<%=auxPosturas.get(p) %>
		<%=auxSaltoLinea %>		
<%
	if(p==0)
		auxSaltoLinea = "<br/>";
	}
%>
		</td>
		<td><%=auxDia%></td>
		<td><%=auxHora%></td>
		<td><%=lstProgramaciones.get(l).getHoraFinFormateada()%></td>
		<td><%=lstProgramaciones.get(l).getDuracion()%></td>
		<td><%=auxInicioAbono%></td>
		<td><%=auxFinAbono%></td>
		<td><%=lstProgramaciones.get(l).getDuracionAbono()%></td>
		<td><%=auxInicioAcido%></td>
		<td><%=auxFinAcido%></td>
		<td><%=lstProgramaciones.get(l).getDuracionAcido()%></td>
		<td>
		<div style="text-align: center" >
		<input type="button" class="btnCambiar" value="" alt="Cambiar" onclick="CambiarProg(<%=auxIdProgramacion%>, '<%=auxDia%>', '<%=auxHora%>')"/>
		<input type="button" class="btnEliminar" value="" alt="Eliminar" onclick="eliminar(<%=auxIdProgramacion%>)"/></div></td>
<%
	}
%>
		
	</tr>

</table>
<p>
	<input type="button"  class="btn" value="Nueva Plantilla" alt="Nueva" onclick="NuevaProg()"/>
</p>
<%
}
else{
%>
<h1>Listado de acciones sobre v�vlulas</h1>
<h2>No hay ninguna plantilla</h2>
<%} %>


<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.Sectores"%>
<%@page import="es.sronline.bermejo.ProgramacionPosturasFormateada"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
String isMonitorizacion = "false";
if(request.getParameter("isMonitorizacion")!=null)
	isMonitorizacion = (String) request.getParameter("isMonitorizacion");

List<ProgramacionPosturasFormateada> lstProgramaciones;
Object obj = request.getAttribute("lstProgramacionesPosturas");
Object objPagina = request.getAttribute("monitorizacion");

String auxDia;
String auxHora;
// SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
// SimpleDateFormat sdfhora=new SimpleDateFormat("hh:mm");
 Sectores[] lstSectores = null;
 Object objVal = request.getAttribute("lstSectores");
 if (objVal != null)
	 lstSectores = (Sectores[]) objVal; 

int auxIdProgramacion;

if (obj != null){
	lstProgramaciones = (List<ProgramacionPosturasFormateada>) obj; 

String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 }
 
 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
 String fHoy =  format.format(now);

%>
 
<script type="text/javascript">
function cancelarCambio(){
		this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModalCambio").style.display ="none";
}

function CambiarProg(idProgramacion, dia, hora){
	this.document.forms.cambiarProg.idProgramacion.value = idProgramacion;
	this.document.forms.cambiarProg.datetimepickerDiaCambio.value = dia;
	this.document.forms.cambiarProg.datetimepickerHoraCambio.value = hora;
	this.document.getElementById("formularioModalCambio").style.display ="block";
}


function NuevaProg(){
	this.document.getElementById("formularioModalAlta").style.display ="block";
}

function Plantillas(){
	document.location.href = 'SrvCopiarParcela';
}

function Actualizar(){
	document.location.href = 'SrvSectores?isMonitorizacion=true';
}

function CancelarNueva(){
// 	this.document.forms.altaPosturas.idProgramacion.value = "vacio";
	this.document.getElementById("formularioModalAlta").style.display ="none";
}

function eliminar(idProgramacion){
	if(confirm("Eliminar la programaci�n")){
		this.document.forms.eliminarProg.idProgramacion.value = idProgramacion;
		this.document.forms.eliminarProg.submit();
	}
}
function nuevasPosturas(){
	var ok = "si";
	var errores = "Indique: \n";
	var aux = this.document.getElementById("datetimepickerDia").value;
	if(aux == null || aux == ""){
		errores += " - D�a.\n";
		ok = "no";
	}
	this.document.forms.altaPosturas.cuando.value = aux;
	this.document.forms.altaPosturas.cuando.value += " ";

	aux = this.document.getElementById("datetimepickerHora").value;
	if(aux == null || aux == ""){
		errores += " - Hora.\n";
		ok = "no";
	}
	this.document.forms.altaPosturas.cuando.value += aux;
	
	aux = this.document.getElementById("datetimepickerDuracion").value;
	if(aux == null || aux == ""){
		errores += " - Duracion";
		ok = "no";
	}
	this.document.forms.altaPosturas.duracion.value = aux;
 	if(ok == "si")
		this.document.forms.altaPosturas.submit();
 	else
 		alert(errores);
	
}

function enviarCambio(){
	this.document.forms.cambiarProg.cuando.value = this.document.getElementById("datetimepickerDiaCambio").value;
	this.document.forms.cambiarProg.cuando.value += " ";
	this.document.forms.cambiarProg.cuando.value += this.document.getElementById("datetimepickerHoraCambio").value;
 	this.document.forms.cambiarProg.submit();
	
}

function Consultar(){
	this.document.getElementById("formularioModalConsulta").style.display ="block";
}

function cancelarConsulta(){
	this.document.getElementById("formularioModalConsulta").style.display ="none";
}

function enviarConsulta(){
	this.document.forms.consultarProg.ConsultaDesde.value = this.document.getElementById("datetimepickerConsultaDesde").value;
	this.document.forms.consultarProg.ConsultaHasta.value = this.document.getElementById("datetimepickerConsultaHasta").value;
	this.document.forms.consultarProg.submit();
}

</script>

<%
	if(lstProgramaciones.size()>0){
%>
<script type="text/javascript">

	$(function() {
		$("#listado").tablesorter({sortList:[[2,0],[3,0]], widgets: ['zebra']});
		$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	});


</script>
<%
	}
%>

<div class="modal" id="formularioModalCambio" style="display:none" >
	<div class="formularioModal" >
		<h2>Cambiar programaci�n</h2>
		<form action="SrvSectores" name="eliminarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="0" />
			<input type="hidden" name="accion" value="<%=Constantes.getAccionEliminar()%>" />
		</form>
		<form action="SrvSectores" name="cambiarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="vacio" />
			<input type="hidden" name="accion" value="<%=Constantes.getAccionCambiar()%>" />
			<p>
				Indique nuevo d�a 
			</p>
			<p>
				<input type="hidden" name="cuando"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker10'>
					                    <input type='text' class="form-control" id="datetimepickerDiaCambio" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker10').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			 	Indique nueva hora
 				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker20'>
						            <input type='text' class="form-control" id='datetimepickerHoraCambio'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker20').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 
 			</p>
 			<p>
 				<input type="checkbox" name="cambiarPosturas" value="true" /> Cambiar planificaciones.
 			</p>
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Cambiar" alt="Cambiar" onclick="enviarCambio()" class="btn"/>
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="cancelarCambio()" class="btn"/>
				</li>
			</ul>
		</form>
		
	</div>

</div>

<div class="modal" id="formularioModalAlta" style="display:none" >
	<div class="formularioModal" >
		<h2>Programaci�n</h2>
		<form action="SrvPosturas" name="altaPosturas" method="post" >
			<input type="hidden" name="accion" value="<%=Constantes.getAccionNuevaProgramacion()%>" />
			<input type="hidden" name="valvulas" value="vacio" />
			<p>
				<select name="sector" id="sector">
<%
	String valChecked = "checked";
	for(int v = 0; v < lstSectores.length; v++){
%>
					<option value="<%=lstSectores[v].getIdSector()%>" <%=valChecked %> ><%=lstSectores[v].getNombre()%></option>
<%
	if(v==0)
		valChecked = "";
	} 
%>
				</select>
			</p> 
			<p>
				Indique d�a de la acci�n
				<input type="hidden" name="cuando"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker1'>
					                    <input type='text' class="form-control" id="datetimepickerDia" value="<%=fHoy%>"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker1').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>

			</p>
			<p>
				Indique hora de la acci�n
 				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" id='datetimepickerHora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 
			</p>
			<p>
				Indique duraci�n 
				<input type="hidden" id="inputField" name="duracion" value="06:00"/>
				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker3'>
				                    <input type='text' class="form-control" id="datetimepickerDuracion" value="01:00"/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker3').datetimepicker({
				                    format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			
				<input type="hidden" name="posturas" value="1"/>
			</p>			
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Aceptar" alt="Aceptar"  onclick="nuevasPosturas()" class="btn"/>
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="CancelarNueva()" class="btn"/>
				</li>
			</ul>
		</form>
		
	</div>

</div>

<div class="modal" id="formularioModalConsulta" style="display:none" >
	<div class="formularioModal" >
		<h2>Consultar programaciones</h2>
		<form action="SrvSectores" name="consultarProg" method="post" >
			<input type="hidden" name="accion" value="<%=Constantes.getAccionConsultar()%>" />
			<p>
				Indique d�a desde 
			</p>
			<p>
				<input type="hidden" name="ConsultaDesde"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker30'>
					                    <input type='text' class="form-control" id="datetimepickerConsultaDesde" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker30').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			<p>
				Indique d�a hasta 
			</p>
			<p>
				<input type="hidden" name="ConsultaHasta"/>
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker31'>
					                    <input type='text' class="form-control" id="datetimepickerConsultaHasta" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker31').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					</div>
 				</p>
			<p>
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Consultar" alt="Consultar" onclick="enviarConsulta()" class="btn"/>
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="cancelarConsulta()" class="btn"/>
				</li>
			</ul>
		</form>
		
	</div>

</div>


<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<table class="tablesorter" id="listado">
	<thead> 
	<tr>
		<th>Parcela</th>
		<th>Postura</th>
		<th>D�a</th>
		<th>Hora<br>Inicio</th>
		<th>Hora<br>Fin</th>
		<th>Durac.</th>
		<th>Ini.<br>Abono</th>
		<th>Fin<br>Abono</th>
		<th>Dur.<br>Abono</th>
		<th>Lav.<br>Abono</th>
		<th>Ini.<br>Soplte</th>
		<th>Fin<br>Soplte</th>
		<th>Dur.<br>Soplte</th>
		<th>Ini.<br>Acido</th>
		<th>Fin<br>Acido</th>
		<th>Dur.<br>Acido</th>
		<th>Lav.<br>Acido</th>
		<th>Ini.<br>Disol.</th>
		<th>Fin<br>Disol.</th>
		<th>Dur.<br>Disol.</th>
		<th>Lav.<br>Disol.</th>
		<th>Acci�n</th>
	</tr>
	</thead>
	<tbody> 
<%
	String auxInicioAbono;
	String auxInicioSoplante;
	String auxInicioAcido;
	String auxInicioDisolvente;
	String auxFinAbono;
	String auxFinSoplante;
	String auxFinAcido;
	String auxFinDisolvente;
	String auxSaltoLinea = "";
	String auxDuracionAbono = "-";
	String auxDuracionSoplante = "-";
	String auxDuracionAcido = "-";
	String auxDuracionDisolvente = "-";
	String auxDuracionLavadoAbono = "-";
	String auxDuracionLavadoAcido = "-";
	String auxDuracionLavadoDisolvente = "-";
	List<String> auxPosturas = new ArrayList<String>(); 
	for (int l = 0; l < lstProgramaciones.size();l++){
		auxDia = lstProgramaciones.get(l).getFechaFormateada();
		auxHora = lstProgramaciones.get(l).getHoraFormateada();
		auxIdProgramacion = lstProgramaciones.get(l).getIdprogramacion_Postura();

		if(lstProgramaciones.get(l).getDuracionAbono() > 0 ){ 
			auxInicioAbono = lstProgramaciones.get(l).getInicioAbono();
			auxFinAbono = lstProgramaciones.get(l).getFinAbono();
			auxDuracionAbono = "" + lstProgramaciones.get(l).getDuracionAbono();
		}
		else{ 
			auxInicioAbono = "-";
			auxFinAbono = "-";
			auxDuracionAbono = "�";
		}
		if(lstProgramaciones.get(l).getDuracionSoplante() > 0 ){ 
		 	auxInicioSoplante = lstProgramaciones.get(l).getInicioSoplante();
			auxFinSoplante = lstProgramaciones.get(l).getFinSoplante();
			auxDuracionSoplante = "" + lstProgramaciones.get(l).getDuracionSoplante();
		}
		else{ 
			auxInicioSoplante = "-";
			auxFinSoplante = "-";
			auxDuracionSoplante = "�";
		}
		if(lstProgramaciones.get(l).getDuracionAcido() > 0 ){ 
		 	auxInicioAcido = lstProgramaciones.get(l).getInicioAcido();
			auxFinAcido = lstProgramaciones.get(l).getFinAcido();
			auxDuracionAcido = "" + lstProgramaciones.get(l).getDuracionAcido();
		}
		else{ 
			auxInicioAcido = "-";
			auxFinAcido = "-";
			auxDuracionAcido = "�";
		}
		if(lstProgramaciones.get(l).getDuracionDisolvente() > 0 ){ 
		 	auxInicioDisolvente = lstProgramaciones.get(l).getInicioDisolvente();
			auxFinDisolvente = lstProgramaciones.get(l).getFinDisolvente();
		}
		else{ 
			auxInicioDisolvente = "-";
			auxFinDisolvente = "-";
		}
		if(lstProgramaciones.get(l).getDuracionLavadoDisolvente()>0)
			auxDuracionDisolvente = "" + lstProgramaciones.get(l).getDuracionDisolvente();
		else
			auxDuracionDisolvente = "�";
		if(lstProgramaciones.get(l).getDuracionLavadoAbono()>0)
			auxDuracionLavadoAbono = "" + lstProgramaciones.get(l).getDuracionLavadoAbono();
		else
			auxDuracionLavadoAbono = "�";
		if(lstProgramaciones.get(l).getDuracionLavadoAcido()>0)
			auxDuracionLavadoAcido = "" + lstProgramaciones.get(l).getDuracionLavadoAcido();
		else
			auxDuracionLavadoAcido = ".";
		if(lstProgramaciones.get(l).getDuracionLavadoDisolvente()>0)
			auxDuracionLavadoDisolvente = "" + lstProgramaciones.get(l).getDuracionLavadoDisolvente();
		else
			auxDuracionLavadoDisolvente = "�";
		
%>
	<tr >
		<td><%=lstProgramaciones.get(l).getNombreSector()%></td>
		<td>
<%
	auxPosturas = lstProgramaciones.get(l).getPosturas();
	for (int p = 0; p < auxPosturas.size(); p++){
%>
		<%=auxPosturas.get(p) %>
		<%=auxSaltoLinea %>		
<%
	if(p==0)
		auxSaltoLinea = "<br/>";
	}
%>
		</td>
		<td><%=auxDia%></td>
		<td><%=auxHora%></td>
		<td><%=lstProgramaciones.get(l).getHoraFinFormateada()%></td>
		<td><%=lstProgramaciones.get(l).getDuracion()%></td>
		<td><%=auxInicioAbono%></td>
		<td><%=auxFinAbono%></td>
		<td><%=auxDuracionAbono%></td>
		<td><%=auxDuracionLavadoAbono%></td>
		<td><%=auxInicioSoplante%></td>
		<td><%=auxFinSoplante%></td>
		<td><%=auxDuracionSoplante%></td>
		<td><%=auxInicioAcido%></td>
		<td><%=auxFinAcido%></td>
		<td><%=auxDuracionAcido%></td>
		<td><%=auxDuracionLavadoAcido%></td>
		<td><%=auxInicioDisolvente%></td>
		<td><%=auxFinDisolvente%></td>
		<td><%=auxDuracionDisolvente%></td>
		<td><%=auxDuracionLavadoDisolvente%></td>
		<td>
<%
		if(lstProgramaciones.get(l).getFechaMili() > now.getTime()){
%>
			<div style="text-align: center">		
<%	
		}
		else{
%>
		<div style="background-color: green;color: white;text-align: center" >
<%
		}
%>
		<input type="button" class="btnCambiar" value="" alt="Cambiar" onclick="CambiarProg(<%=auxIdProgramacion%>, '<%=auxDia%>', '<%=auxHora%>')" class="btn"/>
		<input type="button" class="btnEliminar" value="" alt="Eliminar" onclick="eliminar(<%=auxIdProgramacion%>)" class="btn"/></div></td>
<%
	}
%>
		
	</tr>

</table>
<p>
	<input type="button"  value="Nueva" alt="Nueva" onclick="NuevaProg()" class="btn"/>
	<input type="button"  value="Consulta" alt="Consuta" onclick="Consultar()" class="btn"/>
<%if(isMonitorizacion.equals("true")){ %>
	<input type="button"  value="Actualizar" alt="Actualizar" onclick="Actualizar()" class="btn"/>
<%} %>
</p>
<%
	if(isMonitorizacion.equals("true")){ 
%>
<!--  		<div class="contenido" id="manometro"> -->
<%-- 			<jsp:include page="auxiliares/manometro.jsp"> --%>
<%-- 				<jsp:param name="manometroConexion" value="83.230.251.104/502" /> --%>
<%-- 				<jsp:param name="manometroEtiquetas" value="Presion/Otro" /> --%>
<%-- 				<jsp:param name="manometroTrazas" value="000000000006FF04069F0001/000000000006FF0406A00001" /> --%>
<%-- 				<jsp:param name="manometroIntervalo" value="10000" /> --%>
<%-- 				<jsp:param name="manometroPosicionDatos" value="16-18/16-18" /> --%>
				
<%-- 			</jsp:include> --%>
<!-- 		</div> -->
		<div class="" id="webvisu" style="transform: scale(0.7);position: relative; margin-left: -300px" >
		<table width="100%"  >
			<tr height="100%" >
				<td width="33%" >
					<iframe src="http://83.230.251.104:8082/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
				<td width="33%" >
					<iframe src="http://83.230.251.104:8083/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
				<td width="33%" >
					<iframe src="http://83.230.251.104:8084/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
			</tr>
		</table>
		</div>
<%
	}
%>

<%
}
else{
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listado de Valvulas</title>
<script type="text/javascript">
</script>
<link rel="stylesheet" type="text/css" href="resources/css/regadio.css" media="all">
</head>
<body>
<h1>Listado de acciones sobre v�vlulas</h1>
<h2>No hay ninguna programaci�n</h2>
<%} %>

</body>
</html>

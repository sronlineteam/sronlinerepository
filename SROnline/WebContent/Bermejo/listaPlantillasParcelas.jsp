<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Plantillas de Procesos de Riego</title>
		<%@ include file="../auxiliares/header.html" %>
		<%@ include file="../auxiliares/headerListas.html" %>
		<%@ include file="auxiliares/headerPosturas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="../auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de Plantillas de Procesos de Riego</h1>
			<%@ include file="auxiliares/lstPlantillasParcelas.jsp" %>
		</div>
	</body>
</html>

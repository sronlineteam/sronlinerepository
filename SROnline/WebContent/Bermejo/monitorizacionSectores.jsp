<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Procesos de Riego</title>
		<%@ include file="../auxiliares/header.html" %>
		<%@ include file="../auxiliares/headerListas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="../auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de Procesos de Riego</h1>
			<%@ include file="auxiliares/lstSectores.jsp" %>
		</div>
<!-- 		<div class="contenido" id="manometro"> -->
<%-- 			<jsp:include page="auxiliares/manometro.jsp"> --%>
<%-- 				<jsp:param name="manometroConexion" value="83.230.251.104/502" /> --%>
<%-- 				<jsp:param name="manometroEtiquetas" value="Presion/Otro" /> --%>
<%-- 				<jsp:param name="manometroTrazas" value="000000000006FF04069F0001/000000000006FF0406A00001" /> --%>
<%-- 				<jsp:param name="manometroIntervalo" value="10000" /> --%>
<%-- 				<jsp:param name="manometroPosicionDatos" value="16-18/16-18" /> --%>
				
<%-- 			</jsp:include> --%>
<!-- 		</div> -->
		<div class="" id="webvisu" style="transform: scale(0.8);position: relative; margin-left: -100px" >
		<table width="100%"  >
			<tr height="100%" >
				<td width="33%" >
					<iframe src="http://83.230.251.104:8082/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
				<td width="33%" >
					<iframe src="http://83.230.251.104:8083/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
				<td width="33%" >
					<iframe src="http://83.230.251.104:8084/webvisu.htm" width="750px" height="500px" ></iframe>
				</td>
			</tr>
		</table>
		</div>


	</body>
</html>
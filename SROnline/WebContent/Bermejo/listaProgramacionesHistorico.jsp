<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Consulta de Procesos Riego</title>
		<%@ include file="../auxiliares/header.html" %>
		<%@ include file="../auxiliares/headerListas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="../auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Consulta del histórico de Proceso de Riego</h1>
			<%@ include file="auxiliares/lstProgramacionesHistorico.jsp" %>
		</div>
	</body>
</html>

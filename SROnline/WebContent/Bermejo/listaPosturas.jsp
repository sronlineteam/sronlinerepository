<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de acciones del Riego</title>
		<%@ include file="../auxiliares/header.html" %>
		<%@ include file="../auxiliares/headerListas.html" %>
		<%@ include file="auxiliares/headerPosturas.html" %>
	</head>
	<body>
		<div class="cabecera">
				<%@ include file="../auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de acciones del Proceso de Riego</h1>
			<%@ include file="auxiliares/lstPosturas.jsp" %>
		</div>
	</body>
</html>

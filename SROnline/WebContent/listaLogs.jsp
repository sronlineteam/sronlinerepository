<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de logs</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
</head>
<body>
	<div class="cabecera">
			<%@ include file="auxiliares/menu.jsp" %>
	</div>
	<div class="contenido" id="contenido">
		<h1>Listado de logs</h1>
		<%@ include file="admin/viewLog.jsp" %>
	</div>

</body>
</html>
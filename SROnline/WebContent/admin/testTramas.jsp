<%@page import="es.sronline.core.Constantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
String resultado = "";
Object obj = request.getAttribute("resultado");
if (obj != null)
	resultado = (String) obj; 

%>

	<h1>Test de comunicaci�n</h1>
	<form name="test" action="SrvTestComunicacion" method="post" >
		<label>URL/IP:</label><input type="text" name="ip" value="<%=Constantes.getHostIP()%>"/><br/>
		<label>Puerto: </label><input type="text" name="puerto" value="<%=Constantes.getPuertoIP()%>"/><br/>
		<label>Esclavo: </label><input type="text" name="esclavo" value=""/><br/>
		<label>Comando: </label><input type="text" name="comando" value=""/><br/>
		<label>Direcci�n: </label><input type="text" name="direccionHex" value=""/><br/>
		<label>Acci�n: </label><input type="text" name="accion" value=""/><br/>
		<label>Byte6: </label><input type="text" name="byte6" value=""/><br/>
		<label>Con CRC: </label><input type="checkbox" name="conCRC" value="1" checked="checked"/><br/>
		<label>Tiempo Respuesta: </label><input type="text" name="tiempoRespuesta" value="<%=Constantes.getEsperaEntreIntentosLectura() %>" /><br/>
		<label>Trama: </label><input type="text" name="trama" value="" /><br/>
		<input type="button" class="btn" name="enviar" value="enviar" onclick="enviarTrama()" /><br/>
		<label>Respuesta:</label><br/>
		<input type="text" name="resultado" value="<%=resultado%>" size="40" />
		
	</form>

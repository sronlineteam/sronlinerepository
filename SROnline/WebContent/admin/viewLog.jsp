<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.io.File"%>
<%
String ficheroLog = "logs/logSROnline.log";

File[] ficheros = null;
SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

if(request.getAttribute("listaFicherosLog")!=null)
	ficheros = (File[]) request.getAttribute("listaFicherosLog");

%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Logs</title>
<script type="text/javascript">

$(function() {
	$("#listado").tablesorter({sortList:[[0,2],[1,0]], widgets: ['zebra']});
});

</script>



</head>
<body>
<p>
	<a href="<%=ficheroLog %>" target="_blank" ><%=ficheroLog %></a>
</p>
<table class="tablesorter" id="listado">
	<thead> 
		<tr>
			<th>Ficheros disponibles</th>
			<th>Fecha</th>
		</tr>
	</thead>
<%
if (ficheros != null){ // Directorio existe 
    
    for (int x=0;x<ficheros.length;x++){
%>
<tr>
	<td>
    	<a href="logs/<%=ficheros[x].getName()%>" target="_blank" ><%=ficheros[x].getName()%></a>
	</td>
	<td>
		<%=sdf.format(ficheros[x].lastModified())%>
	</td>
</tr>
<%
    }

}
%>
</table>

</body>
</html>
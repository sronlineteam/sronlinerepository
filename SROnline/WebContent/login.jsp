<%@page  language="java" import="es.sronline.core.Constantes" %>
<!DOCTYPE html>
<%
String error = (String) request.getParameter("error");
String logout = (String) request.getParameter("logout");
%>
<html>
  <head>
	<!-- C�digo  para mostrar el favicon -->
	<LINK REL="SHORTCUT ICON" HREF="resources/img/favicon.ico" />
    <title tiles:fragment="title">Control de Acceso</title>
	<link type="text/css" href="resources/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="resources/css/regadio.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
  	<div class="cabecera">
		<h1>Sistema de Riego Online de <strong style="text-transform: uppercase;"><%=Constantes.getNombreInstalacion() %></strong></h1>
	</div>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

	<div class="contenido" id="contenido">
		<div class="formlogin">
			<h1>Control de acceso</h1>
			<form name="f" action="j_spring_security_check" method="post">               
	            <fieldset>
	                <%if(error != null){ %>
	                <div class="alert alert-error">    
	                    Usuario o password incorrecto.
	                </div>
	                <%
	                } 
	                if(logout != null){%>
	                <div th:if="${param.logout}" class="alert alert-success"> 
	                    Se ha desconectado.
	                </div>
	                <%} %>
	                <label for="username">Usuario</label>
	                <input type="text" id="username" name="j_username" autofocus/> </br>       
	                <label for="password">Password</label>
	                <input type="password" id="password" name="j_password"/>    
	                <div class="form-actions" style="text-align: center">
	                    <button type="submit" class="btn">Acceso</button>
	                </div>
	            </fieldset>
	        </form>
	       </div>
	       <div align="center">
	       		<img alt="SROnline logo" src="resources/img/logo2016.jpg">
	       </div>
		</div>
  </body>
</html>
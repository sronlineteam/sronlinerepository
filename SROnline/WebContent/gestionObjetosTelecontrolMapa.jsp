<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Gestion de Objetos Telecontrol</title>
			<style type="text/css">
			
			#dipositivos_panel ul {
			    list-style-type: none;
			    margin: 0px;
			    padding: 0px;
			    width: 100%;
			    font-family: Arial, sans-serif;
			    font-size: 8pt;
			}
			
			
			#dipositivos_panel ul li {
			    border-bottom: 1px solid white;
			}
			
			#dipositivos_panel ul li a {
			    text-decoration: none;
			    text-transform: uppercase;
			    display: block;
			    padding: 6px;
			}
			
			
			#dipositivos_panel ul li a:hover {
			    background: #ccc;
			    color: #fff;
			}
			
			</style>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<body onload="initialize()" style="margin: 0px; overflow: hidden;">
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido" style="height: 90%">
			<h1>Gestion de Objetos Telecontrol</h1>
			<%@ include file="auxiliares/gstDispositivosMapa.jsp" %>
		</div>
	</body>
</html>

<%@page import="java.util.HashMap"%>
<%@page import="es.sronline.bbdd.dto.ObjetosTelecontrol"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
 <%
 LstObjetosTelecontrol[]  lstObjTeleCtrl = null;
 Object obj = request.getAttribute("lstObjTelecontrol");
 if (obj != null)
	 lstObjTeleCtrl = (LstObjetosTelecontrol[]) obj; 

 String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 
 String dialogo = Constantes.isConAvisos();
 Object oDialogo = request.getAttribute("dialogo");

 String enSincronizacion = "xxxx";
 Object objSincro = request.getAttribute("enSincronizacion");

 if (objSincro != null)
	 enSincronizacion = (String) objSincro; 

 //confirmacion
 String confirmacion = "";
 Object oConfirmacion = request.getAttribute("confirmacion");
 
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - [x]":""; 
 }
 
 if(oDialogo != null)
	 dialogo = (String) oDialogo;

 if(oConfirmacion != null)
	 confirmacion = (String) oConfirmacion;

 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
 String fHoy =  format.format(now);
 now.setTime(now.getTime() + 86400000);
 String fMa�ana = format.format(now);
 
 %>
 
 <script type="text/javascript" src="resources/js/simpletree/simpletreemenu.js">

/***********************************************
* Simple Tree Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Please keep this notice intact
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

var peticion = "";
//recarga controlada de pagina
var sincronizar = 0;
<%
if(Constantes.isSincronizado()){%>;
	sincronizar = 1;
<%
}
%>
var recarga;
var milisegundos = 300000;

if(sincronizar == 1)
	recargar();
	
function recargar(){
	recarga = setTimeout('document.location.href = document.location.href;', milisegundos);
}

function pararRecarga(){
	clearTimeout(recarga);
}

function limpiarAviso(){
	  //document.getElementById("procesando").innerHTML = peticion;
	document.getElementById("procesando").style.display='none';
}

function cambiarFecha(fecha){
	var hoy = "<%=fHoy%>";
	var ma�ana = "<%=fMa�ana%>";
	if(fecha==ma�ana){
		this.document.forms.bloqueObj.cambiarFechaM.style.display='none';
		this.document.forms.bloqueObj.cambiarFechaH.style.display='inherit';
		this.document.forms.bloqueObj.dia.value = fecha;
	}
	else{
		this.document.forms.bloqueObj.cambiarFechaH.style.display='none';
		this.document.forms.bloqueObj.cambiarFechaM.style.display='inherit';
		this.document.forms.bloqueObj.dia.value = fecha;
	}
			
}

function enviarObjSelec(objetos, accion){
	if(objetos!=""){
		pararRecarga();
		this.document.forms.bloqueObj.controlTarifa.value = "sin control";
		this.document.forms.bloqueObj.objetos.value = objetos;
		this.document.forms.bloqueObj.accion.value = accion;
		this.document.forms.bloqueObj.submit();
		
	}
	else
		alert("Seleccione al menos un objeto");
}

function abrirObjSelec(){
	if(this.document.forms.bloqueObj.hora.value == "")
		alert("Indique la hora!");
	else
		console.log(this.document.forms.bloqueObj.hora.value)
		enviarObjSelec(recogerObjSelec(),"abrir");
}
function cerrarObjSelec(){
	if(this.document.forms.bloqueObj.hora.value == "")
		alert("Indique la hora!");
	else
		enviarObjSelec(recogerObjSelec(),"cerrar");
}


function recogerObjSelec(){
	var array = [];
	var cboxes = document.getElementsByName('selectObj');
    var len = cboxes.length;
    for (var i=0; i<len; i++) {
    	if(cboxes[i].checked)
	       array.push(cboxes[i].value);
    }
    return array;
}


function accionComun(idObjeto, accion, controlTarifa, mensaje){
	pararRecarga();
	document.getElementById("contenido").style.opacity="0.2";
	this.document.forms.gestionObjeto.accion.value = accion;
	this.document.forms.gestionObjeto.idObjeto.value = idObjeto;
	this.document.forms.gestionObjeto.controlTarifa.value = controlTarifa;
	this.document.forms.gestionObjeto.submit();
}

function confirmarAbrirObjeto(idObjeto, multiple){
	if(multiple == 'si'){
		this.document.forms.bloqueObj.confirmacion.value =  "<%=Constantes.CONFIRMADA %>";

		abrirObjSelec();
	}
	else{
		this.document.forms.gestionObjeto.confirmacion.value = "<%=Constantes.CONFIRMADA %>";
		$('#abrirId'+idObjeto).click();
	}
}
function rechazar(){
	  document.getElementById("procesando").innerHTML = peticion;
}



function abrirObj(idObjeto, controlTarifa){
	accionComun(idObjeto, "<%=Constantes.getAccionAbrir() %>", controlTarifa, "Abriendo Objeto");
}
function cerrarObj(idObjeto){
	accionComun(idObjeto, "<%=Constantes.getAccionCerrar() %>", 0, "Cerrando Objeto");
}
function estadoObj(idObjeto){
	accionComun(idObjeto, "<%=Constantes.getAccionEstado()%>", 0, "Consultando Objeto");
}

<%
if (enSincronizacion.equals("true")){
%>
setTimeout('document.location.reload()',10000);
<%
}
%>

</script>

<% if(!dialogo.equals(Constantes.DIALOGO_VACIO)) {
		if(dialogo.equals(Constantes.DIALOGO_INFORMAR) || dialogo.equals(Constantes.DIALOGO_ERROR)){
%>
			<div class="respuesta_modal"  id="procesando" onclick="this.style.display='none'" ><%=resultado %></div>
<%
		}
		else if(dialogo.equals(Constantes.DIALOGO_CONFIRMAR)){
%>
<div class="respuesta_modal"  id="procesando"  ><%=resultado %><br/>
	<input type="button" value="Confirmar" onclick="confirmarAbrirObjeto(<%=confirmacion %>)" /> 
	<input type="button" value="Rechazar" onclick="limpiarAviso()" /> 
	
</div>
<%
		}
	}
%>
	
	<div class="bloque" >
		<h2>Objetos telecontrol</h2>
		<ul id="treemenu" class="treeview" style="padding-left: 0px">
<%
	int idEmplazamientoActual = 0;
	int idEmplazamientoPadreActual = 0;
	int nivelActual = 0;
	int nivelAnterior = 0;
	int auxIdEmplazamiento = 0;
	int auxIdEmplazamientoPadre = 0;
	boolean cambiaEmplazamiento = true;
	boolean esEmplazamientoHijo = true;
	boolean esEmplazamientoHermano = false;
	boolean esElPrimero = true;
	
	for(int v = 0; v < lstObjTeleCtrl.length; v++){
		auxIdEmplazamiento = lstObjTeleCtrl[v].getIdEmplazamiento();
		//cambia el emplazamiento????
		if(auxIdEmplazamiento != idEmplazamientoActual){
			idEmplazamientoActual = auxIdEmplazamiento;
			cambiaEmplazamiento=true;
			nivelAnterior = nivelActual;
			nivelActual = lstObjTeleCtrl[v].getNivel();
		}
		if(cambiaEmplazamiento){
			if(nivelAnterior == nivelActual && !esElPrimero){
%>			
			</ul>
		</li>	
<%
			}
			else if(nivelAnterior > nivelActual){
				for(int r = 0; r < (nivelAnterior - nivelActual); r++){
%>			
			</ul title="subiendo_de_nivel">
		</li>	
<%
				}
%>
		</ul title="subiendo_de_nivel_fin">
	</li>	
<%		
			}			
%>
	<li>			
		<div class="textoemplazamiento"><%=lstObjTeleCtrl[v].getEmplazamiento()%></div>
		<ul>
<%
			cambiaEmplazamiento = false;
			if(nivelAnterior<=nivelActual){
				for(int n = 0; n < nivelActual - nivelAnterior - 1; n++){
%>			
			<li title="1">
				<ul>
<%
				}
			}
			else{
				for(int n = 0; n < nivelAnterior - nivelActual - 1; n++){
%>			
				</ul>		
			</li title="fin">
			
<%
				}
				for(int n = 0; n < nivelAnterior - nivelActual - 1; n++){
%>			
			<ul>	
			<li title="inicio">
			
<%
				}
	
			}
		}
		if(esElPrimero)
			esElPrimero = false;

%>			
			<li title="1">
				<p id="objeto-<%=lstObjTeleCtrl[v].getIdObjeto()%>">
					<div class="respuesta" id="respuesta-<%=lstObjTeleCtrl[v].getIdObjeto()%>" onclick="this.innerHTML=''"></div>
								<input type="checkbox" class="checkObj" name="selectObj" value="<%=lstObjTeleCtrl[v].getIdObjeto()%>" > 
								<input type="text" class="texto<%=lstObjTeleCtrl[v].getEstado().toLowerCase()%>" id="txtv-<%=lstObjTeleCtrl[v].getIdObjeto()%>" value="<%=lstObjTeleCtrl[v].getNombre()%>: &nbsp;<%=lstObjTeleCtrl[v].getEstado().toUpperCase()%>" />
								<input type="button" class="btnAbrir" id="abrirId<%=lstObjTeleCtrl[v].getIdObjeto()%>" value="" alt="Abrir" onclick="abrirObj('<%=lstObjTeleCtrl[v].getIdObjeto()%>', '<%=lstObjTeleCtrl[v].getControlTarifa() %>');" />
								<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarObj('<%=lstObjTeleCtrl[v].getIdObjeto()%>');" />
								<input type="button" class="btnEstado" value="" alt="Estado" onclick="estadoObj('<%=lstObjTeleCtrl[v].getIdObjeto()%>');" />
				</p>
			</li>
<%
	if(cambiaEmplazamiento){
%>	
		</ul>
<%
		if(!esEmplazamientoHijo){
%>			
	</li>
<%
		}
		cambiaEmplazamiento = false;
	}
%>			
<%
	}
%>	
	</div>
	<div class="progBloque">
		<h2>Programaci�n en Bloque</h2>
		<form action="SrvGstObjetosTelecontrol" name="gestionObjeto" method="post" >
			<input type="hidden" name="accion" /> 	
			<input type="hidden" name="idObjeto" />
			<input type="hidden" name="confirmacion" />
			<input type="hidden" name="controlTarifa" value="sin control"/>
		</form>
		
		<form action="SrvGestBloqueObjTelecontrol" name="bloqueObj" method="post" >
			<input type="hidden" name="confirmacion" />
					<div >
					    <div class="row">
					        <div class='col-sm-5'>
								<label >D�a a programar</label>
					        </div>
					        <div class='col-sm-5'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker1'>
					                    <input type='text' class="form-control" name="dia" value="<%=fHoy%>"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <div class='col-sm-1'>
								<input type="button" name="cambiarFechaM" value="+" onclick="cambiarFecha('<%=fMa�ana %>')" />
								<input type="button" name="cambiarFechaH" value="-" onclick="cambiarFecha('<%=fHoy %>')" style="display: none;"/>
							</div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker1').datetimepicker({
					                	format: 'DD-MM-YY'
					                });
					                
					            });
					        </script>
					    </div>
				    <div class="row">
					        <div class='col-sm-5'>
								<label>Hora a programar </label> 
					        </div>
				        <div class='col-sm-5'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" name='hora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 

					<input type="hidden" name="objetos" value="vacio" />
					<input type="hidden" name="accion" />
					<input type="hidden" name="controlTarifa" />
		</form>
			<ul class="col3" id="bloque" style="text-align:right">
				<li>
					<input type="button" class="btnAbrir" value="" alt="Abrir" onclick="abrirObjSelec()" />
				</li>
				<li>
					<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarObjSelec()" />
				</li>
			</ul>
	</div>
<script type="text/javascript">

//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

ddtreemenu.createTree("treemenu", true)

</script>
<%@page import="es.sronline.bbdd.dto.Emplazamiento"%>
<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%

 Emplazamiento[]  lstEmplzmto = null;
 Object obj = request.getAttribute("lstEmplazamientos");
 if (obj != null)
	 lstEmplzmto = (Emplazamiento[]) obj; 

%>


<script type="text/javascript">

	function eliminarEmplazamiento(idObj){
		if(!confirm("Eliminar el objeto"))
			return;
		document.getElementById("gestionAccion").value = <%=Constantes.getAdminEliminar()%>;
		document.getElementById("gestionIdEmplazamiento").value = idObj;
		document.forms.gestionEmplazamiento.submit();	
	}
	
	function accionNuevo(){
		document.forms.nuevoEmplazamiento.submit();
	}
	
	function accionModificar(){
		document.forms.gestionEmplazamiento.submit();
	}
	
	
	function Cancelar(){
			//this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModalAlta").style.display ="none";
		this.document.getElementById("formularioModalEdicion").style.display ="none";
	}
	
	function NuevoEmplazto(){
		this.document.getElementById("formularioModalAlta").style.display ="block";
	}
	function editarEmplazto(idEmplazamiento, nombre, posicion, idEmplazamientoPadre){
		document.getElementById("gestionIdEmplazamiento").value = idEmplazamiento;
		document.getElementById("gestionNombre").value = nombre;
		document.getElementById("gestionPosicion").value = posicion;
		document.getElementById("gestionIdEmplazamientoPadre").value = idEmplazamientoPadre;
		document.getElementById("gestionAccion").value = <%=Constantes.getAdminActualizar()%>;
		document.getElementById("formularioModalEdicion").style.display ="block";
	}
	

</script>
	<div class="modal" id="formularioModalAlta" style="display:none" >
		<div class="formularioModal" >
		
			<h2>Nuevo Emplazamiento</h2>
			<form action="SrvConfigEmplazamientos"  id="nuevoEmplazamiento" name="nuevoEmplazamiento" method="post">			
				<div class="form-group row">
					<label for="nombre" class="col-sm-4 col-sm-offset-1 control-label">Nombre</label>
					<input type="text" id="nuevoNombre" name="nombre" maxlength="45" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group row">
					<label for="posicion" class="col-sm-4 col-sm-offset-1 control-label">Posicion</label>
					<input type="text" id="nuevaPosicion" name="posicion" maxlength="4000" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group row">
					<label for="esclavo" class="col-sm-4 col-sm-offset-1 control-label">Emplazamiento Padre</label>
					<input type="text" id="nuevoIdEmplazamientoPadre" name="idEmplazamientoPadre" maxlength="16" class="form-control col-sm-9" value="0"></input>
				</div>
				<div class="form-group">
					<input type="hidden" id="accion" name="accion" value="<%=Constantes.getAdminInsertar()%>"></input>
				</div>
				
				<div class="row">
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="submit" class="btn" value=Enviar alt="Cambiar" onclick="accionNuevo()" />
					</div>
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />				
					</div>				
				</div>
				
			</form>
			
		</div>
	</div>
	
	<div class="modal" id="formularioModalEdicion" style="display:none" >
		<div class="formularioModal" >
		
			<h2>Editar Emplazamiento</h2>
			<form action="SrvConfigEmplazamientos"  id="gestionEmplazamiento" name="gestionEmplazamiento" method="post">			
				<div class="form-group row">
					<label for="nombre" class="col-sm-4 col-sm-offset-1 control-label">Nombre</label>
					<input type="text" id="gestionNombre" name="nombre" maxlength="45" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group row">
					<label for="posicion" class="col-sm-4 col-sm-offset-1 control-label">Posicion</label>
					<input type="text" id="gestionPosicion" name="posicion" maxlength="4000" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group row">
					<label for="esclavo" class="col-sm-4 col-sm-offset-1 control-label">Emplazamiento Padre</label>
					<input type="text" id="gestionIdEmplazamientoPadre" name="idEmplazamientoPadre" maxlength="16" class="form-control col-sm-9" value="0"></input>
				</div>
				<div class="form-group">
					<input type="hidden" id="gestionIdEmplazamiento" name="idEmplazamiento"></input>
					<input type="hidden" id="gestionAccion" name="accion"></input>
				</div>
				
				<div class="row">
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="submit" class="btn" value=Enviar alt="Cambiar" onclick="accionModificar()" />
					</div>
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />				
					</div>				
				</div>
				
			</form>
			
		</div>
	</div>
	
	
	
        <p>
			<input type="button"  class="btn"  value="Nuevo" alt="Nuevo" onclick="NuevoEmplazto()"/>
		</p>
		
		<table id="table" class="tablesorter" style="max-width:400px;">
			<thead> 
		        <tr>
		            <th class="nombre-obj">Emplazamiento</th>
		            <th class="nombre-obj">Empl Padre</th>
		            <th>Edici�n</th>
		            
		        </tr>
		    </thead>		
<%
	//los emplazamientos padres se muestran bien siempre que los idEmplazamiento sean consecutivos>
	if(lstEmplzmto!=null){
		String auxEmplaPadre;
		for(int v = 0; v < lstEmplzmto.length; v++){
			auxEmplaPadre = "-";
			if(lstEmplzmto[v].getIdEmplazamientoPadre()!= 0)
				auxEmplaPadre = lstEmplzmto[lstEmplzmto[v].getIdEmplazamientoPadre()-1].getEmplazamiento();
%>		
			<tr>
			    <td><%=lstEmplzmto[v].getEmplazamiento()%></td>
			    <td><%=auxEmplaPadre%></td>
			    <td class="text-center">
			    	<button class="btnCambiar" onclick='editarEmplazto(<%=lstEmplzmto[v].getIdEmplazamiento()%>,"<%=lstEmplzmto[v].getEmplazamiento()%>","<%=lstEmplzmto[v].getPosicion()%>","<%=lstEmplzmto[v].getIdEmplazamientoPadre()%>");'></button>
			    	<button class="btnEliminar" onclick='eliminarEmplazamiento(<%=lstEmplzmto[v].getIdEmplazamiento()%>);'></button>
				</td>
			</tr>
<%
		}
	}
	else{
%>
			<tr>
			    <td></td>
			    <td class="text-center">
				</td>
			</tr>
<%
	}
%>	
		</table>

<script>
	$(document).ready(function(){
		$("#table").tablesorter({widgets: ['zebra']});
		$("#table").DataTable({
	        "language": {
		    	"aria": {
	    	        "sortAscending":  ": activar para ordenar la columna ascendentemente",
	    	        "sortDescending": ": activar para ordenar la columna descendentemente"
	    	    },
				"loadingRecords": "Cargando...",
	            "lengthMenu": "Registros por pagina _MENU_",
	            "zeroRecords": "Sin registros",
	            "info": "Mostrando pagina _PAGE_ de _PAGES_",
	            "infoEmpty": "Sin registros disponibles",
	            "infoFiltered": "(  numero total de registros _MAX_)",
	            "search":         "Buscar:",
	            "zeroRecords":    "No se encuentran registros",
	            "paginate": {
	                "first":      "Primero",
	                "last":       "Ultimo",
	                "next":       "Siguiente",
	                "previous":   "Anterior"
	            }
	        }
	    } ); 
	});
</script>


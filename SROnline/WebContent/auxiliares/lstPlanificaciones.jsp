<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@page import="es.sronline.core.ProgramacionTelecontrolFormateada"%>
<%@page import="es.sronline.core.ProgramacionPlanificaciones"%>
<%@page import="es.sronline.core.Programacion"%>
<%@page import="es.sronline.core.Common"%>
<%@page import="java.sql.Time"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page language="java" import="es.sronline.core.Constantes"%>
<%@page language="java" import="es.sronline.core.Constantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
List<ProgramacionPlanificaciones> lstPlanificaciones;
Object obj = request.getAttribute("lstPlanificaciones");
if (obj != null){
	lstPlanificaciones = (List<ProgramacionPlanificaciones>) obj;


	LstObjetosTelecontrol[] objetosTelecontrol = null;
	Object objTelec = request.getAttribute("objetosTelecontrol");
	if (objTelec != null){
		 objetosTelecontrol = (LstObjetosTelecontrol[]) objTelec;
	}
	
	String auxInicio;
	String auxFin;
	String nombre;

%>

	<script type="text/javascript">
		function limpiarFormulario(){
			$("[name=nombrePlanificacion]").val("");
			$(".checkTele").prop('checked', false);
			$("#todosDias").prop('checked', false);
			$("[name=dia_semana]").prop('checked', false);
			$("#diaInicio, #diaFin ,#horaInicio, #horaFin").val('');
			$('#diaFinCheck, #horaFinCheck').prop( "checked", true );
			$("#duracionDiaFinCheck, #duracionHoraFinCheck").prop('checked', false);
			$('#diaFin, #horaFin').prop( "disabled", false );
			$("#duracionDiaFin, #duracionHoraFin").prop('disabled', true);
		}

		function CancelarNueva(){
			limpiarFormulario();
			this.document.getElementById("formularioModalAlta").style.display ="none";
		}
		
		function NuevaProg(){
			$('#guardar-como').hide();
			this.document.forms.bloquePlani.accion.value = "<%=Constantes.getAccionNuevaPlanificacion()%>";
			this.document.getElementById("formularioModalAlta").style.display ="block";
		}
		
		function changeDiasSemana(){
			if ($("#todosDias").is( ":checked")){
				$('#1, #2, #3, #4, #5, #6, #0').prop('checked', true);
			} else {
				$('#1, #2, #3, #4, #5, #6, #0').prop('checked', false);
			}
		}
		$( document ).ready(function() {
			$('#diaFinCheck, #horaFinCheck').prop( "checked", true );
			$("#duracionDiaFinCheck, #duracionHoraFinCheck").prop('checked', false);
		});
		

		function changeDuracionDiaFin(){
			$('#diaFin').prop( "disabled", true );
			$('#diaFin').val("");
			$('#diaFinCheck').prop( "checked", false );
			$('#duracionDiaFin').prop( "disabled", false );
		}
		
		function changeDiaFin(){
			$('#duracionDiaFin').prop( "disabled", true );
			$('#duracionDiaFin').val("");
			$('#duracionDiaFinCheck').prop( "checked", false );
			$('#diaFin').prop( "disabled", false );
		}
		
		function changeDuracionHoraFin(){
			$('#horaFin').prop( "disabled", true );
			$('#horaFin').val( "" );
			$('#horaFinCheck').prop( "checked", false  );
			$('#duracionHoraFin').prop( "disabled", false );
		}
		
		function changeHoraFin(){
			$('#duracionHoraFin').prop( "disabled", true );
			$('#duracionHoraFin').val( "" );
			$('#duracionHoraFinCheck').prop( "checked", false  );
			$('#horaFin').prop( "disabled", false );
		}
		

		function recogerObjTel(){
			var arrayTelecontrol = [];
			var cboxes = document.getElementsByName('selectTel');
		    var len = cboxes.length;
		    for (var i=0; i<len; i++) {
		    	if(cboxes[i].checked)
		    		arrayTelecontrol.push(cboxes[i].value);
		    }
		  
		    
		    return arrayTelecontrol;
		}
		
		function recogerDiaSemama(){
		    var arrayDiasSemana = [];
			var cboxes = document.getElementsByName('dia_semana');
		    var len = cboxes.length;
		    for (var i=0; i<len; i++) {
		    	if(cboxes[i].checked)
		    		arrayDiasSemana.push(cboxes[i].value);
		    }
		    
		    return arrayDiasSemana;
		}
		
		function guardarComo(){
			this.document.forms.bloquePlani.accion.value = "<%=Constantes.getAccionNuevaPlanificacion()%>";
			if (nombrePlani===$('[name=nombrePlanificacion]').val()){
				alert("Introduzca un nuevo nombre de planificaci�n")
			}else{
				planificar();
			}			
		}
		
		function planificar(){
			if(this.document.forms.bloquePlani.nombrePlanificacion.value == "")
				alert("Indique nombre de planificacion!");
			else if(this.document.forms.bloquePlani.diaInicio.value == "")
				alert("Indique la fecha inicial!");
			else if(this.document.forms.bloquePlani.horaInicio.value == "")
				alert("Indique la hora inicial!");
			else
				enviarPlanificacion(recogerObjTel(), recogerDiaSemama());
		}
		
		function compara_fecha(fecha, fecha2){
			// La fecha de fin puede ser vac�a
			if (fecha2!=''){
				suma = new Date(fecha.substring(6,10),fecha.substring(3,5)-1,fecha.substring(0,2));
				var anio=suma.getYear();
				var mes =suma.getMonth()+1;
				var dia =suma.getDate();
				suma2 = new Date(fecha2.substring(6,10),fecha2.substring(3,5)-1,fecha2.substring(0,2));
				var anio2=suma2.getYear();
				var mes2 =suma2.getMonth()+1;
				var dia2 =suma2.getDate();
				if(anio<anio2){
					return false;
				}
				if(anio<=anio2 && mes<=mes2){
					if(anio==anio2 && mes==mes2 && dia>dia2){
						return false;
					}
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}
		
		function enviarPlanificacion(objetos, diasSemana){
			if(objetos==""){
				alert("Seleccione al menos un objeto de telecontrol");
			}
			else if(diasSemana==""){
				alert("Seleccione al menos un dia de la semana");
			}
			else{
				if ($('.diaFin:checked').val()==1){
					if ($('#diaFin').val()){
						$('#fechaFinal').val($('#diaFin').val());
					} else {
						alert("Indique la fecha final!");
					}
					
				} else {
					if ($('#duracionDiaFin').val()){
						$('#fechaFinal').val(moment($('#diaInicio').val(),'DD-MM-YYYY').add(parseInt($('#duracionDiaFin').val()),'Days').format('DD-MM-YYYY'));
					} else {
						alert("Indique la fecha final!");
					}
					
				}
				
				if ($('.horaFin:checked').val()==1) {
					if ($('#horaFin').val()){
						$('#horaFinal').val($('#horaFin').val());
					} else {
						alert("Indique la hora final!");
					}
					
				} else {
					if ($('#duracionHoraFin').val()){
						$('#horaFinal').val(moment($('#horaInicio').val(),"HH:mm").add($('#duracionHoraFin').val()/60,'hours').format("HH:mm"));
					} else {
						alert("Indique la hora final!");
					}
				}
				
				if(!compara_fecha(this.document.forms.bloquePlani.diaInicio.value, this.document.forms.bloquePlani.fechaFinal.value))
					alert('La fecha inicial no puede ser mayor que la fecha final');
				else{ 
					this.document.forms.bloquePlani.diasSemana.value = diasSemana;
					this.document.forms.bloquePlani.diaInicio.value = moment($('#diaInicio').val(),'DD-MM-YYYY').format('YYYY-MM-DD');
					this.document.forms.bloquePlani.fechaFinal.value = moment($('#fechaFinal').val(),'DD-MM-YYYY').format('YYYY-MM-DD');
					this.document.forms.bloquePlani.idObjetosTelecontrol.value = objetos;
					this.document.forms.bloquePlani.submit();
				}	
			}
		}
			
		function eliminarPlani(idPlani){
			if(confirm("Eliminar la planificaci�n")){
				this.document.forms.bloquePlani.accion.value = "<%=Constantes.getAccionEliminar()%>";
				this.document.forms.bloquePlani.idPlanificacion.value = idPlani;
				this.document.forms.bloquePlani.submit();
			}
		}
		var nombrePlani;
		function editarPlani(idPlani){
			$('#guardar-como').show();
			limpiarFormulario();
			$('[name=nombrePlanificacion]').val($("#" + idPlani + "_nombre").val());
			nombrePlani = $("#" + idPlani + "_nombre").val();
			$('#diaInicio').val(moment($("#" + idPlani + "_FechaInicio").val(),'YYYY-MM-DD').format('DD-MM-YYYY'));
			//$('.diaFin').click();
			$('#diaFin').val(moment($("#" + idPlani + "_FechaFin").val(),'YYYY-MM-DD').format('DD-MM-YYYY'));
			$('#horaInicio').val($("#" + idPlani + "_HoraInicio").val());
			//$('.horaFin').click();
			$('#horaFin').val($("#" + idPlani + "_HoraFin").val());
			
			var diasSemana = $("#" + idPlani + "_ArraySemana").val().split(",");
			$("[name=dia_semana]").prop('checked', false);
			for (var i = 0; i < diasSemana.length; i++){
				$("#" + diasSemana[i]).click();
			}
			if (diasSemana.length===7){
				$("#todosDias").prop('checked', true);
			} else {
				$("#todosDias").prop('checked', false);
			}
			
		
			var idObjetos = $("#" + idPlani + "_ArrayIdObjetos").val().split(",");
			$(".checkTele").prop('checked', false);
			for (var i = 0; i < idObjetos.length; i++){
				$("#idObjeto_" + idObjetos[i]).click();
			}
			this.document.forms.bloquePlani.idPlanificacion.value = idPlani;
			this.document.forms.bloquePlani.accion.value = "<%=Constantes.getAccionCambiar()%>";
			this.document.getElementById("formularioModalAlta").style.display ="block";
	
		}
</script>

<%
	if(lstPlanificaciones.size()>0){
%>

<script type="text/javascript">

$(function() {
	$("#listado").tablesorter({sortList:[[1,1]], widgets: ['zebra']});
	$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}}); 
});


</script>
<%
	}
%>

	<div class="modal" id="formularioModalAlta" style="display:none" >
		<div class="formularioModal" class="row">
			<h2>Planificaci�n del Riego</h2>
			<form action="SrvPlanificacion" name="bloquePlani" method="post">
				<p> <b>Nombre Planificaci�n:</b> <input type='text' name="nombrePlanificacion" /></p>
				<div class="progListadoBloque col-sm-4">
					<input type="hidden" name="idObjetosTelecontrol" value="vacio" />
					<input type="hidden" name="idPlanificacion" /> 
					<input type="hidden" name="accion" />
					<%
	for(int v = 0; v < objetosTelecontrol.length; v++){
%>
					<p>
						<input type="checkbox" class="checkTele" name="selectTel"
							id="idObjeto_<%=objetosTelecontrol[v].getIdObjeto()%>"
							value="<%=objetosTelecontrol[v].getIdObjeto()%>"> <input
							type="text" class="textoplano"
							value="<%=objetosTelecontrol[v].getNombre() %>" readonly />
					</p>
					<%
	}
%>
				</div>
				<div class="formPlanificacion col-sm-8">
					<h2> Horas de Inicio y Fin del Riego (o Duraci�n)</h2>
					<div class="row">
						<div class="col-sm-6">
							Hora de inicio del riego:
							<div class="container">
								<div class="row">
									<div class='col-sm-2'>
										<div class="form-group">
											<div class='input-group date' id='datetimepicker5'>
												<input type='text' class="form-control" name='horaInicio' id="horaInicio" /> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-time"></span>
												</span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$(function () {
										    $('#datetimepicker5').datetimepicker({
										    	format: 'LT'
										    });
										});
									</script>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
								<input type="radio" id="horaFinCheck" class="horaFin" value="1" onclick="changeHoraFin()" checked alt="Marque esta opci�n para indicar hora fin" />
								Hora del fin del riego:
							<div class="container">
								<div class="row">
									<div class='col-sm-2'>
										<div class="form-group">
											<div class='input-group date' id='datetimepicker6'>
												<input type='text' class="form-control" id="horaFin" /> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-time"> </span>
												</span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$(function () {
										    $('#datetimepicker6').datetimepicker({
										    	format: 'LT'
										    });
										});
									</script>
								</div>
							</div>
								<input type="radio" id="duracionHoraFinCheck" class="horaFin" value="0" onclick="changeDuracionHoraFin()" /> 
								Duraci�n del riego en minutos:
								<input type='number' id="duracionHoraFin" name="duracion" min="1" disabled />
						
							<input type="text" id="horaFinal" name="horaFinal" hidden />
						</div>
					</div>
					<h2>Calendario en que se realiza el Riego</h2>
					<div class="row">
						<div class="col-sm-6">
							Comenzar a regar el d�a:
							<div class="container">
								<div class="row">
									<div class='col-sm-2'>
										<div class="form-group">
											<div class='input-group date' id='datetimepicker3'>
												<input type='text' class="form-control" id="diaInicio" name="diaInicio" /> 
													<span class="input-group-addon"> 
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$(function () {
										    $('#datetimepicker3').datetimepicker({
										    	format: 'DD-MM-YYYY'
										    });
										    
										});
									</script>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
								<input type="radio" id="diaFinCheck" class="diaFin" value="1" onclick="changeDiaFin()" checked />
								Finalizar de regar el d�a:
							<div class="container">
								<div class="row">
									<div class='col-sm-2'>
										<div class="form-group">
											<div class='input-group date' id='datetimepicker4'>
												<input type='text' class="form-control" id="diaFin" name="diaFin" /> 
												<span class="input-group-addon"> 
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$(function () {
										    $('#datetimepicker4').datetimepicker({
										    	format: 'DD-MM-YYYY'
										    });
										    
										});
									</script>
								</div>
							</div>
								<input type="radio" id="duracionDiaFinCheck" class="diaFin" value="0" onclick="changeDuracionDiaFin()" />
								Duraci�n del calendario en d�as: 
								<input type='number' id="duracionDiaFin" name="duracion" min="1" disabled />
							
							<input type="text" id="fechaFinal" name="fechaFinal" hidden />
							
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<p>
								Regar en los siguientes d�as de la semana:
							</p>
							<p align="center">
								L <input type="checkbox" id="1" name="dia_semana" value="1"> 
								M <input type="checkbox" id="2" name="dia_semana" value="2"> 
								X <input type="checkbox" id="3" name="dia_semana" value="3"> 
								J <input type="checkbox" id="4" name="dia_semana" value="4"> 
								V <input type="checkbox" id="5" name="dia_semana" value="5"> 
								S <input type="checkbox" id="6" name="dia_semana" value="6"> 
								D <input type="checkbox" id="0" name="dia_semana" value="0" style="margin-right: 20px;">
								Todos <input type="checkbox" id="todosDias" onclick="changeDiasSemana()"><br/>
							    <input type="text" name="diasSemana" hidden />
							</p>
						</div>
					</div>
					<p>&nbsp;</p>
					<h2>&nbsp;</h2>
					<div class="row">
						<div class="col-sm-10 col-sm-offset-2" style="margin-top: -12px;">
							<ul class="col3" id="bloque">
								<li><input type="button" class="btn" value="Aceptar"
									alt="Aceptar" onclick="planificar()" /></li>
								<li><input type="button" class="btn" value="Cancelar"
									alt="Cancelar" onclick="CancelarNueva()" /></li>
								<li><input type="button" id="guardar-como" class="btn"
									value="Guardar como..." onclick="guardarComo()" /></li>
							</ul>
						</div>
					</div>
				</div>
			</form>

		</div>
	</div>



	<div class="contenido" id="contenido">
		<div class="respuesta" id="procesando" onclick="this.innerHTML=''"></div>
		<table class="tablesorter" id="listado">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<%
	if(lstPlanificaciones.size()>0){

		String[] tipo = {"odd","even"} ;
		int id = 0;
		for (int l = 0; l < lstPlanificaciones.size();l++){
			nombre = lstPlanificaciones.get(l).getNombre();
			auxInicio = lstPlanificaciones.get(l).getF_inicio();
			auxFin = lstPlanificaciones.get(l).getF_fin();
			id = l%2;
%>
				<tr>
					<td><%=nombre %></td>
					<td><%=auxInicio%></td>
					<td><%=auxFin%></td>
					<td class="btnBox"><input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_nombre"
						value="<%=lstPlanificaciones.get(l).getNombre()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_FechaInicio"
						value="<%=lstPlanificaciones.get(l).getF_inicio()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_FechaFin"
						value="<%=lstPlanificaciones.get(l).getF_fin()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_HoraInicio"
						value="<%=lstPlanificaciones.get(l).getH_inicio()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_HoraFin"
						value="<%=lstPlanificaciones.get(l).getH_fin()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_ArraySemana"
						value="<%=lstPlanificaciones.get(l).getArrayDiasSemana()%>" hidden>
						<input
						id="<%=lstPlanificaciones.get(l).getIdPlanificacion()%>_ArrayIdObjetos"
						value="<%=lstPlanificaciones.get(l).getArrayIdObjetosTelecontrol()%>"
						hidden>
						<button class='btnCambiar btnAccion' title='editar planificacion'
							onclick="editarPlani('<%=lstPlanificaciones.get(l).getIdPlanificacion()%>')"></button>
						<button class='btnEliminar btnAccion' title='borrar planificacion'
							onclick="eliminarPlani('<%=lstPlanificaciones.get(l).getIdPlanificacion()%>')"></button>
					</td>

				</tr>
				<%		
		} 
	} 

%>

			</tbody>
		</table>
		<p>
			<input type="button" class="btn" value="Nueva" alt="Nueva" onclick="NuevaProg()" />
		</p>
	</div>
<%
}
else{
%>
<h2>No hay ninguna Planificacion</h2>
<%} %>


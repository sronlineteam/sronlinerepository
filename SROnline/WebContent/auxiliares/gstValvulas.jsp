<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.LstValvulas"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
 <%
 LstValvulas[] lstValvulas = null;
 Object obj = request.getAttribute("lstValvulas");
 if (obj != null)
	 lstValvulas = (LstValvulas[]) obj; 

 String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 
 String dialogo = Constantes.isConAvisos();
 Object oDialogo = request.getAttribute("dialogo");

 String enSincronizacion = "xxxx";
 Object objSincro = request.getAttribute("enSincronizacion");

 if (objSincro != null)
	 enSincronizacion = (String) objSincro; 

 //confirmacion
 String confirmacion = "";
 Object oConfirmacion = request.getAttribute("confirmacion");
 
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - [x]":""; 
	 System.out.println(resultado);
 }
 
 if(oDialogo != null)
	 dialogo = (String) oDialogo;

 if(oConfirmacion != null)
	 confirmacion = (String) oConfirmacion;

 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
 String fHoy =  format.format(now);
 now.setTime(now.getTime() + 86400000);
 String fMa�ana = format.format(now);
 
 %>
<script type="text/javascript">

var peticion = "";

function limpiarAviso(){
	  document.getElementById("procesando").innerHTML = peticion;
}

function cambiarFecha(fecha){
	var hoy = "<%=fHoy%>";
	var ma�ana = "<%=fMa�ana%>";
	if(fecha==ma�ana){
		this.document.forms.bloqueValv.cambiarFechaM.style.display='none';
		this.document.forms.bloqueValv.cambiarFechaH.style.display='inherit';
		this.document.forms.bloqueValv.dia.value = fecha;
	}
	else{
		this.document.forms.bloqueValv.cambiarFechaH.style.display='none';
		this.document.forms.bloqueValv.cambiarFechaM.style.display='inherit';
		this.document.forms.bloqueValv.dia.value = fecha;
	}
			
}


function accionComun(remota, valvula, accion, mensaje){
	document.getElementById("contenido").style.opacity="0.2";
//	arrayValvulas[remota][valvula]=false;
	document.getElementById("respuesta-"+remota+"-"+valvula).innerHTML = mensaje;
	//document.getElementById("valvula-"+remota+"-"+valvula).style.opacity="0.2";
	//execGestionVal("accion=" + accion + "&idRemota=" + remota + "&idValvula=" + valvula);
	this.document.forms.gestionValvula.accion.value = accion;
	this.document.forms.gestionValvula.idRemota.value = remota;
	this.document.forms.gestionValvula.idValvula.value = valvula;
	this.document.forms.gestionValvula.submit();
	
}
function confirmarAbrirValvula(remota, valvula, multiple){
	if(multiple == 'si'){
		this.document.forms.bloqueValv.confirmacion.value =  "<%=Constantes.CONFIRMADA %>";

		abrirValSelec();
	}
	else{
		this.document.forms.gestionValvula.confirmacion.value = "<%=Constantes.CONFIRMADA %>";
		accionComun(remota, valvula, "<%=Constantes.getAccionAbrir() %>", "Abriendo valvula");
	}
}
function rechazar(){
	  document.getElementById("procesando").innerHTML = peticion;
}


function abrirVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionAbrir() %>", "Abriendo valvula");
}
function cerrarVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionCerrar() %>", "Cerrando valvula");
}
function estadoVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionEstado()%>", "Consultando valvula");
}
<%
if (enSincronizacion.equals("true")){
%>
setTimeout('document.location.reload()',10000);
<%
}
%>

</script>

<% if(!dialogo.equals(Constantes.DIALOGO_VACIO)) {
		if(dialogo.equals(Constantes.DIALOGO_INFORMAR) || dialogo.equals(Constantes.DIALOGO_ERROR)){
%>
			<div class="respuesta_modal"  id="procesando" onclick="this.innerHTML=''" ><%=resultado %></div>
<%
		}
		else if(dialogo.equals(Constantes.DIALOGO_CONFIRMAR)){
%>
<div class="respuesta_modal"  id="procesando"  ><%=resultado %><br/>
	<input type="button" value="Confirmar" onclick="confirmarAbrirValvula(<%=confirmacion %>)" /> 
	<input type="button" value="Rechazar" onclick="limpiarAviso()" /> 
	
</div>
<%
		}
	}
%>
	
	<div class="bloque">
		<h2>Valvulas</h2>
<%
	int auxRemota = 0;
	boolean mismaRemota = false;

	for(int v = 0; v < lstValvulas.length; v++){

%>	
		<div class="respuesta" id="respuesta-<%=lstValvulas[v].getIdRemota()%>-<%=lstValvulas[v].getIdvalvula()%>" onclick="this.innerHTML=''">
		</div>
		<ul class="col fila<%=lstValvulas[v].getEstado().toLowerCase()%>" id="valvula-<%=lstValvulas[v].getIdRemota()%>-<%=lstValvulas[v].getIdvalvula()%>">
			<li>
				<ul class="col3">
					<li>
						<input type="checkbox" class="checkValv" name="selectVal" value="v<%=lstValvulas[v].getIdRemota()%>-<%=lstValvulas[v].getIdvalvula()%>" > 
						<input type="text" class="textoplano" value="<%=lstValvulas[v].getValvula() %>" />
					</li>
					<li>
						<input type="text" class="texto<%=lstValvulas[v].getEstado().toLowerCase()%>" id="txtv-<%=lstValvulas[v].getIdRemota()%>-<%=lstValvulas[v].getIdvalvula()%>" value="<%=lstValvulas[v].getEstado() %>" />
					</li>
					<li>
						<input type="button" class="btnAbrir" value="" alt="Abrir" onclick="abrirVal(<%=lstValvulas[v].getIdRemota()%>,<%=lstValvulas[v].getIdvalvula()%>)" />
						<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarVal(<%=lstValvulas[v].getIdRemota()%>,<%=lstValvulas[v].getIdvalvula()%>)" />
						<input type="button" class="btnEstado" value="" alt="Estado" onclick="estadoVal(<%=lstValvulas[v].getIdRemota()%>,<%=lstValvulas[v].getIdvalvula()%>)" />
					</li>
				</ul>
			</li>
		</ul>
	<%
	}
%>	
		
	</div>
	<div class="progBloque">
		<h2>Programaci�n en Bloque</h2>
		<form action="SrvGestBomba" name="gestionValvula" method="post" >
		<!-- execGestionVal("accion=" + accion + "&idRemota=" + remota + "&idValvula=" + valvula); -->
			<input type="hidden" name="accion" />
			<input type="hidden" name="idRemota" />
			<input type="hidden" name="idValvula" />
			<input type="hidden" name="confirmacion" />
		</form>
		
		<form action="SrvGestBloqueBombas" name="bloqueValv" method="post" >
			<input type="hidden" name="confirmacion" />
					<div >
					    <div class="row">
					        <div class='col-sm-5'>
								<label >D�a a programar</label>
					        </div>
					        <div class='col-sm-5'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker1'>
					                    <input type='text' class="form-control" name="dia" value="<%=fHoy%>"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <div class='col-sm-1'>
								<input type="button" name="cambiarFechaM" value="+" onclick="cambiarFecha('<%=fMa�ana %>')" />
								<input type="button" name="cambiarFechaH" value="-" onclick="cambiarFecha('<%=fHoy %>')" style="display: none;"/>
							</div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker1').datetimepicker({
					                	format: 'DD-MM-YY'
					                });
					                
					            });
					        </script>
					    </div>
				    <div class="row">
					        <div class='col-sm-5'>
								<label>Hora a programar </label> 
					        </div>
				        <div class='col-sm-5'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" name='hora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>			 	 

					<input type="hidden" name="valvulas" value="vacio" /><input type="hidden" name="accion" />
			<p></p>
			<ul class="col3" id="bloque" style="text-align:right">
				<li>
					<input type="button" class="btnAbrir" value="" alt="Abrir" onclick="abrirValSelec()" />
				</li>
				<li>
					<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarValSelec()" />
				</li>
			</ul>
		</form>
	</div>

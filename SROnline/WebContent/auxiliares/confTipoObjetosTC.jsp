<%@page import="es.sronline.core.Constantes"%>
<%@page import="es.sronline.bbdd.dto.TipoObjetoTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.AccionesTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
function Cancelar(){
	document.location.href = 'SrvGestTipoObjetosTelecontrol';
}

</script>
<%
TipoObjetoTelecontrol tipoObjetoTelecontrol = null;
Object objVal = request.getAttribute("objeto_telecontrol");
if (objVal != null)
	 tipoObjetoTelecontrol = (TipoObjetoTelecontrol) objVal; 

AccionesTelecontrol[] lstAccionesTelecontrolDao = null;
Object obj_2 = request.getAttribute("lstAccionesTelecontrolDao");	
if (obj_2 != null)
	lstAccionesTelecontrolDao = (AccionesTelecontrol[]) obj_2;


%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="modal" id="formularioModalAlta" style="display:block;" >
		<div class="formularioModal formularioModalSmall" >
		
			<h2>Tipo de Objeto Telecontrol</h2>
			<form action="SrvGestTipoObjetosTelecontrol"  id="gestionTipoObjTelecontrol" name="gestionTipoObjTelecontrol" method="post">			
				<div class="form-group row">
					<label for="Tipo Objeto" class="col-sm-4 col-sm-offset-1 control-label">Nombre del tipo</label>
					<input type="text" id="tipoObjeto" name="tipoObjeto" maxlength="45" class="form-control col-sm-9" value="<%= tipoObjetoTelecontrol.getTipoObjetoTelecontrol()%>"></input>
				</div>
				<div class="form-group row">
					<label for="Descripci&oacute;n" class="col-sm-4 col-sm-offset-1 control-label">Descripci&oacute;n</label>
					<input type="text" id="descripcion" name="descripcion" maxlength="6" class="form-control col-sm-9" value="<%= tipoObjetoTelecontrol.getDescripcion()%>"></input>
				</div>
				<div class="form-group row">
					<label for="Tiempo Respuesta" class="col-sm-4 col-sm-offset-1 control-label">Tiempo Respuesta</label>
					<input type="text" id="tiempoRespuesta" name="tiempoRespuesta" maxlength="6" class="form-control col-sm-9" value="<%= tipoObjetoTelecontrol.getTiempoRespuesta()%>"></input>
				</div>
				<div class="form-group">
					<input type="hidden" id="idTipoObjTeleControl" name="idTipoObjTeleControl" value="<%=tipoObjetoTelecontrol.getIdTipoObjetoTelecontrol()%>"></input>
					<input type="hidden" id="accion" name="accion" value="<%=Constantes.getAdminActualizar()%>"></input>
				</div>
				<h2>Abrir</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="abrir_comando" class="control-label">Comando</label>
		            	<input id="abrir_comando" name="abrir_comando" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[0].getComando()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_accion" class="control-label">Accion</label>
		            	<input id="abrir_accion" name="abrir_accion" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[0].getAccion()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_byte6" class="control-label">Byte 6</label>
		            	<input id="abrir_byte6" name="abrir_byte6" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[0].getByte6()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_crc" class="control-label">CRC</label>
		            	<input id="abrir_crc" name="abrir_crc" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[0].getTieneCRC()%>">
		    	    </div>
					<div class="col-xs-2 " >
		            	<label for="abrir_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="abrir_respuestaOk" name="abrir_respuestaOk" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[0].getRespuestaOK()%>">
		    	    </div>
				</div>
				<h2>Cerrar</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="cerrar_comando" class="control-label">Comando</label>
		            	<input id="cerrar_comando" name="cerrar_comando" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[1].getComando()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_accion" class="control-label">Accion</label>
		            	<input id="cerrar_accion" name="cerrar_accion" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[1].getAccion()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_byte6" class="control-label">Byte 6</label>
		            	<input id="cerrar_byte6" name="cerrar_byte6" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[1].getByte6()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_crc" class="control-label">CRC</label>
		            	<input id="cerrar_crc" name="cerrar_crc" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[1].getTieneCRC()%>">
		    	    </div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="cerrar_respuestaOk" name="cerrar_respuestaOk" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[1].getRespuestaOK()%>">
		    	    </div>
				</div>
				<h2>Consultar</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="consultar_comando" class="control-label">Comando</label>
		            	<input id="consultar_comando" name="consultar_comando" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[2].getComando()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_accion" class="control-label">Accion</label>
		            	<input id="consultar_accion" name="consultar_accion" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[2].getAccion()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_byte6" class="control-label">Byte 6</label>
		            	<input id="consultar_byte6" name="consultar_byte6" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[2].getByte6()%>">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_crc" class="control-label">CRC</label>
		            	<input id="consultar_crc" name="consultar_crc" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[2].getTieneCRC()%>">
		    	    </div>
					<div class="col-xs-2 " >
		            	<label for="consultar_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="consultar_respuestaOk" name="consultar_respuestaOk" type="text" class="form-control" style="width:100px" value="<%=lstAccionesTelecontrolDao[2].getRespuestaOK()%>">
		    	    </div>
				</div>

				<div class="row">
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="submit" class="btn" value=Aceptar alt="Aceptar" />
					</div>
					<div class="col-sm-3 col-sm-offset-1" style="margin-bottom: 10px; margin-top: 10px;">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />				
					</div>				
				</div>
				
			</form>
			
		</div>
	</div>

</body>
</html>
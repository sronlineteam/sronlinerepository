<%@page import="es.sronline.bbdd.dto.ObjetosTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.Emplazamiento"%>
<%@page import="es.sronline.bbdd.dto.TipoObjetoTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%

	 LstObjetosTelecontrol[]  lstObjTeleCtrl = null;
	 Object obj = request.getAttribute("lstObjTelecontrol");
	 if (obj != null)
		 lstObjTeleCtrl = (LstObjetosTelecontrol[]) obj; 

	 Emplazamiento[] lstEmplazamientos = null;
	 Object objEmpl = request.getAttribute("lstEmplazamientos");
	 if (objEmpl != null)
		 lstEmplazamientos = (Emplazamiento[]) objEmpl; 

	 TipoObjetoTelecontrol[] lstTipoObjetoTelecontrol = null;
	 Object objVal = request.getAttribute("lstTipoObjetoTelecontrol");
	 if (objVal != null)
		 lstTipoObjetoTelecontrol = (TipoObjetoTelecontrol[]) objVal; 
%>


<script type="text/javascript">

	function gestObjTelecontrol(idObj, nombre, accion){
		if(accion==<%=Constantes.getAdminEliminar()%>)
			if(!confirm("Eliminar el objeto"))
				return;
		document.getElementById("accion").value = accion;
		document.getElementById("idObjTeleControl").value = idObj;
		document.getElementById("nombreObjTeleControl").value = nombre;
		document.forms.gestionObjTelecontrol.submit();	
	}
	
	function putAccionNuevo(idObj, accion){
		document.getElementById("accion").value = <%=Constantes.getAdminInsertar()%>;	
	}
	
	
	function Cancelar(){
			//this.document.forms.cambiarProg.idProgramacion.value = "vacio";
			this.document.getElementById("formularioModalAlta").style.display ="none";
	}
	
	function NuevaProg(){
		this.document.getElementById("formularioModalAlta").style.display ="block";
	}
	

</script>
	<div class="modal" id="formularioModalAlta" style="display:none" >
		<div class="formularioModal" >
		
			<h2>Nuevo Objeto Telecontrol</h2>
			<form action="SrvConfigObjTelecontrol"  id="gestionObjTelecontrol" name="gestionObjTelecontrol" method="post">			
				<div class="form-group">
					<label for="nombre" class="col-sm-4 control-label">Nombre</label>
					<input type="text" id="nombre" name="nombre" maxlength="45" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="tipo_conexion" class="col-sm-4 control-label">Tipo Conexion</label>
					<input type="text" id="tipo_conexion" name="tipo_conexion" maxlength="6" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="conexion" class="col-sm-4 control-label">Conexion</label>
					<input type="text" id="conexion" name="conexion" maxlength="45" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="direccionHex" class="col-sm-4 control-label">Dir Hexadecimal</label>
					<input type="text" id="direccionHex" name="direccionHex" maxlength="8" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="estado" class="col-sm-4 control-label">Estado</label>
					<input type="text" id="estado" name="estado" maxlength="10" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="posicion" class="col-sm-4 control-label">Posicion</label>
					<input type="text" id="posicion" name="posicion" maxlength="4000" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="esclavo" class="col-sm-4 control-label">Esclavo</label>
					<input type="text" id="esclavo" name="esclavo" maxlength="16" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="controlTarifa" class="col-sm-4 control-label">Control de tarifa</label>
		            <input id="controlTarifa" name="controlTarifa" type="text" maxlength="16" class="form-control"></input>
		        </div>	        
				<div class="form-group">
					<label for="orden" class="col-sm-4 control-label">Orden</label> 
		            <input id="orden" name="orden" type="text" maxlength="3" class="form-control" ></input>
		        </div>	        
				<div class="form-group">
					<label for="idEmplazamiento" class="col-sm-4 control-label">Emplazamiento (id)</label> 
		            <select name="idEmplazamiento" id="idEmplazamiento" class="form-control">
		            <%
						for(int e = 0; e < lstEmplazamientos.length; e++){
					%>
										<option value="<%=lstEmplazamientos[e].getIdEmplazamiento() %>" ><%=lstEmplazamientos[e].getEmplazamiento()%></option>
					<%} %>
					</select>
		        </div>
				<div class="form-group">
					<label for="idTipoObjetoTC" class="col-sm-4 control-label">Tipo Objeto (id)</label> 
		            <select name="idTipoObjetoTC" id="idTipoObjetoTC" class="form-control">
		            <%
						for(int t = 0; t < lstTipoObjetoTelecontrol.length; t++){
					%>
										<option value="<%=lstTipoObjetoTelecontrol[t].getIdTipoObjetoTelecontrol() %>" ><%=lstTipoObjetoTelecontrol[t].getTipoObjetoTelecontrol()%></option>
					<%} %>
					</select>
		        </div>	        
				<div class="form-group">
					<input type="hidden" id="idObjTeleControl" name="idObjTeleControl"></input>
					<input type="hidden" id="nombreObjTeleControl" name="nombreObjTeleControl"></input>
					<input type="hidden" id="accion" name="accion"></input>
				</div>
						
				 <div class="form-group">
   					<div class="col-sm-4">
						<input type="submit" class="btn" value=Enviar alt="Cambiar" onclick="putAccionNuevo()" />
					</div>
   					<div class="�col-sm-4">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />
					</div>
				</div>
			</form>
			
		</div>
	</div>
	
	
	
	
        <p>
			<input type="button"  class="btn"  value="Nuevo" alt="Nuevo" onclick="NuevaProg()"/>
		</p>
		
		<table id="table" class="tablesorter" style="max-width:400px;">
			<thead> 
		        <tr>
		            <th class="nombre-obj">Objeto Telecontrol</th>
		            <th>Edici�n</th>
		            
		        </tr>
		    </thead>		
<%
	for(int v = 0; v < lstObjTeleCtrl.length; v++){
%>	
			<tr>
			    <td><%=lstObjTeleCtrl[v].getNombre()%></td>
			    <td class="text-center">
			    	<button class="btnCambiar" onclick='gestObjTelecontrol(<%=lstObjTeleCtrl[v].getIdObjeto()%>,"<%=lstObjTeleCtrl[v].getNombre()%>","<%=Constantes.getAdminMostrar()%>");'></button>
			    	<button class="btnEliminar" onclick='gestObjTelecontrol(<%=lstObjTeleCtrl[v].getIdObjeto()%>,"<%=lstObjTeleCtrl[v].getNombre()%>","<%=Constantes.getAdminEliminar()%>");'></button>
				</td>
			</tr>
<%
	}
%>	
		</table>

<script>
	$(document).ready(function(){
		$("#table").tablesorter({widgets: ['zebra']});
	});
</script>	

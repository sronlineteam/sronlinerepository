<%@page import="es.sronline.bbdd.dto.AccionesTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.TipoObjetoTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.Emplazamiento"%>
<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%

 TipoObjetoTelecontrol[] lstTipoObjetoTelecontrol = null;
 Object objVal = request.getAttribute("lstTipoObjetoTelecontrol");
 if (objVal != null)
	 lstTipoObjetoTelecontrol = (TipoObjetoTelecontrol[]) objVal; 
 
%>


<script type="text/javascript">

	function gestObjTelecontrol(idObj, accion){
		if(accion==<%=Constantes.getAdminEliminar()%>)
			if(!confirm("Eliminar el objeto"))
				return;
		document.getElementById("accion").value = accion;
		document.getElementById("idTipoObjTeleControl").value = idObj;
		document.forms.gestionTipoObjTelecontrol.submit();	
	}
	
	function putAccionNuevo(idObj, accion){
		document.getElementById("accion").value = <%=Constantes.getAdminInsertar()%>;	
	}
	
	
	function Cancelar(){
			//this.document.forms.cambiarProg.idProgramacion.value = "vacio";
			this.document.getElementById("formularioModalAlta").style.display ="none";
	}
	
	function NuevoTipo(){
		this.document.getElementById("formularioModalAlta").style.display ="block";
	}
	

</script>
	<div class="modal" id="formularioModalAlta" style="display:none" >
		<div class="formularioModal" >
		
			<h1>Nuevo Tipo de Objeto Telecontrol</h1>
			<form action="SrvGestTipoObjetosTelecontrol"  id="gestionTipoObjTelecontrol" name="gestionTipoObjTelecontrol" method="post">			
				<div class="form-group row">
					<label for="Tipo Objeto" class="col-sm-4 col-sm-offset-1 control-label">Nombre del tipo</label>
					<input type="text" id="tipoObjeto" name="tipoObjeto" maxlength="45" class="form-control"></input>
				</div>
				<div class="form-group row">
					<label for="Descripci&oacute;n" class="col-sm-4 col-sm-offset-1 control-label">Descripci&oacute;n</label>
					<input type="text" id="descripcion" name="descripcion" maxlength="6" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group row">
					<label for="Tiempo Respuesta" class="col-sm-4 col-sm-offset-1  control-label">Tiempo Respuesta</label>
					<input type="text" id="tiempoRespuesta" name="tiempoRespuesta" maxlength="6" class="form-control col-sm-9"></input>
				</div>
				<div class="form-group">
					<input type="hidden" id="idTipoObjTeleControl" name="idTipoObjTeleControl"></input>
					<input type="hidden" id="accion" name="accion"></input>
				</div>
				<h2>Abrir</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="abrir_comando" class="control-label">Comando</label>
		            	<input id="abrir_comando" name="abrir_comando" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_accion" class="control-label">Accion</label>
		            	<input id="abrir_accion" name="abrir_accion" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_byte6" class="control-label">Byte 6</label>
		            	<input id="abrir_byte6" name="abrir_byte6" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="abrir_crc" class="control-label">CRC</label>
		            	<input id="abrir_crc" name="abrir_crc" type="text" class="form-control" style="width:100px">
		    	    </div>
					<div class="col-xs-4 " >
		            	<label for="abrir_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="abrir_respuestaOk" name="abrir_respuestaOk" type="text" class="form-control" style="width:100px">
		    	    </div>
				</div>
				<h2>Cerrar</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="cerrar_comando" class="control-label">Comando</label>
		            	<input id="cerrar_comando" name="cerrar_comando" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_accion" class="control-label">Accion</label>
		            	<input id="cerrar_accion" name="cerrar_accion" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_byte6" class="control-label">Byte 6</label>
		            	<input id="cerrar_byte6" name="cerrar_byte6" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="cerrar_crc" class="control-label">CRC</label>
		            	<input id="cerrar_crc" name="cerrar_crc" type="text" class="form-control" style="width:100px">
		    	    </div>
					<div class="col-xs-4 " >
		            	<label for="cerrar_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="cerrar_respuestaOk" name="cerrar_respuestaOk" type="text" class="form-control" style="width:100px">
		    	    </div>
				</div>
				<h2>Consultar</h2>
				<div class="row">
					<div class="col-xs-2 " >
		            	<label for="consultar_comando" class="control-label">Comando</label>
		            	<input id="consultar_comando" name="consultar_comando" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_accion" class="control-label">Accion</label>
		            	<input id="consultar_accion" name="consultar_accion" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_byte6" class="control-label">Byte 6</label>
		            	<input id="consultar_byte6" name="consultar_byte6" type="text" class="form-control" style="width:100px">
					</div>
					<div class="col-xs-2 " >
		            	<label for="consultar_crc" class="control-label">CRC</label>
		            	<input id="consultar_crc" name="consultar_crc" type="text" class="form-control" style="width:100px">
		    	    </div>
					<div class="col-xs-4 " >
		            	<label for="consultar_respuestaOk" class="control-label">Resp OK</label>
		            	<input id="consultar_respuestaOk" name="consultar_respuestaOk" type="text" class="form-control" style="width:100px">
		    	    </div>
				</div>

				<div class="row">
					<div class="col-sm-3 " style="margin-bottom: 10px; margin-top: 10px;">
						<input type="submit" class="btn" value=Enviar alt="Embiar" onclick="putAccionNuevo()" />
					</div>
					<div class="col-sm-3 " style="margin-bottom: 10px; margin-top: 10px;">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />				
					</div>				
				</div>
				
			</form>
			
		</div>
	</div>
	
        <p>
			<input type="button"  class="btn"  value="Nuevo" alt="Nuevo" onclick="NuevoTipo()"/>
		</p>
		
		<table id="table" class="tablesorter" style="max-width:400px;">
			<thead> 
		        <tr>
		            <th class="nombre-obj">Tipos Objeto Telecontrol</th>
		            <th>Descripci�n</th>
		            <th>Acci�n</th>
		            
		        </tr>
		    </thead>		
<%
	for(int v = 0; v < lstTipoObjetoTelecontrol.length; v++){
%>	
			<tr>
			    <td><%=lstTipoObjetoTelecontrol[v].getTipoObjetoTelecontrol()%></td>
			    <td><%=lstTipoObjetoTelecontrol[v].getDescripcion()%></td>
			    <td class="text-center">
			    	<button class="btnCambiar" onclick='gestObjTelecontrol(<%=lstTipoObjetoTelecontrol[v].getIdTipoObjetoTelecontrol()%>,"<%=Constantes.getAdminMostrar()%>");'></button>
			    	<button class="btnEliminar" onclick='gestObjTelecontrol(<%=lstTipoObjetoTelecontrol[v].getIdTipoObjetoTelecontrol()%>,"<%=Constantes.getAdminEliminar()%>");'></button>
				</td>
			</tr>
<%
	}
%>	
		</table>

<script>
	$(document).ready(function(){
		$("#table").tablesorter({widgets: ['zebra']});
		$("#table").DataTable({
	        "language": {
		    	"aria": {
	    	        "sortAscending":  ": activar para ordenar la columna ascendentemente",
	    	        "sortDescending": ": activar para ordenar la columna descendentemente"
	    	    },
				"loadingRecords": "Cargando...",
	            "lengthMenu": "Registros por pagina _MENU_",
	            "zeroRecords": "Sin registros",
	            "info": "Mostrando pagina _PAGE_ de _PAGES_",
	            "infoEmpty": "Sin registros disponibles",
	            "infoFiltered": "(  numero total de registros _MAX_)",
	            "search":         "Buscar:",
	            "zeroRecords":    "No se encuentran registros",
	            "paginate": {
	                "first":      "Primero",
	                "last":       "Ultimo",
	                "next":       "Siguiente",
	                "previous":   "Anterior"
	            }
	        }
	    } ); 
	});
</script>


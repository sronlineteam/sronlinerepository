<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@page import="es.sronline.core.ProgramacionTelecontrolFormateada"%>
<%@page import="es.sronline.core.Common"%>
<%@page import="java.sql.Time"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="es.sronline.core.Programacion"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
List<ProgramacionTelecontrolFormateada> lstProgramaciones;
Object obj = request.getAttribute("lstProgramaciones");
DateFormat df3 = DateFormat.getDateInstance(DateFormat.LONG);
java.sql.Time sqlTime;
SimpleDateFormat formateador1 = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat formateador2 = new SimpleDateFormat("hh:mm");
String auxDia;
String auxHora;
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
// SimpleDateFormat sdfhora=new SimpleDateFormat("hh:mm");
 LstObjetosTelecontrol[] objetosTelecontrol = null;
 Object objTelec = request.getAttribute("objetosTelecontrol");
 if (objTelec != null)
	 objetosTelecontrol = (LstObjetosTelecontrol[]) objTelec; 

int auxIdProgramacion;

if (obj != null){
	lstProgramaciones = (List<ProgramacionTelecontrolFormateada>) obj; 

String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 }
 
 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
 String fHoy =  format.format(now);

%>
	
<script type="text/javascript">
	
function Cancelar(){
		this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModal").style.display ="none";
}

function EnviarModificacionProg(){
	if(this.document.forms.cambiarProg.hora.value == "")
		alert("Indique la hora!");
	else if(this.document.forms.cambiarProg.dia.value == "")
		alert("Indique la fecha!");	
	else
		this.document.forms.cambiarProg.submit();
}

function CambiarProg(idProgramacion, dia, hora){
	this.document.forms.cambiarProg.idProgramacion.value = idProgramacion;
	this.document.forms.cambiarProg.dia.value = dia;
	this.document.forms.cambiarProg.hora.value = hora;
	this.document.getElementById("formularioModal").style.display ="block";
}

function NuevaProg(){
	this.document.getElementById("formularioModalAlta").style.display ="block";
}

function CancelarNueva(){
	this.document.forms.cambiarProg.idProgramacion.value = "vacio";
	this.document.getElementById("formularioModalAlta").style.display ="none";
}

function eliminar(idProgramacion){
	if(confirm("Eliminar la programaci�n")){
		this.document.forms.eliminarProg.idProgramacion.value = idProgramacion;
		this.document.forms.eliminarProg.submit();
	}
}

function compara_fecha(fecha, fecha2){
	// La fecha de fin puede ser vac�a
	if (fecha2!=''){
		suma = new Date(fecha.substring(6,10),fecha.substring(3,5)-1,fecha.substring(0,2));
		var anio=suma.getYear();
		var mes =suma.getMonth()+1;
		var dia =suma.getDate();
		suma2 = new Date(fecha2.substring(6,10),fecha2.substring(3,5)-1,fecha2.substring(0,2));
		var anio2=suma2.getYear();
		var mes2 =suma2.getMonth()+1;
		var dia2 =suma2.getDate();
		if(anio<anio2){
			return false;
		}
		if(anio<=anio2 && mes<=mes2){
			if(anio==anio2 && mes==mes2 && dia>dia2){
				return false;
			}
			return true;
		}else{
			return false;
		}
	}else{
		return true;
	}
}

function enviarTelSelec(objetos, accion){
	if(objetos!=""){
		this.document.forms.bloqueTele.idObjetosTelecontrol.value = objetos;
		this.document.forms.bloqueTele.accion.value = accion;
		this.document.forms.bloqueTele.submit();
	}
	else
		alert("Seleccione al menos un objeto de telecontrol");
}

function abrirTelSelec(){
	if(this.document.forms.bloqueTele.hora.value == "")
		alert("Indique la hora!");
	else if(this.document.forms.bloqueTele.diaInicio.value == "")
		alert("Indique la fecha de inicio!");
	else if(!compara_fecha(this.document.forms.bloqueTele.diaInicio.value, this.document.forms.bloqueTele.diaFin.value))
		alert('La fecha inicial no puede ser mayor que la fecha final');
	else
		enviarTelSelec(recogerTelSelec(),"abrir");
}

function cerrarTelSelec(){
	if(this.document.forms.bloqueTele.hora.value == "")
		alert("Indique la hora!");
	else if(this.document.forms.bloqueTele.diaInicio.value == "")
		alert("Indique la fecha de inicio!");
	else if(!compara_fecha(this.document.forms.bloqueTele.diaInicio.value, this.document.forms.bloqueTele.diaFin.value))
		alert('La fecha inicial no puede ser mayor que la fecha final');
	else
		enviarTelSelec(recogerTelSelec(),"cerrar");
}

function recogerTelSelec(){
	var array = [];
	var cboxes = document.getElementsByName('selectTel');
    var len = cboxes.length;
    for (var i=0; i<len; i++) {
    	if(cboxes[i].checked)
	       array.push(cboxes[i].value);
    }
    return array;
}
</script>
<div class="modal" id="formularioModal" style="display:none" >
	<div class="formularioModal formularioModalSmall" >
		<h2>Programaci�n en Bloque</h2>
		<form action="SrvListadoProgramacionesTelecontrol" name="eliminarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="vacio" />
			<input type="hidden" name="accion" value="<%=Constantes.getAccionEliminar()%>" />
		</form>
		<form action="SrvListadoProgramacionesTelecontrol" name="cambiarProg" method="post" >
			<p>
				Indique nuevo d�a de la acci�n 
			</p>
			<p>
			<div class="container">
			    <div class="row">
			        <div class='col-sm-2'>
			            <div class="form-group">
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="dia"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
			        </div>
			        <script type="text/javascript">
			            $(function () {
			                $('#datetimepicker1').datetimepicker({
			                	format: 'DD-MM-YYYY'
			                });
			                
			            });
			        </script>
			    </div>
			 </div>
				
			</p>
			<p>
			 	Indique nueva hora de la acci�n 
			 </p>
			<p>
				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" name='hora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>
				<input type="hidden" name="idProgramacion" value="vacio" />  
				<input type="hidden" name="accion" value="<%=Constantes.getAccionCambiar()%>" />
			</p>
			
			<ul class="col3" id="bloque">
				<li>
					<input type="button" class="btn" value="Cambiar" alt="Cambiar" onclick="EnviarModificacionProg()" />
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />
				</li>
			</ul>
				
		</form>
		
	</div>

</div>

<div class="modal" id="formularioModalAlta" style="display:none" >
	<div class="formularioModal" >
		<h2>Programaci�n en Bloque</h2>
				<form action="SrvListadoProgramacionesTelecontrol" name="bloqueTele" method="post" >
					<div class="row">
						 <div class="col-md-6">
						<input type="hidden" name="idObjetosTelecontrol" value="vacio" />
						<div class="progListadoBloque">
		<%
			for(int v = 0; v < objetosTelecontrol.length; v++){
		%>
						<p>
							<input type="checkbox" class="checkTele" name="selectTel" value="<%=objetosTelecontrol[v].getIdObjeto()%>" >
							<input type="text" class="textoplano" value="<%=objetosTelecontrol[v].getNombre() %>" />
						</p> 
		<%
			}
		%>
						</div>
						</div>
					  	<div class="col-md-6">
						<label for="tipo_conexion" class="col-sm-6 control-label">Indique d�a de inicio de la acci�n</label>
						<div class="container">
						    <div class="row">
						        <div class='col-sm-2'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker3'>
						                    <input type='text' class="form-control" name="diaInicio"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
									$(function () {
									    $('#datetimepicker3').datetimepicker({
									    	format: 'DD-MM-YYYY'
									    });
									    
									});
								</script>
							</div>
						</div>
						<label for="tipo_conexion" class="col-sm-6 control-label">Indique d�a de fin de la acci�n</label> 
						<div class="container">
						    <div class="row">
						        <div class='col-sm-2'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker4'>
						                    <input type='text' class="form-control" name="diaFin"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
									$(function () {
									    $('#datetimepicker4').datetimepicker({
									    	format: 'DD-MM-YYYY'
									    });
									    
									});
								</script>
							</div>
						</div>
					 	<label for="tipo_conexion" class="col-sm-6 control-label">Indique hora de la acci�n</label>
						<div class="container">
						    <div class="row">
						        <div class='col-sm-2'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker5'>
								            <input type='text' class="form-control" name='hora'  />
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-time"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
									$(function () {
									    $('#datetimepicker5').datetimepicker({
									    	format: 'LT'
									    });
									});
								</script>
							</div>
						</div>
						<input type="hidden" name="idProgramacion" value="vacio" />  
						<input type="hidden" name="accion" value="<%=Constantes.getAccionCambiar()%>" />
						<div class="form-group">
		  					<div class="col-sm-3">
								<input type="button" class="btnAbrir" value="" alt="Abrir" onclick="abrirTelSelec()" />
							</div>
							<div class="col-sm-3">
								<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarTelSelec()" />
							</div>
							<div class="col-sm-3">
								<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="CancelarNueva()" />
							</div>
						</div>
					</div>
					</div>
				</form>
			</div>
</div>


<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<p>
	<input type="button"  value="Nueva" alt="Nueva" onclick="NuevaProg()" class="btn"/>
</p>
<table class="tablesorter" id="listado">
	<thead> 
		<tr>
			<th>Objeto telecontrol</th>
			<th>Cambia a</th>
			<th width="50px">Ejec.</th>
			<th>D�a</th>
			<th>hora</th>
			<th>Acci�n</th>
		</tr>
	</thead>
	<tbody> 
	
<%
	String[] tipo = {"odd","even"} ;
	int id = 0;
	for (int l = 0; l < lstProgramaciones.size();l++){
		auxDia = sdf.format(lstProgramaciones.get(l).getFechaHora());
		auxHora = lstProgramaciones.get(l).getFechaHoraFormateada().substring(11, 16);
		auxIdProgramacion = lstProgramaciones.get(l).getIdprogramacion();
		id = l%2;
%>
	<tr>
		<td><%=lstProgramaciones.get(l).getNombreObjeto() %></td>
		<td class="tabla<%=lstProgramaciones.get(l).getAccionFormateada().toLowerCase()%>"><%=lstProgramaciones.get(l).getAccionFormateada() %>	</td>
		<td><div class="estado"><img src="resources/img/estado<%=lstProgramaciones.get(l).getEnvio()%>.png" width="20" height="20" /></div></td>	
		<td><%=auxDia%></td>
		<td><%=auxHora%></td>
		<td>
<%
	if(lstProgramaciones.get(l).getEnvio()<2){
%>			<input type="button" class="btnCambiar" value="" alt="Cambiar" onclick="CambiarProg(<%=auxIdProgramacion%>, '<%=auxDia%>', '<%=auxHora%>')"/>
			<input type="button" class="btnEliminar" value="" alt="Eliminar" onclick="eliminar(<%=auxIdProgramacion%>)"/>
<%
	}
%>			
		</td>
	</tr>
<%		
	}
%>
	</tbody>
</table>
<p>
	<input type="button"  value="Nueva" alt="Nueva" onclick="NuevaProg()" class="btn"/>
</p>

<%
	if(lstProgramaciones.size()>0){
%>

<script type="text/javascript">

$(function() {
	$("#listado").tablesorter({sortList:[[3,0],[4,0]], widgets: ['zebra']});
});


</script>
<%
	}
%>



<%
}
else{
%>
<h1>Listado de acciones sobre objetos de telecontrol</h1>
<h2>No hay ninguna objeto</h2>
<%} %>


<%@page import="es.sronline.core.Common"%>
<%@page import="es.sronline.core.ProgramacionFormateada"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="es.sronline.core.Programacion"%>
<%@page import="es.sronline.bbdd.dto.LstValvulas"%>
<%@page import="java.util.List"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
<%
List<ProgramacionFormateada> lstProgramaciones;
int numeroFilas = 0;
Object obj = request.getAttribute("lstProgramaciones");
DateFormat df3 = DateFormat.getDateInstance(DateFormat.LONG);
SimpleDateFormat formateador1 = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat formateador2 = new SimpleDateFormat("hh:mm");
String auxDia;
String auxHora;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
// SimpleDateFormat sdfhora=new SimpleDateFormat("hh:mm");
 LstValvulas[] lstValvulas = null;
 Object objVal = request.getAttribute("lstValvulas");
 if (objVal != null)
	 lstValvulas = (LstValvulas[]) objVal; 

int auxIdProgramacion;

if (obj != null){
	lstProgramaciones = (List<ProgramacionFormateada>) obj; 
	numeroFilas = lstProgramaciones.size();

	String resultado = "";
 	Object oRespuesta = request.getAttribute("resultado");
	if (oRespuesta != null){
		 resultado = (String) oRespuesta;
		 resultado = (resultado.trim()!="")?resultado + " - cerrar [x]":""; 
 	}
 
	Date now = new Date();
	SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
	String fHoy =  format.format(now);

%>
	
<script type="text/javascript">
	
function Cancelar(){
		this.document.forms.cambiarProg.idProgramacion.value = "vacio";
		this.document.getElementById("formularioModal").style.display ="none";
}

function CambiarProg(idProgramacion, dia, hora){
	this.document.forms.cambiarProg.idProgramacion.value = idProgramacion;
	this.document.forms.cambiarProg.dia.value = dia;
	this.document.forms.cambiarProg.hora.value = hora;
	this.document.getElementById("formularioModal").style.display ="block";
}

function NuevaProg(){
	this.document.getElementById("formularioModalAlta").style.display ="block";
}

function CancelarNueva(){
	this.document.forms.cambiarProg.idProgramacion.value = "vacio";
	this.document.getElementById("formularioModalAlta").style.display ="none";
}

function eliminar(idProgramacion){
	if(confirm("Eliminar la programaci�n")){
		this.document.forms.eliminarProg.idProgramacion.value = idProgramacion;
		this.document.forms.eliminarProg.submit();
	}
}
</script>

<div class="modal" id="formularioModal" style="display:none" >
	<div class="formularioModal formularioModalSmall" >
		<h2>Programaci�n en Bloque </h2>
		<form action="SrvListadoProgramaciones" name="eliminarProg" method="post" >
			<input type="hidden" name="idProgramacion" value="vacio" />
			<input type="hidden" name="accion" value="<%=Constantes.getAccionEliminar()%>" />
		</form>
		<form action="SrvListadoProgramaciones" name="cambiarProg" method="post" >
			<p>
				Indique nuevo d�a de la acci�n 
			</p>
			<p>
			<div class="container">
			    <div class="row">
			        <div class='col-sm-2'>
			            <div class="form-group">
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="dia"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
			        </div>
			        <script type="text/javascript">
			            $(function () {
			                $('#datetimepicker1').datetimepicker({
			                	format: 'DD-MM-YYYY'
			                });
			                
			            });
			        </script>
			    </div>
			 </div>
				
			</p>
			<p>
			 	Indique nueva hora de la acci�n 
			 </p>
			<p>
				<div class="container">
				    <div class="row">
				        <div class='col-sm-2'>
				            <div class="form-group">
				                <div class='input-group date' id='datetimepicker2'>
						            <input type='text' class="form-control" name='hora'  />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
				                </div>
				            </div>
				        </div>
				        <script type="text/javascript">
				            $(function () {
				                $('#datetimepicker2').datetimepicker({
				                	format: 'LT'
				                });
				            });
				        </script>
				    </div>
				</div>
				<input type="hidden" name="idProgramacion" value="vacio" />  
				<input type="hidden" name="accion" value="<%=Constantes.getAccionCambiar()%>" />
			</p>
			<ul class="col3" id="bloque" style="padding-left: 10px;">
				<li>
					<input type="submit" class="btn" value="Cambiar" alt="Cambiar" />
				</li>
				<li>
					<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />
				</li>
			</ul>
		</form>
		
	</div>

</div>

<div class="modal" id="formularioModalAlta" style="display:none" >
	<div class="formularioModal" >
		<h2>Programaci�n en Bloque</h2>
		<form action="SrvListadoProgramaciones" name="bloqueValv" method="post" >
			<div class="row">
				<div class='col-sm-6'>
					<input type="hidden" name="valvulas" value="vacio" />
		<%
			for(int v = 0; v < lstValvulas.length; v++){
		%>
					<p>
						<input type="checkbox" class="checkValv" name="selectVal" value="v<%=lstValvulas[v].getIdRemota()%>-<%=lstValvulas[v].getIdvalvula()%>" >
						<input type="text" class="textoplano" value="<%=lstValvulas[v].getValvula() %>" />
					</p> 
		<%
			} 
		%>		</div>
				<div class='col-sm-6'>
					<p>
						Indique d�a de la acci�n 
					<div class="container">
					    <div class="row">
					        <div class='col-sm-2'>
					            <div class="form-group">
					                <div class='input-group date' id='datetimepicker3'>
					                    <input type='text' class="form-control" name="dia"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					        </div>
					        <script type="text/javascript">
					            $(function () {
					                $('#datetimepicker3').datetimepicker({
					                	format: 'DD-MM-YYYY'
					                });
					                
					            });
					        </script>
					    </div>
					 </div>
					</p>
					<p>
					 	Indique hora de la acci�n 
						<div class="container">
						    <div class="row">
						        <div class='col-sm-2'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker4'>
								            <input type='text' class="form-control" name='hora'  />
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-time"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
						            $(function () {
						                $('#datetimepicker4').datetimepicker({
						                	format: 'LT'
						                });
						            });
						        </script>
						    </div>
						</div>
						<input type="hidden" name="idProgramacion" value="vacio" />  
						<input type="hidden" name="accion" value="<%=Constantes.getAccionCambiar()%>" />
					</p>
					<div class="row">
						<div class="col-sm-12">
							<ul class="col3" id="bloque">
								<li>
									<input type="button" class="btnAbrir" value="" alt="Abrir" onclick="abrirValSelec()" />
								</li>
								<li>
									<input type="button" class="btnCerrar" value="" alt="Cerrar" onclick="cerrarValSelec()" />
								</li>
								<li>
									<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="CancelarNueva()" />
								</li>
							</ul>
						</div>
					</div>	
				</div>
			</div>
		</form>
		
	</div>

</div>


<div class="respuesta"  id="procesando"  onclick="this.innerHTML=''"><%=resultado %></div>
<table id="lista" class="tablesorter" >
	<thead> 
		<tr>
			<th>Remota</th>
			<th>Valvula</th>
			<th>Cambia a</th>
			<th>D�a</th>
			<th>hora</th>
			<th>Acci�n</th>
		</tr>
	</thead>
	<tbody> 
	
<%
	String[] tipo = {"odd","even"} ;
	int id = 0;
	for (int l = 0; l < lstProgramaciones.size();l++){
		auxDia = sdf.format(lstProgramaciones.get(l).getFechaHora());
		auxHora = lstProgramaciones.get(l).getFechaHoraFormateada().substring(11, 16);
		auxIdProgramacion = lstProgramaciones.get(l).getIdprogramacion();
		id = l%2;
%>
	<tr style="border: 1px dotted gray;">
		<td><%=lstProgramaciones.get(l).getIdRemota() %></td>
		<td><%=lstProgramaciones.get(l).getNombreValvula() %></td>
		<td class="tabla<%=lstProgramaciones.get(l).getAccionFormateada().toLowerCase()%>"><%=lstProgramaciones.get(l).getAccionFormateada() %>	</td>
		<td><%=auxDia%></td>
		<td><%=auxHora%></td>
		<td><input type="button" class="btnCambiar" value="" alt="Cambiar" onclick="CambiarProg(<%=auxIdProgramacion%>, '<%=auxDia%>', '<%=auxHora%>')"/>
		<input type="button" class="btnEliminar" value="" alt="Eliminar" onclick="eliminar(<%=auxIdProgramacion%>)"/></td>
		
	</tr>
<%		
	}
%>
	</tbody>
</table>
<%
	if(numeroFilas>0){
%>

<script type="text/javascript">

$(document).ready(function(){
	$("#lista").tablesorter({sortList:[[3,0],[4,0]]});
	$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	$("#lista").DataTable({
        "language": {
	    	"aria": {
    	        "sortAscending":  ": activar para ordenar la columna ascendentemente",
    	        "sortDescending": ": activar para ordenar la columna descendentemente"
    	    },
			"loadingRecords": "Cargando...",
            "lengthMenu": "Registros por pagina _MENU_",
            "zeroRecords": "Sin registros",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "Sin registros disponibles",
            "infoFiltered": "(  numero total de registros _MAX_)",
            "search":         "Buscar:",
            "zeroRecords":    "No se encuentran registros",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    } ); 
	
});


</script>
<%
	}
%>


<p>
	<input type="button"  value="Nueva" alt="Nueva" onclick="NuevaProg()" class="btn"/>
</p>
<%
}
else{
%>
<h1>Listado de acciones sobre v�lvulas</h1>
<h2>No hay ninguna programaci�n</h2>
<%} %>


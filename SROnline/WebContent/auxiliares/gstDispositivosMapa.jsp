<%@page import="es.sronline.bbdd.dto.ObjetosTelecontrol"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.LstValvulas"%>
<%@page import="es.sronline.bbdd.dto.LstObjetosTelecontrol"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
 <%
 String dialogo = Constantes.isConAvisos();
 Object oDialogo = request.getAttribute("dialogo");
 if(oDialogo != null)
	 dialogo = (String) oDialogo;
 //confirmacion
 String confirmacion = "";
 Object oConfirmacion = request.getAttribute("confirmacion");
 if(oConfirmacion != null)
	 confirmacion = (String) oConfirmacion;

 String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - [x]":""; 
 }
 
 
 LstObjetosTelecontrol[]  lstObjTeleCtrl = null;
 Object obj = request.getAttribute("lstObjTelecontrol");
 if (obj != null)
	 lstObjTeleCtrl = (LstObjetosTelecontrol[]) obj; 

 %>

<script type="text/javascript">

var peticion = "";

function limpiarAviso(){
	  document.getElementById("procesando").innerHTML = peticion;
}

function accionComun(idObjeto, accion, controlTarifa, mensaje){
	document.getElementById("contenido").style.opacity="0.2";
	this.document.forms.gestionObjeto.accion.value = accion;
	this.document.forms.gestionObjeto.idObjeto.value = idObjeto;
	this.document.forms.gestionObjeto.controlTarifa.value = controlTarifa;
	this.document.forms.gestionObjeto.submit();
}

function confirmarAbrirObjeto(idObjeto, multiple){
	if(multiple == 'si'){
		this.document.forms.bloqueObj.confirmacion.value =  "<%=Constantes.CONFIRMADA %>";

		abrirObjSelec();
	}
	else{
		this.document.forms.gestionObjeto.confirmacion.value = "<%=Constantes.CONFIRMADA %>";
		$('#abrirId'+idObjeto).click();
	}
}
function rechazar(){
	  document.getElementById("procesando").innerHTML = peticion;
}


function abrirObj(idObjeto, controlTarifa){
	accionComun(idObjeto, "<%=Constantes.getAccionAbrir() %>", controlTarifa, "Abriendo Objeto");
}
function cerrarObj(idObjeto){
	accionComun(idObjeto, "<%=Constantes.getAccionCerrar() %>", 0, "Cerrando Objeto");
}
function estadoObj(idObjeto){
	accionComun(idObjeto, "<%=Constantes.getAccionEstado()%>", 0, "Consultando Objeto");
}

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3OAwCyL_q_ZDhrxHjcp1zjvPp9FUlwDI&callback=initMap"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&language=es"></script> 

<script type="text/javascript">

var dipositivos=[];

<%
for (int v = 0; v < lstObjTeleCtrl.length; v++) { 
	StringBuffer auxDescripObjeto = new StringBuffer();
	auxDescripObjeto.append("{idObjeto:'").append(lstObjTeleCtrl[v].getIdObjeto());
	auxDescripObjeto.append("',tipoConexion:'").append(lstObjTeleCtrl[v].getTipoConexion());
	auxDescripObjeto.append("',datosConexion:'").append(lstObjTeleCtrl[v].getConexion());
	auxDescripObjeto.append("',DireccionHex:'").append(lstObjTeleCtrl[v].getDireccionHex());
	auxDescripObjeto.append("',nombre:'").append(lstObjTeleCtrl[v].getNombre());
	auxDescripObjeto.append("',estado:'").append(lstObjTeleCtrl[v].getEstado().toLowerCase());
	auxDescripObjeto.append("',posicion:'").append(lstObjTeleCtrl[v].getPosicion());
	auxDescripObjeto.append("',crtlTarifa:'").append(lstObjTeleCtrl[v].getControlTarifa());
	auxDescripObjeto.append("'}");
%>

	dipositivos.push(<%=auxDescripObjeto.toString()%>);

<%}%>


</script>

<script type="text/javascript">
var map;

var markers = [];

var infos = [];

var activeIndex=0;

	function initialize() {

		var myLatlng = new google.maps.LatLng(dipositivos[0].posicion.split(",")[0], 
				                              dipositivos[0].posicion.split(",")[1]);

		var mapOptions = {
			zoom : 16,
			center : myLatlng,
			mapTypeId : google.maps.MapTypeId.SATELLITE
		};

		map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);

		addMarkerdipositivos();
	}

	function openIndication(index) {

		infos[activeIndex].close(map, markers[activeIndex]);

		infos[index].open(map, markers[index]);
		activeIndex = index;
	}

	function addMarkerdipositivos() {

		var summaryPanel = document.getElementById("dipositivos_panel");
		summaryPanel.innerHTML = "";

		var menu = document.createElement("ul");

		var inp;

		for ( var i = 0; i < dipositivos.length; i++) {
			inp = document.createElement("li");
			inp.setAttribute("onclick", "openIndication(" + i + ");");
			inp.setAttribute("class", "listaMapa"+dipositivos[i].estado);
			inp.setAttribute("style", "width:95%");
			inp.innerHTML = getDispositivoShortInfo(dipositivos[i]);
			menu.appendChild(inp);
			addMarker(i, dipositivos[i]);
		}
		summaryPanel.appendChild(menu);
	}

	function getDispositivoShortInfo(_dispositivo) {
		var desc = "<a href=\'#\'>" + _dispositivo.nombre + "</a>";
		return desc;
	}

	function getDispositivoInfo(_dispositivo) {
		var desc = "<b>" + _dispositivo.nombre
				+ "</b><br/><br/>";
		desc += "<i class='tabla" + _dispositivo.estado.toLowerCase() + "'>- Estado: " + _dispositivo.estado + "</i><br/>";
		desc +="<input type='button' class='btnAbrir' value='' alt='Abrir' onclick='abrirObj("+_dispositivo.idObjeto+","+_dispositivo.crtlTarifa+")' />";

		desc +="<input type='button' class='btnCerrar' value='' alt='Cerrar' onclick='cerrarObj("+_dispositivo.idObjeto+")' />";

		desc +="<input type='button' class='btnEstado' value='' alt='Estado' onclick='estadoObj("+_dispositivo.idObjeto+")' />";

		
		return desc;
	}

	function addMarker(index, _dispositivo) {

		var contentString = getDispositivoInfo(_dispositivo);

		var info = new google.maps.InfoWindow({
			content : contentString
		});

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(_dispositivo.posicion.split(",")[0],
					                          _dispositivo.posicion.split(",")[1]),
			map : map,
			draggable : false,
			animation : google.maps.Animation.DROP
		});

		infos.push(info);

		google.maps.event.addListener(marker, "click", function() {
			infos[index].open(map, marker);
		});

		markers.push(marker);
	}
</script>


	<div style="width: 20%;float: right;overflow-y: scroll;max-height: 95%;">
		<div id="dipositivos_panel" ></div>
	</div>
	<div id="map_canvas" style="float: left; width: 80%; height: 95%;"></div>
		

<% if(!dialogo.equals(Constantes.DIALOGO_VACIO)) {
		if(dialogo.equals(Constantes.DIALOGO_INFORMAR) || dialogo.equals(Constantes.DIALOGO_ERROR)){
%>
			<div class="respuesta_modal"  id="procesando" onclick="this.style.display='none'" ><%=resultado %></div>
<%
		}
		else if(dialogo.equals(Constantes.DIALOGO_CONFIRMAR)){
%>
<div class="respuesta_modal"  id="procesando"  ><%=resultado %><br/>
	<input type="button" value="Confirmar" onclick="confirmarAbrirObjeto(<%=confirmacion %>)" /> 
	<input type="button" value="Rechazar" onclick="limpiarAviso()" /> 
	
</div>
<%
		}
	}
%>
	
	
		<form action="SrvGstObjetosTelecontrol" name="gestionObjeto" method="post" >
			<input type="hidden" name="accion" /> 	
			<input type="hidden" name="idObjeto" />
			<input type="hidden" name="confirmacion" />
			<input type="hidden" name="controlTarifa" />
			<input type="hidden" name="mapa" value="1">
		</form>
	
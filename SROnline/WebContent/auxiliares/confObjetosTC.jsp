<%@page import="es.sronline.bbdd.dto.Emplazamiento"%>
<%@page import="es.sronline.bbdd.dto.TipoObjetoTelecontrol"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.lang.Object"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.Integer"%>
<%@page import="es.sronline.bbdd.dto.AccionesTelecontrol"%>
<%@page import="es.sronline.bbdd.dto.ObjetosTelecontrol"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<script type="text/javascript">
function Cancelar(){
	document.location.href = 'SrvConfigObjTelecontrol';
}

</script>
<%
	String idObjTelecontrol = null;
		Object obj_0 = request.getAttribute("idObjTelecontrol");
		if (obj_0 != null)
			idObjTelecontrol = (String) obj_0;
	
	ObjetosTelecontrol objTeleControl = null;
	Object obj_1 = request.getAttribute("objeto_telecontrol");
	if (obj_1 != null)
		objTeleControl = (ObjetosTelecontrol) obj_1;
	
		
	 Emplazamiento[] lstEmplazamientos = null;
	 Object objEmpl = request.getAttribute("lstEmplazamientos");
	 if (objEmpl != null)
		 lstEmplazamientos = (Emplazamiento[]) objEmpl; 

	 TipoObjetoTelecontrol[] lstTipoObjetoTelecontrol = null;
	 Object objVal = request.getAttribute("lstTipoObjetoTelecontrol");
	 if (objVal != null)
		 lstTipoObjetoTelecontrol = (TipoObjetoTelecontrol[]) objVal; 
%>


  
<style>	
	
	table{
		margin-top:2rem;
	}
	th,td{
	    text-align: center;
	    min-width: 20rem;
	    padding: 2px;
	    border: none;
	}
	th{
		background-color: #F6FAFA;
		color: #0067b4;
	}
	
	.no-border{
		border:none;
	}
	
</style>

	<div class="modal" id="formularioModalAlta" style="display:block" >
		<div class="formularioModal" >
		
			<h2>Edici�n Objeto Telecontrol</h2>
			<form action="SrvConfigObjTelecontrol"  id="gestionObjTelecontrol" name="gestionObjTelecontrol" method="post">			
				<div class="form-group">
					<label for="nombre" class="col-sm-4 control-label">Nombre</label>
					<input type="text" id="nombre" name="nombre" maxlength="45" class="form-control" value="<%=objTeleControl.getNombre()%>"></input>
				</div>
				<div class="form-group">
					<label for="tipo_conexion" class="col-sm-4 control-label">Tipo Conexion</label>
					<input type="text" id="tipo_conexion" name="tipo_conexion" maxlength="6" class="form-control" value="<%=objTeleControl.getTipoConexion()%>"></input>
				</div>
				<div class="form-group">
					<label for="conexion" class="col-sm-4 control-label">Conexion</label>
					<input type="text" id="conexion" name="conexion" maxlength="45" class="form-control" value="<%=objTeleControl.getConexion()%>"></input>
				</div>
				<div class="form-group">
					<label for="direccionHex" class="col-sm-4 control-label">Direccion Hexadecimal</label>
					<input type="text" id="direccionHex" name="direccionHex" maxlength="8" class="form-control" value="<%=objTeleControl.getDireccionHex()%>"></input>
				</div>
				<div class="form-group">
					<label for="estado" class="col-sm-4 control-label">Estado</label>
					<input type="text" id="estado" name="estado" maxlength="10" class="form-control" value="<%=objTeleControl.getEstado()%>"></input>
				</div>
				<div class="form-group">
					<label for="posicion" class="col-sm-4 control-label">Posicion</label>
					<input type="text" id="posicion" name="posicion" maxlength="4000" class="form-control" value="<%=objTeleControl.getPosicion()%>"></input>
				</div>
				<div class="form-group">
					<label for="esclavo" class="col-sm-4 control-label">Esclavo</label>
					<input type="text" id="esclavo" name="esclavo" maxlength="16" class="form-control" value="<%=objTeleControl.getEsclavo()%>"></input>
				</div>
				<div class="form-group">
					<label for="controlTarifa" class="col-sm-4 control-label">Control de tarifa</label>
		            <input id="controlTarifa" name="controlTarifa" type="text" maxlength="16" class="form-control" value="<%=objTeleControl.getControlTarifa()%>">
		        </div>	        
				<div class="form-group">
					<label for="orden" class="col-sm-4 control-label">Orden</label> 
		            <input id="orden" name="orden" type="text" maxlength="3" class="form-control" value="<%=objTeleControl.getOrden()%>"></input>
		        </div>	        
				<div class="form-group">
					<label for="idEmplazamiento" class="col-sm-4 control-label">Emplazamiento (id)</label> 
		            <select name="idEmplazamiento" id="idEmplazamiento" class="form-control" >
		            <%
						String valChecked;
						for(int v = 0; v < lstEmplazamientos.length; v++){
							valChecked = "";
							if(lstEmplazamientos[v].getIdEmplazamiento()== objTeleControl.getIdEmplazamiento())
								valChecked = "selected";
					%>
										<option value="<%=lstEmplazamientos[v].getIdEmplazamiento() %>" <%=valChecked %> ><%=lstEmplazamientos[v].getEmplazamiento()%></option>
					<%
						} 
					%>
					</select>
		        </div>
				<div class="form-group">
					<label for="idTipoObjetoTC" class="col-sm-4 control-label">Tipo Objeto (id)</label> 
		            <select name="idTipoObjetoTC" id="idTipoObjetoTC" class="form-control" >
		            <%
						String valChecked2;
						for(int v = 0; v < lstTipoObjetoTelecontrol.length; v++){
							valChecked2 = "";
							if(lstTipoObjetoTelecontrol[v].getIdTipoObjetoTelecontrol()== objTeleControl.getIdTipoObjetoTelecontrol())
								valChecked2 = "selected";
					%>
										<option value="<%=lstTipoObjetoTelecontrol[v].getIdTipoObjetoTelecontrol() %>" <%=valChecked2 %> ><%=lstTipoObjetoTelecontrol[v].getTipoObjetoTelecontrol()%></option>
					<%
						} 
					%>
					</select>
		        </div>	        
				<div class="form-group">
				    <input type="hidden" id="idObjTeleControl" name="idObjTeleControl" value="<%=idObjTelecontrol%>"></input>
					<input type="hidden" id="accion" name="accion" value="2"></input>
				</div>
						
				 <div class="form-group">
   					<div class="col-sm-3">
						<input type="submit" class="btn" value=Aceptar alt="Aceptar"  />
					</div>
   					<div class="�col-sm-3">
						<input type="button" class="btn" value="Cancelar" alt="Cancelar" onclick="Cancelar()" />
					</div>
				</div>
			</form>
			
		</div>
	</div>

</body>
</html>
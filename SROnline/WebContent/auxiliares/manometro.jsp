<%@page import="es.sronline.graficos.GrafConstantes"%>
<% 
	String manometroConexion      = request.getParameter("manometroConexion");
	String manometroEtiquetas     = request.getParameter("manometroEtiquetas");
	String manometroTrazas        = request.getParameter("manometroTrazas");
	String manometroIntervalo     = request.getParameter("manometroIntervalo");
	String manometroPosicionDatos = request.getParameter("manometroPosicionDatos");
	String[] etiquetas = manometroEtiquetas.split("/");
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["gauge"]});
	google.setOnLoadCallback(drawChart);

	 function drawChart() {
		var jsonData = $.ajax({
			type:'POST',
			dataType: 'json',
			data: '<%=GrafConstantes.PARAMETRO_CONEXION %>=<%=manometroConexion%>&<%=GrafConstantes.PARAMETRO_TRAZAS%>=<%=manometroTrazas%>&<%=GrafConstantes.PARAMETRO_DATOS%>=<%=manometroPosicionDatos%>&<%=GrafConstantes.PARAMETRO_ETIQUETAS%>=<%=manometroEtiquetas%>',
			url: 'SrvConsultaDato',
			async: false
		}).responseText;
		 
		 
		var datos = new google.visualization.DataTable(jsonData);
		
		var options = {
			width: 400, height: 120,
			redFrom: 6, redTo: 15,
			yellowFrom:4, yellowTo: 6,
			mayorTicks: 15,
			minorTicks: 2,
			max: 15
		};
	
		var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
	
		chart.draw(datos, options);
    }
	

	 
	(
		function poll() {
		 	setTimeout(function() {
				$.ajax({
					type:'POST',
					dataType: 'application/json; charset=utf-8',
					data: '<%=GrafConstantes.PARAMETRO_CONEXION %>=<%=manometroConexion%>&<%=GrafConstantes.PARAMETRO_TRAZAS%>=<%=manometroTrazas%>&<%=GrafConstantes.PARAMETRO_DATOS%>=<%=manometroPosicionDatos%>',
					url: 'SrvConsultaDato',
					success: drawChart(),
					complete: poll
					});
				},
				<%=manometroIntervalo%>);
		}
	)();
	</script>
    
    <div id="chart_div" style="width: 400px; height: 120px;"></div>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="es.sronline.bbdd.dto.LstValvulas"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page  language="java" import="es.sronline.core.Constantes" %>
 <%
 String dialogo = Constantes.isConAvisos();
 Object oDialogo = request.getAttribute("dialogo");
 if(oDialogo != null)
	 dialogo = (String) oDialogo;
 //confirmacion
 String confirmacion = "";
 Object oConfirmacion = request.getAttribute("confirmacion");
 if(oConfirmacion != null)
	 confirmacion = (String) oConfirmacion;

 LstValvulas[] lstValvulas = null;
 Object obj = request.getAttribute("lstValvulas");
 if (obj != null)
	 lstValvulas = (LstValvulas[]) obj; 

 String resultado = "";
 Object oRespuesta = request.getAttribute("resultado");
 if (oRespuesta != null){
	 resultado = (String) oRespuesta;
	 resultado = (resultado.trim()!="")?resultado + " - [x]":""; 
 }
 Date now = new Date();
 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
 String fHoy =  format.format(now);
 now.setTime(now.getTime() + 86400000);
 String fMa�ana = format.format(now);
 
 %>


<script type="text/javascript">

var peticion = "";

function limpiarAviso(){
	  document.getElementById("procesando").innerHTML = peticion;
}

function cambiarFecha(fecha){
	var hoy = "<%=fHoy%>";
	var ma�ana = "<%=fMa�ana%>";
	if(fecha==ma�ana){
		this.document.forms.bloqueValv.cambiarFechaM.style.display='none';
		this.document.forms.bloqueValv.cambiarFechaH.style.display='inherit';
		this.document.forms.bloqueValv.dia.value = fecha;
	}
	else{
		this.document.forms.bloqueValv.cambiarFechaH.style.display='none';
		this.document.forms.bloqueValv.cambiarFechaM.style.display='inherit';
		this.document.forms.bloqueValv.dia.value = fecha;
	}
			
}


function accionComun(remota, valvula, accion, mensaje){
	document.getElementById("contenido").style.opacity="0.2";
//	arrayValvulas[remota][valvula]=false;
	document.getElementById("respuesta-"+remota+"-"+valvula).innerHTML = mensaje;
	//document.getElementById("valvula-"+remota+"-"+valvula).style.opacity="0.2";
	//execGestionVal("accion=" + accion + "&idRemota=" + remota + "&idValvula=" + valvula);
	this.document.forms.gestionValvula.accion.value = accion;
	this.document.forms.gestionValvula.idRemota.value = remota;
	this.document.forms.gestionValvula.idValvula.value = valvula;
	this.document.forms.gestionValvula.submit();
	
}

function abrirVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionAbrir() %>", "Abriendo valvula");
}
function cerrarVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionCerrar() %>", "Cerrando valvula");
}
function estadoVal(remota, valvula){
	accionComun(remota, valvula, "<%=Constantes.getAccionEstado()%>", "Consultando valvula");
}

</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&language=es"></script> 

<script type="text/javascript">

var valvulas=[];
var claseValvulas=[];

<%
for (int v = 0; v < lstValvulas.length; v++) { 
%>

valvulas.push(<%="{idValvula:'"+lstValvulas[v].getIdvalvula()+
				 "',idRemota:'"+lstValvulas[v].getIdRemota()+
				 "',nombre:'"+lstValvulas[v].getValvula()+
				 "',estado:'"+lstValvulas[v].getEstado().toLowerCase()+
				 "',posicion:'"+lstValvulas[v].getPosicion()+"'}"%>);

<%}%>


</script>

<script type="text/javascript">
var map;

var markers = [];

var infos = [];

var activeIndex=0;

	function initialize() {

		var myLatlng = new google.maps.LatLng(valvulas[0].posicion.split(",")[0], 
				                              valvulas[0].posicion.split(",")[1]);

		var mapOptions = {
			zoom : 16,
			center : myLatlng,
			mapTypeId : google.maps.MapTypeId.SATELLITE
		};

		map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);

		addMarkerValvulas();
	}

	function openIndication(index) {

		infos[activeIndex].close(map, markers[activeIndex]);

		infos[index].open(map, markers[index]);
		activeIndex = index;
	}

	function addMarkerValvulas() {

		var summaryPanel = document.getElementById("valvulas_panel");
		summaryPanel.innerHTML = "";

		var menu = document.createElement("ul");

		var inp;

		for ( var i = 0; i < valvulas.length; i++) {
			inp = document.createElement("li");
			inp.setAttribute("onclick", "openIndication(" + i + ");");
			inp.setAttribute("class", "tabla"+valvulas[i].estado);
			inp.setAttribute("style", "width:95%");
			inp.innerHTML = getValvulaShortInfo(valvulas[i]);
			menu.appendChild(inp);
			addMarker(i, valvulas[i]);
		}
		summaryPanel.appendChild(menu);
	}

	function getValvulaShortInfo(_valvula) {
		var desc = "<a href=\'#\'>" + _valvula.nombre + "</a>";
		return desc;
	}

	function getValvulaInfo(_valvula) {
		var desc = "<b>" + _valvula.nombre
				+ "</b><br/><br/>";
		desc += "<i class='tabla" + _valvula.estado.toLowerCase() + "'>- Estado: " + _valvula.estado + "</i><br/>";
		
		desc +="<input type='button' class='btnAbrir' value='' alt='Abrir' onclick='abrirVal("+_valvula.idRemota+","+_valvula.idValvula+")' />";

		desc +="<input type='button' class='btnCerrar' value='' alt='Cerrar' onclick='cerrarVal("+_valvula.idRemota+","+_valvula.idValvula+")' />";

		desc +="<input type='button' class='btnEstado' value='' alt='Estado' onclick='estadoVal("+_valvula.idRemota+","+_valvula.idValvula+")' />";

		
		return desc;
	}

	function addMarker(index, _valvula) {

		var contentString = getValvulaInfo(_valvula);

		var info = new google.maps.InfoWindow({
			content : contentString
		});

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(_valvula.posicion.split(",")[0],
					                          _valvula.posicion.split(",")[1]),
			map : map,
			draggable : false,
			animation : google.maps.Animation.DROP
		});

		infos.push(info);

		google.maps.event.addListener(marker, "click", function() {
			infos[index].open(map, marker);
		});

		markers.push(marker);
	}
</script>


	<div style="width: 20%; float: right;">
		<div id="valvulas_panel" style="height: 100%;  height: 100%; overflow:auto; background-color: #667;"></div>
	</div>
	<div id="map_canvas" style="float: left; width: 80%; height: 100%;"></div>
		

<% if(!dialogo.equals(Constantes.DIALOGO_VACIO)) {
		if(dialogo.equals(Constantes.DIALOGO_INFORMAR) || dialogo.equals(Constantes.DIALOGO_ERROR)){
%>
			<div class="respuesta_modal"  id="procesando" onclick="this.style.display='none'" ><%=resultado %></div>
<%
		}
		else if(dialogo.equals(Constantes.DIALOGO_CONFIRMAR)){
%>
<div class="respuesta_modal"  id="procesando"  ><%=resultado %><br/>
	<input type="button" value="Confirmar" onclick="confirmarAbrirObjeto(<%=confirmacion %>)" /> 
	<input type="button" value="Rechazar" onclick="limpiarAviso()" /> 
	
</div>
<%
		}
	}
%>
	
	

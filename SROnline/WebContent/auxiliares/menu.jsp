<%@page import="es.sronline.core.Constantes"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<p class="info_usuario"><security:authentication property="name"/></p>
<div id='cssmenu'>
	<ul >
		<%if(Constantes.isTieneTelecontrol()){%>
		<li class='has-sub'><a href="SrvObjetosTelecontrol"  >Telecontrol</a>
			<ul>
				<%if(Constantes.isTieneMapa()){%><li><a href="SrvObjetosTelecontrol?mapa=1">Mapa de Dispositivos</a></li><%} %>
				<li><a href="SrvListadoProgramacionesTelecontrol">Programaciones</a></li>
				<li><a href="interfazWebvisu.jsp">Monitorización</a></li>
			</ul>
		</li>
		<%} %>
		<li><a href="SrvPlanificacion">Planificaciones</a></li>
		<security:authorize access="hasRole('ROLE_ADMIN')">
		<li class='has-sub'><a href="SrvGestValvulas">Válvulas</a>
			<ul>
				<%if(Constantes.isTieneMapa()){%><li><a href="SrvGestValvulasMapa">Mapa de Válvulas</a></li><%} %>
				<li><a href="SrvListadoProgramaciones">Programaciones</a></li>
			</ul>
		</li>
		</security:authorize>
		<%if(Constantes.isTieneSector()){%>
		<li class='has-sub'><a href="SrvSectores"  >Parcelas</a>
			<ul>
				<li><a href="SrvSectores?isMonitorizacion=true">Monitorización</a></li>
				<li><a href="SrvCopiarParcela">Plantillas</a></li>
				<li><a href="SrvSectores?accion=<%=Constantes.getAccionConsultar()%>">Consulta Historico</a></li> 
			</ul>
		</li>
		<%} %>
		<security:authorize access="hasRole('ROLE_ADMIN')">
		<li class='has-sub'><a haref="#">Administración</a>
			<ul>
				<li><a href="testComunicacion.jsp">Test Comunicación</a></li>
				<li><a href="SrvHistoricoProgramaciones">Historial Programaciones</a></li>
				<li><a href="SrvConfigEmplazamientos">Emplazamientos</a></li>
				<li><a href="SrvGestTipoObjetosTelecontrol">Tipos Objetos TC</a></li>
				<li><a href="SrvConfigObjTelecontrol">Objetos TC</a></li>
				<li><a href="SrvViewLog">Logs</a></li> 
			</ul>
		</li>
		</security:authorize>
		<li><a href="j_spring_security_logout" >Desconexión</a></li>
	</ul>
</div>

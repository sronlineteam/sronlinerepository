<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Planificaciones</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<style>
		
		.formularioModal {
			min-height:570px;
			max-height: 90%;
			margin-top: 2%;
		}
		.textoplano {
		    width: 85%;
		}
		[name=nombrePlanificacion]{	
    		width: 65%;
		}
	
	</style>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de acciones sobre planificaciones</h1>
			<%@ include file="auxiliares/lstPlanificaciones.jsp" %>
		</div>
	</body>
</html>

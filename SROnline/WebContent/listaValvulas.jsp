<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Valvulas</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<style>
		
    	.formularioModalSmall {
		    width: 60%;
		    margin-left: 20%;
		}
		
	</style>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Listado de acciones sobre v�lvulas</h1>
			<%@ include file="auxiliares/lstValvulas.jsp" %>
		</div>
	</body>
</html>
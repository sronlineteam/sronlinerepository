<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Listado de Objetos de Telecontrol</title>
		<%@ include file="auxiliares/header.html" %>
		<%@ include file="auxiliares/headerListas.html" %>
	</head>
	<style>
    	.formularioModalSmall {
		    width: 42%;
		    margin-left: 27%;
		    max-height: 93%;
			margin-top: 1%;
		}
			
	 	#table {
	 		text-align:center;
	   	 	max-width: none;
		}
		thead {
			text-align:center;
		}
		.dataTables_wrapper {
		    width: 60%;
			margin-left: 22%;
		    margin-bottom: 20px;
		}
		.form-group {
		    margin-bottom: 2px;
		}

	</style>
	<body>
		<div class="cabecera">
				<%@ include file="auxiliares/menu.jsp" %>
		</div>
		<div class="contenido" id="contenido">
			<h1>Configuración de elementos telecontrolables</h1>
			<%@ include file="auxiliares/confLstTipoObjetosTC.jsp" %>
		</div>
	</body>
</html>

function enviarValSelec(valvulas, accion){
	if(valvulas!=""){
// 		alert("abriendo : " + valvulas);
		this.document.forms.bloqueValv.valvulas.value = valvulas;
		this.document.forms.bloqueValv.accion.value = accion;
		this.document.forms.bloqueValv.submit();
	}
	else
		alert("Seleccione al menos una valvula");
}

function abrirValSelec(){
	if(this.document.forms.bloqueValv.hora.value == "")
		alert("Indique la hora!");
	else
		enviarValSelec(recogerValSelec(),"abrir");
}
function cerrarValSelec(){
	if(this.document.forms.bloqueValv.hora.value == "")
		alert("Indique la hora!");
	else
		enviarValSelec(recogerValSelec(),"cerrar");
}


function recogerValSelec(){
	var array = [];
	var cboxes = document.getElementsByName('selectVal');
    var len = cboxes.length;
    for (var i=0; i<len; i++) {
    	if(cboxes[i].checked)
	       array.push(cboxes[i].value);
    }
    return array;
}
/*
  clock_standalone.js — proinf.net — nov-2011, may-2013

  Muestra una ventana emergente para cambiar la hora y los minutos

   * No es compatible con MS-IE
   * Requisitos: clock.css
   * El formato de hora admitido es 'hh:mm'

  Para que una etiqueta <input> tenga reloj emergente
  tan sólo hay que hacer que sea de la clase 'clock'
  Por ej: <input type='text' id='hora' class='clock' />

  Aunque el elemento <input> no sea de la clase 'clock'
  también se le puede hacer mostrar un reloj emergente:
    ClockWidget.observe('id_input');
*/

var ClockWidget = (function()
{
  var boxClock  = { width:440, height:102, client:420, margin:0 };
  var boxSlider = { width:21,  height:29 };
  var classes   = { clock:"clock_widget", hour:"clock_hour", minute:"clock_minute" };

  // main ----------------------------------------

  //window.addEventListener('load', observe_all_inputs_of_class_clock, false);
  document.addEventListener("DOMContentLoaded", observe_all_inputs_of_class_clock, false);

  return {
    observe: observe,
  }

  function observe_all_inputs_of_class_clock() {
    var inputs = document.querySelectorAll('input.clock');
    for (var i=0; i<inputs.length; i++) {
      observe(inputs[i]);
    }
  }

  function observe(input) {
    input.addEventListener('click', togglePopup, false);
    input.addEventListener('blur', endPopup, false);
    input.addEventListener('keydown', keyPopup, false);
  }

  // input controller ----------------------------

  var input = null; // Elemento <input> que ha solicitado mostrar el reloj

  function keyPopup(event) {
    if (process()) event.preventDefault();

    function process() {
      var KEY_RETURN = 13;
      var KEY_ESC = 27;
      var KEY_LEFT = 37;
      var KEY_UP = 38;
      var KEY_RIGHT = 39;
      var KEY_DOWN = 40;

      switch(event.keyCode) {
        case KEY_RETURN:
        case KEY_ESC:
        case KEY_UP:
          if (isPopupVisible()) {
            endPopup(event);
            return true;
          }
          break;
        case KEY_DOWN:
          startPopup(event);
          return true;
        case KEY_RIGHT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(1);
            else if (event.ctrlKey) offsetMinutes(5);
            else offsetMinutes(1);
            return true;
          }
          break;
        case KEY_LEFT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(-1);
            else if (event.ctrlKey) offsetMinutes(-5);
            else offsetMinutes(-1);
            return true;
          }
          break;
      }
      return false;
    }
  }

  function togglePopup(event) {
    if (isPopupVisible()) endPopup(event); else startPopup(event);
  }

  function startPopup(event) {
    makePopup();
    input = event.target;
    inputToTime();
    timeToPosition();
    movePopupBelowInput();
    controlClock.style.display = '';
  }

  function endPopup(event) {
    if (isPopupVisible()) {
      controlClock.style.display = 'none';
      input = null;
    }
  }

  // popup ---------------------------------------

  var controlClock = null; // El <div> con el reloj
  var sliderHour = null; // El <div> con el indicador de la hora
  var sliderMinute = null;

  function makePopup() {
    if (controlClock == null) {
      controlClock = document.createElement('DIV');
      sliderHour   = document.createElement('DIV');
      sliderMinute = document.createElement('DIV');

      controlClock.className = classes.clock;
      sliderHour.className = classes.hour;
      sliderMinute.className = classes.minute;

      controlClock.addEventListener('mousemove', onMouseMove, false);
      controlClock.addEventListener('mousedown', onMouseDown, false);
      controlClock.addEventListener('mouseup', onMouseUp, false);

      controlClock.appendChild(sliderHour);
      controlClock.appendChild(sliderMinute);
      document.body.appendChild(controlClock);
    }
  }

  function isPopupVisible() {
    return controlClock && controlClock.style.display != 'none';
  }

  function movePopupBelowInput() {
    var offset = getOffset(input);
    var x = offset.left;
    var y = offset.top;
    y += input.offsetHeight;
    controlClock.style.left = x+'px';
    controlClock.style.top = y+'px';
  }

  // mouse events --------------------------------

  var mousePressed = false;
  var slider = null;

  function onMouseDown(event) {
    var mouse = getMousePosition(event);
    rememberSlider(mouse);
    mousePressed = true;
    //
    event.preventDefault();
  }

  function onMouseUp(event) {
    mousePressed = false;
    var mouse = getMousePosition(event);
    update(mouse);
    //
    event.preventDefault();
  }

  function onMouseMove(event) {
    if (mousePressed) {
      var mouse = getMousePosition(event);
      update(mouse);
    }
    //
    event.preventDefault();
  }

  function getMousePosition(event) {
    var offset = getOffset(controlClock);
    var mouse = {
      x: event.clientX - offset.left,
      y: event.clientY - offset.top,
    };
    ////document.getElementById('debug').innerHTML = mouse.x + ":" + mouse.y;
    return mouse;
  }

  function rememberSlider(mouse) {
    slider = (mouse.y < boxClock.height/2)? sliderHour: sliderMinute;
  }

  function update(mouse) {
    var x = Math.max(mouse.x - boxSlider.width/2, 0);
    slider.style.left = x+'px';
    positionToTime();
    timeToPosition(); // Si se deja hace saltitos
    timeToInput();
  }

  // time ----------------------------------------

  var time = new Date(); // hora actual seleccionada del último input

  function offsetHours(offset) {
    inputToTime();
    time.setHours(time.getHours()+offset);
    timeToPosition();
    timeToInput();
  }
  function offsetMinutes(offset) {
    inputToTime();
    time.setMinutes(time.getMinutes()+offset);
    timeToPosition();
    timeToInput();
  }

  function positionToTime() {
    var xh = parseInt(sliderHour.style.left);
    var xm = parseInt(sliderMinute.style.left);
    var hour = Math.round((xh - boxClock.margin) * 24/boxClock.client);
    var minute = Math.round((xm - boxClock.margin) * 60/boxClock.client);
    hour = Math.min(hour, 23);
    minute = Math.min(minute, 59);
    time.setHours(hour);
    time.setMinutes(minute);
  }

  function timeToPosition() {
    var hour = time.getHours();
    var minute = time.getMinutes();
    var xh = boxClock.margin + hour * boxClock.client/24;
    var xm = boxClock.margin + minute * boxClock.client/60;
    sliderHour.style.left = xh+'px';
    sliderMinute.style.left = xm+'px';
  }

  function timeToInput() {
    var two = function (n) { return (n<10?'0':'')+n; }
    var str = two(time.getHours()) + ':' + two(time.getMinutes());
    input.value = str;
    fireEvent(input, 'change');
  }

  function inputToTime() {
    time = parseDateTime(input.value) || new Date();
  }

  // tools ---------------------------------------

  /*
    Dynamically retrieve the position (X,Y) of an HTML element
  */
  function getOffset(element) {
    var x = 0;
    var y = 0;
    while (element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
      x += element.offsetLeft - element.scrollLeft;
      y += element.offsetTop - element.scrollTop;
      element = element.offsetParent;
    }
    return { top: y, left: x };
  }

  /*
    http://jehiah.cz/archive/firing-javascript-events-properly
  */
  function fireEvent(element, event) {
    if (document.createEventObject){ // Dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }
    else { // Dispatch for Firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type, bubbling, cancelable
        return !element.dispatchEvent(evt);
    }
  }

  /*
    Crear fecha a partir de un texto en formato
      "yyyy-mm-dd hh:nn:ss", "yyyy-mm-dd hh:nn", "yyyy-mm-dd" ó "hh:nn:ss" ó "hh:nn"
  */
  function parseDateTime(str) {
    var year=0, month=0, day=0;
    var hour=0, minute=0, second=0;
    var i=0;
    var ok = false;
    if (str.length == 19 || str.length == 16 || str.length == 10) {
      year   = parseInt(str.substring(i,i+4), 10); i+=5;
      month  = parseInt(str.substring(i,i+2), 10); i+=3;
      day    = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (str.length == 19 || str.length == 8) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      second = parseInt(str.substring(i,i+2), 10);
      ok = true;
    }
    if (str.length == 16 || str.length == 5) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (ok) return new Date (year, month-1, day, hour, minute, second);
    else return null;
  }

})(); // ClockWidget


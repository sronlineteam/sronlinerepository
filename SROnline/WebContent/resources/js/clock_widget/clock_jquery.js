/*
  clock_jquery.js — proinf.net — nov-2011, may-2013

  Muestra una ventana emergente para cambiar la hora y los minutos

   * Requisitos: jquery.js, clock.css,
   * El formato de hora admitido es 'hh:mm'

  Para que una etiqueta <input> tenga reloj emergente
  tan sólo hay que hacer que sea de la clase 'clock'
  Por ej: <input type='text' id='hora' class='clock' />

  Aunque el elemento <input> no sea de la clase 'clock'
  también se le puede hacer mostrar un reloj emergente:
    ClockWidget.observe('id_input');
*/

var ClockWidget = (function()
{
  var boxClock  = { width:440, height:102, client:420, margin:0 };
  var boxSlider = { width:21,  height:29 };
  var classes   = { clock:"clock_widget", hour:"clock_hour", minute:"clock_minute" };

  // main ----------------------------------------

  $(document).ready(function() {
    observe($('input.clock'));
  });

  return {
    observe: observe,
  }

  function observe(input) {
    $(input)
      .on('click', togglePopup)
      .on('blur', endPopup)
      .on('keydown', keyPopup);
  }

  // input controller ----------------------------

  var $input = null; // Elemento <input> que ha solicitado mostrar el reloj

  function keyPopup(event) {
    if (process()) event.preventDefault();

    function process() {
      var KEY_RETURN = 13;
      var KEY_ESC = 27;
      var KEY_LEFT = 37;
      var KEY_UP = 38;
      var KEY_RIGHT = 39;
      var KEY_DOWN = 40;

      switch(event.keyCode) {
        case KEY_RETURN:
        case KEY_ESC:
        case KEY_UP:
          if (isPopupVisible()) {
            endPopup(event);
            return true;
          }
          break;
        case KEY_DOWN:
          startPopup(event);
          return true;
        case KEY_RIGHT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(1);
            else if (event.ctrlKey) offsetMinutes(5);
            else offsetMinutes(1);
            return true;
          }
          break;
        case KEY_LEFT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(-1);
            else if (event.ctrlKey) offsetMinutes(-5);
            else offsetMinutes(-1);
            return true;
          }
          break;
      }
      return false;
    }
  }

  function togglePopup(event) {
    if (isPopupVisible()) endPopup(event); else startPopup(event);
  }

  function startPopup(event) {
    makePopup();
    $input = $(event.target);
    inputToTime();
    timeToPosition();
    movePopupBelowInput();
    $controlClock.show(150);
  }

  function endPopup(event) {
    if (isPopupVisible()) {
      $controlClock.hide();
      $input = null;
    }
  }

  // popup ---------------------------------------

  var $controlClock = null; // El <div> con el reloj
  var $sliderHour = null; // El <div> con el indicador de la hora
  var $sliderMinute = null;

  function makePopup() {
    if ($controlClock == null) {
      $controlClock = $('<div/>').addClass(classes.clock);
      $sliderHour   = $('<div/>').addClass(classes.hour);
      $sliderMinute = $('<div/>').addClass(classes.minute);

      $controlClock
        .mousemove(onMouseMove)
        .mousedown(onMouseDown)
        .mouseup(onMouseUp)
        .append($sliderHour)
        .append($sliderMinute)
        .hide();

      $('body').append($controlClock);
    }
  }

  function isPopupVisible() {
    return $controlClock && $controlClock.is(':visible');
  }

  function movePopupBelowInput() {
    var offset = $input.offset();
    $controlClock.css({
      left: offset.left,
      top:  offset.top + $input.outerHeight(false),
    });
  }

  // mouse events --------------------------------

  var mousePressed = false;
  var $slider = null;


  function onMouseDown(event) {
    event.preventDefault();
    var mouse = getMousePosition(event);
    rememberSlider(mouse);
    mousePressed = true;
  }

  function onMouseUp(event) {
    mousePressed = false;
    var mouse = getMousePosition(event);
    update(mouse);
  }

  function onMouseMove(event) {
    if (mousePressed) {
      var mouse = getMousePosition(event);
      update(mouse);
    }
  }

  function getMousePosition(event) {
    var offset = $controlClock.offset();
    return {
      x: event.pageX - offset.left,
      y: event.pageY - offset.top
    };
  }

  function rememberSlider(mouse) {
    $slider = (mouse.y < boxClock.height/2)? $sliderHour: $sliderMinute;
  }

  /*function update(mouse) {
    var x = Math.max(mouse.x - boxSlider.width/2, 0);
    $slider.css('left', x);
    positionToTime();
    timeToPosition(); // Si se deja hace saltitos
    timeToInput();
  }*/

  function update(mouse) {
    var x = Math.max(mouse.x - boxSlider.width/2, 0);
    if (mousePressed) {
      $slider.stop().css('left', x);
      positionToTime();
      timeToInput();
    }
    else {
      $slider.stop().animate({ left: x }, 200, function () {
        positionToTime();
        timeToPosition(); // Si se deja hace saltitos
        timeToInput();
      });
    }
  }

  // time ----------------------------------------

  var time = new Date(); // hora actual seleccionada del último input

  function offsetHours(offset) {
    inputToTime();
    time.setHours(time.getHours()+offset);
    timeToPosition();
    timeToInput();
  }
  function offsetMinutes(offset) {
    inputToTime();
    time.setMinutes(time.getMinutes()+offset);
    timeToPosition();
    timeToInput();
  }

  function positionToTime() {
    var xh = parseInt($sliderHour.css('left'));
    var xm = parseInt($sliderMinute.css('left'));
    var hour = Math.round((xh - boxClock.margin) * 24/boxClock.client);
    var minute = Math.round((xm - boxClock.margin) * 60/boxClock.client);
    hour = Math.min(hour, 23);
    minute = Math.min(minute, 59);
    time.setHours(hour);
    time.setMinutes(minute);
  }

  function timeToPosition() {
    var hour = time.getHours();
    var minute = time.getMinutes();
    var xh = boxClock.margin + hour * boxClock.client/24;
    var xm = boxClock.margin + minute * boxClock.client/60;
    $sliderHour.css('left', xh);
    $sliderMinute.css('left', xm);
  }

  function timeToInput() {
    var two = function (n) { return (n<10?'0':'')+n; }
    var str = two(time.getHours()) + ':' + two(time.getMinutes());
    $input.val(str).trigger('change');
  }

  function inputToTime() {
    time = parseDateTime($input.val()) || new Date();
  }

  // tools ---------------------------------------

  /*
    Crear fecha a partir de un texto en formato
      "yyyy-mm-dd hh:nn:ss", "yyyy-mm-dd hh:nn", "yyyy-mm-dd" ó "hh:nn:ss" ó "hh:nn"
  */
  function parseDateTime(str) {
    var year=0, month=0, day=0;
    var hour=0, minute=0, second=0;
    var i=0;
    var ok = false;
    if (str.length == 19 || str.length == 16 || str.length == 10) {
      year   = parseInt(str.substring(i,i+4), 10); i+=5;
      month  = parseInt(str.substring(i,i+2), 10); i+=3;
      day    = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (str.length == 19 || str.length == 8) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      second = parseInt(str.substring(i,i+2), 10);
      ok = true;
    }
    if (str.length == 16 || str.length == 5) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (ok) return new Date (year, month-1, day, hour, minute, second);
    else return null;
  }

})(); // ClockWidget


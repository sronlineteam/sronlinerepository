/*
  clock_prototype.js — proinf.net — nov-2011, may-2013

  Muestra una ventana emergente para cambiar la hora y los minutos

   * Requisitos: prototype.js, clock.css,
   * El formato de hora admitido es 'hh:mm'

  Para que una etiqueta <input> tenga reloj emergente
  tan sólo hay que hacer que sea de la clase 'clock'
  Por ej: <input type='text' id='hora' class='clock' />

  Aunque el elemento <input> no sea de la clase 'clock'
  también se le puede hacer mostrar un reloj emergente:
    ClockWidget.observe('id_input');
*/

var ClockWidget = (function()
{
  var boxClock  = { width:440, height:102, client:420, margin:0 };
  var boxSlider = { width:21,  height:29 };
  var classes   = { clock:"clock_widget", hour:"clock_hour", minute:"clock_minute" };

  // main ----------------------------------------

  Event.observe(window, 'load', observe_all_inputs_of_class_clock);

  return {
    observe: observe,
  }

  function observe_all_inputs_of_class_clock() {
    document.body.select('input.clock').each (function(input) {
      observe(input);
    });
  }

  function observe(input) {
    var input = $(input);
    input.observe('click', togglePopup);
    input.observe('blur', endPopup);
    input.observe('keydown', keyPopup);
  }

  // input controller ----------------------------

  var input = null; // Elemento <input> que ha solicitado mostrar el reloj

  function keyPopup(event) {
    if (process()) event.stop();

    function process() {
      switch(event.keyCode) {
        case Event.KEY_RETURN:
        case Event.KEY_ESC:
        case Event.KEY_UP:
          if (isPopupVisible()) {
            endPopup(event);
            return true;
          }
          break;
        case Event.KEY_DOWN:
          startPopup(event);
          return true;
        case Event.KEY_RIGHT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(1);
            else if (event.ctrlKey) offsetMinutes(5);
            else offsetMinutes(1);
            return true;
          }
          break;
        case Event.KEY_LEFT:
          if (isPopupVisible()) {
            if (event.shiftKey) offsetHours(-1);
            else if (event.ctrlKey) offsetMinutes(-5);
            else offsetMinutes(-1);
            return true;
          }
          break;
      }
      return false;
    }
  }

  function togglePopup(event) {
    if (isPopupVisible()) endPopup(event); else startPopup(event);
  }

  function startPopup(event) {
    makePopup();
    input = event.target;
    inputToTime();
    timeToPosition();
    movePopupBelowInput();
    controlClock.show();
  }

  function endPopup(event) {
    if (isPopupVisible()) {
      controlClock.hide();
      input = null;
    }
  }

  // popup ---------------------------------------

  var controlClock = null; // El <div> con el reloj
  var sliderHour = null; // El <div> con el indicador de la hora
  var sliderMinute = null;

  function makePopup() {
    if (controlClock == null) {
      controlClock = new Element('DIV', {'class':classes.clock} );
      sliderHour   = new Element('DIV', {'class':classes.hour} );
      sliderMinute = new Element('DIV', {'class':classes.minute} );

      controlClock.observe('mousemove', onMouseMove);
      controlClock.observe('mousedown', onMouseDown);
      controlClock.observe('mouseup', onMouseUp);

      controlClock.insert(sliderHour);
      controlClock.insert(sliderMinute);
      document.body.insert(controlClock);
    }
  }

  function isPopupVisible() {
    return controlClock && controlClock.visible();
  }

  function movePopupBelowInput() {
    var offset = input.cumulativeOffset();
    var x = offset.left;
    var y = offset.top;
    y += input.getHeight();
    controlClock.setStyle({ left:x+'px', top:y+'px' });
  }

  // mouse events --------------------------------

  var mousePressed = false;
  var slider = null;

  function onMouseDown(event) {
    event.stop();
    var mouse = getMousePosition(event);
    rememberSlider(mouse);
    mousePressed = true;
  }

  function onMouseUp(event) {
    mousePressed = false;
    var mouse = getMousePosition(event);
    update(mouse);
  }

  function onMouseMove(event) {
    if (mousePressed) {
      var mouse = getMousePosition(event);
      update(mouse);
    }
  }

  function getMousePosition(event) {
    var offset = controlClock.cumulativeOffset();
    return {
      x: event.pointerX() - offset.left,
      y: event.pointerY() - offset.top
    };
  }

  function rememberSlider(mouse) {
    slider = (mouse.y < boxClock.height/2)? sliderHour: sliderMinute;
  }

  function update(mouse) {
    var x = Math.max(mouse.x - boxSlider.width/2, 0);
    slider.setStyle({ left:x+'px' });
    positionToTime();
    timeToPosition(); // Si se deja hace saltitos
    timeToInput();
  }

  // time ----------------------------------------

  var time = new Date(); // hora actual seleccionada del último input

  function offsetHours(offset) {
    inputToTime();
    time.setHours(time.getHours()+offset);
    timeToPosition();
    timeToInput();
  }
  function offsetMinutes(offset) {
    inputToTime();
    time.setMinutes(time.getMinutes()+offset);
    timeToPosition();
    timeToInput();
  }

  function positionToTime() {
    var xh = parseInt(sliderHour.style.left);
    var xm = parseInt(sliderMinute.style.left);
    var hour = Math.round((xh - boxClock.margin) * 24/boxClock.client);
    var minute = Math.round((xm - boxClock.margin) * 60/boxClock.client);
    hour = Math.min(hour, 23);
    minute = Math.min(minute, 59);
    time.setHours(hour);
    time.setMinutes(minute);
  }

  function timeToPosition() {
    var hour = time.getHours();
    var minute = time.getMinutes();
    var xh = boxClock.margin + hour * boxClock.client/24;
    var xm = boxClock.margin + minute * boxClock.client/60;
    sliderHour.setStyle({ left:xh+'px' });
    sliderMinute.setStyle({ left:xm+'px' });
  }

  function timeToInput() {
    var two = function (n) { return (n<10?'0':'')+n; }
    var str = two(time.getHours()) + ':' + two(time.getMinutes());
    input.value = str;
    fireEvent(input, 'change');
  }

  function inputToTime() {
    time = parseDateTime(input.value) || new Date();
  }

  // tools ---------------------------------------

  /*
    http://jehiah.cz/archive/firing-javascript-events-properly
  */
  function fireEvent(element, event) {
    if (document.createEventObject){ // Dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }
    else { // Dispatch for Firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type, bubbling, cancelable
        return !element.dispatchEvent(evt);
    }
  }

  /*
    Crear fecha a partir de un texto en formato
      "yyyy-mm-dd hh:nn:ss", "yyyy-mm-dd hh:nn", "yyyy-mm-dd" ó "hh:nn:ss" ó "hh:nn"
  */
  function parseDateTime(str) {
    var year=0, month=0, day=0;
    var hour=0, minute=0, second=0;
    var i=0;
    var ok = false;
    if (str.length == 19 || str.length == 16 || str.length == 10) {
      year   = parseInt(str.substring(i,i+4), 10); i+=5;
      month  = parseInt(str.substring(i,i+2), 10); i+=3;
      day    = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (str.length == 19 || str.length == 8) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      second = parseInt(str.substring(i,i+2), 10);
      ok = true;
    }
    if (str.length == 16 || str.length == 5) {
      hour   = parseInt(str.substring(i,i+2), 10); i+=3;
      minute = parseInt(str.substring(i,i+2), 10); i+=3;
      ok = true;
    }
    if (ok) return new Date (year, month-1, day, hour, minute, second);
    else return null;
  }

})(); // ClockWidget


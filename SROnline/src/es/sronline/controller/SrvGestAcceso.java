package es.sronline.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Servlet implementation class SrvGestAcceso
 */
@WebServlet("/SrvGestAcceso")
public class SrvGestAcceso extends HttpServlet implements AuthenticationSuccessHandler{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestAcceso() {
        super();
        // TODO Auto-generated constructor stub
    }

	 @Override 
	 public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		String usuario = authentication.getName();
		Logger log = Logger.getLogger(getClass().getName());
		log.info("Acceso de " + usuario);
//		getServletContext().getRequestDispatcher("/SrvObjetosTelecontrol").forward(request, response);
		 response.sendRedirect("SrvObjetosTelecontrol");
	 } 

    
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 getServletContext().getRequestDispatcher("/SrvObjetosTelecontrol").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package es.sronline.bermejo;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bermejo.bbdd.PosturasConexionBBDD;
import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
/**
 * Servlet implementation class SrvCopiarParcela
 */
@WebServlet("/SrvCopiarParcela")
public class SrvCopiarParcela extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;    
    
 /**
  * @see HttpServlet#HttpServlet()
  */
 public SrvCopiarParcela() {
     super();
     log = Logger.getLogger("es.sronline");
     // TODO Auto-generated constructor stub
 }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
 	String accion;
 	String resultado = "";
 	int idProgramacion;
 	String destino = "/Bermejo/listaPlantillasParcelas.jsp";
 	
		log.debug("*********recibida petición en SrvCopiarParcela*********");
		
		Sectores[] lstSectores = null;
		try {
			SectoresDao _dao = SectoresDaoFactory.create();
			lstSectores = _dao.findAll();
		
		}
		catch (Exception _e) {
			_e.printStackTrace();
		}
		
		request.setAttribute("lstSectores", lstSectores);
		
		if(request.getParameter("accion")!=null && request.getParameter("idProgramacion")!=null)
			accion = request.getParameter("accion");
			
			//consulto la lista de plantillas.
			List<ProgramacionPosturasFormateada> lstProgramaciones = cargarListaPlantillasProgramaciones();
			//mando la lista a la jsp
			request.setAttribute("lstProgramacionesPosturas", lstProgramaciones);
			request.setAttribute("resultado", resultado);
		
			getServletContext().getRequestDispatcher(destino).forward(request, response);

	}
		private String copiarProgramacion(int idProgramacion, Date fechaHora) {
		// TODO Auto-generated method stub
		return null;
	}

		private String eliminarProgramacion(int idProgramacion) {
			
			ProgramacionPosturasPk progPk = new ProgramacionPosturasPk(idProgramacion);	
			ProgramacionPosturasDao _dao = getProgramacionPosturasDao();
			
			PosturasDao _posturas_dao = PosturasDaoFactory.create();
			AbonosDao _abonos_dao = AbonosDaoFactory.create();
			
			String resultado =  "No se ha podido eliminar, intentelo nuevamente.";

			try {
				Posturas[] posturas = _posturas_dao.findWhereIdprogramacion_posturaEquals(idProgramacion);
				
				for(int p = 0; p < posturas.length; p++){
					PosturasPk posturasPk = new PosturasPk(posturas[p].getIdposturas());
					Abonos[] abonos = _abonos_dao.findWhereIdPosturasEquals(posturas[p].getIdposturas());
					
					for (int a = 0; a < abonos.length; a++){
						AbonosPk abonoPk = new AbonosPk(abonos[a].getIdAbonos());	
						_abonos_dao.delete(abonoPk);
					}	
					_posturas_dao.delete(posturasPk);
				}
					_dao.delete(progPk);
					resultado =  "Programación eliminada.";
			} catch (ProgramacionPosturasDaoException e) {
				log.error(e.getMessage());
			
			} catch (PosturasDaoException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (AbonosDaoException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
			
			
			return resultado;
		}

		/**
		 * Method 'getProgramacionDao'
		 * 
		 * @return ProgramacionDao
		 */
		public static ProgramacionPosturasDao getProgramacionPosturasDao()
		{
			return ProgramacionPosturasDaoFactory.create();
		}


		private List<ProgramacionPosturasFormateada> cargarListaPlantillasProgramaciones() {
			PosturasConexionBBDD con = new PosturasConexionBBDD();
			List<ProgramacionPosturasFormateada> resultado = con.consultarPlantillasProgramacionesPosturas();
			return resultado;
		}

}

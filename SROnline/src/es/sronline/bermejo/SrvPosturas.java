package es.sronline.bermejo;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.AbonosDao;
import es.sronline.bbdd.dao.ClasesAbonosDao;
import es.sronline.bbdd.dao.ParcelasDao;
import es.sronline.bbdd.dao.PosturasDao;
import es.sronline.bbdd.dao.ProgramacionPosturasDao;
import es.sronline.bbdd.dto.Abonos;
import es.sronline.bbdd.dto.AbonosPk;
import es.sronline.bbdd.dto.ClasesAbonos;
import es.sronline.bbdd.dto.Parcelas;
import es.sronline.bbdd.dto.Posturas;
import es.sronline.bbdd.dto.PosturasPk;
import es.sronline.bbdd.dto.ProgramacionPosturas;
import es.sronline.bbdd.dto.ProgramacionPosturasPk;
import es.sronline.bbdd.exceptions.AbonosDaoException;
import es.sronline.bbdd.exceptions.ClasesAbonosDaoException;
import es.sronline.bbdd.exceptions.ParcelasDaoException;
import es.sronline.bbdd.exceptions.PosturasDaoException;
import es.sronline.bbdd.exceptions.ProgramacionPosturasDaoException;
import es.sronline.bbdd.factory.AbonosDaoFactory;
import es.sronline.bbdd.factory.ClasesAbonosDaoFactory;
import es.sronline.bbdd.factory.ParcelasDaoFactory;
import es.sronline.bbdd.factory.PosturasDaoFactory;
import es.sronline.bbdd.factory.ProgramacionPosturasDaoFactory;
import es.sronline.core.Common;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvPosturas
 */
@WebServlet("/SrvPosturas")
public class SrvPosturas extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final int numeroTratamientos = 7;
	Logger log;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvPosturas() {
        super();
        log = Logger.getLogger("es.sronline");
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
 
		String accion;
    	String resultado = "";
    	int idSector = 0;
    	String auxIdSector = "0";
    	int idProgramacion = -1;
    	int idProgramacionCopiada = -1;
    	String cuando= "";
    	String duracion= "";
    	int posturas= 0;
    	String auxPosturas = "0";
		String auxHayAcido = "";
		String auxHayAbono = "";
		String auxHaySoplante = "";
		String auxHayDisolvente = "";
		String auxHayLavadoAbono = "";
		String auxHayLavadoAcido = "";
		String auxHayLavadoDisolvente = "";
		String isCambio = "true";
		String isPlantilla = "false";

    	Posturas[] lstPosturas = null;
		ProgramacionPosturasPk pkProgramacionPosturas = null;
		PosturasDao _daoPosturas = getPosturasDao();
		ProgramacionPosturasDao _dao = getProgramacionPostruasDao();
		ProgramacionPosturas progmPosturasDto = new ProgramacionPosturas();
		AbonosDao _abonoDao = AbonosDaoFactory.create();
		Abonos[][] abonos = null;
		ClasesAbonosDao _clasesAbonosDao = ClasesAbonosDaoFactory.create();
		ClasesAbonos[] clasesAbonos = null;
		PosturasPk pkPostura;
		Parcelas[] lstParcelas = null;
		ParcelasDao _daoParcelas = ParcelasDaoFactory.create();
   	
		String redireccion = "/Bermejo/listaPosturas.jsp";
		
		log.debug("*********recibida petición en SrvPosturas*********");
		if(request.getParameter("idProgramacion")!= null){
			idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion") );
			idProgramacionCopiada = idProgramacion;
		}
		
		if(request.getParameter("accion")!=null){
			accion = request.getParameter("accion");
			//crear una nueva programación o cargar para cambiar una programación existente
			if (accion.equals(Constantes.getAccionNuevaProgramacion()) || accion.equals(Constantes.getAccionCambiar()) || accion.equals(Constantes.getAccionCopiar())){
				
				if(request.getParameter("sector")!=null){
					auxIdSector = (String) request.getParameter("sector");
					idSector = Integer.parseInt(auxIdSector);
				}
				if(request.getParameter("cuando")!=null)
					cuando = (String) request.getParameter("cuando");
				if(request.getParameter("duracion")!=null)
					duracion = (String) request.getParameter("duracion");
				if(request.getParameter("posturas")!=null)
					auxPosturas = (String) request.getParameter("posturas");
				posturas = Integer.parseInt(auxPosturas);
				//isPlantilla
				if(request.getParameter("isPlantilla")!=null)
					isPlantilla = (String) request.getParameter("isPlantilla");

				if(accion.equals(Constantes.getAccionNuevaProgramacion())){
					
					isCambio = "false";
										
					resultado = "No se ha podido crear la programación";
					Calendar cal = Calendar.getInstance();
					int espacioFechaHora = cuando.indexOf(" ");
					String dia = cuando.substring(0,espacioFechaHora);
					String hora = cuando.substring(espacioFechaHora + 1, cuando.length());
					cal = Common.getFechaHoraProgramacion(dia, hora);
					Date fechaHora = new Date();
					fechaHora.setTime(cal.getTimeInMillis());
					String[] diaHora = duracion.split(":");
					int intDuracion = Integer.parseInt(diaHora[0])*60 + Integer.parseInt(diaHora[1]);

					progmPosturasDto.setidsector(idSector);
					progmPosturasDto.setfecha(fechaHora);
					progmPosturasDto.setminutos(intDuracion);
					
					
					try {
						//primero creo la programacion
						
						pkProgramacionPosturas = _dao.insert(progmPosturasDto);
						idProgramacion = pkProgramacionPosturas.getidprogramacion_postura();
						resultado = "Se han creado las programación para el sector";
						clasesAbonos = _clasesAbonosDao.findAll();
						
						if (pkProgramacionPosturas != null){
							Posturas auxPostura;
							String auxResultado = "";
							abonos = new Abonos[posturas][numeroTratamientos];
							for (int p= 0; p < posturas; p++){
								auxPostura = new Posturas();
								auxPostura.setDuracion(intDuracion / posturas);
								auxPostura.setIdprogramacion_postura(idProgramacion);
								//segundo creo las posturas.
								pkPostura = _daoPosturas.insert(auxPostura);
								auxResultado += " y sus planificaciones";
								for(int c = 0; c < clasesAbonos.length; c++){
									//por ultimo los abonos
									Abonos auxAbonos = new Abonos();
									auxAbonos.setIdPosturas(pkPostura.getIdposturas());
									auxAbonos.setIdClasesAabonos(clasesAbonos[c].getIdclases_abonos());
									auxAbonos.setDuracion(clasesAbonos[c].getDuracion());
									auxAbonos.setInicio(clasesAbonos[c].getInicio());
									_abonoDao.insert(auxAbonos);
									abonos[p][c] = auxAbonos;
									
								}
								auxResultado += " y sus abonos";
							}
							resultado += auxResultado;
						}
					} catch (ProgramacionPosturasDaoException e) {
						log.error(e.getMessage());
						resultado = "No se han podido crear la programación";
					} catch (ClasesAbonosDaoException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (AbonosDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
						resultado += " pero no se han podido crear los tratamientos";
					} catch (PosturasDaoException e) {
						log.error(e.getMessage());
						resultado += " pero no se han podido crear las planificaciones";
					}
				}
			
				//copiar desde plantilla
				if(accion.equals(Constantes.getAccionCopiar())){
					isCambio = "false";
					
					resultado = "No se ha podido crear la programación";
					redireccion = "/SrvCopiarParcela";
					
					Calendar cal = Calendar.getInstance();
					int espacioFechaHora = cuando.indexOf(" ");
					String dia = cuando.substring(0,espacioFechaHora);
					String hora = cuando.substring(espacioFechaHora + 1, cuando.length());
					cal = Common.getFechaHoraProgramacion(dia, hora);
					Date fechaHora = new Date();
					fechaHora.setTime(cal.getTimeInMillis());
					
					try {
						//primero creo la programacion
						progmPosturasDto = _dao.findByPrimaryKey(idProgramacionCopiada);
						progmPosturasDto.setIdprogramacion_Postura(0);
						progmPosturasDto.setfecha(fechaHora);
						pkProgramacionPosturas = _dao.insert(progmPosturasDto);
						idProgramacion = pkProgramacionPosturas.getidprogramacion_postura();
						resultado = "Se han creado las programación para el sector";
	
						//segundo creo las posturas.
						lstPosturas = _daoPosturas.findWhereIdprogramacion_posturaEquals(idProgramacionCopiada);
						Posturas auxPostura = new Posturas();
						String auxResultado = "";
						abonos = new Abonos[lstPosturas.length][];
						for (int p= 0; p < lstPosturas.length; p++){
							auxPostura.setDuracion(lstPosturas[p].getDuracion());
							auxPostura.setIdParcelas(lstPosturas[p].getIdParcelas());
							auxPostura.setObservaciones(lstPosturas[p].getObservaciones());
							auxPostura.setIdprogramacion_postura(idProgramacion);
							pkPostura = _daoPosturas.insert(auxPostura);
							auxResultado += " y sus posturas";
							abonos[p] = _abonoDao.findWhereIdPosturasEquals(lstPosturas[p].getIdposturas());
						//por ultimo los abonos
							Abonos auxAbonos = new Abonos();
							for(int c = 0; c < abonos[p].length; c++){
								auxAbonos.setDuracion( abonos[p][c].getDuracion());
								auxAbonos.setIdClasesAabonos( abonos[p][c].getIdClasesAabonos());
								auxAbonos.setInicio( abonos[p][c].getInicio());
								auxAbonos.setIdPosturas(pkPostura.getIdposturas());
								_abonoDao.insert(auxAbonos);
								auxResultado += " y sus tratamientos";
							}
						}
	
					} catch (ProgramacionPosturasDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
						resultado = "No se han podido crear la programación";
					} catch (PosturasDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
						resultado += " pero no se han podido crear las posturas";
					} catch (AbonosDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
						resultado += " pero no se han podido crear los tratamientos";
					}
					
				}
			}
			//actualizar tiempos,abonos y accidos
			if(accion.equals(Constantes.getAccionActualizar())){
				redireccion = "/SrvSectores";
				isCambio = "true";
				if(request.getParameter("hayAcido")!=null)
					auxHayAcido = (String) request.getParameter("hayAcido");
				if(request.getParameter("hayAbono")!=null)
					auxHayAbono = (String) request.getParameter("hayAbono");
				if(request.getParameter("haySoplante")!=null)
					auxHaySoplante = (String) request.getParameter("haySoplante");
				if(request.getParameter("hayDisolvente")!=null)
					auxHayDisolvente = (String) request.getParameter("hayDisolvente");
				if(request.getParameter("hayLavadoAbono")!=null)
					auxHayLavadoAbono = (String) request.getParameter("hayLavadoAbono");
				if(request.getParameter("hayLavadoAcido")!=null)
					auxHayLavadoAcido = (String) request.getParameter("hayLavadoAcido");
				if(request.getParameter("hayLavadoDisolvente")!=null)
					auxHayLavadoDisolvente = (String) request.getParameter("hayLavadoDisolvente");
				if(request.getParameter("isPlantilla")!=null)
					if( ((String) request.getParameter("isPlantilla")).equals("true"))
						redireccion = "/SrvCopiarParcela";

				Abonos auxAbonos;
				AbonosPk auxPkAbono = new AbonosPk();
				PosturasDao _posturaDao = PosturasDaoFactory.create();
				Posturas auxPosturaCambio;
				PosturasPk auxPkPosturas = new PosturasPk();
				
				int numPosturas = Integer.valueOf((String) request.getParameter("numeroPosturas"));
				String auxPosturasCambios;
				String[] cambios;
				for (int p = 0; p < numPosturas; p++){
					int n = 0;
					int tipoAbono = 0;
					abonos = new Abonos[numPosturas][numeroTratamientos];

					auxPosturasCambios = (String) request.getParameter("resultado" + p);
					//   0            |     1     |     2        |         3      |         4      |    5           |      6       |       7        |  
					//duracionPostura | idPostura | idParcela    |  observaciones |   inicio abono | duración Abono | inicio Acido | duración acido |
					//        8        |       9           |       10        |        11                 12                 13                 14          |        15           |            16            |        17
					// inicio Soplante | duración Soplante | inicio LavadoAb | duración LavadoAb | inicio LavadoAc | duración LavadoAc | inicio Disolvente | duración Disolvente | inicio Lavado Disolvente | duración Lavado Disolvente |
					cambios = auxPosturasCambios.split(";");
					int cambioDuracion = Integer.valueOf(cambios[n++]);
					int auxIdPostura = Integer.valueOf(cambios[n++]);
					auxPkPosturas.setIdposturas(auxIdPostura);
					int auxIdParcela = Integer.valueOf(cambios[n++]);
					String auxcambiosComentarios = cambios[n++];
					
					try {
						auxPosturaCambio = _posturaDao.findByPrimaryKey(auxPkPosturas);
						auxPosturaCambio.setDuracion(cambioDuracion);
						auxPosturaCambio.setIdParcelas(auxIdParcela);
						auxPosturaCambio.setObservaciones(auxcambiosComentarios);
						_posturaDao.update(auxPkPosturas, auxPosturaCambio);
						//actualizo tipo abono
						tipoAbono = 1;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayAbono.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][0] = auxAbonos;

						//actualizo tipo acido
						tipoAbono = 2;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayAcido.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][1] = auxAbonos;

						//actualizo tipo soplante
						tipoAbono = 3;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHaySoplante.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][2] = auxAbonos;

						//actualizo tipo LavadoAbono
						tipoAbono = 4;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayLavadoAbono.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][3] = auxAbonos;

						//actualizo tipo LavadoAcido
						tipoAbono = 5;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayLavadoAcido.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][4] = auxAbonos;

						//actualizo tipo Disolvente
						tipoAbono = 6;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayDisolvente.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][5] = auxAbonos;

						//actualizo tipo LavadoDisolvente
						tipoAbono = 7;
						auxAbonos = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(auxIdPostura, tipoAbono)[0];
						auxAbonos.setInicio(Integer.valueOf(cambios[n++]));
						if(auxHayLavadoDisolvente.equals(""))
							auxAbonos.setDuracion(Integer.valueOf(cambios[n++]));
						else
							auxAbonos.setDuracion(0);
						auxPkAbono.setIdabonos(auxAbonos.getIdAbonos());
						auxPkAbono.setIdposturas(auxAbonos.getIdPosturas());
						_abonoDao.update(auxPkAbono, auxAbonos);
						abonos[p][6] = auxAbonos;

					} catch (PosturasDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
					} catch (AbonosDaoException e) {
						// TODO Auto-generated catch block
						log.error(e.getMessage());
					}
				
				}
			}
				
		}
			
		try {
			lstPosturas = _daoPosturas.findWhereIdprogramacion_posturaEquals(idProgramacion);
			progmPosturasDto = _dao.findByPrimaryKey(idProgramacion);
			abonos = new Abonos[lstPosturas.length][numeroTratamientos];
			Abonos[] auxAbonosEncontrados;
			for (int p = 0; p < lstPosturas.length; p++){
				abonos[p][0] =  _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 1)[0];
				abonos[p][1] =  _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 2)[0];
				auxAbonosEncontrados = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 3);
				if (auxAbonosEncontrados.length > 0)
					abonos[p][2] =  auxAbonosEncontrados[0];
				else
					abonos[p][2] = null;
				auxAbonosEncontrados = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 4);
				if (auxAbonosEncontrados.length > 0)
					abonos[p][3] =  auxAbonosEncontrados[0];
				else
					abonos[p][3] = null;
				auxAbonosEncontrados = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 5);
				if (auxAbonosEncontrados.length > 0)
					abonos[p][4] =  auxAbonosEncontrados[0];
				else
					abonos[p][4] = null;
				auxAbonosEncontrados = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 6);
				if (auxAbonosEncontrados.length > 0)
					abonos[p][5] =  auxAbonosEncontrados[0];
				else
					abonos[p][5] = null;
				auxAbonosEncontrados = _abonoDao.findWhereIdPosturasIdClaseAbonoEquals(lstPosturas[p].getIdposturas(), 7);
				if (auxAbonosEncontrados.length > 0)
					abonos[p][6] =  auxAbonosEncontrados[0];
				else
					abonos[p][6] = null;
			}
			lstParcelas = _daoParcelas.findWhereIdSectorEquals(progmPosturasDto.getidsector());
			
		} catch (PosturasDaoException e) {
			log.error(e.getMessage());
		} catch (ProgramacionPosturasDaoException e) {
			log.error(e.getMessage());
		} catch (AbonosDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} catch (ParcelasDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		request.setAttribute("idProgramacion", idProgramacion);
		request.setAttribute("programaSector", progmPosturasDto);
		request.setAttribute("lstPosturas", lstPosturas);
		request.setAttribute("lstParcelas", lstParcelas);
		request.setAttribute("abonos", abonos);
		request.setAttribute("resultado", resultado);
		request.setAttribute("isCambio", isCambio);
		request.setAttribute("isPlantilla", isPlantilla);
		getServletContext().getRequestDispatcher(redireccion ).forward(request, response);
		
	}


	private static PosturasDao getPosturasDao() {
		return PosturasDaoFactory.create();
	}

	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionPosturasDao getProgramacionPostruasDao()
	{
		return ProgramacionPosturasDaoFactory.create();
	}


}

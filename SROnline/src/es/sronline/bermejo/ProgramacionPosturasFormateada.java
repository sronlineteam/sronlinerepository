package es.sronline.bermejo;

import java.util.ArrayList;
import java.util.List;

import es.sronline.bbdd.dto.ProgramacionPosturas;

public class ProgramacionPosturasFormateada extends ProgramacionPosturas {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fechaFormateada;
	private String horaFormateada;
	private String horaFinFormateada;
	private String duracion;
	private int numeroPosturas;
	private List<String> posturas;
	private String nombreSector;
	private int duracionAbono;
	private String inicioAbono;
	private String finAbono;
	private int duracionLavadoAbono;
	private int duracionAcido;
	private String inicioAcido;
	private String finAcido;
	private int duracionLavadoAcido;
	private int duracionSoplante;
	private String inicioSoplante;
	private String finSoplante;
	private int duracionDisolvente;
	private String inicioDisolvente;
	private String finDisolvente;
	private int duracionLavadoDisolvente;
	private long fechaMili;
	private String observaciones;
	
	public String getFechaFormateada() {
		return fechaFormateada;
	}
	public void setFechaFormateada(String fecha) {
		this.fechaFormateada = fecha;
	}
	public String getHoraFormateada() {
		return horaFormateada;
	}
	public void setHoraFormateada(String hora) {
		this.horaFormateada = hora;
	}
	public String getHoraFinFormateada() {
		return horaFinFormateada;
	}
	public void setHoraFinFormateada(String horaFinFormateada) {
		this.horaFinFormateada = horaFinFormateada;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public int getNumeroPosturas() {
		return numeroPosturas;
	}
	public void setNumeroPosturas(int numeroPosturas) {
		this.numeroPosturas = numeroPosturas;
	}
	public String getNombreSector() {
		return nombreSector;
	}
	public List<String> getPosturas() {
		return posturas;
	}
	public void setPosturas(List<String> posturas) {
		this.posturas = posturas;
	}
	public void setNombreSector(String nombreSector) {
		this.nombreSector = nombreSector;
	}
	public int getDuracionAbono() {
		return duracionAbono;
	}
	public void setDuracionAbono(int duracionAbono) {
		this.duracionAbono = duracionAbono;
	}
	public String getInicioAbono() {
		return inicioAbono;
	}
	public void setInicioAbono(String inicioAbono) {
		this.inicioAbono = inicioAbono;
	}
	public String getFinAbono() {
		return finAbono;
	}
	public void setFinAbono(String finAbono) {
		this.finAbono = finAbono;
	}
	public int getDuracionAcido() {
		return duracionAcido;
	}
	public void setDuracionAcido(int duracionAcido) {
		this.duracionAcido = duracionAcido;
	}
	public String getInicioAcido() {
		return inicioAcido;
	}
	public void setInicioAcido(String inicioAcido) {
		this.inicioAcido = inicioAcido;
	}
	public String getFinAcido() {
		return finAcido;
	}
	public void setFinAcido(String finAcido) {
		this.finAcido = finAcido;
	}
	public long getFechaMili() {
		return fechaMili;
	}
	public void setFechaMili(long fechaMili) {
		this.fechaMili = fechaMili;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public int getDuracionLavadoAbono() {
		return duracionLavadoAbono;
	}
	public void setDuracionLavadoAbono(int duracionLavadoAbono) {
		this.duracionLavadoAbono = duracionLavadoAbono;
	}
	public int getDuracionLavadoAcido() {
		return duracionLavadoAcido;
	}
	public void setDuracionLavadoAcido(int duracionLavadoAcido) {
		this.duracionLavadoAcido = duracionLavadoAcido;
	}
	public int getDuracionSoplante() {
		return duracionSoplante;
	}
	public void setDuracionSoplante(int duracionSoplante) {
		this.duracionSoplante = duracionSoplante;
	}
	public String getInicioSoplante() {
		return inicioSoplante;
	}
	public void setInicioSoplante(String inicioSoplante) {
		this.inicioSoplante = inicioSoplante;
	}
	public String getFinSoplante() {
		return finSoplante;
	}
	public void setFinSoplante(String finSoplante) {
		this.finSoplante = finSoplante;
	}
	public int getDuracionDisolvente() {
		return duracionDisolvente;
	}
	public void setDuracionDisolvente(int duracionDisolvente) {
		this.duracionDisolvente = duracionDisolvente;
	}
	public String getInicioDisolvente() {
		return inicioDisolvente;
	}
	public void setInicioDisolvente(String inicioDisolvente) {
		this.inicioDisolvente = inicioDisolvente;
	}
	public String getFinDisolvente() {
		return finDisolvente;
	}
	public void setFinDisolvente(String finDisolvente) {
		this.finDisolvente = finDisolvente;
	}
	public int getDuracionLavadoDisolvente() {
		return duracionLavadoDisolvente;
	}
	public void setDuracionLavadoDisolvente(int duracionLavadoDisolvente) {
		this.duracionLavadoDisolvente = duracionLavadoDisolvente;
	}



}

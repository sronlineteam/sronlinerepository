package es.sronline.bermejo.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dto.LstTarifasElectricas;
import es.sronline.core.Constantes;
import es.sronline.core.Programacion;
import es.sronline.core.ProgramacionFormateada;
import es.sronline.bermejo.ProgramacionPosturasFormateada;

public class PosturasConexionBBDD {
	Connection conexion = null;
	Logger log;
	static int ARRANCAR_SECTOR = 1;
	private static int PARAR_SECTOR = 2;
	private static int ARRANCAR_POSTURA = 3;
	private static int PARAR_POSTURA = 4;
	private static int ARRANCAR_ABONO = 5;
	private static int PARAR_ABONO = 6;
	private static int ARRANCAR_ACIDO = 7;
	private static int PARAR_ACIDO = 8;
	private static int ARRANCAR_SOPLANTE = 9;
	private static int PARAR_SOPLANTE = 10;
	
	
	
	
	private void conectar(){
		try {
			conexion = DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	
	}
	
	public ResultSet consultarSQL (String sql){
		if (conexion == null)
			conectar();
		
		Statement s;
		ResultSet rs= null;
		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql);
			//conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		
		return rs;
		
		
	}
	
	public int guardarSQL (String sql){
		if (conexion == null)
			conectar();
		
		Statement s;
		int rs= 0;
		try {
			s = conexion.createStatement();
			rs = s.executeUpdate(sql);
			log.info("Registros afectados: " + rs + " registros");
		
			conexion.close();
			
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		
		
		return rs;
		
		
	}

	public void cerrar(){
		try {
			conexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}
	public PosturasConexionBBDD() {
		log = Logger.getLogger("es.sronline");
        // Se mete todo en un try por los posibles errores de MySQL
        try
        {
            // Se registra el Driver de MySQL
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
            
            // Se obtiene una conexi�n con la base de datos. Hay que
            // cambiar el usuario "root" y la clave "la_clave" por las
            // adecuadas a la base de datos que estemos usando.
            conexion = DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
            
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }
 

	}
	
	public List<Integer> consultarSectoresArranca() {
		return consultarSectoresPosturasTratamientos(ARRANCAR_SECTOR);
	}
	
	public List<Integer> consultarSectoresPara() {
		return consultarSectoresPosturasTratamientos(PARAR_SECTOR);
	}

	public List<Integer> consultarPosturasArranca() {
		return consultarSectoresPosturasTratamientos(ARRANCAR_POSTURA);
	}
	
	public List<Integer> consultarPosturasPara() {
		return consultarSectoresPosturasTratamientos(PARAR_POSTURA);
	}

	public List<Integer> consultarAbonosArranca() {
		return consultarSectoresPosturasTratamientos(ARRANCAR_ABONO);
	}
	
	public List<Integer> consultarAbonosPara() {
		return consultarSectoresPosturasTratamientos(PARAR_ABONO);
	}

	public List<Integer> consultarAcidosArranca() {
		return consultarSectoresPosturasTratamientos(ARRANCAR_ACIDO);
	}
	
	public List<Integer> consultarAcidosPara() {
		return consultarSectoresPosturasTratamientos(PARAR_ACIDO);
	}

	public List<Integer> consultarSoplanteArranca() {
		return consultarSectoresPosturasTratamientos(ARRANCAR_SOPLANTE);
	}
	
	public List<Integer> consultarSoplantePara() {
		return consultarSectoresPosturasTratamientos(PARAR_SOPLANTE);
	}

	private List<Integer> consultarSectoresPosturasTratamientos(int tipoConsulta) {
		//ConexionBBDD con = new ConexionBBDD();
		try {
			if(conexion == null || conexion.isClosed())
				conectar();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Statement s;
		ResultSet rs= null;
		
		List<Integer> sectores = new ArrayList<Integer>() {
		};

		String sql = null;
		switch (tipoConsulta) {
			//ARRANCAR_SECTOR
			case 1:
				sql = "SELECT idsector FROM sectores_arranca;";
				break;
			//PARAR_SECTOR
			case 2:
				sql = "SELECT idsector FROM sectores_para;";
				break;
			//ARRANCAR_POSTURA
			case 3:
				sql = "SELECT idposturas FROM posturas_arranca;";
				break;
			//PARAR_POSTURA
			case 4:
				sql = "SELECT idposturas FROM posturas_para;";
			break;
			//ARRANCAR_ABONO
			case 5:
				sql = "SELECT idsector FROM v_tratamiento_arranca WHERE idclases_abonos = 1;";
				break;
			//PARAR_ABONO
			case 6:
				sql = "SELECT idsector FROM v_tratamiento_para WHERE idclases_abonos = 1;";
				break;
			//ARRANCAR_ACIDO
			case 7:
				sql = "SELECT idsector FROM v_tratamiento_arranca WHERE idclases_abonos = 2;";
				break;
			//PARAR_ACIDO
			case 8:
				sql = "SELECT idsector FROM v_tratamiento_para WHERE idclases_abonos = 2;";
				break;
			//ARRANCAR_SOPLANTE
			case 9:
				sql = "SELECT idsector FROM v_tratamiento_arranca WHERE idclases_abonos = 3;";
				break;
			//PARAR_SOPLANTE
			case 10:
				sql = "SELECT idsector FROM v_tratamiento_para WHERE idclases_abonos = 3;";
				break;
		default:
			break;
		}

		if(sql!=null)
			try {
				s = conexion.createStatement();
				rs = s.executeQuery (sql);
				while (rs.next())
				{
					sectores.add(new Integer(rs.getInt(1)));
				}
				conexion.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
		
		return  sectores;
	}

	private List<Integer> consultarTareasParcela(String sql) {
		//ConexionBBDD con = new ConexionBBDD();
		try {
			if(conexion == null || conexion.isClosed())
				conectar();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Statement s;
		ResultSet rs= null;
		
		List<Integer> sectores = new ArrayList<Integer>();

		if(sql!=null)
			try {
				s = conexion.createStatement();
				rs = s.executeQuery (sql);
				while (rs.next())
				{
					sectores.add(new Integer(rs.getInt(1)));
				}
				conexion.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
		
		return  sectores;
	}

	public List<ProgramacionPosturasFormateada> consultarProgramacionesPosturasFuturas() {
		return consultarProgramacionesParcelas("date_add(pp.fecha, interval pp.minutos MINUTE) > now()  ");
	}

	public List<ProgramacionPosturasFormateada> consultarPlantillasProgramacionesPosturas() {
		return consultarProgramacionesParcelas("date(pp.fecha)='2000-01-01' ");
			}

	public List<ProgramacionPosturasFormateada> consultarProgramacionesParcelas(String whereCuando, boolean completa) {
		List<ProgramacionPosturasFormateada> programacion = new ArrayList<ProgramacionPosturasFormateada>();
		//ResultSet rs = con.consultarSQL("SELECT regadio.programacion.trama FROM regadio.programacion;");
		StringBuffer sql = new StringBuffer("SELECT ");
		sql.append("pp.idprogramacion_postura, pp.idsector, pp.fecha, pp.minutos, s.nombre, count(p.idposturas)");
		sql.append(", date_format(date_add(pp.fecha, interval pp.minutos MINUTE), '%T'),ab.duracion as abono, ac.duracion as acido");
		sql.append(", (pp.fecha + INTERVAL ab.inicio MINUTE) AS inicioAbono, (pp.fecha + INTERVAL (ab.inicio + ab.duracion) MINUTE) AS finAbono");
		sql.append(", (pp.fecha + INTERVAL ac.inicio MINUTE) AS inicioAcido, (pp.fecha + INTERVAL (ac.inicio + ac.duracion) MINUTE) AS finAcido");
		if(completa)
			sql.append(", p.observaciones AS observaciones ");
		sql.append(" FROM ");
		sql.append(Constantes.getNombreBBDD());
		sql.append(".programacion_posturas pp, ");
		sql.append(Constantes.getNombreBBDD()); 
		sql.append(".posturas p, ");
		sql.append(Constantes.getNombreBBDD());
		sql.append(".sectores s, ");
		sql.append(Constantes.getNombreBBDD());
		sql.append(".abonos ab, ");
		sql.append(Constantes.getNombreBBDD());
		sql.append(".abonos ac ");
		sql.append("where pp.idprogramacion_postura = p.idprogramacion_postura ");
		sql.append("and pp.idsector = s.idsector ");
		sql.append("and ab.idposturas = p.idposturas ");
		sql.append("and ab.idclases_abonos = 1 ");
		sql.append("and ac.idposturas = p.idposturas ");
		sql.append("and ac.idclases_abonos = 2 ");
		sql.append("and ");
		sql.append(whereCuando);
		sql.append(" group by pp.idprogramacion_postura, pp.idsector, pp.fecha, pp.minutos "); 

		Statement s;
		ResultSet rs= null;
		ProgramacionPosturasFormateada auxProg;
		int auxDuracion = 0;
		int auxHoras = 0;
		int auxMinutos = 0;

		Date auxDate;
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdfhora=new SimpleDateFormat("HH:mm");

		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql.toString());
			while (rs.next())
			{
				auxProg = new ProgramacionPosturasFormateada();
				auxProg.setIdprogramacion_Postura(rs.getInt(1));
				auxProg.setidsector(rs.getInt(2));
				auxDate = rs.getDate(3);
				auxProg.setfecha(auxDate);
				auxProg.setFechaFormateada("" + sdf.format(auxDate));
				auxProg.setHoraFormateada("" + sdfhora.format(rs.getTime(3)));
				auxProg.setHoraFinFormateada(rs.getString(7));
				auxDuracion = rs.getInt(4);
				auxHoras = auxDuracion/60;
				auxMinutos = auxDuracion - auxHoras * 60;
				auxProg.setDuracion(auxHoras + ":" + ((auxMinutos < 10)?"0"+auxMinutos : auxMinutos));
				auxProg.setNombreSector(rs.getString(5));
				auxProg.setNumeroPosturas(rs.getInt(6));
				auxProg.setPosturas(consultarPosturasProgamacion(rs.getInt(1)));
				auxProg.setDuracionAbono(rs.getInt(8));
				auxProg.setDuracionAcido(rs.getInt(9));
				auxProg.setInicioAbono("" + sdfhora.format(rs.getTime(10)));
				auxProg.setFinAbono("" + sdfhora.format(rs.getTime(11)));
				auxProg.setInicioAcido("" + sdfhora.format(rs.getTime(12)));
				auxProg.setFinAcido("" + sdfhora.format(rs.getTime(13)));
				auxProg.setFechaMili(rs.getTimestamp(3).getTime());
				if(completa)
					auxProg.setObservaciones(rs.getString(14));
			    programacion.add(auxProg);
			}
		
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  programacion;
	}

	
	private List<String> consultarPosturasProgamacion(int idProgramacionPostura) {
		//ConexionBBDD con = new ConexionBBDD();
		List<String> posturas = new ArrayList<String>();
		try {
			if(conexion == null || conexion.isClosed())
				conectar();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		StringBuffer sbSqlPosturas = new StringBuffer("SELECT prc.descripcion ");
			sbSqlPosturas.append("FROM posturas pst, parcelas prc  ");
			sbSqlPosturas.append("where prc.idparcelas = pst.idparcelas ");
			sbSqlPosturas.append("and pst.idprogramacion_postura = ");
			sbSqlPosturas.append( idProgramacionPostura );
			sbSqlPosturas.append(";");
		
		String sql = sbSqlPosturas.toString();
		Statement s;
		ResultSet rs= null;
		
		if(sql!=null)
			try {
				s = conexion.createStatement();
				rs = s.executeQuery (sql);
				while (rs.next())
				{
					posturas.add(rs.getString(1));
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
		
		return  posturas;
	}

	public List<Integer> consultarValvulasParcelaAbre() {
		// TODO Auto-generated method stub
		return consultarTareasParcela("SELECT idparcela FROM v_valvula_abre;");
	}

	public List<Integer> consultarValvulasParcelaCierra() {
		// TODO Auto-generated method stub
		return consultarTareasParcela("SELECT idparcela FROM v_valvula_cierra;");
	}

	public List<Integer> consultarEjecucionesEnCurso() {
		// TODO Auto-generated method stub
		return consultarTareasParcela("SELECT idparcela FROM v_ejecuciones_curso;");
	}

	public List<ProgramacionPosturasFormateada> consultarProgramacionesParcelas(String whereCuando) {
		return consultarProgramacionesParcelas(whereCuando, false);
	}

	public List<Integer> consultarPosturaSectorArranca() {
		// TODO Auto-generated method stub
		return consultarTareasParcela("SELECT idparcelas FROM posturaparcelaarranca;");
	}
}

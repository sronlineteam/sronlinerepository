package es.sronline.bermejo;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bermejo.bbdd.PosturasConexionBBDD;
import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Common;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvSectores
 */
@WebServlet("/SrvSectores")
public class SrvSectores extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvSectores() {
        super();
        log = Logger.getLogger("es.sronline");
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String accion;
    	String resultado = "";
    	int idProgramacion;
    	String isPlantilla = "false";
		if(request.getParameter("isPlantilla")!=null)
			isPlantilla = (String) request.getParameter("isPlantilla");
    	
		String isMonitorizacion = "false";
		if(request.getParameter("isMonitorizacion")!=null)
			isMonitorizacion = (String) request.getParameter("isMonitorizacion");

		request.setAttribute("isMonitorizacion", isMonitorizacion);

    	String destino = "/Bermejo/listaSectores.jsp";
    	
		log.debug("*********recibida petici�n en SrvSectores*********");
		
		Sectores[] lstSectores = null;
		try {
			SectoresDao _dao = SectoresDaoFactory.create();
			lstSectores = _dao.findAll();
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		
		request.setAttribute("lstSectores", lstSectores);
		
		if(request.getParameter("accion")!=null ){
			accion = request.getParameter("accion");
			
			if(request.getParameter("idProgramacion")!=null){
				if(accion.equals(Constantes.getAccionCambiar()) || accion.equals(Constantes.getAccionCopiar())){
					if(request.getParameter("cuando")!=null){
						String cuando;
						idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
						cuando = (String) request.getParameter("cuando");
						int espacioFechaHora = cuando.indexOf(" ");
						String dia = cuando.substring(0,espacioFechaHora);
						String hora = cuando.substring(espacioFechaHora + 1, cuando.length());
						Calendar cal = Calendar.getInstance();
						cal = Common.getFechaHoraProgramacion(dia, hora);
						Date fechaHora = new Date();
						fechaHora.setTime(cal.getTimeInMillis());
						if(accion.equals(Constantes.getAccionCambiar()))
							resultado = cambiarFechaHoraProgramacion(idProgramacion, fechaHora);
						else if(accion.equals(Constantes.getAccionCopiar())){
							//resultado = copiarProgramacion(idProgramacion, fechaHora);
							request.setAttribute("accion", Constantes.getAccionCopiar());
							request.setAttribute("idProgramacion", idProgramacion);
							destino = "/SrvPosturas";
						}
						
						if(request.getParameter("cambiarPosturas")!= null)
							if(request.getParameter("cambiarPosturas").equals("true")){
								request.setAttribute("accion", Constantes.getAccionCambiar());
								request.setAttribute("idProgramacion", idProgramacion);
								destino = "/SrvPosturas";
		
							}
							
					}
				}
				else if(accion.equals(Constantes.getAccionEliminar())){
					idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
					if(isPlantilla.equals("true"))
						destino = "/SrvCopiarParcela";
					resultado = eliminarProgramacion(idProgramacion);
				}
			}
			if(accion.equals(Constantes.getAccionConsultar())){
				destino = "/Bermejo/listaProgramacionesHistorico.jsp";
				List<ProgramacionPosturasFormateada> lstProgramaciones;
				if(request.getParameter("ConsultaDesde")!=null && request.getParameter("ConsultaHasta")!=null){
					String desde = (String) request.getParameter("ConsultaDesde");
					String hasta = (String) request.getParameter("ConsultaHasta");
					//consulto la lista de programaciones posteriores a ahora.
					lstProgramaciones = cargarListaProgramacionesHistoricas(desde, hasta);
				}
				else{
					//consulto la lista de programaciones posteriores a ahora.
					lstProgramaciones = cargarListaProgramacionesFuturas();
				}
				//mando la lista a la jsp
				request.setAttribute("lstProgramacionesPosturas", lstProgramaciones);
				request.setAttribute("resultado", lstProgramaciones.size() + " programaciones encontradas.");
					
				
			}

		}
		
		if(destino.equals("/Bermejo/listaSectores.jsp")){
			//consulto la lista de programaciones posteriores a ahora.
			List<ProgramacionPosturasFormateada> lstProgramaciones = cargarListaProgramacionesFuturas();
			//mando la lista a la jsp
			request.setAttribute("lstProgramacionesPosturas", lstProgramaciones);
			request.setAttribute("resultado", resultado);
		}
		getServletContext().getRequestDispatcher(destino).forward(request, response);

	}

		private String copiarProgramacion(int idProgramacion, Date fechaHora) {
		// TODO Auto-generated method stub
		return null;
	}

		private String eliminarProgramacion(int idProgramacion) {
			
			ProgramacionPosturasPk progPk = new ProgramacionPosturasPk(idProgramacion);	
			ProgramacionPosturasDao _dao = getProgramacionPosturasDao();
			
			PosturasDao _posturas_dao = PosturasDaoFactory.create();
 			AbonosDao _abonos_dao = AbonosDaoFactory.create();
 			
			String resultado =  "No se ha podido eliminar, intentelo nuevamente.";
			//TODO AQU� HAY QUE BORRAR TAMBIEN LA PROGRAMACI�N DE QUARTZ ADEMAS DE GUARDARLA

			try {
				Posturas[] posturas = _posturas_dao.findWhereIdprogramacion_posturaEquals(idProgramacion);
				
				for(int p = 0; p < posturas.length; p++){
					PosturasPk posturasPk = new PosturasPk(posturas[p].getIdposturas());
					Abonos[] abonos = _abonos_dao.findWhereIdPosturasEquals(posturas[p].getIdposturas());
					
					for (int a = 0; a < abonos.length; a++){
						AbonosPk abonoPk = new AbonosPk(abonos[a].getIdAbonos());	
						_abonos_dao.delete(abonoPk);
					}	
					_posturas_dao.delete(posturasPk);
				}
					_dao.delete(progPk);
					resultado =  "Programaci�n eliminada.";
			} catch (ProgramacionPosturasDaoException e) {
				log.error(e.getMessage());
			
			} catch (PosturasDaoException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (AbonosDaoException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
			
			
			return resultado;
		}

		/**
		 * Method 'getProgramacionDao'
		 * 
		 * @return ProgramacionDao
		 */
		public static ProgramacionPosturasDao getProgramacionPosturasDao()
		{
			return ProgramacionPosturasDaoFactory.create();
		}

		private String cambiarFechaHoraProgramacion(int idProgramacion, Date fechaHora) {
			ProgramacionPosturasPk progPk = new ProgramacionPosturasPk(idProgramacion);	
			ProgramacionPosturasDao _dao = getProgramacionPosturasDao();
			String resultado =  "No se ha podido cambiar, intentelo nuevamente.";
			ProgramacionPosturas dto;
			try {
				//TODO AQU� HAY QUE CAMBIAR LA PROGRAMACI�N DE QUARTZ ADEMAS DE GUARDARLA
				dto = _dao.findByPrimaryKey(progPk);
				dto.setfecha(fechaHora);
				_dao.update(progPk, dto);
				resultado =  "Programaci�n cambiada.";
			} catch (ProgramacionPosturasDaoException e) {
				log.error(e.getMessage());
			}
			
			return resultado;
			
		}

		private List<ProgramacionPosturasFormateada> cargarListaProgramacionesFuturas() {
			PosturasConexionBBDD con = new PosturasConexionBBDD();
			List<ProgramacionPosturasFormateada> resultado = con.consultarProgramacionesPosturasFuturas();
			return resultado;
		}
		private List<ProgramacionPosturasFormateada> cargarListaProgramacionesHistoricas(String desde, String hasta) {
			PosturasConexionBBDD con = new PosturasConexionBBDD();
			String[] desdeArray = desde.split("-");
			String[] hastaArray = hasta.split("-");
			StringBuffer cuando = new StringBuffer("date(lst_programacion_posturas.fecha) between '");
			cuando.append(desdeArray[2]);
			cuando.append("-");
			cuando.append(desdeArray[1]);
			cuando.append("-");
			cuando.append(desdeArray[0]);
			cuando.append("' and '");
			cuando.append(hastaArray[2]);
			cuando.append("-");
			cuando.append(hastaArray[1]);
			cuando.append("-");
			cuando.append(hastaArray[0]);
			cuando.append("' ");
			List<ProgramacionPosturasFormateada> resultado = con.consultarProgramacionesParcelas( cuando.toString(), true);
			return resultado;
		}


}

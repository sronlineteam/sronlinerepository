package es.sronline.parcelas;

import java.io.InputStream;
import java.util.Properties;
import java.util.stream.Stream;

import org.apache.log4j.Logger;


/**
 * @author julian.rodriguez
 *
 */
public class Constantes {

	/**
	* Propiedad que almacena los valores preconfigurados
	*/
	private static Properties properties;

	public static int[] getValvulasSector(){
		String[] auxValvulasSector = properties.getProperty("valvulas_sector").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	
	//idCaseta
	public static int[] getIdCaseta(){
		String[] auxValvulasSector = properties.getProperty("idCaseta").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//valvulas
	public static int[] getValvulas(){
		String[] auxValvulasSector = properties.getProperty("valvulas").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//remotas
	public static int[] getRemotas(){
		String[] auxValvulasSector = properties.getProperty("remotas").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//valvulas2
	public static int[] getValvulasSecundarias(){
		String[] auxValvulasSector = properties.getProperty("valvulas2").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//remotas2
	public static int[] getRemotasSecundarias(){
		String[] auxValvulasSector = properties.getProperty("remotas2").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//idObjetoVAbono
	public static int[] getIdObjetoVAbono(){
		String[] auxValvulasSector = properties.getProperty("idObjetoVAbono").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	//idObjetoVAcido
	public static int[] getIdObjetoVAcido(){
		String[] auxValvulasSector = properties.getProperty("idObjetoVAcido").split(",");
		return Stream.of(auxValvulasSector).mapToInt(Integer::parseInt).toArray();
	}
	
	static {
	init();
	}

	public static void init() {

	try {

	ClassLoader cl = Thread.currentThread().getContextClassLoader();
	if (cl == null)
	cl = ClassLoader.getSystemClassLoader();

	InputStream is = cl.getResourceAsStream("es/sronline/parcelas/parcelas.properties");
	properties = new Properties();
	properties.load(is);
	is.close();
	
	} catch (Exception e) {

		Logger.getLogger("es.sronline.parcelas.Constantes").error("Error al cargar las propiedades " + e.getMessage());;

	}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}


}

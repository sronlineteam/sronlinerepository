package es.sronline.aviso;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Parametros {
	private static Properties properties;
////avisos por SMS
//numeroSMS=616xxxxxx
////avisos por mail
//mailTo=julian.f.rguez@outlook.es
//mailHost=matute.caton.es
//mailPort=25
//mailUsu=avisos@sronline.es
//mailPassw=tudoluis1!
////avisos Whatsapp
//numeroWhatsapp=616xxxxxx
////Notificaciones Push
//token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJlMzA0YjZmYi0yNzE1LTQ0MmEtOTY4Zi05MTU3ZjE5MjJlYTYifQ.1Qml6i6vAZRH0xX9FU0C1wd7j8TwCmIDaAn6sKaXENo";
//profile="sronlineapp";
//url="https://api.ionic.io/push/notifications";
//title="SR Online";

	/**
	 * @return numero telefono para alertas SMS
	 */
	public static int getNumeroSMS()
	  {
	    return Integer.parseInt(properties.getProperty("numeroSMS"));
	  }
	
	/**
	 * @return a que correo se envian las alertas por Mail
	 */
	public static String getMailTo() {
		return properties.getProperty("mailTo");
	}

	/**
	 * @return que servidor de correo se usa en las alertas por Mail
	 */
	public static String getMailHost() {
		return properties.getProperty("mailHost");
	}

	/**
	 * @return que puerto de correo se usa en las alertas por Mail
	 */
	public static String getMailPort() {
		return properties.getProperty("mailPort");
	}

	/**
	 * @return que usuario de correo se usa en las alertas por Mail
	 */
	public static String getMailUsu() {
		return properties.getProperty("mailUsu");
	}

	/**
	 * @return que contraseņa de correo se usa en las alertas por Mail
	 */
	public static String getMailPassw() {
		return properties.getProperty("mailPassw");
	}

	/**
	 * @return numero telefono para alertas Whatsapp
	 */
	public static String getNumeroWhatsapp()
	  {
	    return properties.getProperty("numeroWhatsapp");
	  }
	
	/**
	 * @return token para anotificaciones Push
	 */
	public static String getPushToken()
	  {
	    return properties.getProperty("pushToken");
	  }
	
	/**
	 * @return profile para notificaciones Push
	 */
	public static String getPushProfile()
	  {
	    return properties.getProperty("pushProfile");
	  }
	
	/**
	 * @return numero telefono para notificaciones Push
	 */
	public static String getPushUrl()
	  {
	    return properties.getProperty("pushUrl");
	  }
	
	/**
	 * @return numero telefono para notificaciones Push
	 */
	public static String getPushTitle()
	  {
	    return properties.getProperty("pushTitle");
	  }
	

	  
	static {
	init();
	}

	public static void init() {

	try {

	ClassLoader cl = Thread.currentThread().getContextClassLoader();
	if (cl == null)
	cl = ClassLoader.getSystemClassLoader();

	InputStream is = cl.getResourceAsStream("avisos.properties");
	properties = new Properties();
	properties.load(is);
	is.close();
	
	} catch (Exception e) {

		Logger.getLogger("es.sronline.aviso").error("Error al cargar las propiedades " + e.getMessage());;

	}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}

}


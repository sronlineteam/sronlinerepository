package es.sronline.aviso;

import org.apache.log4j.Logger;

import es.sronline.aviso.whatsapp.WhatsappSender;
import es.sronline.core.Constantes;
import sun.util.logging.resources.logging;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class Avisos {
	 Logger log;
	
	public Avisos() {
		log = Logger.getLogger(getClass().getName());
	}


	public static boolean enviarAviso(String textoAviso){
		//TODO implementar envio de aviso por SMS o Mail
//		log.debug("******���AVISO!!!******   " + textoAviso);
		Logger.getLogger("es.sronline.aviso").debug("******���AVISO!!!******   " + textoAviso);
		if(Constantes.isAlertaSMS())
			enviarSMS(textoAviso);
		if(Constantes.isAlertaMail())
			enviarMail(textoAviso);
		if(Constantes.isAlertaWhatsapp())
			enviarWhatsapp(textoAviso);
		if(Constantes.isAlertaPush())
			enviarPush(textoAviso);
		return true;
	}

	private static void enviarMail(String textoAviso) {
		 String from = Parametros.getMailUsu();
		 String mensaje = "Att de " + Constantes.getNombreInstalacion() + "\n\n" + textoAviso + "\n\n Un saludo \n\nEste mail se ha enviado automaticamente, no lo responda.";
		// Get system properties
	    Properties properties = new Properties();

	    // Setup mail server
	    properties.put("mail.smtp.host", Parametros.getMailHost());
	    properties.put("mail.smtp.socketFactory.port", Parametros.getMailPort());
	    properties.put("mail.smtp.auth", "true");

	    // Get the default Session object.
	    Session session = Session.getDefaultInstance(properties,  
	    	    new javax.mail.Authenticator() {  
	    	      protected PasswordAuthentication getPasswordAuthentication() {  
	    	    return new PasswordAuthentication(Parametros.getMailUsu(),Parametros.getMailPassw());  
	    	      }  
	    	    }); 	 
	 
			try {
	 
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(Parametros.getMailTo()));
				message.setSubject("SROnline Aviso");
				message.setText(mensaje);
	 
				Transport.send(message);
	 	 
			} catch (MessagingException e) {
				Logger.getLogger("es.sronline.aviso").error("Error al enviar mail" + e.getMessage());
				throw new RuntimeException(e);
			}		
	}

	private static void enviarSMS(String textoAviso) {
		//Se inicia el objeto HTTP
//		HttpClient client = new HttpClient();

		
	}
	private static void enviarWhatsapp(String textoAviso) {
		// TODO Auto-generated method stub
		try {
			WhatsappSender.sendMessage(Parametros.getNumeroWhatsapp(), textoAviso);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getLogger("es.sronline.aviso").error("Error al enviar Whatsapp" + e.getMessage());
		}
		
	}
	private static void enviarPush(String textoAviso){
		
		URL obj;

		try {
			obj = new URL(Parametros.getPushUrl());
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	
			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("Authorization", "Bearer "+Parametros.getPushToken());
			con.setRequestProperty("Content-Type", "application/json");
	
			String urlParameters = "{\"tokens\": [\"\"],\"send_to_all\": true,\"profile\": \""+Parametros.getPushProfile()+"\","
								+ "\"notification\": {\"message\": \""+textoAviso+"\",\"title\":\""+Parametros.getPushTitle()+"\"}}";
	
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	
			int responseCode = con.getResponseCode();
			Logger.getLogger("es.sronline.aviso").debug("\nSending 'POST' request to URL : " + Parametros.getPushUrl());
			Logger.getLogger("es.sronline.aviso").debug("Post parameters : " + urlParameters);
			Logger.getLogger("es.sronline.aviso").debug("Response Code : " + responseCode);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getLogger("es.sronline.aviso").error("Error al enviar Notificacion Push" + e.getMessage());
		}

	}


}

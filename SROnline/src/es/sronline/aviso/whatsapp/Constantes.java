package es.sronline.aviso.whatsapp;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Constantes {
	private static Properties properties;
  private static final String INSTANCE_ID = "YOUR_INSTANCE_ID_HERE";
  private static final String CLIENT_ID = "YOUR_CLIENT_ID_HERE";
  private static final String CLIENT_SECRET = "YOUR_CLIENT_SECRET_HERE";
	
	public static String getInstanceID(){
	    return properties.getProperty("Instance_ID").trim();
	}
	public static String getClientID(){
	    return properties.getProperty("Client_ID").trim();
	}
	public static String getClienteSecretID(){
	    return properties.getProperty("Client_secret_ID").trim();
	}

	  
	static {
	init();
	}

	public static void init() {

	try {

	ClassLoader cl = Thread.currentThread().getContextClassLoader();
	if (cl == null)
	cl = ClassLoader.getSystemClassLoader();

	InputStream is = cl.getResourceAsStream("avisoWhatsapp.properties");
	properties = new Properties();
	properties.load(is);
	is.close();
	
	} catch (Exception e) {

		Logger.getLogger("es.sronline.aviso.whatsapp").error("Error al cargar las propiedades " + e.getMessage());;

	}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}

}

package es.sronline.tareas;


/* 
 * All content copyright Terracotta, Inc., unless otherwise indicated. All rights reserved. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
 
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import es.sronline.aviso.Avisos;
import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.core.Constantes;
import es.sronline.telecontrol.Telecontrol;


/**
 * 
 * @author Sergios
 */
public class EnvioTramaObjTel implements Job {

    private static Logger _log = Logger.getLogger(EnvioTramaObjTel.class);

    
    public EnvioTramaObjTel() {
    }

   
    public void execute(JobExecutionContext context)
        throws JobExecutionException {

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        
    	
    	String trama = dataMap.getString("trama");
    	Integer idObjeto = dataMap.getInt("IdObjeto");
    	Integer desAccion = dataMap.getInt("DesAccion");
    	Integer idprogramacion = dataMap.getInt("Idprogramacion");
    	
        _log.debug("group2.job" + idprogramacion + ": Se ha realizado el envio " + trama + " a las " + new Date()
        	+ " con objeto telecontrol " + idObjeto + " accion " + desAccion );
        
        ConexionBBDD conexion = new ConexionBBDD();
        String resultado = new String();
		StringBuffer aviso = null;
		
        try{
			ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
			ObjetosTelecontrol objetoTC = _dao.findByPrimaryKey(idObjeto);
			TipoObjetoTelecontrolDao _daoTOT = TipoObjetoTelecontrolDaoFactory.create();
			
			if (objetoTC!=null){
	    		
	    		Telecontrol objTelecontrol = new Telecontrol(objetoTC);
	    		TipoObjetoTelecontrol tipoObjetoTC = _daoTOT.findByPrimaryKey(objetoTC.getIdTipoObjetoTelecontrol());
				
	    		int intentos = 0;
	    		boolean ejecutada = false;
    			while (intentos++ < Constantes.getIntentosEjecucion() && !ejecutada) {
	    			conexion.actualizarEnvioObjeto(idprogramacion,Constantes.ENVIADO);
		    		if (desAccion==Telecontrol.ABRIR) {
		    			ejecutada = objTelecontrol.abrirObjeto();
		    			_log.debug("SE ABRE"+ intentos + " " + ejecutada);
		    		}else{
		    			if (desAccion==Telecontrol.CERRAR){
		    				ejecutada = objTelecontrol.cerrarObjeto();
		    				_log.debug("SE CIERRA"+ intentos + " "  + ejecutada);
			    		}
					}
			  	    try
				    {
				        //TODO RECOGER EL TIEMPO DE ESPERA DEL OBJETO TELECONTROL 
			  	    	Thread.sleep(tipoObjetoTC.getTiempoRespuesta());
				    }
				    catch (InterruptedException e)
				    {
				    	_log.error(e.getMessage());
				    }
				    
					if(intentos == Constantes.getIntentosLecturaParaAviso() && !ejecutada){
						aviso = new StringBuffer();
			    		conexion.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_ERROR);
					}else{
						if(ejecutada){
							resultado = "La planificación de " + Telecontrol.textoAccion[desAccion] + " sobre " + objetoTC.getNombre() + " ejecutada correctamente" ;
			    			conexion.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_CORRECTO);
							conexion.actualizarResultadoObjeto(idprogramacion, resultado);
						}
					}
					
	    		}
    		
	    		if(!ejecutada){
	    			resultado = "La planificación de " 
	    						+ Telecontrol.textoAccion[desAccion] + " sobre " 
	    						+ objetoTC.getNombre() + " no se ha podido ejecutar, intentelo manualmente.";
	    			Avisos.enviarAviso(resultado);
	    			conexion.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_ERROR);
	    		}else{
	    			resultado = "La instrucción planificada de " + Telecontrol.textoAccion[desAccion] + " sobre " + objetoTC.getNombre() + " ejecutada correctamente" ;	
	    			_log.debug(resultado);
	    			conexion.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_CORRECTO);
	    		}
	    		
    			conexion.actualizarResultadoObjeto(idprogramacion, resultado);
			}
			
		    try{
		    	  Thread.sleep(Constantes.getPausaEjecucionProgramacion());
		    }
		    catch (InterruptedException e){
		    	_log.error(e.getMessage());
		    }
		    
		    conexion.cerrar();

    	}catch (ObjetosTelecontrolDaoException | TipoObjetoTelecontrolDaoException e){
    		conexion.actualizarErrorObjeto(idprogramacion, e.getMessage());
    		_log.error(e.getMessage());
    		conexion.cerrar();
    	}

    }

}
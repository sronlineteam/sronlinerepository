package es.sronline.tareas;


/* 
 * All content copyright Terracotta, Inc., unless otherwise indicated. All rights reserved. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
 
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import es.sronline.aviso.Avisos;
import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.core.Constantes;
import es.sronline.telecontrol.Telecontrol;


/**
 * 
 * @author Sergios
 */
public class EnvioTramaContorlFlujo implements Job {

    private static Logger _log = Logger.getLogger(EnvioTramaContorlFlujo.class);

    
    public EnvioTramaContorlFlujo() {
    }

   
    public void execute(JobExecutionContext context)
        throws JobExecutionException {

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        
    	
    	Integer idObjeto = dataMap.getInt("IdObjeto");
    	Integer desAccion = dataMap.getInt("DesAccion");
    	
        _log.debug("group3.job" + idObjeto + "-" + desAccion + ": Se ha comprobado el flujo a las " + new Date()
        	+ " del objeto telecontrol " + idObjeto + " accion " + desAccion );
        
        ConexionBBDD conexion = new ConexionBBDD();
        String resultado = new String();
		
        try{
			ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
			ObjetosTelecontrol objetoTC = _dao.findByPrimaryKey(idObjeto);
			TipoObjetoTelecontrolDao _daoTOT = TipoObjetoTelecontrolDaoFactory.create();
			
			if (objetoTC!=null){
	    		
	    		Telecontrol objTelecontrol = new Telecontrol(objetoTC);
	    		TipoObjetoTelecontrol tipoObjetoTC = _daoTOT.findByPrimaryKey(objetoTC.getIdTipoObjetoTelecontrol());
				
	    		int intentos = 0;
	    		boolean ejecutada = false;
    			while (intentos++ < Constantes.getIntentosEjecucion() && !ejecutada) {
    					ejecutada = objTelecontrol.verificarFlujo(desAccion);
					try{
				        //TODO RECOGER EL TIEMPO DE ESPERA DEL OBJETO TELECONTROL 
			  	    	Thread.sleep(tipoObjetoTC.getTiempoRespuesta());
				    }
				    catch (InterruptedException e)
				    {
				    	_log.error(e.getMessage());
				    }
				    
					if(intentos == Constantes.getIntentosLecturaParaAviso() && !ejecutada){
						//TO-DO
					}else{
						if(ejecutada){
							resultado = "La verificación del flujo " + Telecontrol.textoAccion[desAccion] + " sobre " + objetoTC.getNombre() + " ejecutada correctamente" ;
						}
					}
					
	    		}
    		
	    		if(!ejecutada){
	    			resultado = "La comprobación del flujo sobre " 
	    						+ objetoTC.getNombre() + " indica que hay un problema, revise el dispositivo.";
	    			Avisos.enviarAviso(resultado);
	    		}else{
	    			resultado = "La instrucción planificada de " + Telecontrol.textoAccion[desAccion] + " sobre " + objetoTC.getNombre() + " ejecutada correctamente" ;	
	    			_log.debug(resultado);
	    		}
			}
			
		    conexion.cerrar();

    	}catch (ObjetosTelecontrolDaoException | TipoObjetoTelecontrolDaoException e){
    		_log.error(e.getMessage());
    		conexion.cerrar();
    	}

    }

}
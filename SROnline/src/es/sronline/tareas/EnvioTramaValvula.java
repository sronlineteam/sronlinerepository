package es.sronline.tareas;


/* 
 * All content copyright Terracotta, Inc., unless otherwise indicated. All rights reserved. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
 
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import es.sronline.core.Valvulas;
import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dto.ProgramacionPk;
import es.sronline.core.Bomba;
import es.sronline.core.Trama;


/**
 * 
 * @author Sergios
 */
public class EnvioTramaValvula implements Job {

    private static Logger _log = Logger.getLogger(EnvioTramaValvula.class);

    
    public EnvioTramaValvula() {
    }

   
    public void execute(JobExecutionContext context) {

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
            	
    	String trama = dataMap.getString("trama");
    	Integer IdRemota = dataMap.getInt("IdRemota");
    	Integer IdValvula = dataMap.getInt("IdValvula");
    	Integer DesAccion = dataMap.getInt("DesAccion");
    	Integer Idprogramacion = dataMap.getInt("Idprogramacion");
    	
        _log.debug("group1.job" + Idprogramacion + ": Se ha realizado el envio " + trama + " a las " + new Date()
        	+ " con valvula " + IdValvula + " y remota " + IdRemota + " accion " + DesAccion );
        
		Bomba bomba = new Bomba();
		Valvulas valvula = new Valvulas();
		boolean resultado = false;
	
		bomba.setIdRemota(IdRemota);
		bomba.setIdValvula(IdValvula);
		if(DesAccion == 1)
			resultado = valvula.abrirValvula(bomba);
		else if(DesAccion == 0)
			resultado = valvula.cerrarValvula(bomba);
		
		ConexionBBDD conexion = new ConexionBBDD();
		conexion.actualizarProgramacionValvulas(Idprogramacion);
		String resultado_final = new String();
		if (resultado){
			resultado_final = "Se ha realizado el envio correctamente";
		}else{
			resultado_final = "Se ha producido un error";
			conexion.actualizarErrorProgramacion(Idprogramacion, resultado_final);
		}
		
		_log.debug(resultado_final);
		conexion.actualizarResultadoProgramacion(Idprogramacion, resultado_final);
		conexion.cerrar();
    }

}
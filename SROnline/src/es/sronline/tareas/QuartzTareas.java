package es.sronline.tareas;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.quartz.DateBuilder.evenMinuteDate;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.CronTrigger;
import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import es.sronline.core.Constantes;
import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dto.Programacion;
import es.sronline.bbdd.dto.ProgramacionTelecontrol;
import es.sronline.servicio.ServicioHoraPlanificada;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;


/**
 * This Example will demonstrate how to start and shutdown the Quartz scheduler and how to schedule a job to run in
 * Quartz.
 * 
 * @author Sergios
 */
public class QuartzTareas {
	
	static Scheduler sched;
	static SchedulerFactory sf = new StdSchedulerFactory();
	static ConexionBBDD conexion = new ConexionBBDD();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(QuartzTareas.class);

	public static Scheduler getSched(){
		return sched;
	}

	public static void programacionObjTelecontrol(ProgramacionTelecontrol programacion) {
		
	    log.debug("------- Lanzando Job programacionObjTelecontrol");
	    
	    Integer Idprogramacion = programacion.getIdprogramacion();
	    try {
	    	JobKey jobKey = new JobKey("job"+programacion.getIdprogramacion(), "group2");
	    	if (!sched.checkExists(jobKey)){
				sched = sf.getScheduler();
				Date runTime = programacion.getFechaHora(); 
	
			    // define the job and tie it to our programacionObjTelecontrol class
			    JobDetail job = newJob(EnvioTramaObjTel.class).withIdentity("job"+programacion.getIdprogramacion(), "group2")
			    		.usingJobData("trama", programacion.getTrama())
			    		.usingJobData("IdObjeto", programacion.getIdObjeto())
			    		.usingJobData("DesAccion", programacion.getDesAccion())
			    		.usingJobData("Idprogramacion", programacion.getIdprogramacion())
			    		.build();
			    
			    Trigger trigger = newTrigger().withIdentity("trigger"+programacion.getIdprogramacion(), "group2").startAt(runTime).build();
			    
			    sched.scheduleJob(job, trigger);
				sched.start();
				
				String resultado = "Se ha realizado la programacion correctamente";
			    log.debug(job.getKey() + " will run at: " + runTime);
			    conexion.actualizarResultadoProgramacion(programacion.getIdprogramacion(),resultado);
	    	}else{
	    		log.debug("job"+programacion.getIdprogramacion()+ " actualmente ya lanzado");
	    	}
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			conexion.actualizarErrorProgramacion(Idprogramacion,e.getMessage());
		}
	 
		   
	}

	public static void programacionControlFlujo(int idObjeto, int accion) {
		
	    log.debug("------- Lanzando Job programacionControlFlujo");
	    
	    try {
	    	JobKey jobKey = new JobKey("job"+idObjeto+"-"+accion, "group3");
	    	if (!sched.checkExists(jobKey)){
				sched = sf.getScheduler();
				//siempre se planifica en funci�n de la fecha y hora en la que se guarda
				Date runTime = new Date(System.currentTimeMillis() + Constantes.getEsperaControlFlujo());
	
			    // define the job and tie it to our programacionObjTelecontrol class
			    JobDetail job = newJob(EnvioTramaContorlFlujo.class).withIdentity("job"+idObjeto+"-"+accion, "group3")
			    		.usingJobData("IdObjeto", idObjeto)
			    		.usingJobData("DesAccion", accion)
			    		.build();
			    
			    Trigger trigger = newTrigger().withIdentity("trigger"+idObjeto+"-"+accion, "group3").startAt(runTime).build();
			    
			    sched.scheduleJob(job, trigger);
				sched.start();
				
			    log.debug(job.getKey() + " will run at: " + runTime);
	    	}else{
	    		log.debug("jobCtrlFlujo"+ idObjeto + " actualmente ya lanzado");
	    	}
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		   
	}

	
	public void programacionesPeriodicas() {
		
	    log.debug("------- Lanzando Job de programacionesPeriodicas");
	    
	    try {
			sched = sf.getScheduler();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    JobDetail job = newJob(ProgramacionPeriodicas.class)
	    	    .withIdentity("job", "group")
	    	    .build();

	    Trigger trigger = newTrigger()
	    	    .withIdentity("trigger", "group")
	    	    .withSchedule(simpleSchedule().withIntervalInMinutes(60).repeatForever())
	    	    .build();
	    try {
			sched.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    try {
			sched.start();
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			ServicioHoraPlanificada.sethoraPlanificacion(cal);
		    log.debug(job.getKey() + " Se ha iniciado la programacion periodica para valvulas y objetos ");
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	}

}

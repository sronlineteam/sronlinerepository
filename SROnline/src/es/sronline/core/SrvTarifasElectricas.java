package es.sronline.core;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.LstTarifasElectricasDao;
import es.sronline.bbdd.dto.LstTarifasElectricas;
import es.sronline.bbdd.exceptions.LstTarifasElectricasDaoException;
import es.sronline.bbdd.factory.LstTarifasElectricasDaoFactory;

/**
 * Servlet implementation class SrvTarifasElectricas
 */
@WebServlet("/SrvTarifasElectricas")
public class SrvTarifasElectricas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvTarifasElectricas() {
        super();
        log = Logger.getLogger(getClass().getName());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String resultado = "";
		String accion;
		Logger log = Logger.getLogger(getClass().getName());
		log.debug("*********recibida petici�n en SrvTarifas*********");
		//recoger si hay accion 
		if(request.getParameter("accion")!= null){
			accion = request.getParameter("accion");
			// si es nueva insertamos
			if(accion.equals("nueva")){
				LstTarifasElectricasDao _dao = LstTarifasElectricasDaoFactory.create();
				LstTarifasElectricas tarifas = new LstTarifasElectricas();
				tarifas.setDescripcion(request.getParameter("descripcion"));
				
				try {
					_dao.insert(tarifas);
				} catch (LstTarifasElectricasDaoException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				}
			}
		}
		LstTarifasElectricas[] lstTarifasElectricas = null;
		try {
			LstTarifasElectricasDao _dao = LstTarifasElectricasDaoFactory.create();
			lstTarifasElectricas = _dao.findAll();
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		
		request.setAttribute("lstTarifasElectricas", lstTarifasElectricas);

		
		//consulto la lista de programaciones posteriores a ahora.
				List<LstTarifasElectricas> lstTarifasElec = cargarTarifasElectricas();
				//mando la lista a la jsp
				request.setAttribute("lstTarifasElectricas", lstTarifasElec);
				request.setAttribute("resultado", resultado);
		//			getServletContext().getRequestDispatcher("/listaValvulas.jsp").forward(request, response);
			
			
		getServletContext().getRequestDispatcher("/TarifasElectricas.jsp").forward(request, response);
	}
	
private List<LstTarifasElectricas> cargarTarifasElectricas() {
	ConexionBBDD con = new ConexionBBDD();
	List<LstTarifasElectricas> resultado = con.consultarTarifasElectricas();
	return resultado;
}
}

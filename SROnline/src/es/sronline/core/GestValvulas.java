package es.sronline.core;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.ProgramacionDao;
import es.sronline.bbdd.dto.Programacion;
import es.sronline.bbdd.exceptions.ProgramacionDaoException;
import es.sronline.bbdd.factory.ProgramacionDaoFactory;

/**
 * @author j.rodriguezg
 * Gestión de valvulas, prepara las tramas para su utilización
 * ES POSIBLE QUE YA NO ESTE EN USO REVISARLO!!!!!
 */
public class GestValvulas {
	
	
	Logger log;

	/**
	 * Mediante un objeto Bomba con información de la remota y la valvula a 
	 * manipular y el modo de accion codificada:
	 * 0x00 deshabilitada
	 * 0x01 Manual abierta
	 * 0x02 Manual cerrada
	 * 0x03 automatico
	 *  
	 * se genera la trama
	 * para enviar al programador
	 * 
	 * @param bomba
	 * @param modo
	 * @return trama para el programador
	 */
	public String getTrama(Bomba bomba, String modo){
		StringBuffer sbTrama = new StringBuffer();
		sbTrama.append(Constantes.getDMBUS());
		sbTrama.append(Constantes.getByte1());
		sbTrama.append(getDirMemo(bomba));
		sbTrama.append(modo);
		sbTrama.append(Constantes.getByte5());
		
		log.debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();

	}
	public GestValvulas() {
		log = Logger.getLogger(
	            SrvGestBomba.class.getName());
	}
	private String getDirMemo(Bomba bomba){
		String dirMemo = "0000";
		String aux = Integer.toHexString(bomba.getIdRemota() * 4 + bomba.getIdValvula() + 3985);
		
		dirMemo = aux.length()<4? "0"+ aux:aux;
		
		return dirMemo;
		
	}
	public String nuevaProgramacion(String[] valvulas, String hora,
			String dia, String accionVal) {
		String[] auxValvula;
		int accion = -1;
		int auxReomta;
		int auxVal;
		Bomba auxBomba = new Bomba();
		Date fechaHora = new Date();
		Calendar cal = Calendar.getInstance();
		Programacion programacion;
    	String resultado = "";

		if(accionVal.equalsIgnoreCase(Constantes.getAccionAbrir())){
		accion = 1;
		accionVal = Constantes.getManualAbierto();
		}
		else if (accionVal.equalsIgnoreCase(Constantes.getAccionCerrar())){
			accion = 0;
			accionVal = Constantes.getManualCerrado();
		}
		cal = Common.getFechaHoraProgramacion(dia, hora);
		
		for (int i=0; i<valvulas.length; i++){
			programacion = new Programacion();
			fechaHora.setTime(cal.getTimeInMillis() + i*5000);
			auxValvula = valvulas[i].substring(1).split("-");
			auxReomta = Integer.parseInt(auxValvula[0]);
			auxVal = Integer.parseInt(auxValvula[1]);
			auxBomba.setIdRemota(auxReomta);
			auxBomba.setIdValvula(auxVal);
			programacion.setIdRemota(auxReomta);
			programacion.setIdValvula(auxVal);
			programacion.setDesAccion(accion);
			programacion.setTrama(getTrama(auxBomba, accionVal));
			programacion.setFechaHora(fechaHora);
			resultado += "" + guardar(programacion);
			
		}

		resultado += "Se ha guardado la programación para el " + dia + " a las " + hora;

		
		
		return resultado;
	}
	
	private String guardar(es.sronline.bbdd.dto.Programacion programacion) {
		ProgramacionDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido guardar, intentelo nuevamente.";
		try {
			_dao.insert(programacion);
			resultado =  "Programación guardada.";
		} catch (ProgramacionDaoException e) {
			log.error(e.getMessage());
		}
		
		return resultado;
		
	}
	
	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionDao getProgramacionDao()
	{
		return ProgramacionDaoFactory.create();
	}


}

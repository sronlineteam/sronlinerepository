package es.sronline.core;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.ProgramacionDao;
import es.sronline.bbdd.dao.ValvulasDao;
import es.sronline.bbdd.dto.Programacion;
import es.sronline.bbdd.dto.Valvulas;
import es.sronline.bbdd.dto.ValvulasPk;
import es.sronline.bbdd.exceptions.ProgramacionDaoException;
import es.sronline.bbdd.exceptions.ValvulasDaoException;
import es.sronline.bbdd.factory.ProgramacionDaoFactory;
import es.sronline.bbdd.factory.ValvulasDaoFactory;
import es.sronline.servicio.ServicioHoraPlanificada;
import es.sronline.tareas.QuartzTareas;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;



/**
 * @author j.rodriguezg
 * Gesti�n de valvulas, prepara las tramas para su utilizaci�n
 */
public class GestTramasValvulas {
	
	
	Logger log;
	
	static int idProgramacion;

	public GestTramasValvulas() {
		log = Logger.getLogger(getClass().getName());
	}
	private static String getDirMemo(Bomba bomba){
		String dirMemo = "0000";
//		String aux = Integer.toHexString(bomba.getIdRemota() * 4 + bomba.getIdValvula() + 3985);
//		
//		dirMemo = aux.length()<4? "0"+ aux:aux;
		   ValvulasPk valvulaPk = new ValvulasPk(bomba.getIdValvula(), 
				      bomba.getIdRemota());
				    ValvulasDao _dao = ValvulasDaoFactory.create();
				    try
				    {
				      Valvulas valvula = _dao.findByPrimaryKey(valvulaPk);
				      if (valvula != null) {
				        dirMemo = valvula.getDireccionHex();
				      }
				    }
				    catch (ValvulasDaoException e)
				    {
				    	Logger.getLogger("es.sronline").error(e.getMessage());
				    }		
		return dirMemo;
		
	}
	
	private static String getDirRemotaControlFlujo(Bomba bomba){
		String dirMemo = "00";
		String aux = Integer.toHexString(bomba.getIdRemota()  + 59);
		
		dirMemo = aux.length()<2? "0"+ aux:aux;
		
		return dirMemo;
		
	}

	private static String formatUnByte(int n){
		return n<10? "0"+ n:"" + n;
				
	}
	
	public static String nuevaProgramacion(String[] valvulas, String hora, String dia, String accionVal) {
			return nuevaProgramacion(valvulas,  hora, dia,  accionVal,  ControlTarifaElectrica.CONSULTAR);}

	public static String nuevaProgramacion(String[] valvulas, String hora, String dia, String accionVal, int confirmacion) {
		String[] auxValvula;
		int accion = -1;
		int auxReomta;
		int auxVal;
		Bomba auxBomba = new Bomba();
		Date fechaHora = new Date();
		Calendar cal = Calendar.getInstance();
		Programacion programacion = null;
    	String resultado = "";
    	cal = Common.getFechaHoraProgramacion(dia, hora);
    	fechaHora.setTime(cal.getTimeInMillis());
		
		if(accionVal.equalsIgnoreCase(Constantes.getAccionAbrir())){
			if(Constantes.isControlTarifaElectrica()){
				int decision = ControlTarifaElectrica.controlar(fechaHora);
				switch (decision) {
				case ControlTarifaElectrica.NO_PERMITIR:
					return ControlTarifaElectrica.AVISO_NO_PERMITIR;
//					break;

				case ControlTarifaElectrica.CONSULTAR:
					if(confirmacion == ControlTarifaElectrica.CONSULTAR)
						return ControlTarifaElectrica.AVISO_CONSULTAR;
					else if(confirmacion == ControlTarifaElectrica.NO_PERMITIR)
						return ControlTarifaElectrica.AVISO_NO_PERMITIR;
					break;
				
			default:
					break;
				}
						
			}
			
			accion = 1;
			accionVal = Constantes.getManualAbierto();
		}
		else if (accionVal.equalsIgnoreCase(Constantes.getAccionCerrar())){
			accion = 0;
			accionVal = Constantes.getManualCerrado();
		}
		
		for (int i=0; i<valvulas.length; i++){
			programacion = new Programacion();
			fechaHora.setTime(cal.getTimeInMillis() + i*5000);
			auxValvula = valvulas[i].substring(1).split("-");
			auxReomta = Integer.parseInt(auxValvula[0]);
			auxVal = Integer.parseInt(auxValvula[1]);
			auxBomba.setIdRemota(auxReomta);
			auxBomba.setIdValvula(auxVal);
			programacion.setIdRemota(auxReomta);
			programacion.setIdValvula(auxVal);
			programacion.setDesAccion(accion);
			programacion.setTrama(getTramaAccion(auxBomba, accionVal));
			programacion.setFechaHora(fechaHora);
			
			resultado += "" + guardar(programacion);
			resultado +="Se ha guardado la programaci�n para el " + dia + " a las " + hora;
			
			ConexionBBDD conexion = new ConexionBBDD();
			conexion.actualizarResultadoProgramacion(idProgramacion, "Se ha guardado la programaci�n para el " + dia + " a las " + hora);
			
			if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora) && fechaHora.compareTo(new Date())>0){
				try {
					//QuartzTareas.programacionValvula(programacion);
				} catch (Exception e) {
					conexion.actualizarErrorObjeto(idProgramacion, e.getMessage());
					e.printStackTrace();
				}
			}

			conexion.cerrar();
			
			Logger.getLogger(resultado);
			
		}
		
			
		return resultado;
	}
	
	static String guardar(Programacion programacion) {
		ProgramacionDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido guardar, intentelo nuevamente.";
		try {
			_dao.insert(programacion);
			resultado =  "Programaci�n guardada.";
			idProgramacion = programacion.getIdprogramacion();
		} catch (ProgramacionDaoException e) {
			Logger.getLogger("eu.sronline").error(e.getMessage());
		}
		
		return resultado;
		
	}
	
	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	private static ProgramacionDao getProgramacionDao()
	{
		return ProgramacionDaoFactory.create();
	}
	
	/**
	 * Mediante un objeto Bomba con informaci�n de la remota y la valvula a 
	 * manipular y el modo de accion codificada:
	 * 0x00 deshabilitada
	 * 0x01 Manual abierta
	 * 0x02 Manual cerrada
	 * 0x03 automatico
	 *  
	 * se genera la trama
	 * para enviar al programador
	 * 
	 * @param bomba
	 * @param modo
	 * @return trama 
	 * 				para el programador
	 */
	public static String getTramaAccion(Bomba bomba, String modo){
		StringBuffer sbTrama = new StringBuffer();
		boolean estaEnProgramador = bomba.getIdRemota() == 0;
		
		//Byte 1
		sbTrama.append(Constantes.getDMBUS());
		//Byte 2
		if(estaEnProgramador)
			sbTrama.append(Constantes.getComandoCambiarUnaProgramador());
		else
			sbTrama.append(Constantes.getComandoCambiarUna());
		//Byte 3 y 4
		sbTrama.append(getDirMemo(bomba));
		//Byte5 
		if(estaEnProgramador){
			if(modo.equals(Constantes.getManualAbierto()))
				modo=Constantes.getAbrirEnProgramador();
			else
				modo=Constantes.getCerrarEnProgramador();
		}
		sbTrama.append(modo);
		//Byte6 
		if(!estaEnProgramador)
			sbTrama.append(Constantes.getByte6());
		
		Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	
	}
	
	/**
	 * Construye la trama para cambiar N bombas consecutivas
	 * (aun cuando no se cambie el estado de una bomba se envia su estado o modo).
	 * se indica la primera bomba a cambiar y los modos  nuevos para 
	 * la primera y las sucesivas bombas hasta llegar a la �ltima.
	 * 
	 * @param bomba 
	 * 			primera bomba a cambair
	 * @param modo 
	 * 			array de modos para las N bombas
	 * @return
	 */
	public static String getTramaAccionMultiple(Bomba bomba, String modo[]) {
		StringBuffer sbTrama = new StringBuffer();
		int nBytes = modo.length;
		String numModos = formatUnByte(nBytes);
		  
		//Byte 1
		sbTrama.append(Constantes.getDMBUS());
		//Byte 2
		sbTrama.append(Constantes.getComandoCambiarVarias());
		//Byte 3 y 4
		sbTrama.append(getDirMemo(bomba));
		//Byte5 
		sbTrama.append("00");
		//Byte6
		sbTrama.append(numModos);
		//Byte 7 n�mero de v�lvulas
		sbTrama.append(formatUnByte(modo.length));
		//Byte 8 a 8+N
		for(int n=0; n < modo.length; n++)
			sbTrama.append(modo[n]);
	
		Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	}
	
	
	public static String getTramaAccionMultipleProgramador(String estadosValvulasProgramador)
	  {
	    StringBuffer sbTrama = new StringBuffer();
	    
	    sbTrama.append(Constantes.getDMBUS());
	    
	    sbTrama.append(Constantes.getComandoCambiarVariasProgramador());
	    
	    sbTrama.append("0708");
	    
	    sbTrama.append("00");
	    
	    sbTrama.append("01");
	    
	    sbTrama.append("02");
	    
	    sbTrama.append(binariToHex(estadosValvulasProgramador));
	    
	    Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
	    
	    return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	  }
	  
	public static String binariToHex(String estadosValvulasProgramador)
	  {
	    int aux = Integer.parseInt(estadosValvulasProgramador, 2);
	    String result = Integer.toHexString(aux);
	    
	    return completaConCeros(result, 4);
	  }
	  
	public static String completaConCeros(String numero, int longitud)
	  {
	    StringBuffer auxCero = new StringBuffer("");
	    for (int i = 0; i < longitud - numero.length(); i++) {
	      auxCero.append("0");
	    }
	    return auxCero + numero;
	  }
	  
	  
	public static String getTramaConsulta(Bomba bomba) {
			
		return getTramaConsultaMultiple(bomba, 1);
	}

	/**
	 * @param bomba
	 *            bomba a consultar
	 * @param consultar
	 *            numero de valvulas a consultar
	 * @return
	 */
	public static String getTramaConsultaMultiple(Bomba bomba, int consultar) {
		StringBuffer sbTrama = new StringBuffer();
		String auxNumero = "" + consultar;
		//Byte 1
		sbTrama.append(Constantes.getDMBUS());
		//Byte 2
		sbTrama.append(Constantes.getComandoConsultarUna());
		//Byte 3 y 4 bomba inicial
		sbTrama.append(getDirMemo(bomba));
		//Byte5 
		sbTrama.append("00");
		//Byte6 n�mero de v�lvulas
		if(consultar<10)
			auxNumero = "0" + consultar;
		sbTrama.append(auxNumero);
		
		Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	}
	
	public static String getTramaConsultaMultipleProgramador()
	  {
	    StringBuffer sbTrama = new StringBuffer();
	    
	    sbTrama.append(Constantes.getDMBUS());
	    
	    sbTrama.append(Constantes.getComandoConsultarVariasProgramador());
	    
	    sbTrama.append("1086");
	    
	    sbTrama.append("00");
	    
	    sbTrama.append("01");
	    
	    sbTrama.append("02");
	    
	    Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
	    
	    return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	  }
	  
	
	/**
	 * Trama para comprobar si est� o no pasando flujo
	 * 
	 * @param bomba
	 * 			bomba a comprobar
	 * @param comprobar
	 * 			tipo de comprobaci�n (pasando o no corriente de agua)
	 * @return
	 */
	public static String getTramaComprobacionEnRemota(Bomba bomba) {
		StringBuffer sbTrama = new StringBuffer();
		//Byte 1
		sbTrama.append(Constantes.getDMBUS());
		//Byte 2
		sbTrama.append(Constantes.getComandoComprobar());
		//Byte 3 
		sbTrama.append("00");
		//Byte 4 bomba inicial
		sbTrama.append(getDirRemotaControlFlujo(bomba));
		//Byte5 
		sbTrama.append("00");
		//Byte6 
		sbTrama.append("00");
		
		Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	}
	public static String getTramaComprobacionEnProgramador(int posicion) {
		StringBuffer sbTrama = new StringBuffer();
		String idVal = "" + (7 + posicion);
		//Byte 1
		sbTrama.append(Constantes.getDMBUS());
		//Byte 2
		sbTrama.append(Constantes.getComandoConsultarVariasProgramador());
		//Byte 3 
		sbTrama.append("07");
		//Byte 4 
		sbTrama.append(idVal);
		//Byte5 
		sbTrama.append("00");
		//Byte6 
		sbTrama.append("01");
		//Byte 7
	    sbTrama.append("02");
		
		Logger.getLogger("es.sronline").debug("trama: " + sbTrama.toString());
		
		return Common.appendCRC16(sbTrama.toString()).toUpperCase();
	}
	
	public static byte[] transformaByte(String tramaASCII) {
		int nBytes = tramaASCII.length()/2;
		byte[] resultado = new byte[nBytes];
		String aux;

		for (int i = 0; i < nBytes; i++){
			aux = tramaASCII.substring(2*i, 2*i + 2);
			resultado[i] = Integer.valueOf(aux, 16).byteValue();
		}
		
		return resultado;
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}

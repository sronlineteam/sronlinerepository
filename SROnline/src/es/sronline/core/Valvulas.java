package es.sronline.core;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.LstValvulasDao;
import es.sronline.bbdd.dao.SensorDao;
import es.sronline.bbdd.dao.ValvulasDao;
import es.sronline.bbdd.dto.LstValvulas;
import es.sronline.bbdd.dto.Programacion;
import es.sronline.bbdd.dto.ValvulasPk;
import es.sronline.bbdd.exceptions.SensorDaoException;
import es.sronline.bbdd.exceptions.ValvulasDaoException;
import es.sronline.bbdd.factory.LstValvulasDaoFactory;
import es.sronline.bbdd.factory.SensorDaoFactory;
import es.sronline.bbdd.factory.ValvulasDaoFactory;
import es.sronline.comunicacion.FlujoComunicacion;

@Deprecated
public class Valvulas {

	private FlujoComunicacion comFlow;
	private String trama;
	Logger log;
//	GestTramasValvulas gstValvulas = new GestTramasValvulas();

	public Valvulas() {
		log = Logger.getLogger("es.sronline");
	    if ("COM".equalsIgnoreCase(Constantes.getTipoComunicacion())) {
	        this.comFlow = new es.sronline.comunicacion.comm.Comunicacion();
	      }
	    if ("IP".equalsIgnoreCase(Constantes.getTipoComunicacion())) {
	        this.comFlow = new es.sronline.comunicacion.ip.Comunicacion();
	      }
	    if ("WS".equalsIgnoreCase(Constantes.getTipoComunicacion())) {
	        this.comFlow = new es.sronline.comunicacion.webservice.ComunicacionWS();
	      }
	}

	public boolean abrirValvula(Bomba bomba) {
		boolean result = false;
		try {
//			if (bomba.getIdRemota()==1 && bomba.getIdValvula()== 1 && 
//				(bomba.getIdRemota()==0 && bomba.getIdValvula() == 1 && estadoValvula(bomba).equals(Constantes.CERRADA))){
//				log.error("La Valvula no se puede abrir por no estar llena la balsa");
//				result = false;
//				return result;
//			}
			
			trama = GestTramasValvulas.getTramaAccion(bomba, Constantes.getManualAbierto());

			log.debug("trama + CRC: " + trama);
			result = comprueba(trama, comFlow.enviar(trama));
			if (result) 
				actualizaEstadoValvula(bomba, Constantes.ABIERTA);

		} catch (Exception e) {
			log.error("error al abrir " + e.getMessage());
			result = false;
		}

		return result;
	}

	public boolean cerrarValvula(Bomba bomba) {
		boolean result = false;
		try {
			trama = GestTramasValvulas.getTramaAccion(bomba, Constantes.getManualCerrado());

			log.debug("trama + CRC: " + trama);
			result = comprueba(trama, comFlow.enviar(trama));
			if (result) 
				actualizaEstadoValvula(bomba, Constantes.CERRADA);

		} catch (Exception e) {
			log.error(e.getMessage());
			log.error("error al cerrar " + e.getMessage());
			result = false;
		}

		return result;
	}

	public String estadoValvula(Bomba bomba) {
		String result = "error";
		String auxEstado = "";
		int intentos = 0;
		//Si el id de la remota es 0 es que est� en el programador
		boolean enProgramador = (bomba.getIdRemota()==0);
		
		try {
		      if (enProgramador) {
		          trama = GestTramasValvulas.getTramaConsultaMultipleProgramador();
		        } else {
		   			trama = GestTramasValvulas.getTramaConsulta(bomba);
		        }

			log.debug("trama + CRC: " + trama);
			while(result.length()<9 && intentos++ < Constantes.getIntentosEjecucion()){
				result =  comFlow.enviar(trama);
			}
			if(result.length()<9){
				log.warn("No se ha leido completa la respuesta...");
				return "";
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			log.error("error al enviar " + e.getMessage());
		}
	    if (enProgramador) {
	        auxEstado = "0" + (Integer.parseInt(result.substring(6, 7)) + 1);
	      } else {
	    	  //cojo el byte 4 que es donde est� el estado
	    	  auxEstado = result.substring(6, 8);
	      }
			
		return auxEstado;
	}

	public String estadoValvulas(boolean isRemota) {
		String result = "error";
		Bomba primeraBomba = new Bomba();
	    int numValvulas = Constantes.getNumeroMaximoDeValvulas();
	    if (isRemota)
	    {
	      primeraBomba.setIdRemota(1);
	    }
	    else
	    {
	      primeraBomba.setIdRemota(0);
	      numValvulas = Constantes.getNumeroMaximoDeValvulasProgramador();
	    }
	    primeraBomba.setIdValvula(1);
	    try
	    {
	      this.trama = GestTramasValvulas.getTramaConsultaMultiple(primeraBomba, numValvulas);
	      
	      this.log.debug("trama + CRC: " + this.trama);
	      result = this.comFlow.enviar(this.trama);
	    }
	    catch (Exception e)
	    {
	      log.error(e.getMessage());
	      this.log.error("error al enviar " + e.getMessage());
	    }
	    if (result.length() < 10) {
	      result = estadoValvulas(isRemota);
	    }
		//quito los primeros 3 bits y los 2 ultimos
		return result.substring(6, result.length()-4);
	}

	public String estadoValvulasProgramador()
	  {
	    String result = "error";
	    String resultBinario = "00000000";
	    try
	    {
	      this.trama = GestTramasValvulas.getTramaConsultaMultipleProgramador();
	      
	      this.log.debug("trama + CRC: " + this.trama);
	      result = this.comFlow.enviar(this.trama);
	      
	      result = result.substring(6, result.length() - 4);
	      
	      resultBinario = Integer.toBinaryString(Integer.parseInt(result, 16));
	    }
	    catch (Exception e)
	    {
	      log.error(e.getMessage());
	      this.log.error("error al enviar " + e.getMessage());
	    }
	    return GestTramasValvulas.completaConCeros(resultBinario, 16);
	  }

	  
	public void enviarTrazaEstado(String trama, Bomba bomba, String estado) {
		log.debug("enviando traza programada " + trama + " con estado "
				+ estado);
		enviarTraza(trama);
		actualizaEstadoValvula(bomba, estado);

	}

	public void enviarTraza(String trama) {
		log.debug("enviando traza programada " + trama);
		try {
			if (Constantes.isProduccion())
				comFlow.enviar(trama);
			comFlow = null;
		} catch (Exception e) {
			log.error("error al enviar " + e.getMessage());
			log.error(e.getMessage());
		}

	}

	public String test(String string) {
		String result = "error";
		try {
			comFlow.enviar("HELO");
			result = "OK";
		} catch (Exception e) {
			log.error(e.getMessage());
			log.debug("error " + e.getMessage());
		}
		return result;
	
	}

	public void actualizaEstadoValvula(Bomba bomba, String estado) {
		ValvulasPk valvulaPk = new ValvulasPk(bomba.getIdValvula(),
				bomba.getIdRemota());
		ValvulasDao _dao = ValvulasDaoFactory.create();
		es.sronline.bbdd.dto.Valvulas valvula;
		
		try {
			valvula = _dao.findByPrimaryKey(valvulaPk);
			valvula.setEstado(estado);
			_dao.update(valvulaPk, valvula);
			log.info("V�lvula " + valvula.getNombre() + " " + estado);
		} catch (ValvulasDaoException e) {
			log.error(e.getMessage());
		}

	}

	private boolean comprueba(String tramaEnviada, String tramaRecivida) {
//		String trama2 = new String(GestTramasValvulas.transformaByte(tramaEnviada));
//		if (trama2.equalsIgnoreCase(tramaRecivida))
//			return true;
//		log.error("No coincide trama " + trama2 + " con la respuesta "
//				+ tramaRecivida);
//		return false;
	    if (tramaRecivida.startsWith(tramaEnviada)) {
	        return true;
	      }
		log.error("No coincide trama " + tramaEnviada + " con la respuesta "
				+ tramaRecivida);
		return false;
	}

	public void envioMultiple(HashSet<Trama> trazasParaEnviar)
	  {
	    Iterator<Trama> it = trazasParaEnviar.iterator();
	    String auxTrama = "";
	    while (it.hasNext())
	    {
	      Trama trama = (Trama)it.next();
	      auxTrama = trama.getTrama();
	      this.log.debug("Envio multiple de trama: " + auxTrama);
	      switch (trama.getTipo())
	      {
	      case 0: 
	      case 1: 
	        enviarTraza(auxTrama);
	        break;
	      }
	    }
	  }
	  
	  public void sincronizarEstadosValvulas()
	  {
	    String estadoValvulasRemotas = estadoValvulas(true);
	    pausarEjecucion();
	    String estadoValvulasProgramador = estadoValvulas(false);
	    String auxEstado = "";
	    String auxEstadoComprobado = "";
	    LstValvulas[] lstValvulas = null;
	    int nValvula = 0;
	    try
	    {
	      LstValvulasDao _dao = LstValvulasDaoFactory.create();
	      lstValvulas = _dao.findAll();
	    }
	    catch (Exception e)
	    {
	      log.error(e.getMessage());
	    }
	    for (int v = 0; v < lstValvulas.length; v++)
	    {
	      if (lstValvulas[v].getIdRemota() > 0)
	      {
	        nValvula = (lstValvulas[v].getIdRemota() - 1) * 4 + lstValvulas[v].getIdvalvula();
	        auxEstadoComprobado = estadoValvulasRemotas.substring(2 * (nValvula - 1), 2 * nValvula);
	      }
	      else
	      {
	        nValvula = lstValvulas[v].getIdvalvula();
	        auxEstadoComprobado = estadoValvulasProgramador.substring(2 * (nValvula - 1), 2 * nValvula);
	      }
	      if (lstValvulas[v].getEstado().equals("abierta")) {
	        auxEstado = Constantes.getManualAbierto();
	      } else if (lstValvulas[v].getEstado().equals("cerrada")) {
	        auxEstado = Constantes.getManualCerrado();
	      } else {
	        auxEstado = "00";
	      }
	      if (!auxEstado.equals(auxEstadoComprobado))
	      {
	        Bomba bomba = new Bomba();
	        bomba.setIdRemota(lstValvulas[v].getIdRemota());
	        bomba.setIdValvula(lstValvulas[v].getIdvalvula());
	        if (auxEstadoComprobado.equals(Constantes.getManualAbierto())) {
	          actualizaEstadoValvula(bomba, "abierta");
	        } else if (auxEstadoComprobado.equals(Constantes.getManualCerrado())) {
	          actualizaEstadoValvula(bomba, "cerrada");
	        }
	      }
	    }
	  }
	  
	  private void pausarEjecucion()
	  {
	    try
	    {
	      Thread.sleep(Constantes.getPausaEjecucionProgramacion());
	    }
	    catch (InterruptedException e)
	    {
	      log.error(e.getMessage());
	    }
	  }
}

package es.sronline.core;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * @author julian.rodriguez
 *
 */
public class Constantes {

	/**
	* Propiedad que almacena los valores preconfigurados
	*/
	private static Properties properties;
	public final static  String CERRADA = "cerrada";
	public final static  String ABIERTA = "abierta";
	public final static  String AVERIADA = "averiada";
	public final static  String CONFIRMADA = "confirmada";
	public static final String RECHAZADA = "rechazada";
	public static final int TRAMA_ACCION_REMOTA = 0;
	public static final int TRAMA_ACCION_PROGRAMADOR = 1;
	public static final int TRAMA_ACCION_VERIFICAR_FLUJO_REMOTA = 2;
	public static final int TRAMA_ACCION_VERIFICAR_FLUJO_PROGRAMADOR = 3;
	public static final String DIALOGO_VACIO = "VACIO";
	public static final String DIALOGO_ERROR = "ERROR";
	public static final String DIALOGO_INFORMAR = "INFORMAR";
	public static final String DIALOGO_CONFIRMAR = "CONFIRMAR";
	public static final int NO_PLANIFICADO = 0;
	public static final int PLANIFICADO = 1;
	public static final int NO_ENVIADO = 0;
	public static final int ENVIADO = 1;
	public static final int ENVIADO_ERROR = 2;
	public static final int ENVIADO_CORRECTO = 3;
	public static final int CONTROL_FLUJO_ON = 1;
	public static final String CONTROL_TARIFA = "1";
	public static final int CONTROL_FLUJO = 4;
	private static String hostIp;
	

	  public static String getNombreInstalacion()
	  {
	    return properties.getProperty("NombreInstalacion").trim();
	  }
	  
	  public static String getIdAEMET()
	  {
	    return properties.getProperty("idAEMET").trim();
	  }
	  /**
	 * @return si tiene mapa
	 */
	public static boolean isTieneMapa() {
		return properties.getProperty("MenuMapa").equalsIgnoreCase("true");
	}

	/**
	 * @return si tiene sectores
	 */
	public static boolean isTieneSector() {
		return properties.getProperty("MenuSector").equalsIgnoreCase("true");
	}
	

	/**
	 * @return si tiene camaras
	 */
	public static boolean isTieneCamaras()
	  {
	    return properties.getProperty("TieneCamaras").equalsIgnoreCase("true");
	  }

	/**
	 * @return si tiene camaras
	 */
	public static boolean isTieneTelecontrol()
	  {
	    return properties.getProperty("MenuTelecontrol").equalsIgnoreCase("true");
	  }
	
	/**
	 * @return si tiene menú de programaciones de telecontrol
	 */
	public static boolean isTieneProgracionesTelecontrol(){
	    return properties.getProperty("MenuProgramacionesTelecontrol").equalsIgnoreCase("true");
	}
	
	/**
	 * @return si tiene menú de programaciones de telecontrol mediante Quartz
	 */
	public static boolean isLanzarTareasProgramadasQuartz() {
		return properties.getProperty("LanzarTareasProgramadasQuartz").equalsIgnoreCase("true");
	}

	/**
	 * @return si el sistema est� en producci�n
	 */
	public static boolean isProduccion() {
		return properties.getProperty("Produccion").equalsIgnoreCase("true");
	}
	/**
	 * @return si el sistema tiene mas de un programador
	 */
	public static boolean isMultiProgramador() {
		return properties.getProperty("MultiProgramador").equalsIgnoreCase("true");
	}
	/**
	 * @return si el sistema tiene control de flujo
	 */
	public static boolean isControlDeFlujo() {
		return properties.getProperty("ControlDeFlujo").equalsIgnoreCase("true");
	}
	/**
	 * @return Espera cuando el sistema tiene control de flujo (milisegundos)
	 */
	public static long getEsperaControlFlujo() {
	    return Integer.parseInt(properties.getProperty("EsperaControlFlujo"));
	}

	/**
	 * @return si se lanzan el servicio de ejecuci�n y monitorizaci�n de tareas programadas
	 */
	public static boolean isLanzarTareasProgramadas() {
		return properties.getProperty("LanzarTareasProgramadas").equalsIgnoreCase("true");
	}
	/**
	 * @return si se lanzan el servicio de programaci�n de objetos de telecontrol
	 */
	public static boolean isProgramacionTeleControl() {
		return properties.getProperty("ProgramacionTeleControl").equalsIgnoreCase("true");
	}
	/**
	 * @return si se lanzan el servicio de ejecuci�n y monitorizaci�n de tareas programadas
	 */
	public static boolean isControlTarifaElectrica() {
		return properties.getProperty("ControlTarifaElectrica").equalsIgnoreCase("true");
	}
	/**
	 * @return si se muestran avisos
	 */
	public static String isConAvisos() {
		if(properties.getProperty("ConAvisos").equalsIgnoreCase("true"))
			return DIALOGO_INFORMAR;
		else
			return DIALOGO_VACIO;
		
	}
	
	public static String getPathLog() {
		return properties.getProperty("PathLog");
	}

	/**
	 * @return Tipo de comunicacion, puede ser 'COM' o 'IP'
	 */
	public static String getTipoComunicacion() {
		return properties.getProperty("TipoComunicacion").trim();
	}
	
	public static String getTipoConexionValvulas() {
		// TODO Auto-generated method stub
		return properties.getProperty("TipoConexionValvulas");
	}	


	public static String getConexionBBDD() {
		return properties.getProperty("ConexionBBDD");
	}

	public static String getNombreBBDD() {
		return properties.getProperty("NombreBBDD");
	}
	public static String getUsuarioBBDD() {
		return properties.getProperty("UsuarioBBDD");
	}

	public static String getPasswordBBDD() {
		return properties.getProperty("PasswordBBDD");
	}
	
	public static int getEsperaIntentoConexion()
	  {
	    return Integer.parseInt(properties.getProperty("EsperaIntentoConexion"));
	  }
	  
	public static int getNumeroMaximoDeValvulas()
	  {
	    return Integer.parseInt(properties.getProperty("NumeroMaximoDeValvulas"));
	  }
	  
	public static int getNumeroMaximoDeValvulasProgramador()
	  {
	    return Integer.parseInt(properties.getProperty("NumeroMaximoDeValvulasProgramador"));
	  }
	
	public static int getNumeroMaximoDeObjetosTelecontrol()
	  {
	    return Integer.parseInt(properties.getProperty("NumeroMaximoDeObjetosTelecontrol"));
	  }
	  
	public static int getNumeroMaximoDeObjetosTelecontrolProgramador()
	  {
	    return Integer.parseInt(properties.getProperty("NumeroMaximoDeObjetosTelecontrolProgramador"));
	  }
	
	public static boolean isSincronizado()
	  {
	    return properties.getProperty("Sincronizado").equalsIgnoreCase("true");
	  }
	/*
	 * ProgramarAbrir=0
	 * ProgramarCerrar=1
	 * ProgramarComprobarFujoEnRemota=2
	 * ProgramarComprobarNoFujoEnRemota=3
	 * ProgramarComprobarFlujoEnProgramador=4
	 * ProgramarComprobarNoFlujoEnProgramador=5
	 */
	/**
	 * @return parametro de configuraci�n del tipo de programaci�n abrir 
	 */
	public static int getProgramarAbrir(){
		return Integer.parseInt( properties.getProperty("ProgramarAbrir"));
	}	
	/**
	 * @return parametro de configuraci�n del tipo de programaci�n cerrar
	 */
	public static int getProgramarCerrar(){
		return Integer.parseInt( properties.getProperty("ProgramarCerrar"));
	}	
	/**
	 * @return parametro de configuraci�n del tipo de programaci�n comprobar flujo en remota
	 */
	public static int getProgramarComprobarFujo(){
		return Integer.parseInt( properties.getProperty("ProgramarComprobarFujoEnRemota"));
	}	
	/**
	 * @return parametro de configuraci�n del tipo de programaci�n comprobar sin flujo en remota
	 */
	public static int getProgramarComprobarNoFujo(){
		return Integer.parseInt( properties.getProperty("ProgramarComprobarNoFujoEnRemota"));
	}	

	/**
	 * @return parametro de configuraci�n del tipo de programaci�n comprobar flujo en programador
	 */
	public static int getProgramarComprobarFlujoEnProgramador(){
		return Integer.parseInt( properties.getProperty("ProgramarComprobarFlujoEnProgramador"));
	}	
	/**
	 * @return parametro de configuraci�n del tipo de programaci�n comprobar sin flujo en programador
	 */
	public static int getProgramarComprobarNoFlujoEnProgramador(){
		return Integer.parseInt( properties.getProperty("ProgramarComprobarNoFlujoEnProgramador"));
	}

	/*
	 * Comprobaciones de Flujo
	 */
	/**
	 * @return la respuesta del control de flujo si hay
	 */
	public static int getComprobarFlujo(){		
		return Integer.parseInt( properties.getProperty("ComprobarAbierto"));
	}

	/**
	 * @return la respuesta del control de flujo si no lo hay
	 */
	public static int getComprobarNoFlujo(){		
		return Integer.parseInt( properties.getProperty("ComprobarCerrado"));
	}
	
	/**
	 * @return Milisegundos Comprobacion del flujo
	 */
	public static int getMilisegundosComprobacion(){
		return Integer.parseInt( properties.getProperty("MilisegundosComprobacion"));
	}

	/**
	 * @return numero de intentos de comprobaci�n de flujo
	 */
	public static int getNumeroComprobaciones(){
		return Integer.parseInt( properties.getProperty("NumeroComprobaciones"));
	}

	/*
	 * Configuraci�n de la comunicaci�n serie 
	 */
	
	/**
	 * @return Configuraci�n numero de bits por segundo de la comunicaci�n serie 
	 */
	public static int getBitsPorSeg() {
		return Integer.parseInt( properties.getProperty("BitsPorSeg"));	
	}

	/**
	 * @return Configuraci�n numero de bits de respuesta de la comunicaci�n serie 
	 */
	public static int getDataBits() {
		return Integer.parseInt( properties.getProperty("DataBits"));	
	}
	
	/**
	 * @return Configuraci�n de la paridad de la comunicaci�n serie 
	 */
	public static int getParidad() {
		return Integer.parseInt( properties.getProperty("Paridad"));	
	}
	
	/**
	 * @return Configuraci�n del byte de parada la comunicaci�n serie 
	 */
	public static int getBitsParada() {
		return Integer.parseInt( properties.getProperty("BitsParada"));	
	}
	//******************************************************

	/**
	 * @return nombre del puerto COM
	 */
	public static String getNombrePuerto() {
		return properties.getProperty("NombrePuerto");
	}
	
	/**
	 * @return ip del modulo IP
	 */
	public static String getHostIP()
	  {
	    if (hostIp == null)
	    	hostIp = properties.getProperty("HostIP");
		return hostIp;
	  }
	  
	protected static void setHostIP(String ip)
	  {
	    hostIp = ip;
	  }

	/**
	 * @return numero del puerto del modulo IP
	 */
	public static int getPuertoIP() {
		return Integer.parseInt( properties.getProperty("PuertoIP"));	
	}
	/*
	 * Tareas programdas
	 */
	
	/**
	 * @return milisegundos de espacio entre cada envio de una tarea programada
	 */
	public static long getPausaEjecucionProgramacion() {
		return Long.parseLong(properties.getProperty("PausaEjecucionProgramacion"));
	}

	/**
	 * @return milisegundos de espera para iniciar el servicio
	 */
	public static int getInicioLanzaTareasProgramadas(){
		return Integer.parseInt( properties.getProperty("InicioLanzaTareasProgramadas"));
	}
	
	/**
	 * @return milisegundos entre cada lanzamiento de las tareas programadas
	 */
	public static int getEsperaRelanzarTareasProgramadas(){
		return Integer.parseInt( properties.getProperty("EsperaRelanzarTareasProgramadas"));
	}

//	/**
//	 * @return
//	 */
//	public static String getURLRoot() {
//		// http://localhost:8080/regadio
//		return properties.getProperty("URLRoot");
//	}

	
	/*
	 * Configuracion tramas * 
	 */
	/**
	 * @return byte del DMBUS
	 */
	public static Object getDMBUS() {
		return properties.getProperty("DMBUS");
	}
	//comandos del programador
	
	/**
	 * @return byte del comando cambiar una valvula
	 */
	public static Object getComandoCambiarUna() {
		return properties.getProperty("ComandoCambiarUna");
	}
	/**
	 * @return byte del comando consultar una valvula
	 */
	public static Object getComandoConsultarUna() {
		return properties.getProperty("ComandoConsultarUna");
	}
	/**
	 * @return byte del comando cambiar varias valvulas
	 */
	public static Object getComandoCambiarVarias() {
		return properties.getProperty("ComandoCambiarVarias");
	}
	/**
	 * @return byte del comando consultar varias valvulas
	 */
	public static Object getComandoConsultarVarias() {
		return properties.getProperty("ComandoConsultarVarias");
	}

	/**
	 * @return byte del comando comprobar flujo
	 */
	public static Object getComandoComprobar() {
		return properties.getProperty("ComandoComprobar");
	}
	
	public static String getComandoCambiarUnaProgramador()
	  {
	    return properties.getProperty("ComandoCambiarUnaProgramador");
	  }
	  
	public static String getComandoConsultarUnaProgramador()
	  {
	    return properties.getProperty("ComandoConsultarUnaProgramador");
	  }
	  
	public static String getComandoCambiarVariasProgramador()
	  {
	    return properties.getProperty("ComandoCambiarVarias");
	  }
	  
	public static String getComandoConsultarVariasProgramador()
	  {
	    return properties.getProperty("ComandoConsultarVarias");
	  }

	
	/**
	 * @return byte para indicar o informar abierto
	 */
	public static String getManualAbierto() {
		return properties.getProperty("ManualAbierto");
	}
	
	/**
	 * @return byte para indicar o informar cerrado
	 */
	public static String getManualCerrado() {
		return properties.getProperty("ManualCerrado");
	}
	
	public static String getAbrirEnProgramador()
	  {
	    return properties.getProperty("AbrirEnProgramador");
	  }
	  
	public static String getCerrarEnProgramador()
	  {
	    return properties.getProperty("CerrarEnProgramador");
	  }
	  
	  /**
	 * @return byte para indicar estado
	 */
	public static String getEstado() {
		return properties.getProperty("Estado");
	}


	/**
	 * @return valor para el sexto Byte OJO CON SU USO
	 */
	public static Object getByte6() {
		return properties.getProperty("Byte6");
	}
	
	/**
	 * @return valor para el sexto Byte OJO CON SU USO
	 */
	public static Object getByte5() {
		return properties.getProperty("Byte5");
	}
	
	/**
	 * @return valor para el sexto Byte OJO CON SU USO
	 */
	public static Object getByte1() {
		return properties.getProperty("Byte1");
	}
	
	/**
	 * @return Intervalo entre envios de distintas ordenes
	 */
	public static int getEsperaEntreOrdenes(){
		return Integer.parseInt( properties.getProperty("EsperaEntreOrdenes"));
	}

	/**
	 * @return Intentos de lectura
	 */
	public static int getIntentosLectura(){
		return Integer.parseInt( properties.getProperty("IntentosLectura"));
	}

	/**
	 * @return Intentos de ejecuci�n de una acci�n de telecontrol
	 */
	public static int getIntentosEjecucion() {
		return Integer.parseInt( properties.getProperty("IntentosEjecucion"));
	}

	/**
	 * @return Intentos de lectura fallidos para enviar aviso
	 */
	public static int getIntentosLecturaParaAviso() {
		return Integer.parseInt( properties.getProperty("IntentosLecturaParaAviso"));
	}

	/**
	 * @return Milisegundos de espera entre lecturas
	 */
	public static int getEsperaEntreIntentosLectura(){
		return Integer.parseInt( properties.getProperty("EsperaEntreIntentosLectura"));
	}

	/*
	 * Configuraci�n de textos operativos
	 */
	/**
	 * @return texto operativo para la acci�n abrir
	 */
	public static String getAccionAbrir() {
		return properties.getProperty("AccionAbrir");
	}
	
	/**
	 * @return texto operativo para la acci�n cerrar
	 */
	public static String getAccionCerrar() {
		return properties.getProperty("AccionCerrar");
	}
	
	/**
	 * @return texto operativo para la acci�n estado
	 */
	public static String getAccionEstado() {
		return properties.getProperty("AccionEstado");
	}

	/**
	 * @return texto operativo para la acci�n cambiar
	 */
	public static Object getAccionCambiar() {
		return properties.getProperty("AccionCambiar");
	}
	
	/**
	 * @return texto operativo para la acci�n eliminar
	 */
	public static Object getAccionEliminar() {
		return properties.getProperty("AccionEliminar");
	}

	/**
	 * @return texto operativo para la acci�n nueva programaci�n
	 */
	public static Object getAccionNuevaProgramacion() {
		return properties.getProperty("AccionNuevaProgramacion");
	}
	
	/**
	 * @return texto operativo para la acci�n nueva planificacion
	 */
	public static Object getAccionNuevaPlanificacion() {
		return properties.getProperty("AccionNuevaPlanificacion");
	}

	/**
	 * @return texto operativo para la acci�n nueva programaci�n
	 */
	public static Object getAccionActualizar() {
		return properties.getProperty("AccionActualizar");
	}

	/**
	 * @return texto operativo para la acci�n copiar
	 */
	public static Object getAccionCopiar() {
		return properties.getProperty("AccionCopiar");
	}
	
	/**
	 * @return texto operativo para la acci�n consultar
	 */
	public static Object getAccionConsultar() {
		return properties.getProperty("AccionConsultar");
	}

	/**
	 * @return texto operativo para la acci�n copiar
	 */
	public static String getFechaParaPlantilla() {
		return properties.getProperty("FechaParaPlantilla");
	}

	/**
	 * @return texto operativo para la acci�n cuando el estado es desconocido
	 */
	public static String getEstadoDesconocido() {
		return properties.getProperty("EstadoDesconocido");
	}

	/**
	 * @return si hay alertas SMS
	 */
	public static boolean isAlertaSMS() {
		return properties.getProperty("alertaSMS").equalsIgnoreCase("true");
	}


	/**
	 * @return numero telefono para alertas SMS
	 */
	public static int getNumeroSMS()
	  {
	    return Integer.parseInt(properties.getProperty("numeroSMS"));
	  }
	
	/**
	 * @return si hay alertas por Mail
	 */
	public static boolean isAlertaMail() {
		return properties.getProperty("alertaMail").equalsIgnoreCase("true");
	}

	/**
	 * @return a que correo se envian las alertas por Mail
	 */
	public static String getMailTo() {
		return properties.getProperty("mailTo");
	}

	/**
	 * @return que servidor de correo se usa en las alertas por Mail
	 */
	public static String getMailHost() {
		return properties.getProperty("mailHost");
	}

	/**
	 * @return que puerto de correo se usa en las alertas por Mail
	 */
	public static String getMailPort() {
		return properties.getProperty("mailPort");
	}

	/**
	 * @return que usuario de correo se usa en las alertas por Mail
	 */
	public static String getMailUsu() {
		return properties.getProperty("mailUsu");
	}

	/**
	 * @return que contrase�a de correo se usa en las alertas por Mail
	 */
	public static String getMailPassw() {
		return properties.getProperty("mailPassw");
	}

	/**
	 * @return si hay alertas por Whatsapp
	 */
	public static boolean isAlertaWhatsapp() {
		return properties.getProperty("alertaWhatsapp").equalsIgnoreCase("true");
	}

	/**
	 * @return numero telefono para alertas Whatsapp
	 */
	public static String getNumeroWhatsapp()
	  {
	    return properties.getProperty("numeroWhatsapp");
	  }
	
	public static int getAdminEliminar() {
		 return Integer.parseInt(properties.getProperty("AdminAccionEliminar"));
	 }
	 
	 public static int getAdminMostrar() {
		 return Integer.parseInt(properties.getProperty("AdminAccionMostrar"));
	 }
	 
	 public static int getAdminActualizar() {
		 return Integer.parseInt(properties.getProperty("AdminAccionActualizar"));
	 }
	 
	 public static int getAdminInsertar() {
		 return Integer.parseInt(properties.getProperty("AdminAccionInsertar"));
	 }
	
	static {
	init();
	}

	public static void init() {

	try {

	ClassLoader cl = Thread.currentThread().getContextClassLoader();
	if (cl == null)
	cl = ClassLoader.getSystemClassLoader();

	InputStream is = cl.getResourceAsStream("regadio.properties");
	properties = new Properties();
	properties.load(is);
	is.close();
	
	} catch (Exception e) {

		Logger.getLogger("es.sronline.core.Constantes").error("Error al cargar las propiedades " + e.getMessage());;

	}

	}
	
	/**
	* Retorna el valor de la propiedad almacenada en el fichero de propiedades
	*
	* @param key
	* @return El valor de la propiedad
	*/
	public static String getProperty(String key) {
	return (String) properties.getProperty(key);
	}



}

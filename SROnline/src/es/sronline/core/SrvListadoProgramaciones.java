package es.sronline.core;

import static org.quartz.TriggerBuilder.newTrigger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.apache.log4j.Logger;
import org.quartz.DateBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobKey;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.LstValvulasDao;
import es.sronline.bbdd.dao.ProgramacionDao;
import es.sronline.bbdd.dto.LstValvulas;
import es.sronline.bbdd.dto.Programacion;
import es.sronline.bbdd.dto.ProgramacionPk;
import es.sronline.bbdd.exceptions.ProgramacionDaoException;
import es.sronline.bbdd.factory.LstValvulasDaoFactory;
import es.sronline.bbdd.factory.ProgramacionDaoFactory;
import es.sronline.servicio.ServicioHoraPlanificada;
import es.sronline.tareas.QuartzTareas;


/**
 * Servlet implementation class SrvListadoProgramaciones
 */
@WebServlet("/SrvListadoProgramaciones")
public class SrvListadoProgramaciones extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvListadoProgramaciones() {
        super();
        log = Logger.getLogger(getClass().getName());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		ConexionBBDD conexion = new ConexionBBDD();

    	String accion;
    	String resultado = "";
    	int idProgramacion;
    	
		log.debug("*********recibida petición en SrvListadoProgramaciones*********");

		LstValvulas[] lstValvulas = null;
		try {
			LstValvulasDao _dao = LstValvulasDaoFactory.create();
			lstValvulas = _dao.findAll();
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("lstValvulas", lstValvulas);

		
		if(request.getParameter("accion")!=null && request.getParameter("idProgramacion")!=null){
			accion = request.getParameter("accion");
			//Programacion auxProgramacion = new Programacion();
			
			if(accion.equals(Constantes.getAccionCambiar())){
				idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
				Calendar cal = Calendar.getInstance();
				cal = Common.getFechaHoraProgramacion((String) request.getParameter("dia"), (String) request.getParameter("hora"));
				Date fechaHora = new Date();
				fechaHora.setTime(cal.getTimeInMillis());
				try {
					resultado = cambiarFechaHoraProgramacion(idProgramacion, fechaHora);
				} catch (SchedulerException e) {
					conexion.actualizarErrorProgramacion(idProgramacion,e.getMessage());
					e.printStackTrace();
				}
			}
			else if(accion.equals(Constantes.getAccionEliminar())){
				idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
				try {
					resultado = eliminarProgramacion(idProgramacion);
				} catch (SchedulerException e) {
					conexion.actualizarErrorProgramacion(idProgramacion,e.getMessage());
					e.printStackTrace();
				}
				
				
			}
			else if (accion.equals(Constantes.getAccionAbrir())||accion.equals(Constantes.getAccionCerrar())) {
				String[] valvulas;
				//String accionVal;
				String hora;
				String dia;
				String desAccion;
//				GestTramasValvulas gstValvulas = new GestTramasValvulas();
				
				if(request.getParameter("valvulas") != null 
						&& request.getParameter("hora") != null 
						&& request.getParameter("dia") != null 
						&& request.getParameter("accion") != null){
						desAccion = (String) request.getParameter("accion");
						hora  = (String) request.getParameter("hora");
						dia  = (String) request.getParameter("dia");
						valvulas = ((String) request.getParameter("valvulas")).split(",");
				
						resultado += GestTramasValvulas.nuevaProgramacion(valvulas, hora, dia, desAccion);
					}

			}
				

		}
		conexion.cerrar();
		
		//consulto la lista de programaciones posteriores a ahora.
		List<ProgramacionFormateada> lstProgramaciones = cargarListaProgramacionesFuturas();
		//mando la lista a la jsp
		request.setAttribute("lstProgramaciones", lstProgramaciones);
		request.setAttribute("resultado", resultado);
		getServletContext().getRequestDispatcher("/listaValvulas.jsp").forward(request, response);
		
	}

	private String eliminarProgramacion(int idProgramacion) throws SchedulerException {
		Scheduler sched = QuartzTareas.getSched();
		ConexionBBDD conexion = new ConexionBBDD();
		ProgramacionPk progPk = new ProgramacionPk(idProgramacion);	
		ProgramacionDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido eliminar, intentelo nuevamente."; 
		sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"));
		try {
			_dao.delete(progPk);
			resultado =  "Programación eliminada.";
		} catch (ProgramacionDaoException e) {
			conexion.actualizarErrorProgramacion(idProgramacion,e.getMessage());
			log.error(e.getMessage());
		}
		conexion.cerrar();
		log.debug(resultado);
		
		return resultado;
	}

	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionDao getProgramacionDao()
	{
		return ProgramacionDaoFactory.create();
	}

	private String cambiarFechaHoraProgramacion(int idProgramacion, Date fechaHora) throws SchedulerException {
		Scheduler sched = QuartzTareas.getSched();
		ConexionBBDD conexion = new ConexionBBDD();
		Date ahora = new Date();
		ProgramacionPk progPk = new ProgramacionPk(idProgramacion);	
		ProgramacionDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido cambiar, intentelo nuevamente.";
		Programacion dto = new Programacion();
		String dateTime = " el dia " + new SimpleDateFormat("dd/MM/yyyy").format(ahora) 
						     +  " a las " + new SimpleDateFormat("HH:mm").format(ahora);
		try {
			dto = _dao.findByPrimaryKey(progPk);
			dto.setFechaHora(fechaHora);
			_dao.update(progPk, dto);	
			resultado =  "Programación cambiada el dia " + dateTime;

		} catch (ProgramacionDaoException e) {
			conexion.actualizarErrorProgramacion(idProgramacion,e.getMessage());
			log.error(e.getMessage());
		}
		
		JobKey jobKey = new JobKey("job"+idProgramacion, "group2");	
		if (sched.checkExists(jobKey)){
			if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora)){
				log.debug("Actualizando group2.job"+idProgramacion + dateTime);
				Trigger newtrigger1 = newTrigger().withIdentity("trigger"+idProgramacion, "group2").startAt(fechaHora).build();
			    sched.rescheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"), newtrigger1);
			}else{
				log.debug("Borrando group2.job"+idProgramacion);
				sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"));
			}
					
		}
		
		log.debug(resultado);
		conexion.actualizarResultadoProgramacion(idProgramacion,resultado);
		conexion.cerrar();
		
		return resultado;
		
	}

	private List<ProgramacionFormateada> cargarListaProgramacionesFuturas() {
		ConexionBBDD con = new ConexionBBDD();
		List<ProgramacionFormateada> resultado = con.consultarProgramacionesFuturas();
		con.cerrar();
		return resultado;
	}
}
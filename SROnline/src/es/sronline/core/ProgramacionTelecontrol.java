package es.sronline.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.sronline.bbdd.ConexionBBDD;

public class ProgramacionTelecontrol {
	
		private int idprogramacion;

		private int idObjeto;

		private Date fechaHora;

		private int desAccion;

		private String trama;

		private int envio;

	    public ProgramacionTelecontrol() {
	    }

		public int getIdprogramacion() {
			return idprogramacion;
		}

		public void setIdprogramacion(int idprogramacion) {
			this.idprogramacion = idprogramacion;
		}

		public int getIdObjeto() {
			return idObjeto;
		}

		public void setIdObjeto(int idObjeto) {
			this.idObjeto = idObjeto;
		}

		public Date getFechaHora() {
			return fechaHora;
		}

		public void setFechaHora(Date fechaHora) {
			this.fechaHora = fechaHora;
		}

		public int getDesAccion() {
			return desAccion;
		}

		public void setDesAccion(int desAccion) {
			this.desAccion = desAccion;
		}

		public String getTrama() {
			return trama;
		}

		public void setTrama(String trama) {
			this.trama = trama;
		}

		public int getEnvio() {
			return envio;
		}

		public void setEnvio(int envio) {
			this.envio = envio;
		}

		public int guardar() {
			SimpleDateFormat sdf =    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			StringBuffer sb = new StringBuffer("INSERT INTO `regadio`.`programacion_telecontrol`");
			sb.append("`idobj_telecontrol`,");
			sb.append("`fecha_hora`,");
			sb.append("`des_accion`,");
			sb.append("`trama`)");
			sb.append(" VALUES ");
			sb.append("(");
			sb.append(this.getIdObjeto());
			sb.append(",");
			sb.append(sdf.format(this.getFechaHora()));						
			sb.append("',");
			sb.append(this.getDesAccion());			
			sb.append(",'");
			sb.append(this.getTrama());			
			sb.append("');");
			
			ConexionBBDD con = new ConexionBBDD();
			return con.guardarSQL(sb.toString());
			
		}

}

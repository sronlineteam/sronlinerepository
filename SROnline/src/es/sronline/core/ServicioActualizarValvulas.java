package es.sronline.core;

import java.util.TimerTask;

import org.apache.log4j.Logger;

public class ServicioActualizarValvulas extends TimerTask {
	Logger log = Logger.getLogger("es.sronline");
	public void run ( ) {
		log.debug("*********Actualizando valvulas*********");
		try {
			Valvulas v = new Valvulas();
		    v.sincronizarEstadosValvulas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
}

}

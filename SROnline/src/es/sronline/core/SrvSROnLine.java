package es.sronline.core;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class SrvSROnLine
 */
@WebServlet("/SrvSROnLine")
public class SrvSROnLine extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvSROnLine() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Logger log = Logger.getLogger(getClass().getName());
	    String mensaje = "KO";
	    String auxUrl = Constantes.getHostIP();
	    int idProgramador = 1;
	    if (request.getParameter("idProgramador") != null)
	    {
	      idProgramador = new Integer(request.getParameter("idProgramador")).intValue();
	      if (idProgramador > 0)
	      {
	        auxUrl = request.getServerName();
	        if (request.getParameter("IP_Programador") != null)
	        {
	          log.debug("recojo la url del parametro ");
	          auxUrl = request.getParameter("IP_Programador");
	        }
	        log.debug("Peticion de: " + auxUrl);
	        Constantes.setHostIP(auxUrl);
	        mensaje = "OK";
	        log.debug("Establecido host a: " + Constantes.getHostIP());
	      }
	    }
	    PrintWriter out = response.getWriter();
	    out.println(mensaje);
	    out.println(auxUrl);
	}

}

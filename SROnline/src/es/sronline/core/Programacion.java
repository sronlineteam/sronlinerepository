package es.sronline.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.sronline.bbdd.ConexionBBDD;

public class Programacion {
	
		private int idprogramacion;

		private int idRemota;

		private int idValvula;

		private Date fechaHora;

		private int desAccion;

		private String trama;

	    public Programacion() {
	    }

		public int getIdprogramacion() {
			return this.idprogramacion;
		}

		public void setIdprogramacion(int idprogramacion) {
			this.idprogramacion = idprogramacion;
		}

		public int getDesAccion() {
			return this.desAccion;
		}

		public void setDesAccion(int desAccion) {
			this.desAccion = desAccion;
		}

		public Date getFechaHora() {
			return this.fechaHora;
		}

		public void setFechaHora(Date fechaHora) {
			this.fechaHora = fechaHora;
		}

		public int getIdRemota() {
			return this.idRemota;
		}

		public void setIdRemota(int idRemota) {
			this.idRemota = idRemota;
		}

		public int getIdValvula() {
			return this.idValvula;
		}

		public void setIdValvula(int idValvula) {
			this.idValvula = idValvula;
		}

		public String getTrama() {
			return this.trama;
		}

		public void setTrama(String trama) {
			this.trama = trama;
		}

		public int guardar() {
			SimpleDateFormat sdf =    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			StringBuffer sb = new StringBuffer("INSERT INTO `regadio`.`programacion`");
			sb.append("(`id_remota`,");
			sb.append("`id_valvula`,");
			sb.append("`fecha_hora`,");
			sb.append("`des_accion`,");
			sb.append("`trama`)");
			sb.append(" VALUES ");
			sb.append("(");
			sb.append(this.getIdRemota());
			sb.append(",");
			sb.append(this.getIdValvula());
			sb.append(",'");
			sb.append(sdf.format(this.getFechaHora()));			
			sb.append("',");
			sb.append(this.getDesAccion());			
			sb.append(",'");
			sb.append(this.getTrama());			
			sb.append("');");
			
			ConexionBBDD con = new ConexionBBDD();
			return con.guardarSQL(sb.toString());
			
		}

}

package es.sronline.core;

import java.util.Calendar;

import org.apache.log4j.Logger;

/**
 * @author j.rodriguezg
 *
 */
public class Common {
	
	
	/**
	 * A�ade bit a la trama.
	 * @param trama
	 * @return "02" + request + "03" + LRCString
	 */
	public static String appendSTX_ETX_LRC(String trama) {
		String LRCString = calculateLRC(trama + "03");
		Logger log = Logger.getLogger("es.sronline");
		log.debug(LRCString);
		return "02" + trama + "03" + LRCString;
	}

	/**
	 * A�ade por detras los bites de control en base a la trama y algoritmo CRC
	 * @param trama
	 * @return trama con codigo CRC de 16 bites ( 8 + 8 )
	 */
	public static String appendCRC16(String trama) {
		int auxCRC16;
		String auxStringCRC;
		
		auxCRC16 = CRC16.getCRC16(trama, trama.length());
		auxStringCRC = Long.toHexString(auxCRC16).toUpperCase();
		auxStringCRC = GestTramasValvulas.completaConCeros(auxStringCRC, 4);
		
		return trama + auxStringCRC.substring(2, 4)
				+ auxStringCRC.substring(0, 2);

	}


	public static String calculateLRC(String inputString) {
		/*
		 * String is Hex String, need to convert in ASCII.
		 */
		inputString = convertHexToString(inputString);
		int checksum = 0;
		char[] chars = inputString.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			checksum ^= chars[i];
		}
		String val = Integer.toHexString(checksum);
		if (val.length() < 2) {
			val = "0" + val;
		}

		return val;
	}

	/**
	 * convierte un Hexadecimal en String
	 * @param hex
	 * @return El String del Hex
	 */
	public static String convertHexToString(String hex) {

		StringBuilder sb = new StringBuilder();
		StringBuilder temp = new StringBuilder();
		// 49204c6f7665204a617661 split into two characters 49, 20, 4c...
		for (int i = 0; i < hex.length() - 1; i += 2) {
			// grab the hex in pairs
			String output = hex.substring(i, (i + 2));
			// convert hex to decimal
			int decimal = Integer.parseInt(output, 16);
			sb.append((char) decimal);
			// }
			temp.append(decimal);
		}

		return sb.toString();
	}

	public static String asciiToHex(String ascii) {
		StringBuilder hex = new StringBuilder();

		for (int i = 0; i < ascii.length(); i++) {
			hex.append(Integer.toHexString(ascii.charAt(i)));
		}
		return hex.toString();
	}

	/*
	 * This method will check if LRC received is ok.
	 */
	public static boolean verify(String response) {
		Logger log = Logger.getLogger("es.sronline");
		try {
			String newResponse = convertHexToString(response);
			int lrc = getLRCValue(newResponse);
			response = getLRCString(response);
			log.debug(response);
			return verifyLrc(response, lrc);
		} catch (Exception e) {
			log.error("!!!ERROR!!! -Exception while calcualting LRC");
			log.error(e.getMessage());
			return false;
		}
	}

	/**
	 * Devuelve un Calendar de un string fecha hora con formato DD-MM-YYYY HH:YY
	 * @param dia
	 * @param hora
	 * @return la fecha en Calendar
	 */
	public static Calendar getFechaHoraProgramacion(String dia, String hora) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		String[] auxHora;
		String[] auxfecha = null;
		auxHora = hora.split(":");
		if(dia.contains("-"))
			auxfecha = dia.split("-");
		else if(dia.contains("/"))
			auxfecha = dia.split("/");
		int auxYear = Integer.parseInt(auxfecha[2]);
		if (auxYear < 2000)
			auxYear += 2000;
	
		/*
		 * cal.set(year, month, date, hourOfDay, minute, second);
		 */
	
		cal.set(auxYear, Integer.parseInt(auxfecha[1]) - 1,
				Integer.parseInt(auxfecha[0]), Integer.parseInt(auxHora[0]),
				Integer.parseInt(auxHora[1]), 0);
	
		return cal;
	}

	private static int getLRCValue(String response) {
		byte[] bytes = response.getBytes();
		int tmp = -1;
		boolean flag = false;
		for (byte b : bytes) {
			if (b == 3) {
				flag = true;
				continue;
			}
			if (flag) {
				tmp = b;
				break;
			}
		}
		return tmp;
	}

	private static String getLRCString(String response) throws Exception {
		if (response.length() % 2 != 0) {
			System.err
					.println("Response MSG doesn't have even number of chars");
			throw new IllegalArgumentException();
		}
		boolean exit = false;
		boolean start = false;
		String LRCStr = "";
		for (int i = 0; i < response.length() - 1; i += 2) {
			// grab the hex in pairs
			String output = response.substring(i, (i + 2));
			if (output.equals("03")) {
				LRCStr = LRCStr + output;
				exit = true;
			} else if (output.equals("02")) {
				start = true;
				continue;
			}

			if (!exit) {
				if (start) {
					LRCStr = LRCStr + output;
				}
			} else {
				break;
			}
		}
		return LRCStr;
	}

	private static boolean verifyLrc(String resp, int lrcVal) {
		Logger log = Logger.getLogger("es.sronline");
		if (resp == null || resp.length() == 0)
			return false;
		// int lrc_encountered = calculate1byteLRC(resp);
		int lrc_encountered = Integer.parseInt(calculateLRC(resp), 16);

		if (lrcVal != lrc_encountered) {
			log.error("Mismatched LRC (expected [" + lrc_encountered
					+ "], encountered [" + lrcVal + "])");
			return false;
		} else {
			log.info("LRC Matched (expected [" + lrc_encountered
					+ "], encountered [" + lrcVal + "])");
			return true;
		}

	}

}

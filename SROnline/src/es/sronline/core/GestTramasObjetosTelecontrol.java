package es.sronline.core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ProgramacionTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.ProgramacionTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.ProgramacionTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ProgramacionTelecontrolDaoFactory;
import es.sronline.servicio.ServicioHoraPlanificada;
import es.sronline.tareas.QuartzTareas;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;
import es.sronline.telecontrol.Telecontrol;


/**
 * @author j.rodriguezg
 * Gestión de objetos de telecontrol, prepara las tramas para su utilización
 */
public class GestTramasObjetosTelecontrol {
	
	
	Logger log;
	
	static int idProgracionTelecontrol;

	public GestTramasObjetosTelecontrol() {
		log = Logger.getLogger(getClass().getName());
	}	
	
	public static String nuevaProgramacion(String[] objetosTelecontrol, String hora, String diaInicio, String diaFin, String accionObj) {
			return nuevaProgramacion(objetosTelecontrol, hora, diaInicio, diaFin, accionObj,  ControlTarifaElectrica.CONSULTAR);}

	public static String nuevaProgramacion(String[] objetosTelecontrol, String hora, String diaInicio, String diaFin, String accionObj, int confirmacion) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfHora = new SimpleDateFormat("hh:mm:ss");
		Date ahora = new Date();
		String[] auxObjeto;
		int accion = -1;
		Date fechaHoraInicio = new Date();
		Date fechaHoraFin = new Date();
		Calendar calInicio = Calendar.getInstance();		
		calInicio = Common.getFechaHoraProgramacion(diaInicio, hora);
    	fechaHoraInicio.setTime(calInicio.getTimeInMillis());
		Calendar calFin = Calendar.getInstance();
		//Si no hay diaFin entonces el d�a de inicio y el d�a de fin es el mismo d�a
		if (diaFin!=null && diaFin.trim().length()==10){
			calFin = Common.getFechaHoraProgramacion(diaFin, hora);
		}else{
			calFin = Common.getFechaHoraProgramacion(diaInicio, hora);
		}
    	fechaHoraFin.setTime(calFin.getTimeInMillis());
		ProgramacionTelecontrol programacion;
    	String resultado = "";
    	
		if(accionObj.equalsIgnoreCase(Constantes.getAccionAbrir())){
			accion = Telecontrol.ABRIR;
			accionObj = Constantes.getManualAbierto();
		}
		else if (accionObj.equalsIgnoreCase(Constantes.getAccionCerrar())){
			accion = Telecontrol.CERRAR;
			accionObj = Constantes.getManualCerrado();
		}
		
		for (int i=0; i<objetosTelecontrol.length; i++){
			Date fechaHora = new Date(fechaHoraInicio.getTime());
			// Incluimos un registro por cada d�a 
			while(fechaHora.getTime()<=fechaHoraFin.getTime()){
				boolean encontrado = false;
				if(accionObj.equalsIgnoreCase(Constantes.getAccionAbrir())){
					if(Constantes.isControlTarifaElectrica()){
						int decision = ControlTarifaElectrica.controlar(fechaHoraInicio);
						switch (decision) {
						case ControlTarifaElectrica.NO_PERMITIR:
							resultado += ControlTarifaElectrica.AVISO_NO_PERMITIR  + "</br>";
							encontrado = true;
						case ControlTarifaElectrica.CONSULTAR:
							if(confirmacion == ControlTarifaElectrica.CONSULTAR){
								resultado += ControlTarifaElectrica.AVISO_CONSULTAR  + "</br>";
								encontrado = true;
							}else if(confirmacion == ControlTarifaElectrica.NO_PERMITIR){
								resultado += ControlTarifaElectrica.AVISO_NO_PERMITIR  + "</br>";
								encontrado = true;
							}
							break;				
					default:
							break;
						}								
					}
				}
				if (!encontrado){
					programacion = new ProgramacionTelecontrol();
					programacion.setIdObjeto(Integer.parseInt(objetosTelecontrol[i]));
					programacion.setFechaHora(fechaHora);
					programacion.setDesAccion(accion);
				
				
					ObjetosTelecontrol objetoTC;
					ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
					try {
						objetoTC = _dao.findByPrimaryKey(programacion.getIdObjeto());
					
						Telecontrol telecontrol = new Telecontrol(objetoTC);
	
						programacion.setTrama(telecontrol.getTrama(accion));
						resultado += "" + guardar(programacion);
						resultado += "Se ha guardado la programaci�n el " + sdf.format(ahora) + " a las " + sdfHora.format(ahora) + "</br>";

					} catch (ObjetosTelecontrolDaoException e) {
						// TODO Auto-generated catch block
						Logger.getLogger("es.sronline.core.GestTramasObjetosTelecontrol").error(e.getMessage());
					}
					
					ConexionBBDD conexion = new ConexionBBDD();
					conexion.actualizarResultadoObjeto(idProgracionTelecontrol, "Se ha guardado la programaci�n el " + sdf.format(ahora) + " a las " + sdfHora.format(ahora));
					
					if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora) && fechaHora.compareTo(new Date())>0){
						try {
							programacion.setIdprogramacion(idProgracionTelecontrol);
							QuartzTareas.programacionObjTelecontrol(programacion);
							conexion.actualizarPlanificadoObjeto(idProgracionTelecontrol,Constantes.PLANIFICADO);
						} catch (Exception e) {
							conexion.actualizarErrorObjeto(idProgracionTelecontrol, e.getMessage());
							e.printStackTrace();
						}
					}
					conexion.cerrar();
					fechaHora = GestTramasObjetosTelecontrol.sumarDiasFecha(fechaHora, 1);
						
				}
			}
		}


		return resultado;
	}
	
	public static ArrayList<String> nuevaProgramacionPlanificacion(String[] objetosTelecontrol, String hora, String diaInicio, String diaFin, String accionObj, String[] diasSemana) {
		return nuevaProgramacionPlanificacion(objetosTelecontrol, hora, diaInicio, diaFin, accionObj, diasSemana, ControlTarifaElectrica.CONSULTAR);}
	
	@SuppressWarnings("null")
	public static ArrayList<String> nuevaProgramacionPlanificacion(String[] objetosTelecontrol, String hora, String diaInicio, String diaFin, String accionObj, String[] diasSemana,int confirmacion) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfHora = new SimpleDateFormat("hh:mm:ss");
		Date ahora = new Date();
		String[] auxObjeto;
		ArrayList<String> idsGuardados = new ArrayList<String>();
		int accion = -1;
		Date fechaHoraInicio = new Date();
		Date fechaHoraFin = new Date();

		String[] auxdiaInicio = diaInicio.split("-");
		diaInicio = auxdiaInicio[2] + "-" + auxdiaInicio[1] + "-" + auxdiaInicio[0];  
		String[] auxdiaFin = diaFin.split("-");
		diaFin = auxdiaFin[2] + "-" + auxdiaFin[1] + "-" + auxdiaFin[0];
		
		Calendar calInicio = Calendar.getInstance();		
		calInicio = Common.getFechaHoraProgramacion(diaInicio, hora);
    	fechaHoraInicio.setTime(calInicio.getTimeInMillis());
		Calendar calFin = Calendar.getInstance();
		//Si no hay diaFin entonces el d�a de inicio y el d�a de fin es el mismo d�a
		if (diaFin!=null && diaFin.trim().length()==10){
			
			calFin = Common.getFechaHoraProgramacion(diaFin, hora);
		}else{
			calFin = Common.getFechaHoraProgramacion(diaInicio, hora);
		}
    	fechaHoraFin.setTime(calFin.getTimeInMillis());
		ProgramacionTelecontrol programacion;
    	String resultado = "";
    	
		if(accionObj.equalsIgnoreCase("1")){
			accion = Telecontrol.ABRIR;
			accionObj = Constantes.getManualAbierto();
		}
		else if (accionObj.equalsIgnoreCase("2")){
			accion = Telecontrol.CERRAR;
			accionObj = Constantes.getManualCerrado();
		}
		
		for (int i=0; i<objetosTelecontrol.length; i++){
			Date fechaHora = new Date(fechaHoraInicio.getTime());
			// Incluimos un registro por cada d�a 
			while(fechaHora.getTime()<=fechaHoraFin.getTime()){
				String diaSemana = Integer.toString(fechaHora.getDay());
				for(int l=0; l<diasSemana.length; l++){
					if (diaSemana.equals(diasSemana[l])){
						boolean encontrado = false;
						if(accionObj.equalsIgnoreCase(Constantes.getAccionAbrir())){
							if(Constantes.isControlTarifaElectrica()){
								int decision = ControlTarifaElectrica.controlar(fechaHoraInicio);
								switch (decision) {
								case ControlTarifaElectrica.NO_PERMITIR:
									resultado += ControlTarifaElectrica.AVISO_NO_PERMITIR  + "</br>";
									encontrado = true;
								case ControlTarifaElectrica.CONSULTAR:
									if(confirmacion == ControlTarifaElectrica.CONSULTAR){
										resultado += ControlTarifaElectrica.AVISO_CONSULTAR  + "</br>";
										encontrado = true;
									}else if(confirmacion == ControlTarifaElectrica.NO_PERMITIR){
										resultado += ControlTarifaElectrica.AVISO_NO_PERMITIR  + "</br>";
										encontrado = true;
									}
									break;				
							default:
									break;
								}								
							}
						}
						if (!encontrado){
							programacion = new ProgramacionTelecontrol();
							programacion.setIdObjeto(Integer.parseInt(objetosTelecontrol[i]));
							programacion.setFechaHora(fechaHora);
							programacion.setDesAccion(accion);
						
						
							ObjetosTelecontrol objetoTC;
							ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
//							ObjTelecontrol objTC = null;
							try {
								objetoTC = _dao.findByPrimaryKey(programacion.getIdObjeto());
//								objTC = new ObjTelecontrol();
//								objTC.setNombre(objetoTC.getNombre());
//								objTC.setIdObjeto(objetoTC.getIdObjeto()+"");
//								objTC.setEstado(objetoTC.getEstado());
//								objTC.setPosicion(objetoTC.getPosicion());
//								objTC.setDireccionHex(objetoTC.getDireccionHex());
//								objTC.setTipoConexion(objetoTC.getTipoConexion());
//								objTC.setDatosConexion(objetoTC.getConexion());
							
							Telecontrol telecontrol = new Telecontrol(objetoTC);
		
							programacion.setTrama(telecontrol.getTrama(accion));
							resultado += "" + guardar(programacion);
							idsGuardados.add(String.valueOf(idProgracionTelecontrol));
							resultado += "Se ha guardado la programaci�n el " + sdf.format(ahora) + " a las " + sdfHora.format(ahora)+ "</br>";

							} catch (ObjetosTelecontrolDaoException e) {
								// TODO Auto-generated catch block
								Logger.getLogger("eu.sronline").error(e.getMessage());
							}
							
							ConexionBBDD conexion = new ConexionBBDD();
							conexion.actualizarResultadoObjeto(idProgracionTelecontrol, "Se ha guardado la programaci�n el " + sdf.format(ahora) + " a las " + sdfHora.format(ahora));
							
							if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora) && fechaHora.compareTo(new Date())>0){
								try {
									programacion.setIdprogramacion(idProgracionTelecontrol);
									QuartzTareas.programacionObjTelecontrol(programacion);
									conexion.actualizarPlanificadoObjeto(idProgracionTelecontrol,Constantes.PLANIFICADO);
								} catch (Exception e) {
									conexion.actualizarErrorObjeto(idProgracionTelecontrol, e.getMessage());
									e.printStackTrace();
								}
							}
							conexion.cerrar();
									
						}
					}
				}
				fechaHora = GestTramasObjetosTelecontrol.sumarDiasFecha(fechaHora, 1);

			}
		}


		return idsGuardados;
	}
	
	static String guardar(ProgramacionTelecontrol programacion) {
		ProgramacionTelecontrolDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido guardar, intentelo nuevamente.";
		try {
			_dao.insert(programacion);
			resultado =  "Programaci�n guardada.";
			idProgracionTelecontrol = programacion.getIdprogramacion();
		} catch (ProgramacionTelecontrolDaoException e) {
			Logger.getLogger("es.sronline.core.GestTramasObjetosTelecontrol").error(e.getMessage());
		}
		
		return resultado;
		
	}
	
	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	private static ProgramacionTelecontrolDao getProgramacionDao()
	{
		return ProgramacionTelecontrolDaoFactory.create();
	}
	
	public static Date sumarDiasFecha(Date fecha, int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
	}
	
	
}

package es.sronline.core;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ProgramacionTelecontrolDao;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.ProgramacionTelecontrol;
import es.sronline.bbdd.dto.ProgramacionTelecontrolPk;
import es.sronline.bbdd.exceptions.ProgramacionTelecontrolDaoException;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ProgramacionTelecontrolDaoFactory;
import es.sronline.telecontrol.Telecontrol;

/**
 * Servlet implementation class SrvPlanificacion
 */
@WebServlet("/SrvPlanificacion")
public class SrvPlanificacion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SrvPlanificacion() {
		super();
		log = Logger.getLogger(getClass().getName());
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("null")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String accion = null;
		String resultado = "";
		String paginaDestino = null;
		ConexionBBDD con = new ConexionBBDD();
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();
		
		log.debug("*********recibida petici�n de " + usuario + " en SrvPlanificacion*********");

		LstObjetosTelecontrol[] lstObjTelecontrol = null;
		try {
			LstObjetosTelecontrolDao _dao = LstObjetosTelecontrolDaoFactory.create();
			lstObjTelecontrol = _dao.findAll(usuario);

		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("objetosTelecontrol", lstObjTelecontrol);

		
		if (request.getParameter("accion") == null) {
			// consulto la lista de planificaciones posteriores a ahora.
			List<ProgramacionPlanificaciones> lstPlanificaciones = cargarPlanificacionesTelecontrolFuturas(usuario);
			// mando la lista a la jsp
			request.setAttribute("lstPlanificaciones", lstPlanificaciones);
			paginaDestino = "/listaPlanificaciones.jsp";
		} else {
			accion = request.getParameter("accion");
			if (accion.equals(Constantes.getAccionEliminar())) {
				try {
					con.eliminarPlanificacion(Integer.parseInt(request.getParameter("idPlanificacion")));
				} catch (NumberFormatException | SchedulerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<ProgramacionPlanificaciones> lstPlanificaciones = cargarPlanificacionesTelecontrolFuturas(usuario);
				request.setAttribute("lstPlanificaciones", lstPlanificaciones);
				paginaDestino = "/listaPlanificaciones.jsp";
			} else if (accion.equals(Constantes.getAccionNuevaPlanificacion())) {

				String F_inicio = request.getParameter("diaInicio");
				String F_fin = request.getParameter("fechaFinal");
				String H_inicio = request.getParameter("horaInicio");
				String H_fin = request.getParameter("horaFinal");

				ProgramacionPlanificaciones planificacion = new ProgramacionPlanificaciones();
				planificacion.setNombre(request.getParameter("nombrePlanificacion"));
				planificacion.setF_inicio(F_inicio);
				planificacion.setF_fin(F_fin);
				planificacion.setH_inicio(H_inicio);
				planificacion.setH_fin(H_fin);
				planificacion.SetArrayDiasSemana(request.getParameter("diasSemana"));
				planificacion.SetArrayIdObjetosTelecontrol(request.getParameter("idObjetosTelecontrol"));

				String[] idObjetosTelecontrol = ((String) request.getParameter("idObjetosTelecontrol")).split(",");
				String[] ArraydiasSemana = ((String) request.getParameter("diasSemana")).split(",");
				String[] ArraydiasSemana_cerrar = new String[ArraydiasSemana.length];
				
				Date Fecha_inicio_cerrar = null;
				Date Fecha_fin_cerrar = null;
				ArrayList<Date> fechasCerrar = new ArrayList<Date>();
				try {
					fechasCerrar = getFechaCerrar(H_inicio, H_fin, F_inicio, F_fin);				
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String F_inicio_cerrar = new String();
				String F_fin_cerrar = new String();
				if (fechasCerrar!=null) {
					Fecha_inicio_cerrar = fechasCerrar.get(0);
					Fecha_fin_cerrar = fechasCerrar.get(1);
					F_inicio_cerrar = new SimpleDateFormat("yyyy-mm-dd").format(Fecha_inicio_cerrar);
					F_fin_cerrar = new SimpleDateFormat("yyyy-mm-dd").format(Fecha_fin_cerrar);
					
					for (int i = 0; i < ArraydiasSemana.length; i++) {
						ArraydiasSemana_cerrar[i] = String.valueOf(Integer.parseInt(ArraydiasSemana[i]) + 1);
						if (Integer.parseInt(ArraydiasSemana_cerrar[i]) == 7) {
							ArraydiasSemana_cerrar[i] = "0";
						}
					}
				} else {
					F_inicio_cerrar = F_inicio;
					F_fin_cerrar = F_fin;
					ArraydiasSemana_cerrar = ArraydiasSemana;
				}
							
				ArrayList<String> idsAbrir = GestTramasObjetosTelecontrol.nuevaProgramacionPlanificacion(
						idObjetosTelecontrol, H_inicio, F_inicio, F_fin, String.valueOf(Telecontrol.ABRIR),
						ArraydiasSemana);
				ArrayList<String> idsCerrar = GestTramasObjetosTelecontrol.nuevaProgramacionPlanificacion(
						idObjetosTelecontrol, H_fin, F_inicio_cerrar, F_fin_cerrar, String.valueOf(Telecontrol.CERRAR),
						ArraydiasSemana_cerrar);

				ArrayList<String> allIds = new ArrayList<String>();
				allIds.addAll(idsAbrir);
				allIds.addAll(idsCerrar);

				String ArrayidProgramacionesTelecontrol = "";

				for (String s : allIds) {
					ArrayidProgramacionesTelecontrol += s + ",";
				}
				planificacion.SetArrayIdProgramacionesTelecontrol(ArrayidProgramacionesTelecontrol);

				con.insertarPlanificacion(planificacion);

				List<ProgramacionPlanificaciones> lstPlanificaciones = cargarPlanificacionesTelecontrolFuturas(usuario);
				request.setAttribute("lstPlanificaciones", lstPlanificaciones);

				paginaDestino = "/listaPlanificaciones.jsp";
			} else if (accion.equals(Constantes.getAccionCambiar())) {

				String F_inicio = request.getParameter("diaInicio");
				String F_fin = request.getParameter("fechaFinal");
				String H_inicio = request.getParameter("horaInicio");
				String H_fin = request.getParameter("horaFinal");
				int idPlanificacion = Integer.parseInt(request.getParameter("idPlanificacion"));

				ProgramacionPlanificaciones actualizar = new ProgramacionPlanificaciones();
				actualizar.setIdPlanificacion(idPlanificacion);
				actualizar.setNombre(request.getParameter("nombrePlanificacion"));
				actualizar.setF_inicio(F_inicio);
				actualizar.setF_fin(F_fin);
				actualizar.setH_inicio(H_inicio);
				actualizar.setH_fin(H_fin);
				actualizar.SetArrayDiasSemana(request.getParameter("diasSemana"));
				actualizar.SetArrayIdObjetosTelecontrol(request.getParameter("idObjetosTelecontrol"));

				String[] idObjetosTelecontrol = ((String) request.getParameter("idObjetosTelecontrol")).split(",");
				String[] ArraydiasSemana = ((String) request.getParameter("diasSemana")).split(",");
				String[] ArraydiasSemana_cerrar = new String[ArraydiasSemana.length];

				Date Fecha_inicio_cerrar = null;
				Date Fecha_fin_cerrar = null;
				ArrayList<Date> fechasCerrar = new ArrayList<Date>();
				try {
					fechasCerrar = getFechaCerrar(H_inicio, H_fin, F_inicio, F_fin);				
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String F_inicio_cerrar = new String();
				String F_fin_cerrar = new String();
				if (fechasCerrar!=null) {
					Fecha_inicio_cerrar = fechasCerrar.get(0);
					Fecha_fin_cerrar = fechasCerrar.get(1);
					F_inicio_cerrar = new SimpleDateFormat("yyyy-mm-dd").format(Fecha_inicio_cerrar);
					F_fin_cerrar = new SimpleDateFormat("yyyy-mm-dd").format(Fecha_fin_cerrar);
					
					for (int i = 0; i < ArraydiasSemana.length; i++) {
						ArraydiasSemana_cerrar[i] = String.valueOf(Integer.parseInt(ArraydiasSemana[i]) + 1);
						if (Integer.parseInt(ArraydiasSemana_cerrar[i]) == 7) {
							ArraydiasSemana_cerrar[i] = "0";
						}
					}
				} else {
					F_inicio_cerrar = F_inicio;
					F_fin_cerrar = F_fin;
					ArraydiasSemana_cerrar = ArraydiasSemana;
				}
				
				ArrayList<String> idsAbrir = GestTramasObjetosTelecontrol.nuevaProgramacionPlanificacion(
						idObjetosTelecontrol, H_inicio, F_inicio, F_fin, String.valueOf(Telecontrol.ABRIR),
						ArraydiasSemana, idPlanificacion);
				ArrayList<String> idsCerrar = GestTramasObjetosTelecontrol.nuevaProgramacionPlanificacion(
						idObjetosTelecontrol, H_fin, F_inicio_cerrar, F_fin_cerrar, String.valueOf(Telecontrol.CERRAR),
						ArraydiasSemana_cerrar, idPlanificacion);

				ArrayList<String> allIds = new ArrayList<String>();
				allIds.addAll(idsAbrir);
				allIds.addAll(idsCerrar);

				String ArrayidProgramacionesTelecontrol = "";

				for (String s : allIds) {
					ArrayidProgramacionesTelecontrol += s + ",";
				}

				actualizar.SetArrayIdProgramacionesTelecontrol(ArrayidProgramacionesTelecontrol);

				try {
					con.actualizarPlanificacion(actualizar);
				} catch (SchedulerException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				List<ProgramacionPlanificaciones> lstPlanificaciones = cargarPlanificacionesTelecontrolFuturas(usuario);
				request.setAttribute("lstPlanificaciones", lstPlanificaciones);

				paginaDestino = "/listaPlanificaciones.jsp";
			}
		}

		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

	}
	
	private ArrayList<Date> getFechaCerrar(String H_inicio, String H_fin,String F_inicio, String F_fin ) throws ParseException{
		
		Date Hora_inicio = new Date();
		Date Hora_fin = new Date();
		SimpleDateFormat horaFormat = new SimpleDateFormat("HH:mm");
		Date Fecha_inicio_cerrar = new Date();
		Date Fecha_fin_cerrar = new Date();
		SimpleDateFormat fechaFormat = new SimpleDateFormat("yyyy-mm-dd");

		Hora_inicio = horaFormat.parse(H_inicio);
		Hora_fin = horaFormat.parse(H_fin);
		Fecha_inicio_cerrar = fechaFormat.parse(F_inicio);
		Fecha_fin_cerrar = fechaFormat.parse(F_fin);
		ArrayList<Date> fechasCerrar = new ArrayList<Date>();
		if (Hora_inicio.compareTo(Hora_fin) > 0) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(Fecha_inicio_cerrar);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			fechasCerrar.add(calendar.getTime());
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(Fecha_fin_cerrar);
			calendar2.add(Calendar.DAY_OF_YEAR, 1);
			fechasCerrar.add(calendar2.getTime());
			
			return fechasCerrar;
		}
		
		return null;	
	}
	
	private List<ProgramacionPlanificaciones> cargarPlanificacionesTelecontrolFuturas(String username) {
		ConexionBBDD con = new ConexionBBDD();
		List<ProgramacionPlanificaciones> resultado = con.consultarPlanificacionesTelecontrolFuturas(username);
		return resultado;

	}

}

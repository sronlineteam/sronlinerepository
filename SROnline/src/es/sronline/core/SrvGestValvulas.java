package es.sronline.core;

import java.io.IOException;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;
import es.sronline.bbdd.dao.LstValvulasDao;
import es.sronline.bbdd.dto.LstValvulas;
import es.sronline.bbdd.factory.LstValvulasDaoFactory;

/**
 * Servlet implementation class SrvGestValvulas
 */
@WebServlet("/SrvGestValvulas")
public class SrvGestValvulas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestValvulas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger(getClass().getName());
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();
		
		log.debug("*********recibida peticion de " + usuario + " en SrvGestValvulas*********");
		
		LstValvulas[] lstValvulas = null;
		int idProgramador = 1;
		String resultado;
		if (request.getAttribute("resultado")!=null){
			resultado = (String) request.getAttribute("resultado");	
		}else{
			resultado = "";
		}
		String dialogo;
		if (request.getAttribute("dialogo")!=null){
			dialogo = (String) request.getAttribute("dialogo");	
		}else{
			dialogo = Constantes.DIALOGO_VACIO;
		}
		String enSincronizacion = "---";
		if ((request.getSession().getAttribute("sincronizado") == null) && (Constantes.isSincronizado()))
		{
			Timer timer = new Timer ( ) ;
			timer.schedule ( new ServicioActualizarValvulas() , 1) ;
			resultado = "Sinconizando estado de las valvulas...";
			enSincronizacion = "true";
		}
		 
		if(request.getParameter("idProgramador") != null ){
			idProgramador = new Integer((String) request.getParameter("idProgramador"));
		}

		
		try {
			
			LstValvulasDao _dao = LstValvulasDaoFactory.create();
			lstValvulas = _dao.findWhereIdProgramadorEquals(idProgramador);
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("lstValvulas", lstValvulas);
		request.getSession().setAttribute("sincronizado", Boolean.valueOf(true));
		request.setAttribute("resultado", resultado);
		request.setAttribute("dialogo", dialogo);
		request.setAttribute("enSincronizacion", enSincronizacion);
		
		getServletContext().getRequestDispatcher("/gestionValvulas.jsp").forward(request, response);

	}

}

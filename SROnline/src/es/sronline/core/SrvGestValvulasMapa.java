package es.sronline.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.LstValvulasDao;
import es.sronline.bbdd.dto.LstValvulas;
import es.sronline.bbdd.factory.LstValvulasDaoFactory;

/**
 * Servlet implementation class SrvGestValvulas
 */
@WebServlet("/SrvGestValvulasMapa")
public class SrvGestValvulasMapa extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestValvulasMapa() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger(getClass().getName());
		log.debug("*********recibida peticion en SrvGestValvulasMapa*********");

		LstValvulas[] lstValvulas = null;
		int idProgramador = 1;
		if(request.getParameter("idProgramador") != null ){
			idProgramador = new Integer((String) request.getParameter("idProgramador"));
		}

		
		try {
			
			LstValvulasDao _dao = LstValvulasDaoFactory.create();
			lstValvulas = _dao.findWhereIdProgramadorEquals(idProgramador);
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("lstValvulas", lstValvulas);
//		request.setAttribute("resultado", resultado);
		getServletContext().getRequestDispatcher("/gestionValvulasMapa.jsp").forward(request, response);

	}

}

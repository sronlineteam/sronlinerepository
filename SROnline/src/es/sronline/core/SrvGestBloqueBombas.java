package es.sronline.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.ProgramacionDao;
import es.sronline.bbdd.factory.ProgramacionDaoFactory;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;


/**
 * Servlet implementation class SrvGestBloqueBombas
 */
@WebServlet("/SrvGestBloqueBombas")
public class SrvGestBloqueBombas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestBloqueBombas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger("es.sronline");
		//Programacion programacion = new Programacion();
    	String resultado = "";
		String dialogo = Constantes.DIALOGO_VACIO;
    	
		log.debug("*********recibida petición en SrvGestBloqueBombas*********");
		//boolean completado = false;
		//String mensaje = "No se ha podido completar la petición, intentelo nuevamente.";
		String[] valvulas;
		String desAccion;
		String hora;
		String dia;
//		GestTramasValvulas gstValvulas = new GestTramasValvulas();
		
		if(request.getParameter("valvulas") != null 
			&& request.getParameter("hora") != null 
			&& request.getParameter("dia") != null 
			&& request.getParameter("accion") != null){
				
//				int confirmacion = ControlTarifaElectrica.PERMITIR;
				desAccion = (String) request.getParameter("accion");
				hora  = (String) request.getParameter("hora");
				dia  = (String) request.getParameter("dia");
				valvulas = ((String) request.getParameter("valvulas")).split(",");
				
				if(Constantes.isControlTarifaElectrica()){
					String auxConfirmacion ="";
					boolean bAbrir = false;
					if(request.getParameter("confirmacion") != null){
						auxConfirmacion = (String) request.getParameter("confirmacion");
					}
					if(Constantes.CONFIRMADA.equals(auxConfirmacion))
						bAbrir = true;
					else if(Constantes.RECHAZADA.equals(auxConfirmacion))
						bAbrir = false;
					else{
						resultado += GestTramasValvulas.nuevaProgramacion(valvulas, hora, dia, desAccion, ControlTarifaElectrica.CONSULTAR);
						if(resultado.contains(ControlTarifaElectrica.AVISO_CONSULTAR)){
							request.setAttribute("confirmacion", "1,1,'si'");
							dialogo = Constantes.DIALOGO_CONFIRMAR;
						}
							
					}
					if(bAbrir)
						resultado += GestTramasValvulas.nuevaProgramacion(valvulas, hora, dia, desAccion, ControlTarifaElectrica.PERMITIR);
				}
				else
					resultado += GestTramasValvulas.nuevaProgramacion(valvulas, hora, dia, desAccion);
			}

		response.setContentType("text/html");

		request.setAttribute("resultado", resultado);
		request.setAttribute("dialogo", dialogo);

		getServletContext().getRequestDispatcher("/SrvGestValvulas").forward(request, response);
		
		}

//	private String guardar(es.sronline.bbdd.dto.Programacion programacion) {
//		ProgramacionDao _dao = getProgramacionDao();
//		String resultado =  "No se ha podido guardar, intentelo nuevamente.";
//		try {
//			_dao.insert(programacion);
//			resultado =  "Programación guardada.";
//		} catch (ProgramacionDaoException e) {
//			log.error(e.getMessage());
//		}
//		
//		return resultado;
//		
//	}
	
	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionDao getProgramacionDao()
	{
		return ProgramacionDaoFactory.create();
	}


}

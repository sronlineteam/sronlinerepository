package es.sronline.core;

public class ProgramacionTelecontrolFormateada extends ProgramacionTelecontrol {
	private String fechaHoraFormateada;
	private String accionFormateada;
	private String nombreObjeto;
	private String envioFormateada;
	private String resultado;
	  
	
	public String getFechaHoraFormateada() {
		return fechaHoraFormateada;
	}
	public void setFechaHoraFormateada(String fechaHoraFormateada) {
		this.fechaHoraFormateada = fechaHoraFormateada;
	}
	public String getNombreObjeto() {
		return nombreObjeto;
	}
	public void setNombreObjeto(String nombreObjeto) {
		this.nombreObjeto = nombreObjeto;
	}
	public String getAccionFormateada() {
		if (this.getDesAccion()== 1) 
			return "Abierta";
		else 
			return "Cerrada"; 
	}
	public void setAccionFormateada(String accionFormateada) {
		this.accionFormateada = accionFormateada;
	}
	public String getEnvioFormateada() {
		switch (this.getEnvio()) {
		case Constantes.NO_ENVIADO:
			accionFormateada = "SinEnviar";
			break;
		case Constantes.ENVIADO:
			accionFormateada = "Enviada";
			break;
		case Constantes.ENVIADO_ERROR:
			accionFormateada ="ErrorEnvio";
			break;
		case Constantes.ENVIADO_CORRECTO:
			accionFormateada ="EnviadaCorrecta";
			break;
			
		default:
			break;
		}
		return envioFormateada;
	}
	public void setEnvioFormateada(String envio) {
		this.envioFormateada = envio;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	

}

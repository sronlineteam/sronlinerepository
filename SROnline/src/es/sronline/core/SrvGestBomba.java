package es.sronline.core;


import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.tarifas.electrica.ControlTarifaElectrica;

/**
 * Servlet implementation class SrvGestBomba
 */
@WebServlet(description = "Gestion de la Bomba de regadio", urlPatterns = { "/SrvGestBomba" })
public class SrvGestBomba extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public SrvGestBomba() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Logger log = Logger.getLogger(getClass().getName());
		
		log.debug("*********recibida peticion en SrvGestBomba*********");
		//boolean completado = false;
		String mensaje = "No se ha podido completar la peticion, intentelo nuevamente.";
		String dialogo = Constantes.DIALOGO_VACIO;
		
		//		Respuesta a enviar en formato JSON
		//		{ 
		//			mensaje: "...",
		//			parametros: {idRemota: "...", idValvula: "..." }
		//		}

		StringBuffer auxRespuestaJSON = new StringBuffer("{ mensaje: ");

		Bomba bomba = new Bomba();
		Valvulas valvula = new Valvulas();
		String action = "listado";
		Integer auxInteger;
		String redirigir = "/SrvGestValvulas";
		
		if(request.getParameter("idRemota") != null && (String) request.getParameter("idValvula") != null){
			auxInteger = new Integer((String) request.getParameter("idRemota"));
			bomba.setIdRemota(auxInteger.intValue());
			
	      if (request.getParameter("destino") != null) {
	          redirigir = request.getParameter("destino");
	        }

			auxInteger = new Integer((String) request.getParameter("idValvula"));
			bomba.setIdValvula(auxInteger.intValue());
		}
			
			if (request.getParameter("accion") != null){
				action = (String) request.getParameter("accion");
//				int intentos = 0;
				if (action.equalsIgnoreCase(Constantes.getAccionAbrir())){
					boolean bAbrir = true;
					if(Constantes.isControlTarifaElectrica()){
						Date ahora = new Date();
						
						int decision = ControlTarifaElectrica.controlar(ahora);
						switch (decision) {
						case ControlTarifaElectrica.NO_PERMITIR:
							mensaje = ControlTarifaElectrica.AVISO_NO_PERMITIR;
							dialogo = Constantes.DIALOGO_ERROR;
							bAbrir = false;
							break;

						case ControlTarifaElectrica.CONSULTAR:
							String auxConfirmacion ="";
							if(request.getParameter("confirmacion") != null){
								auxConfirmacion = (String) request.getParameter("confirmacion");
							}
							if(Constantes.CONFIRMADA.equals(auxConfirmacion))
								bAbrir = true;
							else if(Constantes.RECHAZADA.equals(auxConfirmacion))
								bAbrir = false;
							else{
								mensaje = ControlTarifaElectrica.AVISO_CONSULTAR;
								dialogo = Constantes.DIALOGO_CONFIRMAR;
								request.setAttribute("confirmacion", bomba.getIdRemota() + "," +bomba.getIdValvula() + ",'no'");
								bAbrir = false;
							}
							break;
						default:
							break;
						}
					}
						
//					while (intentos < 3){
					if(bAbrir){
						if (valvula.abrirValvula(bomba)){
							mensaje = "Valvula abierta!!";
							dialogo = Constantes.DIALOGO_INFORMAR;
//							break;
							}
						else{
							mensaje="Se ha producido un error, intentelo mas tarde";
							dialogo=Constantes.DIALOGO_ERROR;
						}
					}
//						intentos++;
//					}
				}else if (action.equalsIgnoreCase(Constantes.getAccionCerrar())){
//					while (intentos < 3){
						if (valvula.cerrarValvula(bomba)){
							mensaje = "Valvula cerrada!!";
							dialogo = Constantes.DIALOGO_INFORMAR;
//							break;
						}
						else{
							mensaje="Se ha producido un error, intentelo mas tarde";
						}
							
//						intentos++;
//					}
				}
				else if (action.equalsIgnoreCase(Constantes.getAccionEstado())){
					String estadoValvula = valvula.estadoValvula(bomba);
					if (estadoValvula.equalsIgnoreCase(Constantes.getManualAbierto())) 
						mensaje = "abierta";

					else if(estadoValvula.equalsIgnoreCase(Constantes.getManualCerrado()))  {
						mensaje = "cerrada";
					}
					else if(estadoValvula.equalsIgnoreCase(Constantes.AVERIADA))  {
						mensaje = "AVERIADA!!!";
					}
					else {
						mensaje = "desconocido";
					}
					mensaje = "El estado de la v�lvula es " + mensaje;
					dialogo = Constantes.DIALOGO_ERROR;

				}
					
								
			}
		
			auxRespuestaJSON.append("\"").append(mensaje).append("\"").append(',');
			auxRespuestaJSON.append("parametros: {");
			auxRespuestaJSON.append("idRemota: ").append("\"").append(bomba.getIdRemota()).append("\"").append(',');
			auxRespuestaJSON.append("idValvula: ").append("\"").append(bomba.getIdValvula()).append("\"");
			auxRespuestaJSON.append("estado: ").append("\"").append(action.toLowerCase()).append("\"");
			auxRespuestaJSON.append("} }");

			request.setAttribute("resultado", mensaje);
			request.setAttribute("dialogo", dialogo);
			getServletContext().getRequestDispatcher(redirigir).forward(request, response);
		
		
	}

}

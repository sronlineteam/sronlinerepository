package es.sronline.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.ProgramacionDao;
import es.sronline.bbdd.factory.ProgramacionDaoFactory;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;


/**
 * Servlet implementation class SrvGestBloqueObjetos
 */
@WebServlet("/SrvGestBloqueObjTelecontrol")
public class SrvGestBloqueObjTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestBloqueObjTelecontrol() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger(getClass().getName());
		//Programacion programacion = new Programacion();
    	String resultado = "";
		String dialogo = Constantes.DIALOGO_VACIO;
    	
		log.debug("*********recibida petición en SrvGestBloqueObjetos*********");
		//boolean completado = false;
		//String mensaje = "No se ha podido completar la petición, intentelo nuevamente.";
		String[] objetos;
		String desAccion;
		String hora;
		String dia;
//		GestTramasValvulas gstValvulas = new GestTramasValvulas();
		
		if(request.getParameter("objetos") != null 
			&& request.getParameter("hora") != null 
			&& request.getParameter("dia") != null 
			&& request.getParameter("accion") != null
			&& request.getParameter("controlTarifa") != null){
				
//				int confirmacion = ControlTarifaElectrica.PERMITIR;
				desAccion = (String) request.getParameter("accion");
				hora  = (String) request.getParameter("hora");
				dia  = (String) request.getParameter("dia");
				objetos = ((String) request.getParameter("objetos")).split(",");
				dialogo = Constantes.DIALOGO_INFORMAR;
				
				if(Constantes.isControlTarifaElectrica() 
					&& desAccion.equals(Constantes.getAccionAbrir())
					&& request.getParameter("controlTarifa").equals(Constantes.CONTROL_TARIFA)){
					
					String auxConfirmacion ="";
					boolean bAbrir = false;
					if(request.getParameter("confirmacion") != null){
						auxConfirmacion = (String) request.getParameter("confirmacion");
					}
					if(Constantes.CONFIRMADA.equals(auxConfirmacion))
						bAbrir = true;
					else if(Constantes.RECHAZADA.equals(auxConfirmacion))
						bAbrir = false;
					else{
						resultado += GestTramasObjetosTelecontrol.nuevaProgramacion(objetos, hora, dia,dia, desAccion, ControlTarifaElectrica.CONSULTAR);
						if(resultado.contains(ControlTarifaElectrica.AVISO_CONSULTAR)){
							request.setAttribute("confirmacion", "1,1,'si'");
							dialogo = Constantes.DIALOGO_CONFIRMAR;
						}
							
					}
					if(bAbrir)
						resultado += GestTramasObjetosTelecontrol.nuevaProgramacion(objetos, hora, dia, dia, desAccion, ControlTarifaElectrica.PERMITIR);
				}
				else
					resultado += GestTramasObjetosTelecontrol.nuevaProgramacion(objetos, hora, dia,dia, desAccion);
			}

		response.setContentType("text/html");

		request.setAttribute("resultado", resultado);
		request.setAttribute("dialogo", dialogo);

		getServletContext().getRequestDispatcher("/SrvObjetosTelecontrol").forward(request, response);
		
		}

//	private String guardar(es.sronline.bbdd.dto.Programacion programacion) {
//		ProgramacionDao _dao = getProgramacionDao();
//		String resultado =  "No se ha podido guardar, intentelo nuevamente.";
//		try {
//			_dao.insert(programacion);
//			resultado =  "Programación guardada.";
//		} catch (ProgramacionDaoException e) {
//			log.error(e.getMessage());
//		}
//		
//		return resultado;
//		
//	}
	
	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionDao getProgramacionDao()
	{
		return ProgramacionDaoFactory.create();
	}


}

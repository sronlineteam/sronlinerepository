package es.sronline.core;

public class ProgramacionFormateada extends Programacion {
	private String fechaHoraFormateada;
	private String accionFormateada;
	private String nombreValvula;
	  
	public String getNombreValvula() {
		return nombreValvula;
	}
	public void setNombreValvula(String nombreValvula) {
		this.nombreValvula = nombreValvula;
	}
	public String getFechaHoraFormateada() {
		return fechaHoraFormateada;
	}
	public void setFechaHoraFormateada(String fechaHoraFormateada) {
		this.fechaHoraFormateada = fechaHoraFormateada;
	}
	public String getAccionFormateada() {
		if (this.getDesAccion()== 1) 
			return "Abierta";
		else 
			return "Cerrada"; 
	}
	public void setAccionFormateada(String accionFormateada) {
		this.accionFormateada = accionFormateada;
	}
	

}

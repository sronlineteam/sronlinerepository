package es.sronline.core;

public class ProgramacionPlanificaciones {
	
		private int idPlanificacion;

		private String Nombre;

		private String F_inicio;

		private String F_fin;

		private String H_inicio;

		private String H_fin;
		
		private String ArrayDiasSemana;
		
		private String ArrayIdObjetosTelecontrol;
		
		private String ArrayIdProgramacionesTelecontrol;
		
		private String estadoEnvio;

	    public ProgramacionPlanificaciones() {
	    }

		public int getIdPlanificacion() {
			return idPlanificacion;
		}

		public void setIdPlanificacion(int idPlanificacion) {
			this.idPlanificacion = idPlanificacion;
		}

		public String getNombre() {
			return Nombre;
		}

		public void setNombre(String Nombre) {
			this.Nombre = Nombre;
		}

		public String getF_inicio() {
			return F_inicio;
		}
		
		public void setF_inicio(String F_inicio) {
			this.F_inicio = F_inicio;
		}

		
		
		
		public String getF_fin() {
			return F_fin;
		}
		
		public void setF_fin(String F_fin) {
			this.F_fin = F_fin;
		}
		
		
		
		
		public String getH_inicio() {
			return H_inicio;
		}
		
		public void setH_inicio(String H_inicio) {
			this.H_inicio = H_inicio;
		}
		
		public String getH_fin() {
			return H_fin;
		}
		
		public void  setH_fin(String H_fin) {
			this.H_fin = H_fin;
		}

		
		public String getArrayDiasSemana(){
			return ArrayDiasSemana;
		}
		
		public void SetArrayDiasSemana(String ArrayDiasSemana){
			this.ArrayDiasSemana = ArrayDiasSemana;
		}

		
		public String getArrayIdObjetosTelecontrol(){
			return ArrayIdObjetosTelecontrol;
		}
		
		public void SetArrayIdObjetosTelecontrol(String ArrayIdProgramacionesTelecontrol){
			this.ArrayIdObjetosTelecontrol = ArrayIdProgramacionesTelecontrol;
		}		

		
		public String getArrayIdProgramacionesTelecontrol(){
			return ArrayIdProgramacionesTelecontrol;
		}
		
		public void SetArrayIdProgramacionesTelecontrol(String ArrayIdProgramacionesTelecontrol){
			this.ArrayIdProgramacionesTelecontrol = ArrayIdProgramacionesTelecontrol;
		}

		public String getEstadoEnvio() {
			return estadoEnvio;
		}

		public void setEstadoEnvio(String estadoEnvio) {
			this.estadoEnvio = estadoEnvio;
		}
}
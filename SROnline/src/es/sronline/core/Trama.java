package es.sronline.core;
@Deprecated
public class Trama {
	private String trama;
	  private int tipo;
	  private Bomba bomba;
	  private int comprobar;
	  
	  public Trama(String trama, Bomba bomba, int comprobar, int tipo)
	  {
	    this.trama = trama;
	    this.tipo = tipo;
	    this.bomba = bomba;
	    this.comprobar = comprobar;
	  }
	  
	  public String getTrama()
	  {
	    return this.trama;
	  }
	  
	  public void setTrama(String trama)
	  {
	    this.trama = trama;
	  }
	  
	  public int getTipo()
	  {
	    return this.tipo;
	  }
	  
	  public void setTipo(int tipo)
	  {
	    this.tipo = tipo;
	  }
	  
	  public Bomba getBomba()
	  {
	    return this.bomba;
	  }
	  
	  public void setBomba(Bomba bomba)
	  {
	    this.bomba = bomba;
	  }
	  
	  public int getComprobar()
	  {
	    return this.comprobar;
	  }
	  
	  public void setComprobar(int comprobar)
	  {
	    this.comprobar = comprobar;
	  }
}

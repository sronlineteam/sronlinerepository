package es.sronline.core;

import static org.quartz.TriggerBuilder.newTrigger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.TriggerKey;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ProgramacionTelecontrolDao;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.dto.ProgramacionTelecontrol;
import es.sronline.bbdd.dto.ProgramacionTelecontrolPk;
import es.sronline.bbdd.exceptions.ProgramacionTelecontrolDaoException;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ProgramacionTelecontrolDaoFactory;
import es.sronline.servicio.ServicioHoraPlanificada;
import es.sronline.tareas.QuartzTareas;



/**
 * Servlet implementation class SrvListadoProgramacionesTelecontrol
 */
@WebServlet("/SrvListadoProgramacionesTelecontrol")
public class SrvListadoProgramacionesTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvListadoProgramacionesTelecontrol() {
        super();
        log = Logger.getLogger(getClass().getName());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConexionBBDD conexion = new ConexionBBDD();

    	String accion;
    	String resultado = "";
    	int idProgramacion;
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();
    	
		log.debug("*********recibida petición de " + usuario + " en SrvListadoProgramacionesTelecontrol*********");

		LstObjetosTelecontrol[] lstObjTelecontrol = null;
		try {
			LstObjetosTelecontrolDao _dao = LstObjetosTelecontrolDaoFactory.create();
			lstObjTelecontrol = _dao.findAll(usuario);

		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("objetosTelecontrol", lstObjTelecontrol);

		if(request.getParameter("accion")!=null && request.getParameter("idProgramacion")!=null){
			accion = request.getParameter("accion");
			if(accion.equals(Constantes.getAccionCambiar())){
				idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
				Calendar cal = Calendar.getInstance();
				cal = Common.getFechaHoraProgramacion((String) request.getParameter("dia"), (String) request.getParameter("hora"));
				Date fechaHora = new Date();
				fechaHora.setTime(cal.getTimeInMillis());				
				try {
					resultado = cambiarFechaHoraProgramacion(idProgramacion, fechaHora);
				} catch (SchedulerException e) {
					conexion.actualizarErrorObjeto(idProgramacion, e.getMessage());
					e.printStackTrace();
				}
			}
			else if(accion.equals(Constantes.getAccionEliminar())){
				idProgramacion = Integer.parseInt((String) request.getParameter("idProgramacion"));
				try {
					resultado = eliminarProgramacion(idProgramacion);
				} catch (SchedulerException e) {
					conexion.actualizarErrorObjeto(idProgramacion, e.getMessage());
					e.printStackTrace();
				}
			}
			else if (accion.equals(Constantes.getAccionAbrir()) || accion.equals(Constantes.getAccionCerrar())) {
				String[] idObjetosTelecontrol;
				String hora;
				String diaInicio;
				String diaFin;
				String desAccion;
				if(request.getParameter("idObjetosTelecontrol") != null 
						&& request.getParameter("hora") != null 
						&& request.getParameter("diaInicio") != null
						&& request.getParameter("accion") != null){
						desAccion = (String) request.getParameter("accion");
						hora  = (String) request.getParameter("hora");
						diaInicio  = (String) request.getParameter("diaInicio");
						diaFin  = (String) request.getParameter("diaFin");						
						idObjetosTelecontrol = ((String) request.getParameter("idObjetosTelecontrol")).split(",");
						resultado += GestTramasObjetosTelecontrol.nuevaProgramacion(idObjetosTelecontrol, hora, diaInicio, diaFin, desAccion);
					}
			}
		}
		conexion.cerrar();
		
		//consulto la lista de programaciones posteriores a ahora.
		List<ProgramacionTelecontrolFormateada> lstProgramaciones = cargarListaProgramacionesFuturas(usuario);
		//mando la lista a la jsp
		request.setAttribute("lstProgramaciones", lstProgramaciones);
		request.setAttribute("resultado", resultado);
		getServletContext().getRequestDispatcher("/listaObjetosTelecontrol.jsp").forward(request, response);
		
	}

	private String eliminarProgramacion(int idProgramacion) throws SchedulerException {
		ConexionBBDD conexion = new ConexionBBDD();
		Scheduler sched = QuartzTareas.getSched();
		ProgramacionTelecontrolPk progPk = new ProgramacionTelecontrolPk(idProgramacion);	
		ProgramacionTelecontrolDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido eliminar, intentelo nuevamente.";
		sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"));
		try {
			_dao.delete(progPk);
			resultado =  "Programación eliminada.";
		} catch (ProgramacionTelecontrolDaoException e) {
			conexion.actualizarErrorObjeto(idProgramacion, e.getMessage());
			log.error(e.getMessage());
		}
		conexion.cerrar();
		log.debug(resultado);
		
		return resultado;
	}

	/**
	 * Method 'getProgramacionDao'
	 * 
	 * @return ProgramacionDao
	 */
	public static ProgramacionTelecontrolDao getProgramacionDao()
	{
		return ProgramacionTelecontrolDaoFactory.create();
	}

	private String cambiarFechaHoraProgramacion(int idProgramacion, Date fechaHora) throws SchedulerException {
		ConexionBBDD conexion = new ConexionBBDD();
		Scheduler sched = QuartzTareas.getSched();
		ProgramacionTelecontrolPk progPk = new ProgramacionTelecontrolPk(idProgramacion);	
		ProgramacionTelecontrolDao _dao = getProgramacionDao();
		String resultado =  "No se ha podido cambiar, intentelo nuevamente.";
		ProgramacionTelecontrol dto;
		
		try {
			dto = _dao.findByPrimaryKey(progPk);
			dto.setFechaHora(fechaHora);
			_dao.update(progPk, dto);
			String dateTime = " al dia " + new SimpleDateFormat("dd/MM/yyyy").format(fechaHora) 
				     +  " a las " + new SimpleDateFormat("HH:mm").format(fechaHora);

			
			String planificado = conexion.damePlanificadoObjetosTelecontrol(idProgramacion);
			if (planificado.equals(String.valueOf(Constantes.PLANIFICADO))){
				if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora)){
					log.debug("Actualizando group2.job"+idProgramacion + dateTime);
					Trigger newtrigger1 = newTrigger().withIdentity("trigger"+idProgramacion, "group2").startAt(fechaHora).build();
					sched.rescheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"), newtrigger1);
				}else{
					log.debug("Borrando group2.job"+idProgramacion);		
					conexion.actualizarPlanificadoObjeto(idProgramacion,Constantes.NO_PLANIFICADO);
					sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idProgramacion, "group2"));
				}
			}else{
				if (ServicioHoraPlanificada.gethoraPlanificacion(fechaHora)){
					log.debug("Lanzando group2.job"+idProgramacion + dateTime);
					QuartzTareas.programacionObjTelecontrol(dto);
					conexion.actualizarPlanificadoObjeto(idProgramacion,Constantes.PLANIFICADO);
				}
			}
			
			resultado =  "Programación cambiada" + dateTime;

		} catch (ProgramacionTelecontrolDaoException e) {
			conexion.actualizarErrorObjeto(idProgramacion, e.getMessage());
			log.error(e.getMessage());
		}
		
		conexion.actualizarResultadoObjeto(idProgramacion, resultado);
		conexion.cerrar();
		return resultado;
		
	}

	private List<ProgramacionTelecontrolFormateada> cargarListaProgramacionesFuturas(String username) {
		ConexionBBDD con = new ConexionBBDD();
		List<ProgramacionTelecontrolFormateada> resultado = con.consultarProgramacionesTelecontrolFuturas(username);
		con.cerrar();
		return resultado;
	}

}
package es.sronline.telecontrol;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import es.sronline.bbdd.dao.EmplazamientoDao;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dto.Emplazamiento;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.exceptions.EmplazamientoDaoException;
import es.sronline.bbdd.factory.EmplazamientoDaoFactory;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvObjetosTelecontrol
 */
@WebServlet("/SrvObjetosTelecontrol")
public class SrvObjetosTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvObjetosTelecontrol() {
        super();
        log = Logger.getLogger(getClass().getName());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger(getClass().getName());
		LstObjetosTelecontrol[] lstObjTelecontrol = null;
		String paginaDestino = "/gestionObjetosTelecontrol.jsp";
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();

		
		String dialogo;
		if(request.getAttribute("dialogo")!=null){
			dialogo = (String) request.getAttribute("dialogo");
		}else{
			dialogo = Constantes.DIALOGO_VACIO;
		}
		
		String resultado;
		if(request.getAttribute("resultado")!=null ){
			resultado = (String) request.getAttribute("resultado");
		}else{
			resultado = "";
		}

		if(request.getParameter("mapa") != null ){
			paginaDestino = "/gestionObjetosTelecontrolMapa.jsp";
			log.debug("con Mapa!!");
		}

		try {
			
			LstObjetosTelecontrolDao _dao = LstObjetosTelecontrolDaoFactory.create();
			lstObjTelecontrol = _dao.findAll(usuario);
			String[] auxConexion = new String[2];
			LstObjetosTelecontrol auxLstObjTeleCtrl;
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		request.setAttribute("lstObjTelecontrol", lstObjTelecontrol);
		request.setAttribute("resultado", resultado);
		request.setAttribute("dialogo", dialogo);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

	}

	private String[] estudiaConexion(String conexion) {
		String[] auxConexion = new String[2];
		
		if(conexion.startsWith(Constantes.getTipoConexionValvulas())){
			auxConexion[0]=Constantes.getTipoComunicacion();
			auxConexion[1]=conexion.substring(1, conexion.length());
		}
		else{
			auxConexion[0]=null;
			auxConexion[1]=conexion;
		}
			
			
		
		return auxConexion;
	}
	
	private int nivel(int idEmplazamiento, int nivel){
		if(idEmplazamiento == 0)
			return nivel;
		else{
			EmplazamientoDao _daoEmpla = EmplazamientoDaoFactory.create();
			Emplazamiento emplazamiento;
			try {
				emplazamiento = _daoEmpla.findByPrimaryKey(idEmplazamiento);
				int idEmplazamientoPadre = emplazamiento.getIdEmplazamientoPadre();
				return nivel(idEmplazamientoPadre, nivel++);
			} catch (EmplazamientoDaoException e) {
				log.error(e.getMessage());
			}
		}
		return -1;
			
	}


}

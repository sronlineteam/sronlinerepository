package es.sronline.telecontrol;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.AccionesTelecontrolDao;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.SensorDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.AccionesTelecontrol;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.ObjetosTelecontrolPk;
import es.sronline.bbdd.dto.Sensor;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.AccionesTelecontrolDaoException;
import es.sronline.bbdd.exceptions.LstObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.SensorDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.AccionesTelecontrolDaoFactory;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.SensorDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.comunicacion.comm.CommunicationFlow;
import es.sronline.comunicacion.http.ComunicacionHttp;
import es.sronline.comunicacion.ip.Comunicacion;
import es.sronline.comunicacion.webservice.ComunicacionWS;
import es.sronline.core.Common;
import es.sronline.core.Constantes;
import es.sronline.core.GestTramasValvulas;
import es.sronline.tareas.QuartzTareas;

public class Telecontrol {
	private FlujoComunicacion comFlow;
	private String trama;
	Logger log;
	
	private ObjetosTelecontrol objTelCtrl;	
	private TipoObjetoTelecontrol tipObjTC;
	private	String[] auxConexion;
	public static int ABRIR =1;
	public static int CERRAR =2;
	public static int CONSULTAR =3;
	private String respuestaOK = null;
	public static String textoAccion[] = {"", "abrir", "cerrar"};
	private int tiempoEspera = Constantes.getEsperaEntreIntentosLectura();

	public Telecontrol(ObjetosTelecontrol objTelecontrol) {
		log = Logger.getLogger(getClass().getName());
		
		this.objTelCtrl = objTelecontrol;
		auxConexion = objTelecontrol.getConexion().split("/");

		String tipoComunicacion = objTelecontrol.getTipoConexion();

		LstObjetosTelecontrolDao _dto = LstObjetosTelecontrolDaoFactory.create();
		LstObjetosTelecontrol lsObjTelCrtl;
		TipoObjetoTelecontrolDao dtoTipObjTC = TipoObjetoTelecontrolDaoFactory.create();

		try {
			lsObjTelCrtl = _dto.findByPrimaryKey(objTelecontrol.getIdObjeto()+"");
			objTelCtrl.setEsclavo(lsObjTelCrtl.getEsclavo());
			tipObjTC = dtoTipObjTC.findByPrimaryKey(objTelecontrol.getIdTipoObjetoTelecontrol());
			if(tipObjTC.getTiempoRespuesta()>0)
				tiempoEspera = tipObjTC.getTiempoRespuesta();
		} catch (LstObjetosTelecontrolDaoException | TipoObjetoTelecontrolDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		//Dependiendo del tipo de comunicaci�n instancia una clase u otra
		if ("COM".equalsIgnoreCase((tipoComunicacion)))
			comFlow = new CommunicationFlow();
		if ("IP".equalsIgnoreCase(tipoComunicacion))
			comFlow = new Comunicacion();
		if ("WS".equalsIgnoreCase(tipoComunicacion))
			comFlow = new ComunicacionWS();
		if ("HTTP".equalsIgnoreCase(tipoComunicacion))
			comFlow = new ComunicacionHttp();
	}

	public boolean abrirObjeto() {
		return ejecutar(ABRIR);

	}

	public boolean cerrarObjeto() {
		return ejecutar(CERRAR);

	}
	
	public String estadoObjeto(){
		String result = "error";
		String auxEstado = "on";
		String encendido = Constantes.getManualAbierto();
		String apagado = Constantes.getManualCerrado();
		int intentos = 0;

		trama = getTrama(3);
		
		log.debug("trama + CRC: " + trama);
		//TODO intentar si no se ha podiso conectar
		while(intentos++<5 && "error".equals(result))
			result =  comFlow.enviar(trama, objTelCtrl.getConexion(), tiempoEspera);
		//estado para conexiones HTTP o WS
        auxEstado = result.substring(0, 2);
        if(result.length() >= 19){
        	auxEstado = result.substring(18, 20);
        
        if(auxEstado.equals("00"))
        	auxEstado = apagado;
        else if(auxEstado.equals("01"))
        	auxEstado = encendido;
        }
        else if(result.length() >= 8){
        	//estado para IP
        	auxEstado = result.substring(6, 8);
        
        if(auxEstado.equals(Constantes.getManualAbierto()))
        	auxEstado = encendido;
        else if(auxEstado.equals(Constantes.getManualCerrado()))
        	auxEstado = apagado;
        }
        if("error".equals(result))
        	gestionarErrorEnvio();
		
		return auxEstado;
	}

	/**
	 * Verificar con la trama si el objeto est� en estado <comprobar>
	 * @param estadoAcomprobar
	 */
	public boolean verificarFlujo(int estadoAcomprobar) {
		String result = "";
		String estadoToString = estadoAcomprobar + "";
		boolean comprobado = false;
		String auxEstadoLeido;
		Sensor sensor = getSensor(objTelCtrl.getIdObjeto());
		int accion = Constantes.CONTROL_FLUJO;
		
		try {
			
			log.debug("trama + CRC: " + trama);
			result =  comFlow.enviar(getTramaFlujo(accion),objTelCtrl.getConexion(), tiempoEspera);
			// La respuesta es un String de un n� binario de ocho d�gitos 
			// con 1 o 0 dependiendo de se pasa o no flujo
			log.debug("resultado: " + result);
			
			//recojo en la posici�n 1 o 0 dependiendo del estado
			auxEstadoLeido = result.substring(sensor.getPosicion(), sensor.getPosicion() + 1 );
			log.debug("respuesta " + auxEstadoLeido);
			
			if(!estadoToString.equalsIgnoreCase(auxEstadoLeido))
				actualizaEstadoObjeto(objTelCtrl.getIdObjeto(),Constantes.AVERIADA);

			comprobado = true;

		} catch (Exception e) {
			log.error(e.getMessage());
			log.error("error al comprobar flujo " + e.getMessage());
			comprobado = false;
		}
		
		return comprobado;
	
	}
	public String getTrama(int accion) {
			StringBuffer sbTrama = new StringBuffer();
			String trama = "";
	
			AccionesTelecontrolDao _dto =  AccionesTelecontrolDaoFactory.create();
			try {
				AccionesTelecontrol accionTelCtr = _dto.findWhereidTipoObjetoTelecontrolEquals(objTelCtrl.getIdTipoObjetoTelecontrol()+"", "" + accion); 
			
			
			//Byte 1
			sbTrama.append(objTelCtrl.getEsclavo());
			//Byte 2
			sbTrama.append(accionTelCtr.getComando());
			//Byte 3 y 4
			sbTrama.append(objTelCtrl.getDireccionHex());
			//Byte5 
			sbTrama.append(accionTelCtr.getAccion());
			//Byte6 
			sbTrama.append(accionTelCtr.getByte6());
			trama = sbTrama.toString();
			//CRC16
			if(accionTelCtr.getTieneCRC()==1)
				trama = Common.appendCRC16(trama);
			if(accionTelCtr.getRespuestaOK() != null)
				respuestaOK = accionTelCtr.getRespuestaOK();
			
			} catch (AccionesTelecontrolDaoException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
			}
			
			log.debug("trama: " + sbTrama.toString());
		
			return trama;
			
		}
//	Se consulta por remota las 4 v�lvulas, recogiendo 4 bites con 1 � 0 (un binario de 4 cifras, una por v�lvula bbbb ) donde X es el n� de remota.	
//	Trama:
//		01 04 00 (3C + X - 1) 00 01 (CRC1) (CRC2)
//		Respuesta: 
//		01 04 02 (Estado_entradas_de las _4_Remota bbbb) (CRC1) (CRC2)
//		Donde bbbb es:
//		0001 (= 1) activa solo la 1
//		0010 (= 2) activa solo la 2
//		0011 (= 3) activa solo la 1 y la 2
//		0100 (= 4) activa solo la 3
//		0101 (= 5) activas la 3 y la 1
//		0110 (= 6) activa la 3 y la 2
//		0111 (= 7) activa la 1, 2 y 3
//		1111 (= 15) activas todas
//
// 		3C + X - 1 = 3C + Direccion - DireccionInicial

	public String getTramaFlujo(int accion) {
		StringBuffer sbTrama = new StringBuffer();
		String trama = "";

		AccionesTelecontrolDao _dto =  AccionesTelecontrolDaoFactory.create();
		SensorDao _dtoSensor = SensorDaoFactory.create();
		try {
			AccionesTelecontrol accionTelCtr = _dto.findWhereidTipoObjetoTelecontrolEquals(objTelCtrl.getIdTipoObjetoTelecontrol()+"", "" + accion); 
			Sensor sensor = _dtoSensor.findByPrimaryKeyIdObjetoEquals(objTelCtrl.getIdObjeto());
			int direccion = Integer.parseInt("3C",16) + (Integer.parseInt(objTelCtrl.getDireccionHex(), 16) - Integer.parseInt(sensor.getDirInicial(), 16))/sensor.getNumSalidas(); 
			String dir4bytes = "0000" + Integer.toHexString(direccion).toUpperCase();
			dir4bytes = dir4bytes.substring(dir4bytes.length()-4);
			
			//Byte 1
			sbTrama.append(objTelCtrl.getEsclavo());
			//Byte 2
			sbTrama.append(accionTelCtr.getComando());//04
			//Byte 3 y 4
			sbTrama.append(dir4bytes);
			//Byte5 
			sbTrama.append(accionTelCtr.getAccion());
			//Byte6 
			sbTrama.append(accionTelCtr.getByte6());
			trama = sbTrama.toString();
			//CRC16
			if(accionTelCtr.getTieneCRC()==1)
				trama = Common.appendCRC16(trama);
			if(accionTelCtr.getRespuestaOK() != null)
				respuestaOK = accionTelCtr.getRespuestaOK();
		
		} catch (AccionesTelecontrolDaoException | SensorDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		log.debug("trama: " + sbTrama.toString());
	
		return trama;
		
	}

	public void actualizaEstadoObjeto(int idObjetoTC, String estado) {
		ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
		ObjetosTelecontrol objetoTC;
		try {
			objetoTC = _dao.findByPrimaryKey(idObjetoTC);
			objetoTC.setEstado(estado);
			_dao.update(new ObjetosTelecontrolPk(idObjetoTC), objetoTC);;
		} catch (ObjetosTelecontrolDaoException e) {
			log.error(e.getMessage());
		}
	
	}

	protected boolean ejecutar(int accion) {
		boolean result = false;
		String trama = getTrama(accion);
		log.debug("trama + CRC: " + trama);
		
		try {
			result = comprueba(trama, comFlow.enviar(trama, objTelCtrl.getConexion(), tiempoEspera), respuestaOK);
			
			if(result){
				if (accion == ABRIR)
					actualizaEstadoObjeto(Integer.valueOf(objTelCtrl.getIdObjeto()), Constantes.ABIERTA);
				else if (accion == CERRAR)
					actualizaEstadoObjeto(Integer.valueOf(objTelCtrl.getIdObjeto()), Constantes.CERRADA);
				log.info("Realizada correctamente la ejecuci�n de " + accion + "(1 Abrir / 2 Cerrar) en " + objTelCtrl.getNombre() );
				
				if(Constantes.isControlDeFlujo() && objTelCtrl.getControFlujo() == Constantes.CONTROL_FLUJO_ON){
					//programo tarea control de flujo
				    try {
				    	QuartzTareas.programacionControlFlujo(objTelCtrl.getIdObjeto(), accion);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				
				
				
				}
			}
			else
				//Avisos.enviarAviso("Revise la acci�n " + accion + " sobre " + objTelCtrl.getNombre() + ".");
				log.warn("Revise la acci�n " + accion + " sobre " + objTelCtrl.getNombre() + ".");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return result;
	}

	private void gestionarErrorEnvio() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param tramaEnviada
	 * @param tramaRecivida
	 * @return resultado de comparar si son iguales las dos tramas
	 */
	private boolean comprueba(String tramaEnviada, String tramaRecivida, String respuestaOK) {
		//String trama2 = new String(GestTramasValvulas.transformaByte(tramaEnviada));

		if(tramaRecivida.equals(respuestaOK))
			return true;
		if (tramaRecivida.length() > 4 && tramaEnviada.startsWith(tramaRecivida))
			return true;
		log.error("No coincide trama " + tramaEnviada + " con la respuesta "
				+ tramaRecivida);
		return false;
	}

	private Sensor getSensor(int idObjeto) {
		SensorDao _dao = SensorDaoFactory.create();
		Sensor sensor = null;
		try {
			sensor = _dao.findByPrimaryKeyIdObjetoEquals(idObjeto);
		} catch (SensorDaoException e) {
			log.error(e.getMessage());
		}
		return sensor;
	}

}

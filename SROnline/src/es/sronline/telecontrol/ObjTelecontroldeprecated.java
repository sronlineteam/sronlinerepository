package es.sronline.telecontrol;

public class ObjTelecontroldeprecated {
private String idObjeto;
private String nombre;
private String tipoConexion;
private String datosConexion;
private String direccionHex;
private String estado;
private String posicion;
private String esclavo;
private String controlTarifa;
int orden;
int idEmplazamiento;
int idTipoObjetoTelecontrol;


public String getIdObjeto() {
	return idObjeto;
}
public void setIdObjeto(String idObjeto) {
	this.idObjeto = idObjeto;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getDatosConexion() {
	return datosConexion;
}
public void setDatosConexion(String datosConexion) {
	this.datosConexion = datosConexion;
}
public String getTipoConexion() {
	return tipoConexion;
}
public void setTipoConexion(String tipoConexion) {
	this.tipoConexion = tipoConexion;
}
public String getDireccionHex() {
	return direccionHex;
}
public void setDireccionHex(String direccionHex) {
	this.direccionHex = direccionHex;
}
public String getEstado() {
	return estado;
}
public void setEstado(String estado) {
	this.estado = estado;
}
public String getPosicion() {
	return posicion;
}
public void setPosicion(String posicion) {
	this.posicion = posicion;
}
public String getEsclavo() {
	return esclavo;
}
public void setEsclavo(String esclavo) {
	this.esclavo = esclavo;
}
public String getControlTarifa() {
	return controlTarifa;
}
public void setControlTarifa(String controlTarifa) {
	this.controlTarifa = controlTarifa;
}
public int getOrden() {
	return orden;
}
public void setOrden(int orden) {
	this.orden = orden;
}
public int getIdEmplazamiento() {
	return idEmplazamiento;
}
public void setIdEmplazamiento(int idEmplazamiento) {
	this.idEmplazamiento = idEmplazamiento;
}
public int getIdTipoObjetoTelecontrol() {
	return idTipoObjetoTelecontrol;
}
public void setIdTipoObjetoTelecontrol(int idTipoObjetoTelecontrol) {
	this.idTipoObjetoTelecontrol = idTipoObjetoTelecontrol;
}

}

package es.sronline.telecontrol;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.comunicacion.ip.Comunicacion;
import es.sronline.core.Common;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvTestComunicacion
 */
@WebServlet("/SrvTestComunicacion")
public class SrvTestComunicacion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvTestComunicacion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String ip = "";
		String puerto = "";
		String trama = "";
		String conCRC = "";
		String sTiempoRespuesta = "";
		int tiempoRespuesta = Constantes.getEsperaEntreIntentosLectura();
		if(request.getParameter("ip")!=null)
			ip = request.getParameter("ip");
		if(request.getParameter("puerto")!=null)
			puerto = request.getParameter("puerto");
		if(request.getParameter("trama")!=null)
			trama = request.getParameter("trama");
		if(request.getParameter("conCRC")!=null)
			conCRC = request.getParameter("conCRC");
		if(request.getParameter("conTiempoRespuesta")!=null){
			sTiempoRespuesta = request.getParameter("tiempoRespuesta");
			tiempoRespuesta = Integer.parseInt(sTiempoRespuesta);
		}

		if(conCRC.equals("1"))
			trama = Common.appendCRC16(trama);
			

		
		FlujoComunicacion comFlow = new Comunicacion();
		String resultado;
		try {
			resultado = comFlow.enviar(trama, ip + "/" + puerto, tiempoRespuesta);
		} catch (Exception e) {
			resultado = e.getMessage();
		}
		
		request.setAttribute("resultado", resultado);
		getServletContext().getRequestDispatcher("/testComunicacion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package es.sronline.telecontrol;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.core.Bomba;
import es.sronline.core.Constantes;
import es.sronline.core.Valvulas;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;

/**
 * Servlet implementation class SrvGstObjetosTelecontrol
 */
@WebServlet("/SrvGstObjetosTelecontrol")
public class SrvGstObjetosTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGstObjetosTelecontrol() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger log = Logger.getLogger(getClass().getName());
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();

		log.debug("*********recibida peticion en SrvGestObjetos*********");
		String mensaje = "No se ha podido completar la peticion, intentelo nuevamente.";

		Bomba bomba = new Bomba();
		int idObjTelecontrol;
		Telecontrol tlCtrl = null;
		
		Valvulas valvula = new Valvulas();
		String action = "listado";
		Integer auxInteger = -1;
		boolean auxResultado = false;
		String dialogo = Constantes.DIALOGO_VACIO;
		
		if(request.getParameter("idObjeto") != null && request.getParameter("accion") != null){
			
			idObjTelecontrol = Integer.parseInt((String) request.getParameter("idObjeto"));
			ObjetosTelecontrolDao dtoTipObjTC = ObjetosTelecontrolDaoFactory.create();
			ObjetosTelecontrol objTC;
			
			try {
				objTC = dtoTipObjTC.findByPrimaryKey(idObjTelecontrol);
				tlCtrl = new Telecontrol(objTC);
				action = (String) request.getParameter("accion");
				log.info(usuario + " ha pedido " + action + " " + objTC.getNombre());
				//Abrimos el objeto, si es una valvula como hasta ahora
				if (action.equalsIgnoreCase(Constantes.getAccionAbrir())){	
					boolean bAbrir = true;
					
					if(Constantes.isControlTarifaElectrica() && request.getParameter("controlTarifa").equals(Constantes.CONTROL_TARIFA)){
						Date ahora = new Date();
						
						int decision = ControlTarifaElectrica.controlar(ahora);
						switch (decision) {
						case ControlTarifaElectrica.NO_PERMITIR:
							mensaje = ControlTarifaElectrica.AVISO_NO_PERMITIR;
							dialogo = Constantes.DIALOGO_ERROR;
							bAbrir = false;
							break;
	
						case ControlTarifaElectrica.CONSULTAR:
							String auxConfirmacion ="";
							if(request.getParameter("confirmacion") != null){
								auxConfirmacion = (String) request.getParameter("confirmacion");
							}
							if(Constantes.CONFIRMADA.equals(auxConfirmacion))
								bAbrir = true;
							else if(Constantes.RECHAZADA.equals(auxConfirmacion))
								bAbrir = false;
							else{
								mensaje = ControlTarifaElectrica.AVISO_CONSULTAR;
								dialogo = Constantes.DIALOGO_CONFIRMAR;
								request.setAttribute("confirmacion", (String) request.getParameter("idObjeto") + ",'no'");
								bAbrir = false;
							}
							break;
						default:
							break;
						}
					}
					
					if (bAbrir){
							auxResultado = tlCtrl.abrirObjeto();//TODO metodo abrirObjeto
						if (auxResultado){
							mensaje = "Objeto abierto!!";
							dialogo = Constantes.DIALOGO_INFORMAR;
							}
						else{
							mensaje="Se ha producido un error, intentelo mas tarde";
							dialogo = Constantes.DIALOGO_ERROR;
						}
						
					}
					//Cerramos el objeto
				}else if (action.equalsIgnoreCase(Constantes.getAccionCerrar())){
						auxResultado = tlCtrl.cerrarObjeto();//TODO metodo abrirObjeto
						if (auxResultado){
							mensaje = "Objeto cerrado!!";
							dialogo = Constantes.DIALOGO_INFORMAR;
						}
						else{
							mensaje="Se ha producido un error, intentelo mas tarde";
							dialogo = Constantes.DIALOGO_ERROR;
						}		
				}
				else if (action.equalsIgnoreCase(Constantes.getAccionEstado())){
					String estadoValvula = tlCtrl.estadoObjeto();
					
					if (estadoValvula.equalsIgnoreCase(Constantes.getManualAbierto())) 
						mensaje = "abierto";
					else if(estadoValvula.equalsIgnoreCase(Constantes.getManualCerrado()))  {
						mensaje = "cerrado";
					}
					else if(estadoValvula.equalsIgnoreCase(Constantes.AVERIADA))  {
						mensaje = "AVERIADO!!!";
					}
					else {
						mensaje = "desconocido";
					}
					mensaje = "El estado del  objeto es " + mensaje;
					dialogo = Constantes.DIALOGO_INFORMAR;
				}		
							
			} catch (ObjetosTelecontrolDaoException e) {
				log.error(e.getMessage());
			}
		}
		log.info("resultado: " + mensaje);
		request.setAttribute("dialogo", dialogo);
		request.setAttribute("resultado", mensaje);
		getServletContext().getRequestDispatcher("/SrvObjetosTelecontrol").forward(request, response);
			
	}


}

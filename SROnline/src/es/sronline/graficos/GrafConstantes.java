package es.sronline.graficos;

public class GrafConstantes {
	public static final String PARAMETRO_ETIQUETAS = "GraficoEtiquetas";
	public static final String PARAMETRO_CONEXION = "GraficoDatosConexion"; 
	public static final String PARAMETRO_TRAZAS = "GraficoDatosTrazas"; 
	public static final String PARAMETRO_DATOS = "GraficoDatosPosicion"; 

}

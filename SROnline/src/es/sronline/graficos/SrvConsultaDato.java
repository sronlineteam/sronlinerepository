package es.sronline.graficos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.comunicacion.ip.Comunicacion;

/**
 * Servlet implementation class SrvConsultaDato
 */
@WebServlet("/SrvConsultaDato")
public class SrvConsultaDato extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;   
   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvConsultaDato() {
        super();
        log = Logger.getLogger(getClass().getName());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String conexion = null;
		String trazas = null;
		String[] auxTrazas = null;
		String[] auxPosicionDatos = null;
		String posicionDatos = null;
		String[] auxPosicion;
		String etiquetas = null;
		String[] auxEtiquetas = null;
		int desde = 0;
		int hasta = 2;
		int valor = 4;
		FlujoComunicacion comFlow;
		String auxRespuesta = "";
		//List<String> listDatos = new ArrayList<String>();
		StringBuffer sbJson = new StringBuffer("{  \"cols\": [");
		sbJson.append("{\"id\": \"A\", \"label\": \"Etiqueta\", \"type\": \"string\"},");
		sbJson.append("{\"id\": \"B\", \"label\": \"Valor\", \"type\": \"number\"}");
		sbJson.append("],");
		sbJson.append(" \"rows\": [");
		String json = "";
		
		if(request.getParameter(GrafConstantes.PARAMETRO_CONEXION)!=null)
			conexion = (String) request.getParameter(GrafConstantes.PARAMETRO_CONEXION);
		if(request.getParameter(GrafConstantes.PARAMETRO_TRAZAS)!=null){
			trazas = (String) request.getParameter(GrafConstantes.PARAMETRO_TRAZAS);
			auxTrazas = trazas.split("/");
		}
		if(request.getParameter(GrafConstantes.PARAMETRO_DATOS)!=null){
			posicionDatos = (String) request.getParameter(GrafConstantes.PARAMETRO_DATOS);
			auxPosicionDatos = posicionDatos.split("/");
		}
		if(request.getParameter(GrafConstantes.PARAMETRO_ETIQUETAS)!=null){
			etiquetas = (String) request.getParameter(GrafConstantes.PARAMETRO_ETIQUETAS);
			auxEtiquetas = etiquetas.split("/");
		}
		if(conexion != null && trazas != null){
			comFlow = new Comunicacion();
			for(int n=0; n<auxTrazas.length; n++){
				auxPosicion = auxPosicionDatos[n].split("-");
				desde = Integer.parseInt(auxPosicion[0]);
				hasta = Integer.parseInt(auxPosicion[1]);
				while(auxRespuesta.length() < 8)
					auxRespuesta = comFlow.enviar(auxTrazas[n], conexion);
				valor = Integer.parseInt(auxRespuesta.substring(desde, hasta), 16);
				//listDatos.add("" + valor);
				sbJson.append("{\"c\":[{\"v\":\"" + auxEtiquetas[n] + "\"}, ");
				sbJson.append("{ \"v\": " + valor + ".0 }");
				sbJson.append("]},");
			}
			sbJson.deleteCharAt(sbJson.length()-1);
			sbJson.append("]}");
			json = sbJson.toString();
			log.debug(json);
		}
		//json = new Gson().toJson(listDatos);
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

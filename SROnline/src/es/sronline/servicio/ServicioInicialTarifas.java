package es.sronline.servicio;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import es.sronline.core.Constantes;
import es.sronline.tarifas.electrica.ControlTarifaElectrica;


/**
 * Servlet implementation class ServicioInicialTarifas
 */
@WebListener
public class ServicioInicialTarifas implements ServletContextListener {
	Timer timer;
	Logger log ;
	int contadorFinalPivot = 0;
	boolean pivotEnMarcha = false;
	
    /**
     * Default constructor. 
     */
    public ServicioInicialTarifas() {
		log = Logger.getLogger(getClass().getName());
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
		//log = Logger.getLogger(ServicioInicial.class.getName());
		
		log.info("iniciado en contexto servicio inicial tarifas");
		if(Constantes.isControlTarifaElectrica()){
			timer = new Timer ( ) ;
			Date ahora = new Date();
			int segundos = ahora.getSeconds();
			int inicio = (62 - segundos)*1000;
//			timer.schedule ( new Tarea3 ( ) , inicio, Constantes.getEsperaRelanzarTareasProgramadas()) ;

//			SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
//			Scheduler sched;
//
//			try {
//				sched = schedFact.getScheduler();
//				sched.start();
//				JobDetail job = newJob(HelloJob.class)
//				      .withIdentity("apagado", "Eficiencia") // name "myJob", group "group1"
//				      .build();
//				// Trigger the job to run now, and then every 40 seconds
//				Trigger trigger = newTrigger()
//				      .withIdentity("apagar", "Eficiencia")
//				      .forJob(job)
//				      .startNow()
//				      .withSchedule(cronSchedule("0 58 9 * * MON-FRI"))//de lunes a viernes a las 9:58:00
//				      .build();
//			        
//				// Tell quartz to schedule the job using our trigger
//				sched.scheduleJob(job, trigger);
//			} catch (SchedulerException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
    }
	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
    	timer=null;
    }

	class Tarea3 extends TimerTask {

		public void run ( ) {
				try {
//					comprobarTarifa(cambioTarifa());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	
	}

	private int cambioTarifa(){
		Date ahora = new Date();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(ahora);
		calendar.add(Calendar.MINUTE, 3);
		Date mas2 = calendar.getTime();
		
		int decision = ControlTarifaElectrica.controlar(ahora);
		int decision2 = ControlTarifaElectrica.controlar(mas2);
		
		if(decision != decision2)
			return decision2;
		else
			return 0;
	}
	
	  private void pausar() {
	      try
	      {
	        Thread.sleep(Constantes.getPausaEjecucionProgramacion());
	      }
	      catch (InterruptedException e)
	      {
	        e.printStackTrace();
	      }
		
	}


	
}
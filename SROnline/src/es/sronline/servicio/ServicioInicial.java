package es.sronline.servicio;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import es.sronline.aviso.Avisos;
import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.core.Bomba;
import es.sronline.core.Constantes;
import es.sronline.core.GestTramasValvulas;
import es.sronline.core.Programacion;
import es.sronline.core.ProgramacionTelecontrol;
import es.sronline.core.Trama;
import es.sronline.core.Valvulas;
import es.sronline.tareas.QuartzTareas;
import es.sronline.telecontrol.Telecontrol;

/**
 * Application Lifecycle Listener implementation class ServicioInicial
 *
 */
@WebListener
public class ServicioInicial implements ServletContextListener {
	Timer timer;
	Logger log ;

    /**
     * Default constructor. 
     */
    public ServicioInicial() {
		log = Logger.getLogger(getClass().getName());
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
		//log = Logger.getLogger(ServicioInicial.class.getName());
		
		log.debug("iniciado en contexto");
		if(Constantes.isLanzarTareasProgramadas()){
			timer = new Timer ( ) ;
			timer.schedule ( new Tarea1 ( ) , Constantes.getInicioLanzaTareasProgramadas(), Constantes.getEsperaRelanzarTareasProgramadas()) ;
		}
		
		if(Constantes.isLanzarTareasProgramadasQuartz()){
			try {
				QuartzTareas start = new QuartzTareas();
			    start.programacionesPeriodicas();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
    	timer=null;
    }
	class Tarea1 extends TimerTask {

		public void run ( ) {
				try {
					ejecutar();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				}
		}
	
	}
	
	private void ejecutar() throws Exception {
		programacionesValvulas();

		if (Constantes.isProgramacionTeleControl())
			programacionesObjetosTelecontrol();
		
		if(Constantes.isControlDeFlujo())
			controlFlujo();

	}

	private void controlFlujo() {
		// TODO Auto-generated method stub
		
	}

	private void programacionesValvulas() {
		ConexionBBDD con = new ConexionBBDD();
		
		log.debug("comienza la ejecuci�n de v�lvulas");
		//consulto las programaciones
//		List<Programacion> trazasProgramadas = con.consultarProgramaciones();
//		
//		int programaciones = trazasProgramadas.size();
//		
//		//Para todas las programaciones
//		
//		for(int t = 0; t < programaciones; t++){
//			auxBomba = new Bomba();
//			esperar = false;
//			auxBomba.setIdRemota(trazasProgramadas.get(t).getIdRemota());
//			auxBomba.setIdValvula(trazasProgramadas.get(t).getIdValvula());
//			//identificador de valvula de 1 a 24
//			auxIdValvula = (auxBomba.getIdRemota() - 1) * 4 + (auxBomba.getIdValvula() - 1);
//			
//			if(auxPrimera >=  auxIdValvula){
//				auxPrimera = auxIdValvula;
//				//identifico la primera bomba desde donde enviar acciones
//				primeraBomba = auxBomba;
//			}
//			if(auxUltima < auxIdValvula)
//				auxUltima = auxIdValvula;
//			
//			
//		/*			
//		 * 		las posibles acciones son
//		 * 		0 abrir
//		 * 		1 cerrar
//		 * 		2 comprobar flujo en remota
//		 * 		3 comprobar sin flujo en remota
//		 * 		4 comprobar flujo en programador
//		 * 		5 comprobar sin flujo en programador
//		*/		
//			switch (trazasProgramadas.get(t).getDesAccion()) {
//			case 0:
////				valvula.enviarTrazaEstado(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.ABIERTA );
//				auxModos[auxIdValvula] = Constantes.getManualAbierto();
//				break;
//			case 1:
////				valvula.enviarTrazaEstado(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.CERRADA );
//				auxModos[auxIdValvula] = Constantes.getManualCerrado();
//				break;
//			case 2:
//				//paso la trama para comprobar, la bomba a comprobar y el estado que debe tener y si est� mal que se guarde la veria
//				valvula.verificarFlujoEnRemota(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.getComprobarFlujo());
//				esperar = true;
//				break;
//			case 3:
//				//paso la trama para comprobar, la bomba a comprobar y el estado que debe tener y si est� mal que se guarde la veria
//				valvula.verificarFlujoEnRemota(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.getComprobarNoFlujo());
//				esperar = true;
//				break;
//			case 4:
//				//paso la trama para comprobar, la bomba a comprobar y el estado que debe tener y si est� mal que se guarde la veria
//				valvula.verificarFlujoEnProgramador(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.getComprobarFlujo());
//				esperar = true;
//				break;
//			case 5:
//				//paso la trama para comprobar, la bomba a comprobar y el estado que debe tener y si est� mal que se guarde la veria
//				valvula.verificarFlujoEnProgramador(trazasProgramadas.get(t).getTrama(), auxBomba, Constantes.getComprobarNoFlujo());
//				esperar = true;
//
//			default:
//				break;
//				
//			}
//			
//			//si se ha enviado una traza hay que esperar antes de poder enviar otra.
//			if(esperar){
//				try {
//					Thread.sleep(Constantes.getPausaEjecucionProgramacion());
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					log.error(e.getMessage());
//				}
//			}
//
//		}
//
//		if(auxPrimera <= auxUltima && primeraBomba != null){
//			modos = Arrays.copyOfRange(auxModos, auxPrimera, auxUltima);
//			valvula.enviarTraza(GestTramasValvulas.getTramaAccionMultiple(primeraBomba, modos));
//		}
//
//		if(programaciones>0)	
//			log.info("ejecutadas correctamente: " + programaciones + " programaciones");
//		else
//			log.info("No habia programaciones");
//		con.cerrar();
	    List<Programacion> tramasProgramadas = con.consultarProgramaciones();
	    HashSet<Bomba> bombasAbrir = new HashSet();
	    HashSet<Bomba> bombasCerrar = new HashSet();
	    
	    int programaciones = tramasProgramadas.size();
	    if (programaciones > 0)
	    {
	      Valvulas valvula = new Valvulas();
	      
	      HashSet<Trama> tramasParaEnviar = new HashSet();
	      
	      Bomba primeraBomba = null;
	      
	      int auxPrimera = Constantes.getNumeroMaximoDeValvulas();
	      int auxUltima = 0;
	      int auxIdValvula = 0;
	      
	      boolean estaEnProgramador = false;
	      boolean hayValvulasProgramador = false;
	      
	      String estadosValvulasRemotas = valvula.estadoValvulas(true);
	      try
	      {
	        Thread.sleep(Constantes.getPausaEjecucionProgramacion());
	      }
	      catch (InterruptedException e)
	      {
	        log.error(e.getMessage());
	      }
	      String estadosValvulasProgramador = valvula.estadoValvulas(false);
	      this.log.debug("en remotas el estado es " + estadosValvulasRemotas);
	      
	      this.log.debug("en programador el estado es " + estadosValvulasProgramador);
	      
	      String[] auxModos = arrayDeModos(estadosValvulasRemotas, Constantes.getNumeroMaximoDeValvulas());
	      String[] auxModosProgramador = arrayDeModos(estadosValvulasProgramador, Constantes.getNumeroMaximoDeValvulasProgramador());
	      auxModosProgramador[4] = "00";
	      for (int t = 0; t < programaciones; t++)
	      {
	        Bomba auxBomba = new Bomba();
	        auxBomba.setIdRemota(((Programacion)tramasProgramadas.get(t)).getIdRemota());
	        auxBomba.setIdValvula(((Programacion)tramasProgramadas.get(t)).getIdValvula());
	        
	        estaEnProgramador = false;
	        if (auxBomba.getIdRemota() == 0)
	        {
	          auxIdValvula = auxBomba.getIdValvula() - 1;
	          hayValvulasProgramador = true;
	          estaEnProgramador = true;
	        }
	        else
	        {
	          auxIdValvula = (auxBomba.getIdRemota() - 1) * 4 + (auxBomba.getIdValvula() - 1);
	          if (auxPrimera >= auxIdValvula)
	          {
	            auxPrimera = auxIdValvula;
	            
	            primeraBomba = auxBomba;
	          }
	          if (auxUltima < auxIdValvula) {
	            auxUltima = auxIdValvula;
	          }
	        }
	        switch (((Programacion)tramasProgramadas.get(t)).getDesAccion())
	        {
	        case 0: 
	          if (estaEnProgramador) {
	            auxModosProgramador[auxIdValvula] = Constantes.getManualCerrado();
	          } else {
	            auxModos[auxIdValvula] = Constantes.getManualCerrado();
	          }
	          bombasCerrar.add(auxBomba);
	          break;
	        case 1: 
	          if (estaEnProgramador) {
	            auxModosProgramador[auxIdValvula] = Constantes.getManualAbierto();
	          } else {
	            auxModos[auxIdValvula] = Constantes.getManualAbierto();
	          }
	          bombasAbrir.add(auxBomba);
	          break;
	        case 2: 
	          tramasParaEnviar.add(new Trama(((Programacion)tramasProgramadas.get(t)).getTrama(), 
	            auxBomba, 
	            Constantes.getComprobarFlujo(), 
	            2));
	          
	          break;
	        case 3: 
	          tramasParaEnviar.add(new Trama(((Programacion)tramasProgramadas.get(t)).getTrama(), 
	            auxBomba, 
	            Constantes.getComprobarNoFlujo(), 
	            2));
	          
	          break;
	        case 4: 
	          tramasParaEnviar.add(new Trama(((Programacion)tramasProgramadas.get(t)).getTrama(), 
	            auxBomba, 
	            Constantes.getComprobarFlujo(), 
	            3));
	          break;
	        case 5: 
	          tramasParaEnviar.add(new Trama(((Programacion)tramasProgramadas.get(t)).getTrama(), 
	            auxBomba, 
	            Constantes.getComprobarNoFlujo(), 
	            3));
	        }
	      }
	      if ((auxPrimera <= auxUltima) && (primeraBomba != null))
	      {
	        String[] modos = auxModos;
	        primeraBomba = new Bomba();
	        primeraBomba.setIdRemota(1);
	        primeraBomba.setIdValvula(1);
	        tramasParaEnviar.add(new Trama(GestTramasValvulas.getTramaAccionMultiple(primeraBomba, modos), 
	          null, 
	          0, 
	          0));
	      }
	      if (hayValvulasProgramador)
	      {
	        primeraBomba = new Bomba();
	        primeraBomba.setIdRemota(0);
	        primeraBomba.setIdValvula(1);
	        
	        tramasParaEnviar.add(new Trama(GestTramasValvulas.getTramaAccionMultiple(primeraBomba, auxModosProgramador), 
	          null, 
	          0, 
	          1));
	      }
	      if (tramasParaEnviar.size() > 0) {
	        valvula.envioMultiple(tramasParaEnviar);
	      }
	      Iterator<Bomba> it = bombasAbrir.iterator();
	      while (it.hasNext())
	      {
	        Bomba bomba = (Bomba)it.next();
	        valvula.actualizaEstadoValvula(bomba, "abierta");
	      }
	      it = bombasCerrar.iterator();
	      while (it.hasNext())
	      {
	        Bomba bomba = (Bomba)it.next();
	        valvula.actualizaEstadoValvula(bomba, "cerrada");
	      }
	      this.log.info("ejecutadas correctamente: " + programaciones + " programaciones de v�lvulas");
	    }
	    else
	    {
	      this.log.debug("No habia programaciones de v�lvulas");
	    }
	    con.cerrar();		
	}
	
	private void programacionesObjetosTelecontrol() {
		ConexionBBDD con = new ConexionBBDD();
		StringBuffer aviso = null;
		
		log.debug("comienza la ejecuci�n de objetos de telecontrol");
		//consulto las programaciones
	    List<ProgramacionTelecontrol> tramasProgramadas = con.consultarProgramacionesTelecontrol();
	    
	    int programaciones = tramasProgramadas.size();
	    if (programaciones > 0){
	    	ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
	    	ObjetosTelecontrol objetoTC = null;
			TipoObjetoTelecontrolDao _daoTOT = TipoObjetoTelecontrolDaoFactory.create();
	    	con = new ConexionBBDD();
		    
	    	for (int i=0;i<tramasProgramadas.size();i++){
	    		try{
	    			int idObjeto = tramasProgramadas.get(i).getIdObjeto();
	    			int accion = tramasProgramadas.get(i).getDesAccion();
	    			int idprogramacion = tramasProgramadas.get(i).getIdprogramacion();
	    			objetoTC = _dao.findByPrimaryKey(idObjeto);
	    			con.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO);
	    			
	    			if (objetoTC!=null){
			    		
			    		Telecontrol objTelecontrol = new Telecontrol(objetoTC);
			    		TipoObjetoTelecontrol tipoObjetoTC = _daoTOT.findByPrimaryKey(objetoTC.getIdTipoObjetoTelecontrol());
			    		int intentos = 0;
			    		boolean ejecutada = false;
		    			while (intentos++ < Constantes.getIntentosEjecucion() && !ejecutada) {
				    		if (accion==Telecontrol.ABRIR) {
				    			ejecutada = objTelecontrol.abrirObjeto();
				    			 this.log.debug("SE ABRE"+ intentos + " " + ejecutada);
				    		}else{
				    			if (accion==Telecontrol.CERRAR){
				    				ejecutada = objTelecontrol.cerrarObjeto();
				    				this.log.debug("SE CIERRA"+ intentos + " "  + ejecutada);
					    		}
							}
					  	    try
						    {
						        Thread.sleep(tipoObjetoTC.getTiempoRespuesta());
						    }
						    catch (InterruptedException e)
						    {
						        log.error(e.getMessage());
						    }
						    if(intentos == Constantes.getIntentosLecturaParaAviso() && !ejecutada){
						    	aviso = new StringBuffer();
						    	aviso.append("La instrucci�n de ")
						    		.append( Telecontrol.textoAccion[accion])
						    		.append(" sobre ")
						    		.append(objetoTC.getNombre())
						    		.append( " ha fallado " )
						    		.append( Constantes.getIntentosLecturaParaAviso() )
						    		.append( " veces, puede producirse un error.");
						    	log.warn(aviso.toString());
					    		con.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_ERROR);
						    }
					    		
			    		}
			    		if(ejecutada){
			    			con.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_CORRECTO);
			    		}
			    		else{
					    	aviso = new StringBuffer();
					    	aviso.append("La instrucci�n de ")
					    		.append(Telecontrol.textoAccion[accion])
					    		.append( " sobre ") 
			    				.append( objetoTC.getNombre() )
			    				.append( " no se ha podido ejecutar, intentelo manualmente.");
			    			Avisos.enviarAviso(aviso.toString() );
			    			log.warn(aviso.toString());
			    			con.actualizarEnvioObjeto(idprogramacion, Constantes.ENVIADO_ERROR);
			    		}
	    			}
		    		this.log.info("ejecutadas correctamente: " + programaciones + " programaciones de objetos de telecontrol");
	    			
	    			
	    		      try
	    		      {
	    		        Thread.sleep(Constantes.getPausaEjecucionProgramacion());
	    		      }
	    		      catch (InterruptedException e)
	    		      {
	    		        log.error(e.getMessage());
	    		      }

		    	}catch (ObjetosTelecontrolDaoException | TipoObjetoTelecontrolDaoException e){
	   	      		log.error(e.getMessage());
	   	      	}	   	      	
	    	}    	
	    }else{
	    	this.log.debug("No habia programaciones de objetos de telecontrol");
	    }
	    con.cerrar();
	}
	
	  private String[] arrayDeModos(String estadosValvulasRemotas, int i)
	  {
	    String[] auxModos = new String[i];
	    for (int m = 0; m < i; m++) {
	      auxModos[m] = estadosValvulasRemotas.substring(2 * m, 2 * (m + 1));
	    }
	    return auxModos;
	  }

	
}

package es.sronline.servicio;

import java.util.Timer;
import org.apache.log4j.Logger;

import es.sronline.aviso.Avisos;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.core.Constantes;
import es.sronline.telecontrol.Telecontrol;

public class ComprobarBombas extends Thread {
	Timer timer;
	int zona = 0;
	Logger log ;
	int comprobaciones;

	public ComprobarBombas(int a) {
		this.zona= a;
		log = Logger.getLogger("es.sronline");
	}

	
	public void run() {
		this.log.debug("********************  creada instancia de comprobar bombas **************");
		espera(Constantes.getEsperaEntreOrdenes());

		int intentos = 0;
		boolean abroRbom = false;
		boolean abro1 = false;
		//si la zona es la 1 y est�n abiertas B1 y B2 abro rebombeo
		if(this.zona == 1)
			while(intentos++ < Constantes.getIntentosEjecucion() && !(abroRbom)){
				this.log.debug("********************  intento "+ intentos + " **************");
				abroRbom = (estadoB1() == Telecontrol.ABRIR && estadoB2() == Telecontrol.ABRIR);
				if(abroRbom)
					abrirRebombeo();
				espera(Constantes.getEsperaEntreOrdenes());
			}
		//si la zona es la 2 y est�n abiertas B2 y rebombeo entonces abro B1 y
		//cuando est� abierta B1 abro B3
		if(this.zona == 2)
			while(intentos++ < Constantes.getIntentosEjecucion() && !(abro1)){
				this.log.debug("********************  intento "+ intentos + " **************");
				abro1 = (estadoB2() == Telecontrol.ABRIR && estadoRebombeo() == Telecontrol.ABRIR);
				if(abro1)
					abrirB1yB3();
				espera(Constantes.getEsperaEntreOrdenes());
			}
	}

	private void abrirB1yB3() {
		int intentos = 0;
		boolean abro3 = false;
		
    	ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
    	ObjetosTelecontrol objetoTC = null;
	   	boolean resultado = false;
	
		try {
			objetoTC = _dao.findByPrimaryKey(1);
		} catch (ObjetosTelecontrolDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
//			return false;
		}
		
		resultado = ejecutaObjeto(objetoTC, Telecontrol.ABRIR);
		this.log.info("abrir boma1 y 3 es " + resultado);
			
		espera(Constantes.getEsperaEntreOrdenes());
				
		while(intentos++ < Constantes.getIntentosEjecucion() && !(abro3)){
			abro3 = (estadoB1() == Telecontrol.ABRIR && estadoB2() == Telecontrol.ABRIR);
			if(abro3)
				abrirB3();
			espera(Constantes.getEsperaEntreOrdenes());
		}
		
	}

	private void abrirB3() {
	
    	ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
    	ObjetosTelecontrol objetoTC = null;
	   	boolean resultado = false;
	
		try {
			objetoTC = _dao.findByPrimaryKey(2);
		} catch (ObjetosTelecontrolDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
//			return false;
		}
		
		resultado = ejecutaObjeto(objetoTC, Telecontrol.ABRIR);
		this.log.info("abrir bomba3 es " + resultado);

	}

	private void abrirRebombeo() {
		
    	ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
    	ObjetosTelecontrol objetoTC = null;
	   	boolean resultado = false;
	
		try {
			objetoTC = _dao.findByPrimaryKey(19);
		} catch (ObjetosTelecontrolDaoException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
//			return false;
		}
		
		resultado = ejecutaObjeto(objetoTC, Telecontrol.ABRIR);
		this.log.info("abrir rebombeo es " + resultado);

	}

	private int estadoB2() {
		return estadoBomba(18);
	}

	private int estadoB1() {
		return estadoBomba(1);	}

	private int estadoRebombeo() {
		return estadoBomba(19);	}

	private int estadoBomba(int id) {
	    	ObjetosTelecontrolDao _dao = ObjetosTelecontrolDaoFactory.create();
	    	ObjetosTelecontrol objetoTC = null;
		   	String resultado = "";
		
			try {
				objetoTC = _dao.findByPrimaryKey(id);
			} catch (ObjetosTelecontrolDaoException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
	//			return false;
			}
			while(!(resultado.equals(Constantes.getManualCerrado()) || resultado.equals(Constantes.getManualAbierto()))){
				resultado = consultarObjeto(objetoTC);
				espera(Constantes.getEsperaEntreOrdenes());
			}
			if(resultado.equalsIgnoreCase(Constantes.getManualCerrado()))
				return Telecontrol.CERRAR;
			if(resultado.equalsIgnoreCase(Constantes.getManualAbierto()))
				return Telecontrol.ABRIR;
			return 0;
		}


	private boolean ejecutaObjeto(ObjetosTelecontrol objetoTC, int accion) {
//    	ObjTelecontrol objTC = null;
		boolean ejecutada = false;
		StringBuffer aviso=null;
		

		if (objetoTC!=null){
//			objTC = new ObjTelecontrol();
//			objTC.setNombre(objetoTC.getNombre());
//			objTC.setIdObjeto(objetoTC.getIdObjeto()+"");
//			objTC.setEstado(objetoTC.getEstado());
//			objTC.setPosicion(objetoTC.getPosicion());
//			objTC.setDireccionHex(objetoTC.getDireccionHex());
//			objTC.setTipoConexion(objetoTC.getTipoConexion());
//			objTC.setDatosConexion(objetoTC.getConexion());
    		
    		Telecontrol objTelecontrol = new Telecontrol(objetoTC);
    		int intentos = 0;
			while (intentos++ < Constantes.getIntentosEjecucion() && !ejecutada) {
	    		if (accion==Telecontrol.ABRIR) {
	    			ejecutada = objTelecontrol.abrirObjeto();
	    			 this.log.debug("SE ABRE"+ intentos + " " + ejecutada);
	    		}else{
	    			if (accion==Telecontrol.CERRAR){
	    				ejecutada = objTelecontrol.cerrarObjeto();
	    				this.log.debug("SE CIERRA"+ intentos + " "  + ejecutada);
		    		}
				}
	    		if(intentos == Constantes.getIntentosLecturaParaAviso() && !ejecutada){
	    			aviso = new StringBuffer();
	    			aviso.append("La instrucci�n de ")
	    				.append(Telecontrol.textoAccion[accion])
   						.append(" sobre ")
   						.append(objetoTC.getNombre())
   						.append(" ha fallado ")
   						.append(Constantes.getIntentosLecturaParaAviso())
   						.append(" veces, puede producirse un error.");
	    			Avisos.enviarAviso(aviso.toString());
	    			log.warn(aviso.toString());
	    		}
    		}
    		if(!ejecutada)
    			aviso = new StringBuffer();
    			aviso.append("La instrucci�n de ")
    				.append(Telecontrol.textoAccion[accion])
    				.append(" sobre ")
    				.append(objetoTC.getNombre())
    				.append(" no se ha podido ejecutar, intentelo manualmente.");
    			Avisos.enviarAviso(aviso.toString());
    			log.warn(aviso.toString());
		}
		return ejecutada;
	}

	private String consultarObjeto(ObjetosTelecontrol objetoTC) {
//    	ObjTelecontrol objTC = null;
		String ejecutada = "";

		if (objetoTC!=null){
//			objTC = new ObjTelecontrol();
//			objTC.setNombre(objetoTC.getNombre());
//			objTC.setIdObjeto(objetoTC.getIdObjeto()+"");
//			objTC.setEstado(objetoTC.getEstado());
//			objTC.setPosicion(objetoTC.getPosicion());
//			objTC.setDireccionHex(objetoTC.getDireccionHex());
//			objTC.setTipoConexion(objetoTC.getTipoConexion());
//			objTC.setDatosConexion(objetoTC.getConexion());
    		
    		Telecontrol objTelecontrol = new Telecontrol(objetoTC);
    		int intentos = 0;
			while (intentos++ < Constantes.getIntentosEjecucion() && (ejecutada.equals(""))) {
    			ejecutada = objTelecontrol.estadoObjeto();
    			 this.log.debug("estado: " + ejecutada);
    		}
		}
		
		return ejecutada;
	}


	private boolean espera(int i){
	    try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	    return true;
	
	}
}

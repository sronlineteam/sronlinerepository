package es.sronline.tarifas.electrica;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.LstTarifasElectricasDao;
import es.sronline.bbdd.dto.LstTarifasElectricas;
import es.sronline.bbdd.exceptions.LstTarifasElectricasDaoException;
import es.sronline.bbdd.factory.LstTarifasElectricasDaoFactory;

public  class ControlTarifaElectrica {
	public static final int NO_PERMITIR = 3;
	public static final int CONSULTAR = 2;
	public static final int PERMITIR = 1;
	public static final String AVISO_CONSULTAR = "CONTROL TARIFA: Confirme Apertura";
	public static final String AVISO_NO_PERMITIR = "CONTROL TARIFA: NO ABRIR AHORA!!";
	
	public static int controlar(Date diaHora){
		LstTarifasElectricasDao _dao = LstTarifasElectricasDaoFactory.create();
		LstTarifasElectricas[] lstTarifasElectricas = null;
		SimpleDateFormat format = new SimpleDateFormat("HH:MM:SS");
		String hora = "0000-00-00 " + format.format(diaHora.getTime());
		boolean isFestivo = false;
		int numeroDia;
		String where = " fecha < ? AND Hora_Inicio < ? AND Hora_Fin > ? ORDER BY fecha ASC ";
		Calendar cal= Calendar.getInstance();
	    cal.setTime(diaHora);
	    numeroDia=cal.get(Calendar.DAY_OF_WEEK);
		
		if(numeroDia==1 || numeroDia==7)
			where = "festivo = 1 AND " + where;
		

		try {
			lstTarifasElectricas = _dao.findByDynamicWhere(" fecha < ? AND Hora_Inicio < ? AND Hora_Fin > ? ORDER BY fecha ASC ", new Object[] {diaHora, hora, hora} );
			return lstTarifasElectricas[0].getIdTarifasElectricas();
		} catch (LstTarifasElectricasDaoException e) {
			// TODO Auto-generated catch block
			Logger.getLogger(ControlTarifaElectrica.class.getName()).error(e.getMessage());
		}
		
		return 3;
		
	}
	

}

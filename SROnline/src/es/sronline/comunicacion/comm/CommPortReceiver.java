package es.sronline.comunicacion.comm;

import java.io.IOException;  
import java.io.InputStream;

import org.apache.log4j.Logger;  
   
public class CommPortReceiver extends Thread {  
   
    InputStream in;  
    Protocol protocol = new ProtocolImpl();  
   
	public CommPortReceiver(InputStream in) {  
        this.in = in;  
    }  
      
    public void run() {  
        try {  
            int b;  
            while(true) {  
                  
                // if stream is not bound in.read() method returns -1  
                while((b = in.read()) != -1) {  
                    protocol.onReceive((byte) b);  
                }  
                protocol.onStreamClosed();  
                  
                // wait 10ms when stream is broken and check again  
                sleep(10);  
            }  
        } catch (IOException e) {  
        	Logger.getLogger(getClass().getName()).error(e.getMessage());  
        } catch (InterruptedException e) {  
        	Logger.getLogger(getClass().getName()).error(e.getMessage());  
        }   
    }  
}  
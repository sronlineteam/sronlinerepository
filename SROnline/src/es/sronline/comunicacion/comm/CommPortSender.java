package es.sronline.comunicacion.comm;

import java.io.IOException;  
import java.io.OutputStream;

import org.apache.log4j.Logger;  
   
public class CommPortSender {  
   
    static OutputStream out;  
      
    public static void setWriterStream(OutputStream out) {  
        CommPortSender.out = out;
    }  
      
    public static void send(byte[] bytes) {  
        try {  
            System.out.println("SENDING: " + new String(bytes, 0, bytes.length));  
              
            // sending through serial port is simply writing into OutputStream  
            out.write(bytes);  
            out.flush();  
            System.out.println("SEND OK!!");  
        } catch (IOException e) {  
        	Logger.getLogger("es.sronline.comuncicacion.comm.CommPortSender").error(e.getMessage());  
        }  
    }  
}  
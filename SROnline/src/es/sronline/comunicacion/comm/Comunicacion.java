package es.sronline.comunicacion.comm;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.core.Constantes;
import es.sronline.core.GestTramasValvulas;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

	/**
	*
	*/

	public class Comunicacion implements FlujoComunicacion {

	Socket sc;
	OutputStream salida;
	DataInputStream entrada;
	Logger log;
//	CommPortReceiver cpReceiver;
	DataInputStream respuestaProg;
	CommPortIdentifier portIdentifier;
	SerialPort serialPort;
	int tiempoRespuesta = Constantes.getEsperaEntreIntentosLectura();
	
	public Comunicacion(){
		log = Logger.getLogger(getClass().getName());
	}


	private void conectar() {
        try {
			portIdentifier = CommPortIdentifier.getPortIdentifier(Constantes.getNombrePuerto());
       
	        if (portIdentifier.isCurrentlyOwned()) {  
	            log.error("Port in use!");  
	        } else {  
	            // points who owns the port and connection timeout  
	            serialPort = (SerialPort) portIdentifier.open("CommunicationFlow", 2000);  
	              
	            // setup connection parameters  
	            serialPort.setSerialPortParams(  
	           // 		19200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
	            		Constantes.getBitsPorSeg(), Constantes.getDataBits(), Constantes.getBitsParada(), Constantes.getParidad());
	   
				salida = new DataOutputStream(serialPort.getOutputStream());
				entrada = new DataInputStream(serialPort.getInputStream());
	
	        }   
		}catch(Exception e )

		{

			log.error("Error: "+e.getMessage());

		}		

        }

	@Override
	public String enviar(String mensaje) {
		//enviamos el mensaje
	    String respuesta = "";
	    
	    boolean esperando = true;
	    int intentos = 0;
	    byte[] tramaBytes = GestTramasValvulas.transformaByte(mensaje);
	    try
	    {
	      conectar();
	      log.debug("voy a escribir!!" + tramaBytes);
	      
	      this.salida.write(tramaBytes);
	      this.salida.flush();
	      log.debug("He escrito y voy a leer");
	      while ((esperando) && (intentos < Constantes.getIntentosLectura()))
	      {
	        intentos++;
	        log.debug("Esperando respuesta...");
	        Thread.sleep(tiempoRespuesta);
	        if (this.entrada.available() > 0)
	        {
	          byte[] respuestaByte = new byte[this.entrada.available()];
	          this.entrada.read(respuestaByte);
	          respuesta = GestTramasValvulas.bytesToHex(respuestaByte);
	          esperando = false;
	        }
	      }
	      this.entrada.close();
	      log.debug("Se ha leido:" + respuesta);
	      desconectar();
	    
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return respuesta;
	}
	
	private void desconectar() {
		try

		{
		      log.debug("cerrando socket...");
		      this.entrada.close();
		      this.salida.close();
		      this.serialPort.close();
		}catch(Exception e )

		{

			log.error("Error: "+e.getMessage());

		}		
	}


	@Override
	public String enviar(String mensaje, String comunicación) {
		// TODO Auto-generated method stub
		return enviar(mensaje);
	}


	@Override
	public String enviar(String mensaje, String comunicación, int tRespuesta) {
		 tiempoRespuesta = tRespuesta;
		return enviar(mensaje);
	}

}

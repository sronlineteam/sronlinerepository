package es.sronline.comunicacion.comm;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.core.Constantes;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class CommunicationFlow implements FlujoComunicacion {	
	Logger log;
//	CommPortReceiver cpReceiver;
	DataInputStream respuestaProg;
	CommPortIdentifier portIdentifier;
	SerialPort serialPort;
	int tiempoRespuesta = Constantes.getEsperaEntreIntentosLectura();
	String nombrePuerto = Constantes.getNombrePuerto();
	
	public CommunicationFlow(){
		log = Logger.getLogger(getClass().getName());
	}

	
    private void connect(String portName) throws Exception {  
        portIdentifier = CommPortIdentifier.getPortIdentifier(portName);  
   
        if (portIdentifier.isCurrentlyOwned()) {  
            log.error("Port in use!");  
        } else {  
            // points who owns the port and connection timeout  
            serialPort = (SerialPort) portIdentifier.open("CommunicationFlow", 2000);  
              
            // setup connection parameters  
            serialPort.setSerialPortParams(  
           // 		19200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            		Constantes.getBitsPorSeg(), Constantes.getDataBits(), Constantes.getBitsParada(), Constantes.getParidad());
   
            // setup serial port writer  
            CommPortSender.setWriterStream(serialPort.getOutputStream());  
              
            // setup serial port reader  
//            cpReceiver = new CommPortReceiver(serialPort.getInputStream());
//            cpReceiver.start();
            //new CommPortReceiver(serialPort.getInputStream()).start();
            
        }  
    }  

	


	/**
	 * This is the main method which implements the flow.
	 */
	public String send(String hexRequestData) throws Exception /* this is the only exception thrown by RS232Java */{
		String sbRespuesta = "";
		InputStream io;
		InputStreamReader isr;
		BufferedReader bfReder;

		//preparo la trama para el canal de comunicación
		byte[] tramaByte = transformaByte(hexRequestData);
		
		//creo la conexión de  
		connect(nombrePuerto);
		
		log.info("trama: " + hexRequestData);
		
		io = serialPort.getInputStream();
		
	
		
//		io = cpReceiver.in;

//		isr = new InputStreamReader(io);
		//isr = new InputStreamReader(cpReceiver.in);
//	    String inputLine;
	
//		bfReder = new BufferedReader(isr);
		
		CommPortSender.send(tramaByte);

        StringBuffer readBuffer = new StringBuffer();
        byte[] buffer = new byte[1024];
        int len = -1;
        try
        {
            while ( ( len = io.read(buffer)) > -1 )
            {
            	readBuffer.append(len);
            }
            sbRespuesta = readBuffer.toString();
        }
        catch ( IOException e )
        {
            log.error(e.getMessage());
        }            
		//cpReceiver.sleep(500);
		
		
//		while ((inputLine = bfReder.readLine()) != null) 
//	        System.out.println(inputLine);
//	    bfReder.close();

		
//		sbRespuesta = inputLine;
		
		log.debug("trama respuesta: " + sbRespuesta);

		//cierro la conexion
		CommPortSender.out.close();
		io.close();

		serialPort.close();
		serialPort = null;
		
		return sbRespuesta;

		//log.debug("Respuesta: " + ((ProtocolImpl) cpReceiver.protocol).respuesta);
//		byte[] byteRespuesta = ((ProtocolImpl) cpReceiver.protocol).getMessage("");
//		log.debug("recogido array de byte de longitud:" + byteRespuesta.length); 
		 
	}

	/**
	 * This is the main method which implements the flow.
	 */
//	public byte[] send(byte[] tramaByte) throws Exception /* this is the only exception thrown by RS232Java */{
//		new CommunicationFlow().connect(Constantes.getNombrePuerto());
//		
//		log.debug("trama: " + tramaByte);
//		
//		
//		CommPortSender.send(tramaByte);
//		//cpReceiver.sleep(500);
//		//log.debug("Respuesta: " + ((ProtocolImpl) cpReceiver.protocol).respuesta);
//		byte[] byteRespuesta = ((ProtocolImpl) cpReceiver.protocol).getMessage("");
//		log.debug("recogido array de byte de longitud:" + byteRespuesta.length + " " + byteRespuesta);
//		
//		return byteRespuesta;
//		 
//	}



	private byte[] transformaByte(String hexRequestData) {
		byte[] resultado = new byte[8];
		String aux;

		for (int i = 0; i < 8; i++){
			aux = hexRequestData.substring(2*i, 2*i + 2);
			resultado[i] = Integer.valueOf(aux, 16).byteValue();
		}
		
		return resultado;
	}


	@Override
	public String enviar(String mensaje) {
		String resultado = "error";
		try {
			resultado = send(mensaje);
		} catch (Exception e) {
			log.info("error al abrir " + e.getMessage());
			log.error(e);
		}
		return resultado;
	}


	@Override
	public String enviar(String mensaje, String comunicacion) {
		// TODO Auto-generated method stub
		nombrePuerto = comunicacion;
		return enviar(mensaje);
	}


	@Override
	public String enviar(String mensaje, String comunicación, int tRespuesta) {
		tiempoRespuesta = tRespuesta;
		return null;
	}

	
}

package es.sronline.comunicacion.http;

import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.core.Constantes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;


public class ComunicacionHttp implements FlujoComunicacion {

	int tiempoRespuesta = Constantes.getEsperaEntreIntentosLectura();

	@Override
	@Deprecated
	public String enviar(String mensaje) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String enviar(String mensaje, String comunicacion) {
		return enviar(mensaje, comunicacion, tiempoRespuesta);
	}
	@Override
	public String enviar(String mensaje, String comunicacion, int tRespuesta) {
		//mensaje es la trama que se envia al servicio HTTP con el idObjeto, comando y la accion
		//mensaje=AAAANNNN
		//comunicacion es el protocolo, la direcci�n, puerto y recurso (y opcionalmente usuario y contrase�a)
		//tRespuesta es el tiempo a esperar hasta recibir una respuesta
		
        URL url;
        String respuesta = "KO";
        StringBuffer auxRespuesta = new StringBuffer();
		
        try {
			url = new URL(comunicacion);
	        Map<String, Object> params = new LinkedHashMap<>();
	 
	        params.put("idObjeto", mensaje.substring(0,14).replace("-", ""));
	        params.put("comando", mensaje.substring(14, 16));
	        params.put("accion", mensaje.substring(20,22));
	 
	        StringBuilder postData = new StringBuilder();
	        for (Map.Entry<String, Object> param : params.entrySet()) {
	            if (postData.length() != 0)
	                postData.append('&');
					postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
					postData.append('=');
					postData.append(URLEncoder.encode(String.valueOf(param.getValue()),
					        "UTF-8"));

	        }
	        byte[] postDataBytes;
				postDataBytes = postData.toString().getBytes("UTF-8");
	 
	        HttpURLConnection conn;
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
		        conn.setRequestProperty("Content-Type",
		                "application/x-www-form-urlencoded");
		        conn.setRequestProperty("Content-Length",
		                String.valueOf(postDataBytes.length));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
		    
	        Thread.sleep(tiempoRespuesta);
		     
	        Reader in = new BufferedReader(new InputStreamReader(
	                conn.getInputStream(), "UTF-8"));
	        for (int c = in.read(); c != -1; c = in.read()){
	            auxRespuesta.append((char) c);
	        }
	        respuesta = auxRespuesta.toString();

		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return respuesta;
	}

}

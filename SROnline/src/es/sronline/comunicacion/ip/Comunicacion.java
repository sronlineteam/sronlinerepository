package es.sronline.comunicacion.ip;

import java.net.*;
import java.io.*;

import org.apache.log4j.Logger;

import es.sronline.comunicacion.FlujoComunicacion;
import es.sronline.core.Constantes;
import es.sronline.core.GestTramasValvulas;

	/**
	*
	*/

	public class Comunicacion implements FlujoComunicacion {

	Socket sc;
	DataOutputStream salida;
	DataInputStream entrada;
	int tiempoRespuesta = Constantes.getEsperaEntreIntentosLectura();
	Logger log;

	private void conectar() {
		log = Logger.getLogger(getClass().getName());
	
		conectar( Constantes.getHostIP() , Constantes.getPuertoIP() ); 
	}

	private void conectar(String ip, int puerto) {
		try

		{

		      for (;;)
		      {
		        log.debug("Intentando conectar??");
		        
		        this.sc = new Socket(ip, puerto);
		        if (this.sc.isConnected())
		        {
		          this.salida = new DataOutputStream(this.sc.getOutputStream());
		          this.entrada = new DataInputStream(this.sc.getInputStream());
		          break;
		        }
		        Thread.sleep(Constantes.getEsperaIntentoConexion());
		      }
		      log.debug("Conectado!! a " + ip + ":" + puerto);

		}catch(Exception e )

		{

			log.error("Error al conectar: "+e.getMessage());

		}		
	}
	@Override
	public String enviar(String mensaje) {
		return enviar(mensaje, null);
	}
	/**
	 * metodo para enviar trams y recoger su repuesta
	 * @param mensaje trama a enviar
	 * @param comunicacion cadena de conexi�n con los valores separados por '/', en esta implementacion ip/puerto
	 * @return respuesta leida
	 */
	public String enviar(String mensaje, String comunicacion) {
		//enviamos el mensaje
		String respuesta = "";
		

		byte[] respuestaByte;
		boolean esperando = true;
		int intentos = 0;
		byte[] tramaBytes = GestTramasValvulas.transformaByte(mensaje);
		try {
			if (comunicacion == null)
				conectar();
			else{
				String[] auxConexion = comunicacion.split("/");
				conectar(auxConexion[0], Integer.parseInt(auxConexion[1]));
			}
				
		     Thread.sleep(tiempoRespuesta);
		     log.debug("voy a escribir!!" + tramaBytes);
			//salida.writeUTF(mensaje);
		    if(salida != null){
				salida.write(tramaBytes);
				salida.flush();
				log.debug("He escrito y voy a leer");
				while(esperando && intentos < Constantes.getIntentosLectura()){
					intentos++;
					log.debug("Esperando respuesta...");
				    Thread.sleep(tiempoRespuesta);
				    if (entrada.available()>0){
					//respuesta = entrada.readUTF();
					respuestaByte = new byte[entrada.available()];
					entrada.read(respuestaByte);
					respuesta = GestTramasValvulas.bytesToHex(respuestaByte);
					
					esperando = false;
					}
				}
				entrada.close();
				log.debug("Se ha leido:" + respuesta);
				desconectar();
		    }
		    else{
		    	log.error("no se ha podido conectar a " + comunicacion);
		    	return "error";
		    }
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			return "error";
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			return "error";
		}
		
		return respuesta;
	}

	private void desconectar() {
		try

		{
			log.debug("cerrando socket...");
		      this.entrada.close();
		      this.salida.close();
		      this.sc.close();
		}catch(Exception e )

		{

			log.error("Error: "+e.getMessage());

		}		
	}

	@Override
	public String enviar(String mensaje, String comunicacion, int tRespuesta) {
		tiempoRespuesta = tRespuesta;
		return enviar(mensaje, comunicacion);
	}

}

package es.sronline.comunicacion;

public interface FlujoComunicacion {

	@Deprecated
	public String enviar(String mensaje);
	
	public String enviar(String mensaje, String comunicacion);

	public String enviar(String mensaje, String comunicacion, int tiempoRespuesta);

}

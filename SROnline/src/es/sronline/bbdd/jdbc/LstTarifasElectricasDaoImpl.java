
package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class LstTarifasElectricasDaoImpl extends AbstractDAO implements LstTarifasElectricasDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT Secuencia, Fecha, idTarifas_Electricas, Descripcion, Hora_Inicio, Hora_Fin, Tarifa FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( Secuencia, Fecha, idTarifas_Electricas, Descripcion, Hora_Inicio, Hora_Fin, Tarifa) VALUES ( ?, ?, ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET Secuencia = ?, Fecha = ?,idTarifas_Electricas = ?, Descripcion = ?, Hora_Inicio = ?, Hora_Fin = ?, Tarifa = ? WHERE Secuencia = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE Secuencia = ?";

	/** 
	 * Index of column Secuencia
	 */
	protected static final int COLUMN_SECUENCIA = 1;

	/** 
	 * Index of column Fecha
	 */
	protected static final int COLUMN_FECHA = 2;
	
	/** 
	 * Index of column idLstTarifasElectricas
	 */
	protected static final int COLUMN_IDTARIFAS_ELECTRICAS = 3;

	/** 
	 * Index of column Descripcion
	 */
	protected static final int COLUMN_DESCRIPCION = 4;

	/** 
	 * Index of column Hora_Inicio
	 */
	protected static final int COLUMN_HORA_INICIO = 5;

	/** 
	 * Index of column Hora_Fin
	 */
	protected static final int COLUMN_HORA_FIN = 6;

	/** 
	 * Index of column Tarifas
	 */
	protected static final int COLUMN_TARIFA = 7;

	
	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 7;

	/** 
	 * Index of primary-key column idLstTarifasElectricas
	 
	protected static final int PK_COLUMN_SECUENCIA = 1;
*/
	
	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria ''.
	 */
	public LstTarifasElectricas[] findAll() throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY Secuencia", null );
	}

	/** 
	 * Inserts a new row in the LstTarifasElectricas table.
	 
	public LstTarifasElectricasPk insert(LstTarifasElectricas dto) throws LstTarifasElectricasDaoImpl
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
            stmt.setInt( index++, dto.getSecuencia() );
			stmt.setTimestamp(index++, dto.getFecha()==null ? null : new java.sql.Timestamp( dto.getFecha().getTime() ) );
			stmt.setInt( index++, dto.getIdTarifasElectricas() );
			stmt.setString(index++, dto.getDescripcion() );
			stmt.setTimestamp(index++, dto.getHoraIncio()==null ? null : new java.sql.Timestamp( dto.getFecha().getTime() ) );
			stmt.setTimestamp(index++, dto.getHoraFin()==null ? null : new java.sql.Timestamp( dto.getFecha().getTime() ) );
            stmt.setInt( index++, dto.getTarifa() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdTarifasElectricas( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new LstTarifasElectricasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
		
	}
*/
	/** 
	 * Updates a single row in the LstTarifasElectricas table.
	 */
	public void update(LstTarifasElectricasPk pk, LstTarifasElectricas dto) throws LstTarifasElectricasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getSecuencia() );
			stmt.setTimestamp(index++, dto.getFecha()==null ? null : new java.sql.Timestamp( dto.getFecha().getTime() ) );			
			stmt.setInt( index++, dto.getIdTarifasElectricas() );
			stmt.setString(index++, dto.getDescripcion() );
			stmt.setTimestamp(index++, dto.getHoraInicio()==null ? null : new java.sql.Timestamp( dto.getHoraInicio().getTime() ) );
			stmt.setTimestamp(index++, dto.getHoraFin()==null ? null : new java.sql.Timestamp( dto.getHoraFin().getTime() ) );
            stmt.setInt( index++, dto.getTarifa() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new LstTarifasElectricasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the LstTarifasElectricas table.
	 */
	public void delete(LstTarifasElectricasPk pk) throws LstTarifasElectricasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getSecuencia() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new LstTarifasElectricasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the LstTarifasElectricas table that matches the specified primary-key value.
	 */
	public LstTarifasElectricas findByPrimaryKey(LstTarifasElectricasPk pk) throws LstTarifasElectricasDaoException
	{
		return findByPrimaryKey( pk.getSecuencia() );
	}

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'idLstTarifasElectricas = :idLstTarifasElectricas'.
	 */
	public LstTarifasElectricas findByPrimaryKey(int Secuencia) throws LstTarifasElectricasDaoException
	{
		LstTarifasElectricas ret[] = findByDynamicSelect( SQL_SELECT + " WHERE Secuencia = ?", new Object[] {  new Integer(Secuencia) } );
		return ret.length==0 ? null : ret[0];
	}

	
	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'Secuencia = :Secuencia'.
	 */
	public LstTarifasElectricas[] findWhereSecuenciaEquals(int Secuencia) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Secuencia = ? ORDER BY Secuencia", new Object[] {  new Integer(Secuencia) } );
	}
	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'fecha = :fecha'.
	 */
	public LstTarifasElectricas[] findWhereFechaEquals(Date Fecha) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Fecha = ? ORDER BY Fecha", new Object[] { Fecha==null ? null : new java.sql.Timestamp( Fecha.getTime() ) } );
	}
	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'idTarifas_Electricas = :idTarifas_Electricas'.
	 */
	public LstTarifasElectricas[] findWhereIdLstTarifasElectricasEquals(int idTarifas_Electricas) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idTarifas_Electricas = ? ORDER BY idTarifas_Electricas", new Object[] {  new Integer(idTarifas_Electricas) } );
	}
	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'Descripcion = :Descripcion'.
	 */
	public LstTarifasElectricas[] findWhereDescripcionEquals(int Descripcion) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Descripcion = ? ORDER BY Descripcion", new Object[] {  new Integer(Descripcion) } );
	}

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'Hora_Inicio = :HoraInicio'.
	 */
	public LstTarifasElectricas[] findWhereHoraInicioEquals(Date Hora_Inicio) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Hora_Inicio = ? ORDER BY Hora_Inicio", new Object[] { Hora_Inicio==null ? null : new java.sql.Timestamp( Hora_Inicio.getTime() ) } );
	}

	

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'des_accion = :HoraFin'.
	 */
	public LstTarifasElectricas[] findWhereHoraFinEquals(Date Hora_Fin) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Hora_Fin = ? ORDER BY Hora_Fin", new Object[] { Hora_Fin==null ? null : new java.sql.Timestamp( Hora_Fin.getTime() ) } );
		}

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the criteria 'Tarifa = :Tarifa'.
	 */
	public LstTarifasElectricas[] findWhereTarifaEquals(String Tarifa) throws LstTarifasElectricasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Tarifa = ? ORDER BY Tarifa", new Object[] { Tarifa } );
	}

	/**
	 * Method 'LstTarifasElectricasDaoImpl'
	 * 
	 */
	public LstTarifasElectricasDaoImpl()
	{
	}

	/**
	 * Method 'LstTarifasElectricasDaoImpl'
	 * 
	 * @param userConn
	 */
	public LstTarifasElectricasDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".Tarifas_Electricas";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected LstTarifasElectricas fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			LstTarifasElectricas dto = new LstTarifasElectricas();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected LstTarifasElectricas[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			LstTarifasElectricas dto = new LstTarifasElectricas();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		LstTarifasElectricas ret[] = new LstTarifasElectricas[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(LstTarifasElectricas dto, ResultSet rs) throws SQLException
	{
		dto.setSecuencia( rs.getInt( COLUMN_SECUENCIA) );
		dto.setFecha( rs.getTimestamp( COLUMN_FECHA ) );
		dto.setIdTarifasElectricas( rs.getInt( COLUMN_IDTARIFAS_ELECTRICAS) );
		dto.setDescripcion( rs.getString( COLUMN_DESCRIPCION ) );
		dto.setHoraInicio( rs.getTimestamp( COLUMN_HORA_INICIO ) );
		dto.setHoraFin( rs.getTimestamp( COLUMN_HORA_FIN ) );
		dto.setTarifa( rs.getInt( COLUMN_TARIFA ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(LstTarifasElectricas dto)
	{
	}

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the specified arbitrary SQL statement
	 */
	public LstTarifasElectricas[] findByDynamicSelect(String sql, Object[] sqlParams) throws LstTarifasElectricasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new LstTarifasElectricasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the LstTarifasElectricas table that match the specified arbitrary SQL statement
	 */
	public LstTarifasElectricas[] findByDynamicWhere(String sql, Object[] sqlParams) throws LstTarifasElectricasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new LstTarifasElectricasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	@Override
	public LstTarifasElectricas[] findWhereIdTarifasElectricasEquals(
			int idTarifasElectricas) throws LstTarifasElectricasDaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LstTarifasElectricasPk insert(LstTarifasElectricas dto)
			throws LstTarifasElectricasDaoException {
		// TODO Auto-generated method stub
		return null;
	}
}

	

package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class TipoObjetoTelecontrolDaoImpl extends AbstractDAO implements TipoObjetoTelecontrolDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idtipo_obj_telecontrol, tipo_obj_telecontrol, descripcion, tiempo_respuesta FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( tipo_obj_telecontrol, descripcion, tiempo_respuesta ) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET tipo_obj_telecontrol = ?, descripcion = ?, tiempo_respuesta = ? WHERE idtipo_obj_telecontrol = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idtipo_obj_telecontrol = ?";

	/** 
	 * Index of column idtipo_obj_telecontrol
	 */
	protected static final int COLUMN_ID_TIPO_OBJ_TELECONTROL= 1;

	/** 
	 * Index of column tipo_obj_telecontrol
	 */
	protected static final int COLUMN_TIPO_OBJ_TELECONTROL = 2;

	/** 
	 * Index of column descripcion
	 */
	protected static final int COLUMN_DESCRIPCION = 3;

	
	/** 
	 * Index of column tiempo de respuesta
	 */
	protected static final int COLUMN_TIEMPO_RESPUESTA = 4;
	
	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 4;

	/** 
	 * Index of primary-key column idtipo_obj_telecontrol
	 */
	protected static final int PK_COLUMN_ID_TIPO_OBJ_TELECONTROL = 1;

	/** 
	 * Inserts a new row in the tipo_obj_telecontrol table.
	 */
	public TipoObjetoTelecontrolPk insert(TipoObjetoTelecontrol dto) throws TipoObjetoTelecontrolDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdTipoObjetoTelecontrol() );
			stmt.setString( index++, dto.getTipoObjetoTelecontrol() );
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setInt( index++, dto.getTiempoRespuesta());
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdTipoObjetoTelecontrol( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new TipoObjetoTelecontrolDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the programacion table.
	 */
	public void update(TipoObjetoTelecontrolPk pk, TipoObjetoTelecontrol dto) throws TipoObjetoTelecontrolDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setString( index++, dto.getTipoObjetoTelecontrol() );
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setInt( index++, dto.getTiempoRespuesta() );
			stmt.setInt( index++, dto.getIdTipoObjetoTelecontrol() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new TipoObjetoTelecontrolDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the TipoObjetoTelecontrol table.
	 */
	public void delete(TipoObjetoTelecontrolPk pk) throws TipoObjetoTelecontrolDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdTipoObjetoTelecontrol() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new TipoObjetoTelecontrolDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the tipo_obj_telecontrol table that matches the specified primary-key value.
	 */
	public TipoObjetoTelecontrol findByPrimaryKey(TipoObjetoTelecontrolPk pk) throws TipoObjetoTelecontrolDaoException
	{
		return findByPrimaryKey( pk.getIdTipoObjetoTelecontrol());
	}

	/** 
	 * Returns all rows from the tipo_obj_telecontrol table that match the criteria 'idtipo_obj_telecontrol = :idtipo_obj_telecontrol'.
	 */
	public TipoObjetoTelecontrol findByPrimaryKey(int idtipo_obj_telecontrol) throws TipoObjetoTelecontrolDaoException
	{
		TipoObjetoTelecontrol[] ret = findByDynamicSelect( SQL_SELECT + " WHERE idtipo_obj_telecontrol = ?", new Object[] {  idtipo_obj_telecontrol } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the tipo_obj_telecontrol table that match the criteria ''.
	 */
	public TipoObjetoTelecontrol[] findAll() throws TipoObjetoTelecontrolDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idtipo_obj_telecontrol", null );
	}


	/**
	 * Method 'TipoObjetoTelecontrolDaoImpl'
	 * 
	 */
	public TipoObjetoTelecontrolDaoImpl()
	{
	}

	/**
	 * Method 'TipoObjetoTelecontrolDaoImpl'
	 * 
	 * @param userConn
	 */
	public TipoObjetoTelecontrolDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".tipo_obj_telecontrol";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected TipoObjetoTelecontrol fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			TipoObjetoTelecontrol dto = new TipoObjetoTelecontrol();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected TipoObjetoTelecontrol[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			TipoObjetoTelecontrol dto = new TipoObjetoTelecontrol();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		TipoObjetoTelecontrol ret[] = new TipoObjetoTelecontrol[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(TipoObjetoTelecontrol dto, ResultSet rs) throws SQLException
	{
		dto.setIdTipoObjetoTelecontrol( rs.getInt( COLUMN_ID_TIPO_OBJ_TELECONTROL ) );
		dto.setTipoObjetoTelecontrol(rs.getString( COLUMN_TIPO_OBJ_TELECONTROL ) );
		dto.setDescripcion(rs.getString( COLUMN_DESCRIPCION) );
		dto.setTiempoRespuesta(rs.getInt(COLUMN_TIEMPO_RESPUESTA));
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(TipoObjetoTelecontrol dto)
	{
	}

	/** 
	 * Returns all rows from the tipo_obj_telecontrol table that match the specified arbitrary SQL statement
	 */
	public TipoObjetoTelecontrol[] findByDynamicSelect(String sql, Object[] sqlParams) throws TipoObjetoTelecontrolDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new TipoObjetoTelecontrolDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the tipo_obj_telecontrol table that match the specified arbitrary SQL statement
	 */
	public TipoObjetoTelecontrol[] findByDynamicWhere(String sql, Object[] sqlParams) throws TipoObjetoTelecontrolDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new TipoObjetoTelecontrolDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

}

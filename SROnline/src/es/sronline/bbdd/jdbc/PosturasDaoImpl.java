package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class PosturasDaoImpl extends AbstractDAO implements PosturasDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idposturas, duracion, idprogramacion_postura, idparcelas, observaciones FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idposturas, duracion, idprogramacion_postura, idparcelas, observaciones) VALUES ( ?, ?, ?, ?, ?)";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idposturas = ?, duracion = ?, idprogramacion_postura = ?, idparcelas = ?, observaciones = ? WHERE idposturas = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idposturas = ?";

	/** 
	 * Index of column idposturas
	 */
	protected static final int COLUMN_IDPOSTURAS = 1;

	/** 
	 * Index of column duracion
	 */
	protected static final int COLUMN_DURACION = 2;

	/** 
	 * Index of column idprogramacion_postura
	 */
	protected static final int COLUMN_IDPROGRAMACION_POSTURA = 3;

	/** 
	 * Index of column idparcelas
	 */
	protected static final int COLUMN_IDPARCELAS = 4;

	/** 
	 * Index of column observaciones
	 */
	protected static final int COLUMN_OBSERVACIONES = 5;

	
	protected static final int NUMBER_OF_COLUMNS = 5;

	/** 
	 * Index of primary-key column idposturas
	 */
	protected static final int PK_COLUMN_IDPOSTURAS = 1;

	/** 
	 * Inserts a new row in the posturas table.
	 */
	public PosturasPk insert(Posturas dto) throws PosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdposturas() );
			stmt.setInt( index++, dto.getDuracion() );
			stmt.setInt( index++, dto.getIdprogramacion_postura() );
			stmt.setInt( index++, dto.getIdParcelas() );
			stmt.setString( index++, dto.getObservaciones() );
			// stmt.setTimestamp(index++, dto.getFechaHora()==null ? null : new java.sql.Timestamp( dto.getFechaHora().getTime() ) );
		//	stmt.setInt( index++, dto.getDesAccion() );
		//	stmt.setString( index++, dto.getTrama() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdposturas( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the posturas table.
	 */
	public void update(PosturasPk pk, Posturas dto) throws PosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdposturas() );
			stmt.setInt( index++, dto.getDuracion() );
			stmt.setInt( index++, dto.getIdprogramacion_postura() );
			stmt.setInt( index++, dto.getIdParcelas() );
			stmt.setString( index++, dto.getObservaciones() );
			stmt.setInt( index++, dto.getIdposturas() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the posturas table.
	 */
	public void delete(PosturasPk pk) throws PosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdposturas() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the posturas table that matches the specified primary-key value.
	 */
	public Posturas findByPrimaryKey(PosturasPk pk) throws PosturasDaoException
	{
		return findByPrimaryKey( pk.getIdposturas() );
	}

	/** 
	 * Returns all rows from the posturas table that match the criteria 'idposturas = :idposturas'.
	 */
	public Posturas findByPrimaryKey(int idposturas) throws PosturasDaoException
	{
		Posturas ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idposturas = ?", new Object[] {  new Integer(idposturas) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the posturas table that match the criteria ''.
	 */
	public Posturas[] findAll() throws PosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idposturas", null );
	}

	/** 
	 * Returns all rows from the posturas table that match the criteria 'idposturas = :idposturas'.
	 */
	public Posturas[] findWhereIdPosturasEquals(int idposturas) throws PosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idposturas = ? ORDER BY idposturas", new Object[] {  new Integer(idposturas) } );
	}



	/**
	 * Method 'PosturasDaoImpl'
	 * 
	 */
	public PosturasDaoImpl()
	{
	}

	/**
	 * Method 'PosturasDaoImpl'
	 * 
	 * @param userConn
	 */
	public PosturasDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".posturas";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected Posturas fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Posturas dto = new Posturas();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Posturas[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Posturas dto = new Posturas();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Posturas ret[] = new Posturas[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Posturas dto, ResultSet rs) throws SQLException
	{
		dto.setIdposturas( rs.getInt( COLUMN_IDPOSTURAS ) );
		dto.setDuracion( rs.getInt( COLUMN_DURACION ) );
		dto.setIdprogramacion_postura( rs.getInt( COLUMN_IDPROGRAMACION_POSTURA ) );
		dto.setIdParcelas(rs.getInt(COLUMN_IDPARCELAS));
		dto.setObservaciones(rs.getString(COLUMN_OBSERVACIONES)==null? "" : rs.getString(COLUMN_OBSERVACIONES));
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Posturas dto)
	{
	}

	/** 
	 * Returns all rows from the posturas table that match the specified arbitrary SQL statement
	 */
	public Posturas[] findByDynamicSelect(String sql, Object[] sqlParams) throws PosturasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the posturas table that match the specified arbitrary SQL statement
	 */
	public Posturas[] findByDynamicWhere(String sql, Object[] sqlParams) throws PosturasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	@Override
	public Posturas[] findWhereIdprogramacion_posturaEquals(
			int idprogramacion_postura) throws PosturasDaoException {
		// TODO Auto-generated method stub
		return findByDynamicSelect( SQL_SELECT + " WHERE idprogramacion_postura = ? ORDER BY idposturas", new Object[] {  new Integer(idprogramacion_postura) } );
	}

	@Override
	public int countByIdprogramacion_posturaEquals(int idprogramacion_postura) throws PosturasDaoException {
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = "SELECT count(*) FROM " + getTableName() + " where idprogramacion_postura =";
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( 1 );
		
			// bind parameters
			stmt.setObject( 1, idprogramacion_postura );
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return rs.getInt(1);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new PosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		}
	}

}
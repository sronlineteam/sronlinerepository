package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ProgramacionPosturasDaoImpl extends AbstractDAO implements ProgramacionPosturasDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idprogramacion_postura, idsector, fecha, minutos FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idprogramacion_postura, idsector, fecha, minutos ) VALUES ( ?, ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idprogramacion_postura = ?, idsector = ?, fecha = ?, minutos = ? WHERE idprogramacion_postura = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idprogramacion_postura = ?";

	/** 
	 * Index of column idprogramacion_postura
	 */
	protected static final int COLUMN_IDPROGRAMACION_POSTURA = 1;

	/** 
	 * Index of column idsector
	 */
	protected static final int COLUMN_IDSECTOR = 2;

	/** 
	 * Index of column fecha
	 */
	protected static final int COLUMN_FECHA = 3;

	/** 
	 * Index of column minutos
	 */
	protected static final int COLUMN_MINUTOS = 4;

	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 4;

	/** 
	 * Index of primary-key column idprogramacion_postura
	 */
	protected static final int PK_COLUMN_IDPROGRAMACION_POSTURA = 1;

	/** 
	 * Inserts a new row in the programacion table.
	 */
	public ProgramacionPosturasPk insert(ProgramacionPosturas dto) throws ProgramacionPosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdprogramacion_Postura() );
			stmt.setInt( index++, dto.getidsector());
			stmt.setTimestamp(index++, dto.getfecha()==null ? null : new java.sql.Timestamp( dto.getfecha().getTime() ) );
			stmt.setInt( index++, dto.getminutos() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdprogramacion_Postura( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ProgramacionPosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the programacion table.
	 */
	public void update(ProgramacionPosturasPk pk, ProgramacionPosturas dto) throws ProgramacionPosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdprogramacion_Postura());
			stmt.setInt( index++, dto.getidsector());
			stmt.setTimestamp(index++, dto.getfecha()==null ? null : new java.sql.Timestamp( dto.getfecha().getTime() ) );
			stmt.setInt( index++, dto.getminutos() );
			stmt.setInt( index++, dto.getIdprogramacion_Postura());
			
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ProgramacionPosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the programacion table.
	 */
	public void delete(ProgramacionPosturasPk pk) throws ProgramacionPosturasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getidprogramacion_postura());
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ProgramacionPosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the Programacion_Postura table that matches the specified primary-key value.
	 */
	public ProgramacionPosturas findByPrimaryKey(ProgramacionPosturasPk pk) throws ProgramacionPosturasDaoException
	{
		return findByPrimaryKey( pk.getidprogramacion_postura());
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'idprogramacion = :idprogramacion'.
	 */
	public ProgramacionPosturas findByPrimaryKey(int Idprogramacion_Postura) throws ProgramacionPosturasDaoException
	{
		ProgramacionPosturas ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idprogramacion_postura = ?", new Object[] {  new Integer(Idprogramacion_Postura) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria ''.
	 */
	public ProgramacionPosturas[] findAll() throws ProgramacionPosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idprogramacion_postura", null );
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'idprogramacion = :idprogramacion'.
	 */
	public ProgramacionPosturas[] findWhereIdprogramacion_PosturasEquals(int idprogramacion_Postura) throws ProgramacionPosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idprogramacion_postura = ? ORDER BY idprogramacion_postura", new Object[] {  new Integer(idprogramacion_Postura) } );
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'id_remota = :idRemota'.
	 */
	public ProgramacionPosturas[] findWhereIdSectorEquals(int idsector) throws ProgramacionPosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idsector = ? ORDER BY idsector", new Object[] {  new Integer(idsector) } );
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'minutos = :minutos'.
	 */
	public ProgramacionPosturas[] findWhereminutosEquals(int minutos) throws ProgramacionPosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE minutos = ? ORDER BY minutos", new Object[] {  new Integer(minutos) } );
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'fecha = :fecha'.
	 */
	public ProgramacionPosturas[] findWhereminutosEquals(Date fecha) throws ProgramacionPosturasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE fecha = ? ORDER BY fecha", new Object[] { fecha==null ? null : new java.sql.Timestamp( fecha.getTime() ) } );
	}

	
	/**
	 * Method 'ProgramacionDaoImpl'
	 * 
	 */
	public ProgramacionPosturasDaoImpl()
	{
	}

	/**
	 * Method 'ProgramacionDaoImpl'
	 * 
	 * @param userConn
	 */
	public ProgramacionPosturasDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".Programacion_Posturas";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected ProgramacionPosturas fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			ProgramacionPosturas dto = new ProgramacionPosturas();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected ProgramacionPosturas[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			ProgramacionPosturas dto = new ProgramacionPosturas();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		ProgramacionPosturas ret[] = new ProgramacionPosturas[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(ProgramacionPosturas dto, ResultSet rs) throws SQLException
	{
		dto.setIdprogramacion_Postura( rs.getInt( COLUMN_IDPROGRAMACION_POSTURA ) );
		dto.setidsector( rs.getInt( COLUMN_IDSECTOR ) );
		dto.setfecha( rs.getTimestamp(COLUMN_FECHA ) );
		dto.setminutos( rs.getInt( COLUMN_MINUTOS ) );
	}
	
	

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(ProgramacionPosturas dto)
	{
	}

	/** 
	 * Returns all rows from the programacion table that match the specified arbitrary SQL statement
	 */
	public ProgramacionPosturas[] findByDynamicSelect(String sql, Object[] sqlParams) throws ProgramacionPosturasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ProgramacionPosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the programacion table that match the specified arbitrary SQL statement
	 */
	public ProgramacionPosturas[] findByDynamicWhere(String sql, Object[] sqlParams) throws ProgramacionPosturasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ProgramacionPosturasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	@Override
	public ProgramacionPosturas[] findWhereIdprogramacionPosturasEquals(
			int idprogramacion_Posturas)
			throws ProgramacionPosturasDaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProgramacionPosturas[] findWhereFechaEquals(Date fecha)
			throws ProgramacionPosturasDaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProgramacionPosturas[] findWhereMinutosEquals(int Minutos)
			throws ProgramacionPosturasDaoException {
		// TODO Auto-generated method stub
		return null;
	}

}

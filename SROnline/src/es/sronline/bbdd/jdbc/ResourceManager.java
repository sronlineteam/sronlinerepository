package es.sronline.bbdd.jdbc;

import java.sql.*;

import org.apache.log4j.Logger;

import es.sronline.core.Constantes;

public class ResourceManager
{
    private static String JDBC_DRIVER   = "com.mysql.jdbc.Driver";
    private static String JDBC_URL      = Constantes.getConexionBBDD();

    private static String JDBC_USER     = Constantes.getUsuarioBBDD();
    private static String JDBC_PASSWORD = Constantes.getPasswordBBDD();

    private static Driver driver = null;

    public static synchronized Connection getConnection()
	throws SQLException
    {
        if (driver == null)
        {
            try
            {
                Class jdbcDriverClass = Class.forName( JDBC_DRIVER );
                driver = (Driver) jdbcDriverClass.newInstance();
                DriverManager.registerDriver( driver );
            }
            catch (Exception e)
            {
                System.out.println( "Failed to initialise JDBC driver" );
                Logger.getLogger("es.sronline").error(e.getMessage());
            }
        }

        return DriverManager.getConnection(
                JDBC_URL,
                JDBC_USER,
                JDBC_PASSWORD
        );
    }


	public static void close(Connection conn)
	{
		try {
			if (conn != null) conn.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}

	public static void close(PreparedStatement stmt)
	{
		try {
			if (stmt != null) stmt.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}
	}

	public static void close(ResultSet rs)
	{
		try {
			if (rs != null) rs.close();
		}
		catch (SQLException sqle)
		{
			sqle.printStackTrace();
		}

	}

}

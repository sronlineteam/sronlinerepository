
package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class SectoresDaoImpl extends AbstractDAO implements SectoresDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idsector, nombre FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idsector, nombre ) VALUES ( ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idsector = ?, nombre = ? WHERE idsector = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idsector = ?";

	/** 
	 * Index of column idsector
	 */
	protected static final int COLUMN_IDSECTOR= 1;

	/** 
	 * Index of column nombre
	 */
	protected static final int COLUMN_NOMBRE = 2;

	
	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 2;

	/** 
	 * Index of primary-key column IdSector
	 */
	protected static final int PK_COLUMN_IDSECTOR = 1;

	/** 
	 * Inserts a new row in the Sectores table.
	 */
	public SectoresPk insert(Sectores dto) throws SectoresDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdSector() );
			stmt.setString( index++, dto.getNombre() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdSector( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new SectoresDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the programacion table.
	 */
	public void update(SectoresPk pk, Sectores dto) throws SectoresDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdSector() );
			stmt.setString( index++, dto.getNombre() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new SectoresDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the Sectores table.
	 */
	public void delete(SectoresPk pk) throws SectoresDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdSector() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new SectoresDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the programacion table that matches the specified primary-key value.
	 */
	public Sectores findByPrimaryKey(SectoresPk pk) throws SectoresDaoException
	{
		return findByPrimaryKey( pk.getIdSector() );
	}

	/** 
	 * Returns all rows from the Sectores table that match the criteria 'IdSector = :IdSector'.
	 */
	public Sectores findByPrimaryKey(int IdSector) throws SectoresDaoException
	{
		Sectores ret[] = findByDynamicSelect( SQL_SELECT + " WHERE IdSector = ?", new Object[] {  new Integer(IdSector) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the Sectores table that match the criteria ''.
	 */
	public Sectores[] findAll() throws SectoresDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idsector", null );
	}

	/** 
	 * Returns all rows from the programacion table that match the criteria 'IdSector = :IdSector'.
	 */
	public Sectores[] findWhereIdSectorEquals(int IdSector) throws SectoresDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE IdSector = ? ORDER BY IdSector", new Object[] {  new Integer(IdSector) } );
	}

	/** 
	 * Returns all rows from the Sectores table that match the criteria 'Nombre = :Nombre'.
	 */
	public Sectores[] findWhereNombreEquals(String Nombre) throws SectoresDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Nombre = ? ORDER BY Nombre", new Object[] { Nombre } );
	}

	/**
	 * Method 'SectoresDaoImpl'
	 * 
	 */
	public SectoresDaoImpl()
	{
	}

	/**
	 * Method 'SectoresDaoImpl'
	 * 
	 * @param userConn
	 */
	public SectoresDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".Sectores";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected Sectores fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Sectores dto = new Sectores();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Sectores[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Sectores dto = new Sectores();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Sectores ret[] = new Sectores[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Sectores dto, ResultSet rs) throws SQLException
	{
		dto.setIdSector( rs.getInt( COLUMN_IDSECTOR ) );
		dto.setNombre( rs.getString( COLUMN_NOMBRE ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Sectores dto)
	{
	}

	/** 
	 * Returns all rows from the Sectores table that match the specified arbitrary SQL statement
	 */
	public Sectores[] findByDynamicSelect(String sql, Object[] sqlParams) throws SectoresDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new SectoresDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the Sectores table that match the specified arbitrary SQL statement
	 */
	public Sectores[] findByDynamicWhere(String sql, Object[] sqlParams) throws SectoresDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new SectoresDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

}

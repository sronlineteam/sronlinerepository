package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ParcelasDaoImpl extends AbstractDAO implements ParcelasDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idparcelas, descripcion, idsector FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idparcelas, descripcion, idsector ) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idparcelas = ?, descripcion = ?, idsector = ? WHERE idparcelas = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idparcelas = ?";

	/** 
	 * Index of column idparcelas
	 */
	protected static final int COLUMN_IDPARCELAS = 1;

	/** 
	 * Index of column descripcion
	 */
	protected static final int COLUMN_DESCRIPCION = 2;

	/** 
	 * Index of column idsector
	 */
	protected static final int COLUMN_IDSECTOR = 3;

	
	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 3;

	/** 
	 * Index of primary-key column idparcelas
	 */
	protected static final int PK_COLUMN_IDPARCELAS = 1;

	/** 
	 * Inserts a new row in the Clases_Abonos table.
	 */
	public ParcelasPk insert(Parcelas dto) throws ParcelasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdparcela() );
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setInt(index++, dto.getIdSector());
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdparcela( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ParcelasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the Parcelas table.
	 */
	public void update(ParcelasPk pk, Parcelas dto) throws ParcelasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdparcela());
			stmt.setString( index++, dto.getDescripcion() );
			stmt.setInt(index++, dto.getIdSector());
			stmt.setInt( index++, dto.getIdparcela());
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ParcelasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the Parcelas table.
	 */
	public void delete(ParcelasPk pk) throws ParcelasDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdParcela() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ParcelasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the Parcelas table that matches the specified primary-key value.
	 */
	public Parcelas findByPrimaryKey(ParcelasPk pk) throws ParcelasDaoException
	{
		return findByPrimaryKey( pk.getIdParcela() );
	}

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idParcelas = :idParcelas'.
	 */
	public Parcelas findByPrimaryKey(int idParcelas) throws ParcelasDaoException
	{
		Parcelas ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idparcelas = ?", new Object[] {  new Integer(idParcelas) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the Parcelas table that match the criteria ''.
	 */
	public Parcelas[] findAll() throws ParcelasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idparcelas", null );
	}

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idparcelas = :idParcelas'.
	 */
	public Parcelas[] findWhereIdParcelasEquals(int idParcelas) throws ParcelasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idparcelas = ? ORDER BY idparcelas", new Object[] {  new Integer(idParcelas) } );
	}

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idparcelas = :idParcelas'.
	 */
	public Parcelas[] findWhereIdSectorEquals(int idSector) throws ParcelasDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idsector = ? ORDER BY idparcelas", new Object[] {  new Integer(idSector) } );
	}



	/**
	 * Method 'ParcelasDaoImpl'
	 * 
	 */
	public ParcelasDaoImpl()
	{
	}

	/**
	 * Method 'ParcelasDaoImpl'
	 * 
	 * @param userConn
	 */
	public ParcelasDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".parcelas";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected Parcelas fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Parcelas dto = new Parcelas();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Parcelas[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Parcelas dto = new Parcelas();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Parcelas ret[] = new Parcelas[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Parcelas dto, ResultSet rs) throws SQLException
	{
		dto.setIdparcela( rs.getInt( COLUMN_IDPARCELAS ) );
		dto.setDescripcion( rs.getString( COLUMN_DESCRIPCION ) );
		dto.setIdSector(rs.getInt(COLUMN_IDSECTOR));
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Parcelas dto)
	{
	}

	/** 
	 * Returns all rows from the Parcelas table that match the specified arbitrary SQL statement
	 */
	public Parcelas[] findByDynamicSelect(String sql, Object[] sqlParams) throws ParcelasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ParcelasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the Parcelas table that match the specified arbitrary SQL statement
	 */
	public Parcelas[] findByDynamicWhere(String sql, Object[] sqlParams) throws ParcelasDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ParcelasDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

}

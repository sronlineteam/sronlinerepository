package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class AbonosDaoImpl extends AbstractDAO implements AbonosDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline.bbdd");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 * SELECT `abonos`.`idabonos`,
    `abonos`.`idposturas`,
    `abonos`.`idclases_abonos`,
    `abonos`.`duracion`,
    `abonos`.`inicio`
FROM `regadio`.`abonos`;

	 */
	protected final String SQL_SELECT = "SELECT idAbonos, idposturas, idclases_abonos, duracion, inicio FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idAbonos, idposturas, idclases_abonos, duracion, inicio) VALUES ( ?, ?, ?, ?, ?)";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idAbonos = ?, idposturas = ?, idclases_abonos = ?, duracion = ?, inicio = ? WHERE idAbonos = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idAbonos = ?";

	/** 
	 * Index of column idAbonos
	 */
	protected static final int COLUMN_IDABONOS = 1;


	/** 
	 * Index of column idAbonos_postura
	 */
	protected static final int COLUMN_IDPOSTURAS = 2;

	
	/** 
	 * Index of column duracion
	 */
	protected static final int COLUMN_IDCLASES_ABONOS = 3;

	/** 
	 * Index of column duracion
	 */
	protected static final int COLUMN_DURACION = 4;

	/** 
	 * Index of column duracion
	 */
	protected static final int COLUMN_INICIO = 5;

	protected static final int NUMBER_OF_COLUMNS = 5;

	/** 
	 * Index of primary-key column idAbonos
	 */
	protected static final int PK_COLUMN_IDABONOS = 1;

	/** 
	 * Inserts a new row in the Abonos table.
	 */
	public AbonosPk insert(Abonos dto) throws AbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdAbonos() );
			stmt.setInt( index++, dto.getIdPosturas() );
			stmt.setInt( index++, dto.getIdClasesAabonos() );
			stmt.setInt( index++, dto.getDuracion() );
			stmt.setInt( index++, dto.getInicio() );

			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdAbonos( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new AbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the Abonos table.
	 */
	public void update(AbonosPk pk, Abonos dto) throws AbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdAbonos() );
			stmt.setInt( index++, dto.getIdPosturas() );
			stmt.setInt( index++, dto.getIdClasesAabonos() );
			stmt.setInt( index++, dto.getDuracion() );
			stmt.setInt( index++, dto.getInicio() );
			stmt.setInt( index++, dto.getIdAbonos() );
			
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new AbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the Abonos table.
	 */
	public void delete(AbonosPk pk) throws AbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdabonos() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new AbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the Abonos table that matches the specified primary-key value.
	 */
	public Abonos findByPrimaryKey(AbonosPk pk) throws AbonosDaoException
	{
		return findByPrimaryKey( pk.getIdabonos() );
	}

	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idAbonos = :idAbonos'.
	 */
	public Abonos findByPrimaryKey(int idAbonos) throws AbonosDaoException
	{
		Abonos ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idAbonos = ?", new Object[] {  new Integer(idAbonos) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the Abonos table that match the criteria ''.
	 */
	public Abonos[] findAll() throws AbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idAbonos", null );
	}

	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idAbonos = :idAbonos'.
	 */
	public Abonos[] findWhereIdAbonosEquals(int idAbonos) throws AbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idAbonos = ? ORDER BY idAbonos", new Object[] {  new Integer(idAbonos) } );
	}


	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idAbonos = :idAbonos'.
	 */
	public Abonos[] findWhereIdPosturasIdClaseAbonoEquals(int idPostura, int idClaseAbono) throws AbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idposturas = ? AND idclases_abonos = ? ORDER BY idposturas", new Object[] {  new Integer(idPostura), new Integer(idClaseAbono) } );
	}



	/**
	 * Method 'AbonosDaoImpl'
	 * 
	 */
	public AbonosDaoImpl()
	{
	}

	/**
	 * Method 'AbonosDaoImpl'
	 * 
	 * @param userConn
	 */
	public AbonosDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".abonos";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected Abonos fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			Abonos dto = new Abonos();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected Abonos[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			Abonos dto = new Abonos();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		Abonos ret[] = new Abonos[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(Abonos dto, ResultSet rs) throws SQLException
	{
		dto.setIdAbonos( rs.getInt( COLUMN_IDABONOS ) );
		dto.setIdPosturas( rs.getInt( COLUMN_IDPOSTURAS ) );
		dto.setIdClasesAabonos( rs.getInt( COLUMN_IDCLASES_ABONOS ) );
		dto.setDuracion( rs.getInt( COLUMN_DURACION ) );
		dto.setInicio( rs.getInt( COLUMN_INICIO ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(Abonos dto)
	{
	}

	/** 
	 * Returns all rows from the Abonos table that match the specified arbitrary SQL statement
	 */
	public Abonos[] findByDynamicSelect(String sql, Object[] sqlParams) throws AbonosDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new AbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the Abonos table that match the specified arbitrary SQL statement
	 */
	public Abonos[] findByDynamicWhere(String sql, Object[] sqlParams) throws AbonosDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new AbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	@Override
	public Abonos[] findWhereIdPosturasEquals(
			int idPosturas) throws AbonosDaoException {
		// TODO Auto-generated method stub
		return findByDynamicSelect( SQL_SELECT + " WHERE idposturas = ? ORDER BY idabonos", new Object[] {  new Integer(idPosturas) } );
	}

}

package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ValvulasSectorDaoImpl extends AbstractDAO implements ValvulasSectorDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT id_remota, id_valvula, sectores_idsector FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( id_remota, id_valvula, sectores_idsector ) VALUES ( ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET id_remota = ?, id_valvula = ?, sectores_idsector = ? WHERE id_remota = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE id_remota = ?";

	/** 
	 * Index of column id_remota
	 */
	protected static final int COLUMN_ID_REMOTA = 1;

	/** 
	 * Index of column id_valvula
	 */
	protected static final int COLUMN_ID_VALVULA = 2;

	/** 
	 * Index of column fecha_hora
	 */
	protected static final int COLUMN_SECTORES_IDSECTOR = 3;

	

	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 3;

	/** 
	 * Index of primary-key column idValvulasSector
	 */
	protected static final int PK_COLUMN_ID_REMOTA = 1;

	/** 
	 * Inserts a new row in the ValvulasSector table.
	 */
	public ValvulasSectorPk insert(ValvulasSector dto) throws ValvulasSectorDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getidRemota() );
			stmt.setInt( index++, dto.getIdValvula() );
            stmt.setInt( index++, dto.getsectores_idsector() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setidRremota( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ValvulasSectorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the ValvulasSector table.
	 */
	public void update(ValvulasSectorPk pk, ValvulasSector dto) throws ValvulasSectorDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getidRemota() );
			stmt.setInt( index++, dto.getIdValvula() );
			stmt.setInt( index++, dto.getsectores_idsector() );
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ValvulasSectorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the ValvulasSector table.
	 */
	public void delete(ValvulasSectorPk pk) throws ValvulasSectorDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getIdRemota() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ValvulasSectorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the ValvulasSector table that matches the specified primary-key value.
	 */
	public ValvulasSector findByPrimaryKey(ValvulasSectorPk pk) throws ValvulasSectorDaoException
	{
		return findByPrimaryKey( pk.getIdRemota() );
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the criteria 'idremota = :idremota'.
	 */
	public ValvulasSector findByPrimaryKey(int idremota) throws ValvulasSectorDaoException
	{
		ValvulasSector ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idremota = ?", new Object[] {  new Integer(idremota) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the criteria ''.
	 */
	public ValvulasSector[] findAll() throws ValvulasSectorDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idremota", null );
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the criteria 'idValvulasSector = :idValvulasSector'.
	 */
	public ValvulasSector[] findWhereIdRemotaEquals(int idremota) throws ValvulasSectorDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idremota = ? ORDER BY idremota", new Object[] {  new Integer(idremota) } );
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the criteria 'id_valvula = :idValvula'.
	 */
	public ValvulasSector[] findWhereIdValvulaEquals(int idValvula) throws ValvulasSectorDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE id_valvula = ? ORDER BY id_valvula", new Object[] {  new Integer(idValvula) } );
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the criteria 'sectores_idsector = :sectores_idsector'.
	 */
	public ValvulasSector[] findWheresectores_idsectorEquals(int sectores_idsector) throws ValvulasSectorDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE sectores_idsector = ? ORDER BY sectores_idsector", new Object[] {  new Integer(sectores_idsector) } );
	}

	
	/**
	 * Method 'ValvulasSectorDaoImpl'
	 * 
	 */
	public ValvulasSectorDaoImpl()
	{
	}

	/**
	 * Method 'ValvulasSectorDaoImpl'
	 * 
	 * @param userConn
	 */
	public ValvulasSectorDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".ValvulasSector";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected ValvulasSector fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			ValvulasSector dto = new ValvulasSector();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected ValvulasSector[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			ValvulasSector dto = new ValvulasSector();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		ValvulasSector ret[] = new ValvulasSector[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(ValvulasSector dto, ResultSet rs) throws SQLException
	{
		dto.setidRremota( rs.getInt( COLUMN_ID_REMOTA ) );
		dto.setIdValvula( rs.getInt( COLUMN_ID_VALVULA ) );
		dto.setsectores_idsector( rs.getInt( COLUMN_SECTORES_IDSECTOR ) );
	}
	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(ValvulasSector dto)
	{
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the specified arbitrary SQL statement
	 */
	public ValvulasSector[] findByDynamicSelect(String sql, Object[] sqlParams) throws ValvulasSectorDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ValvulasSectorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the ValvulasSector table that match the specified arbitrary SQL statement
	 */
	public ValvulasSector[] findByDynamicWhere(String sql, Object[] sqlParams) throws ValvulasSectorDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ValvulasSectorDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	@Override
	public ValvulasSector[] findWhereSectores_IdSectorEquals(
			int sectores_idsector) throws ValvulasSectorDaoException {
		// TODO Auto-generated method stub
		return null;
	}

}

package es.sronline.bbdd.jdbc;

import java.util.Date;
import java.sql.Connection;
import java.util.Collection;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;
import es.sronline.core.Constantes;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ClasesAbonosDaoImpl extends AbstractDAO implements ClasesAbonosDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	protected static final Logger logger = Logger.getLogger("es.sronline");

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT idclases_abonos, tipo_abono, inicio, duracion FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( idclases_abonos, tipo_abono, inicio, duracion ) VALUES ( ?, ?, ?, ? )";

	/** 
	 * SQL UPDATE statement for this table
	 */
	protected final String SQL_UPDATE = "UPDATE " + getTableName() + " SET idclases_abonos = ?, tipo_abono = ?, inicio = ?, duracion = ? WHERE idclases_abonos = ?";

	/** 
	 * SQL DELETE statement for this table
	 */
	protected final String SQL_DELETE = "DELETE FROM " + getTableName() + " WHERE idclases_abonos = ?";

	/** 
	 * Index of column idclases_abonos
	 */
	protected static final int COLUMN_IDCLASES_ABONOS = 1;

	/** 
	 * Index of column tipo_abono
	 */
	protected static final int COLUMN_TIPO_ABONO = 2;

	/** 
	 * Index of column inicio
	 */
	protected static final int COLUMN_INICIO = 3;

	/** 
	 * Index of column duraccion
	 */
	protected static final int COLUMN_DURACION = 4;

	
	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 4;

	/** 
	 * Index of primary-key column idclases_abonos
	 */
	protected static final int PK_COLUMN_IDCLASES_ABONOS = 1;

	/** 
	 * Inserts a new row in the Clases_Abonos table.
	 */
	public ClasesAbonosPk insert(ClasesAbonos dto) throws ClasesAbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT, Statement.RETURN_GENERATED_KEYS );
			int index = 1;
			stmt.setInt( index++, dto.getIdclases_abonos() );
			stmt.setString( index++, dto.getTipo_abono() );
			stmt.setInt(index++, dto.getInicio() );
			stmt.setInt( index++, dto.getDuracion() );
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_INSERT + " with DTO: " + dto);
			}
		
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		
			// retrieve values from auto-increment columns
			rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dto.setIdclases_abonos( rs.getInt( 1 ) );
			}
		
			reset(dto);
			return dto.createPk();
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ClasesAbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Updates a single row in the ClasesAbonos table.
	 */
	public void update(ClasesAbonosPk pk, ClasesAbonos dto) throws ClasesAbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_UPDATE + " with DTO: " + dto);
			}
		
			stmt = conn.prepareStatement( SQL_UPDATE );
			int index=1;
			stmt.setInt( index++, dto.getIdclases_abonos() );
			stmt.setString( index++, dto.getTipo_abono() );
			stmt.setInt(index++, dto.getInicio());
			stmt.setInt( index++, dto.getDuracion());
			int rows = stmt.executeUpdate();
			reset(dto);
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ClasesAbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Deletes a single row in the ClasesAbonos table.
	 */
	public void delete(ClasesAbonosPk pk) throws ClasesAbonosDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL_DELETE + " with PK: " + pk);
			}
		
			stmt = conn.prepareStatement( SQL_DELETE );
			stmt.setInt( 1, pk.getidclases_abonos() );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( rows + " rows affected (" + (t2-t1) + " ms)");
			}
		
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ClasesAbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns the rows from the ClasesAbonos table that matches the specified primary-key value.
	 */
	public ClasesAbonos findByPrimaryKey(ClasesAbonosPk pk) throws ClasesAbonosDaoException
	{
		return findByPrimaryKey( pk.getidclases_abonos() );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'idClasesAbonos = :idClasesAbonos'.
	 */
	public ClasesAbonos findByPrimaryKey(int idClasesAbonos) throws ClasesAbonosDaoException
	{
		ClasesAbonos ret[] = findByDynamicSelect( SQL_SELECT + " WHERE idClases_Abonos = ?", new Object[] {  new Integer(idClasesAbonos) } );
		return ret.length==0 ? null : ret[0];
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria ''.
	 */
	public ClasesAbonos[] findAll() throws ClasesAbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " ORDER BY idClases_Abonos", null );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'idClases_Abonos = :idClasesAbonos'.
	 */
	public ClasesAbonos[] findWhereIdClasesAbonosEquals(int idClasesAbonos) throws ClasesAbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE idClases_Abonos = ? ORDER BY idClases_Abonos", new Object[] {  new Integer(idClasesAbonos) } );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'abonos_idabonos = :abonos_idabonos'.
	 */
	public ClasesAbonos[] findWhereAbonos_idabonosEquals(int abonos_idabonos) throws ClasesAbonosDaoException
	{
		
		return findByDynamicSelect( SQL_SELECT + " WHERE abonos_idabonos = ? ORDER BY abonos_idabonos", new Object[] {  new Integer(abonos_idabonos) } );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'tipo_abono = :tipo_abono'.
	 */
	public ClasesAbonos[] findWhereTipo_abonoEquals(int tipo_abono) throws ClasesAbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE tipo_abono = ? ORDER BY tipo_abono", new Object[] {  new Integer(tipo_abono) } );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'Inicio = :Inicio'.
	 */
	public ClasesAbonos[] findWhereInicioEquals(int Inicio) throws ClasesAbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE Inicio = ? ORDER BY Inicio", new Object[] {  new Integer(Inicio) } );
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'duracion = :duracion'.
	 */
	public ClasesAbonos[] findWhereDuracionEquals(int Duracion) throws ClasesAbonosDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE duracion = ? ORDER BY duracion", new Object[] {  new Integer(Duracion) } );
	}

	

	/**
	 * Method 'ClasesAbonosDaoImpl'
	 * 
	 */
	public ClasesAbonosDaoImpl()
	{
	}

	/**
	 * Method 'ClasesAbonosDaoImpl'
	 * 
	 * @param userConn
	 */
	public ClasesAbonosDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return Constantes.getNombreBBDD() + ".Clases_Abonos";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected ClasesAbonos fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			ClasesAbonos dto = new ClasesAbonos();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected ClasesAbonos[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			ClasesAbonos dto = new ClasesAbonos();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		ClasesAbonos ret[] = new ClasesAbonos[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(ClasesAbonos dto, ResultSet rs) throws SQLException
	{
		dto.setIdclases_abonos( rs.getInt( COLUMN_IDCLASES_ABONOS ) );
		dto.setTipo_abono( rs.getString( COLUMN_TIPO_ABONO ) );
		dto.setInicio( rs.getInt(COLUMN_INICIO ) );
		dto.setDuracion( rs.getInt(COLUMN_DURACION ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(ClasesAbonos dto)
	{
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the specified arbitrary SQL statement
	 */
	public ClasesAbonos[] findByDynamicSelect(String sql, Object[] sqlParams) throws ClasesAbonosDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ClasesAbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the ClasesAbonos table that match the specified arbitrary SQL statement
	 */
	public ClasesAbonos[] findByDynamicWhere(String sql, Object[] sqlParams) throws ClasesAbonosDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			if (logger.isDebugEnabled() && !Constantes.isProduccion()) {
				logger.debug( "Executing " + SQL);
			}
		
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			logger.error( "Exception: " + _e.getMessage(), _e );
			throw new ClasesAbonosDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

}

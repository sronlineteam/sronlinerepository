package es.sronline.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;

import es.sronline.bbdd.dto.LstTarifasElectricas;
import es.sronline.core.Constantes;
import es.sronline.core.Programacion;
import es.sronline.core.ProgramacionFormateada;
import es.sronline.core.ProgramacionPlanificaciones;
import es.sronline.core.ProgramacionTelecontrol;
import es.sronline.core.ProgramacionTelecontrolFormateada;
import es.sronline.tareas.QuartzTareas;

public class ConexionBBDD {
	Connection conexion = null;
	Logger log;
	
	private void conectar(){
		try {
			conexion = DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	
	}
	
	public ResultSet consultarSQL (String sql){
		if (conexion == null)
			conectar();
		
		Statement s;
		ResultSet rs= null;
		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql);
			//conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		
		return rs;
		
		
	}
	
	public int guardarSQL (String sql){
		if (conexion == null)
			conectar();
		
		Statement s;
		int rs= 0;
		try {
			s = conexion.createStatement();
			rs = s.executeUpdate(sql);
			log.info("Registros afectados: " + rs + " registros");
		
//					executeQuery (sql);
			conexion.close();
			
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		
		
		return rs;
		
		
	}

	public void cerrar(){
		try {
			conexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}
	public ConexionBBDD() {
		log = Logger.getLogger("es.sronline");
        // Se mete todo en un try por los posibles errores de MySQL
        try
        {
            // Se registra el Driver de MySQL
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
            
            // Se obtiene una conexi�n con la base de datos. Hay que
            // cambiar el usuario "root" y la clave "la_clave" por las
            // adecuadas a la base de datos que estemos usando.
            conexion = DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
            
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }
 

	}
	public List<Programacion> consultarProgramaciones() {
		return consultarProgramaciones(null);
	}	
	public List<Programacion> consultarProgramaciones(String where) {
		
		String stWhere;
		if(where != null)
			stWhere = "fecha_hora between DATE_ADD(now(),INTERVAL -60 SECOND) and now()";
		else
			stWhere = where;
		
		List<Programacion> tramas = new ArrayList<Programacion>();
		//ResultSet rs = con.consultarSQL("SELECT regadio.programacion.trama FROM regadio.programacion;");
		String sql = "SELECT trama, id_remota, id_valvula, des_accion FROM " + Constantes.getNombreBBDD() + ".programacion where "+ stWhere;
		Statement s;
		ResultSet rs= null;
		Programacion auxProg;
		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql);
			while (rs.next())
			{
			    auxProg = new Programacion();
			    auxProg.setTrama(rs.getString(1));
			    auxProg.setIdRemota(rs.getInt(2));
			    auxProg.setIdValvula(rs.getInt(3));
			    auxProg.setDesAccion(rs.getInt(4));
				tramas.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  tramas;
	}

	public List<ProgramacionFormateada> consultarProgramacionesFuturas() {
		List<ProgramacionFormateada> programacion = new ArrayList<ProgramacionFormateada>();
		//ResultSet rs = con.consultarSQL("SELECT regadio.programacion.trama FROM regadio.programacion;");
//		String sql = "SELECT idprogramacion, id_remota, id_valvula, fecha_hora, des_accion FROM " + Constantes.getNombreBBDD() + ".programacion where fecha_hora > now() and des_accion < 2 order by id_remota, id_valvula";
	    StringBuffer sqlBF = new StringBuffer("SELECT idprogramacion, id_remota, id_valvula, fecha_hora, des_accion, nombre ");
	    sqlBF.append("FROM programacion p, valvulas v ");
	    sqlBF.append("where fecha_hora > now() and des_accion < 2 and p.id_remota = v.idRemota and p.id_valvula = v.idvalvula ");
	    sqlBF.append("order by id_remota, id_valvula, fecha_hora");

		Statement s;
		ResultSet rs= null;
		ProgramacionFormateada auxProg;

		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sqlBF.toString());
			while (rs.next())
			{
				auxProg = new ProgramacionFormateada();
				auxProg.setIdprogramacion(rs.getInt(1));
				auxProg.setIdRemota(rs.getInt(2));
				auxProg.setIdValvula(rs.getInt(3));
				auxProg.setFechaHoraFormateada(rs.getString(4));
				auxProg.setFechaHora(rs.getDate(4));
				auxProg.setDesAccion(rs.getInt(5));
		        auxProg.setNombreValvula(rs.getString(6));
		        programacion.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  programacion;
	}
	
	public List<ProgramacionTelecontrol> consultarProgramacionesTelecontrol() {

		List<ProgramacionTelecontrol> tramas = new ArrayList<ProgramacionTelecontrol>();
		String sql = "SELECT idprogramacion, idobj_telecontrol, fecha_hora, des_accion, trama FROM " + Constantes.getNombreBBDD() + ".programacion_telecontrol where fecha_hora between DATE_ADD(now(),INTERVAL -60 SECOND) and now()";
		Statement s;
		ResultSet rs= null;
		ProgramacionTelecontrol auxProg;
		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql);
			while (rs.next())
			{
			    auxProg = new ProgramacionTelecontrol();
			    auxProg.setIdprogramacion(rs.getInt(1));
			    auxProg.setIdObjeto(rs.getInt(2));
			    auxProg.setFechaHora(rs.getDate(3));			    
			    auxProg.setDesAccion(rs.getInt(4));
			    auxProg.setTrama(rs.getString(5));
				tramas.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  tramas;
	}
	
	public List<ProgramacionTelecontrolFormateada> consultarProgramacionesTelecontrolFuturas(String username) {
		List<ProgramacionTelecontrolFormateada> programacion = new ArrayList<ProgramacionTelecontrolFormateada>();
	    StringBuffer sqlBF = new StringBuffer("SELECT idprogramacion, p.idobj_telecontrol, fecha_hora, des_accion, nombre, enviado ");
	    sqlBF.append("FROM programacion_telecontrol p, lst_objetos_emplazamiento_user o ");
	    sqlBF.append("where fecha_hora > now() - 6000 and des_accion < 3 and p.idobj_telecontrol = o.idObjeto and username= '").append(username).append("' ");
	    sqlBF.append("order by p.idobj_telecontrol, fecha_hora");

		Statement s;
		ResultSet rs= null;
		ProgramacionTelecontrolFormateada auxProg;

		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sqlBF.toString());
			while (rs.next())
			{
				auxProg = new ProgramacionTelecontrolFormateada();
				auxProg.setIdprogramacion(rs.getInt(1));
				auxProg.setIdObjeto(rs.getInt(2));
				auxProg.setFechaHoraFormateada(rs.getString(3));
				auxProg.setFechaHora(rs.getDate(3));
				auxProg.setDesAccion(rs.getInt(4));
		        auxProg.setNombreObjeto(rs.getString(5));
		        auxProg.setEnvio(rs.getInt(6));
		        programacion.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  programacion;
	}	

	public List<ProgramacionTelecontrolFormateada> consultarProgramacionesTelecontrolHistoricas(String username, String desde, String hasta) {
		List<ProgramacionTelecontrolFormateada> programacion = new ArrayList<ProgramacionTelecontrolFormateada>();
		StringBuffer sqlBF = new StringBuffer("SELECT idprogramacion, p.idobj_telecontrol, fecha_hora, des_accion, nombre, enviado, resultado ");
	    sqlBF.append("FROM programacion_telecontrol p, lst_objetos_emplazamiento_user o ");
	    sqlBF.append(" where fecha_hora BETWEEN '").append(desde).append("' and (DATE('").append(hasta).append("') + INTERVAL 1 day)");
	    sqlBF.append(" and des_accion < 3 and p.idobj_telecontrol = o.idObjeto and username= '").append(username).append("' ");
	    sqlBF.append("order by p.idobj_telecontrol, fecha_hora");

		Statement s;
		ResultSet rs= null;
		ProgramacionTelecontrolFormateada auxProg;

		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sqlBF.toString());
			while (rs.next())
			{
				auxProg = new ProgramacionTelecontrolFormateada();
				auxProg.setIdprogramacion(rs.getInt(1));
				auxProg.setIdObjeto(rs.getInt(2));
				auxProg.setFechaHoraFormateada(rs.getString(3));
				auxProg.setFechaHora(rs.getDate(3));
				auxProg.setDesAccion(rs.getInt(4));
		        auxProg.setNombreObjeto(rs.getString(5));
		        auxProg.setEnvio(rs.getInt(6));
		        auxProg.setResultado(rs.getString(7));
		        programacion.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  programacion;
	}	

	public List<LstTarifasElectricas> consultarTarifasElectricas() {
		List<LstTarifasElectricas> lstTarifasElec = new ArrayList<LstTarifasElectricas>();
		//ResultSet rs = con.consultarSQL("SELECT regadio.programacion.trama FROM regadio.programacion;");
		String sql = "SELECT p.secuencia, p.fecha , p.idTarifas_Electricas, p.descripcion, p.hora_inicio, p.hora_fin, p.tarifa FROM " + 
						Constantes.getNombreBBDD() + ".tarifas_electricas p ";
		Statement s;
		ResultSet rs= null;
		// ProgramacionPosturasFormateada auxProg;
		LstTarifasElectricas auxProg;

		try {
			s = conexion.createStatement();
			rs = s.executeQuery (sql);
			while (rs.next())
			{
				auxProg = new LstTarifasElectricas();
				auxProg.setSecuencia(rs.getInt(1));
		    	auxProg.setFecha(rs.getDate(2));
				auxProg.setIdTarifasElectricas(rs.getInt(3));
				auxProg.setDescripcion(rs.getString(4));
				auxProg.setHoraInicio(rs.getDate(5));
				auxProg.setHoraFin(rs.getDate(6));
				auxProg.setTarifa(rs.getInt(7));
				lstTarifasElec.add(auxProg);
			}
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		
		return  lstTarifasElec;
	}
	
	public List<ProgramacionPlanificaciones> consultarPlanificacionesTelecontrolFuturas(String username){
		  List<ProgramacionPlanificaciones> planificaciones = new ArrayList<ProgramacionPlanificaciones>();
		  String sql = "SELECT distinct idPlanificacion, Nombre, F_inicio, F_fin, H_inicio, H_fin, ArrayDiasSemana, ArrayIdObjetosTelecontrol,"
		  		+ "ArrayIdProgramacionesTelecontrol FROM " + Constantes.getNombreBBDD() + ".planificaciones, " + Constantes.getNombreBBDD() + ".objetos_user o "
		  		+ "where find_in_set(idobj_telecontrol , ArrayIdObjetosTelecontrol) and o.username='" + username + "' and F_fin >= curdate()";
		  Statement s;
		  ResultSet rs= null;
		  ProgramacionPlanificaciones auxPlan;

		  try {
			   s = conexion.createStatement();
			   rs = s.executeQuery (sql);
			   while (rs.next())
			   {
				    auxPlan = new ProgramacionPlanificaciones();
				    auxPlan.setIdPlanificacion(rs.getInt("idPlanificacion"));
				    auxPlan.setNombre(rs.getString("Nombre"));
				    auxPlan.setF_inicio(rs.getString("F_inicio"));       
				    auxPlan.setF_fin(rs.getString("F_fin"));
				    auxPlan.setH_inicio(rs.getString("H_inicio"));
				    auxPlan.setH_fin(rs.getString("H_fin"));
				    auxPlan.SetArrayDiasSemana(rs.getString("ArrayDiasSemana"));
				    auxPlan.SetArrayIdObjetosTelecontrol(rs.getString("ArrayIdObjetosTelecontrol"));
				    auxPlan.SetArrayIdProgramacionesTelecontrol(rs.getString("ArrayIdProgramacionesTelecontrol"));
				    
				    planificaciones.add(auxPlan);
				    
			   }
			   conexion.close();
		   
		  } catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
		  
		  return  planificaciones;
	}
	
	///Reubicar codigo hasta 
	public void lanzarProgramacionQuartz (){
		  
		try {
			String sql1 = "SELECT * FROM " + Constantes.getNombreBBDD() + ".programacion_telecontrol WHERE"
			      + " enviado = '0' AND fecha_hora BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 90 MINUTE)";
			
			es.sronline.bbdd.dto.ProgramacionTelecontrol programacion_tel_dto = null;
		    
			Statement s1 = conexion.createStatement();
			ResultSet rs1 = s1.executeQuery (sql1);
		    
		    log.debug("Programaciones de objetos de telecontrol");
		    while (rs1.next()){
		    	ConexionBBDD conexionBBDD = new ConexionBBDD();
			    conexionBBDD.actualizarPlanificadoObjeto(rs1.getInt("idprogramacion"),1);
			    programacion_tel_dto = new es.sronline.bbdd.dto.ProgramacionTelecontrol();
			    programacion_tel_dto.setIdprogramacion(rs1.getInt("idprogramacion"));
			    programacion_tel_dto.setIdObjeto(rs1.getInt("idobj_telecontrol"));
			    programacion_tel_dto.setFechaHora(rs1.getTimestamp("fecha_hora"));
			    programacion_tel_dto.setDesAccion(rs1.getInt("des_accion"));
			    programacion_tel_dto.setTrama(rs1.getString("trama"));
			   
			    try {
			    	QuartzTareas.programacionObjTelecontrol(programacion_tel_dto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		   }
		   
		   conexion.close();  
		   
		  } catch (SQLException e) {
			  // TODO Auto-generated catch block
			  log.error(e.getMessage());
		  }
	}
	
	
	
	public int insertarPlanificacion(ProgramacionPlanificaciones plani){
		
	    String sql = "INSERT INTO "+ Constantes.getNombreBBDD()+".planificaciones (Nombre, F_inicio, F_fin, H_inicio, H_fin, ArrayDiasSemana,"
		  				+ " ArrayIdObjetosTelecontrol, ArrayIdProgramacionesTelecontrol) "
		  					+ "VALUES ('"+plani.getNombre()+"', '"+plani.getF_inicio()+"', '"+plani.getF_fin()+"', '"+plani.getH_inicio()+"', '"+plani.getH_fin()+"', '"+plani.getArrayDiasSemana()+"',"
		  					+ " '"+plani.getArrayIdObjetosTelecontrol()+"', '"+plani.getArrayIdProgramacionesTelecontrol()+"')";
		int idPlani = 0;
		try {
			Statement s = conexion.createStatement();
		    s.executeUpdate (sql, Statement.RETURN_GENERATED_KEYS);
		   
		    ResultSet resul = s.getGeneratedKeys();
		    if (resul.next()){
			  idPlani = resul.getInt(1);
		    }  
		    
	    }catch (SQLException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
	    }
		
    	return idPlani;
	}
	
	public void actualizarPlanificacion(ProgramacionPlanificaciones plani) throws SchedulerException, SQLException{
		//BUSCAMOS LAS PROGRAMACIONES ASOCIADAS AL IDPLANIFICACION
		
		try {
			String idsProgramString = null;
			String sql2 = "SELECT ArrayIdProgramacionesTelecontrol FROM " + Constantes.getNombreBBDD() + ".planificaciones where idPlanificacion = "+plani.getIdPlanificacion()+"";
	
			Statement s2 = conexion.createStatement();
			ResultSet rs2 = s2.executeQuery (sql2);
			while (rs2.next()){
					idsProgramString = rs2.getString("ArrayIdProgramacionesTelecontrol");
			}
 
			String[] idsProgram = idsProgramString.split(",");
			Scheduler sched = QuartzTareas.getSched();

			for (int k=0; k < idsProgram.length; k++){
				String sql1 = "DELETE FROM "+Constantes.getNombreBBDD()+".programacion_telecontrol"
						+ " WHERE idprogramacion = "+idsProgram[k]+" AND enviado = 0";
				
					Statement s1 = conexion.createStatement();
					s1.executeUpdate(sql1);
					sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idsProgram[k], "group2"));
				
			}
		
			String sql = "UPDATE "+ Constantes.getNombreBBDD()+".planificaciones SET Nombre='"+plani.getNombre()+"', F_inicio='"+plani.getF_inicio()+"', F_fin='"+plani.getF_fin()+"'"
				  		+ ", H_inicio='"+plani.getH_inicio()+"', H_fin='"+plani.getH_fin()+"', ArrayDiasSemana='"+plani.getArrayDiasSemana()+"',"
			  				+ " ArrayIdObjetosTelecontrol='"+plani.getArrayIdObjetosTelecontrol()+"', ArrayIdProgramacionesTelecontrol='"+plani.getArrayIdProgramacionesTelecontrol()+
			  				"' WHERE idPlanificacion = '"+plani.getIdPlanificacion()+"'";
		
			Statement s = conexion.createStatement();
			s.executeUpdate (sql);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	public void eliminarPlanificacion(int idPlanificacion) throws SchedulerException{

		try {
			//BUSCAMOS LAS PROGRAMACIONES ASOCIADAS AL IDPLANIFICACION
			String idsProgramString = null;
			String sql2 = "SELECT ArrayIdProgramacionesTelecontrol FROM " + Constantes.getNombreBBDD() + ".planificaciones where idPlanificacion = "+idPlanificacion+"";
			
			Statement s2 = conexion.createStatement();
			ResultSet rs2 = s2.executeQuery (sql2);
			while (rs2.next()){
					idsProgramString = rs2.getString("ArrayIdProgramacionesTelecontrol");
			}
			
		
			//CON EL STRING DE TODOS LOS ID PROGRAMACIONES VAMOS UNA A UNA BORR�NDOLAS DE LA TABLA PROGRAMACIONES
			String[] idsProgram = idsProgramString.split(",");
			Scheduler sched = QuartzTareas.getSched();
			Statement s1;
			for (int i=0; i < idsProgram.length; i++){
				String sql1 = "DELETE FROM "+Constantes.getNombreBBDD()+".programacion_telecontrol"
						+ " WHERE idprogramacion = "+idsProgram[i]+"";
				
				s1 = conexion.createStatement();
				s1.executeUpdate(sql1);
				sched.unscheduleJob(TriggerKey.triggerKey("trigger"+idsProgram[i], "group2"));
					
			}
		
			//POR �LTIMO BORRAMOS DE LA TABLA PLANIFICACIONES LA PLANIFICACION
			String sql = "DELETE FROM "+Constantes.getNombreBBDD()+".planificaciones"
					+ " WHERE idPlanificacion = "+idPlanificacion+"";

			Statement s = conexion.createStatement();
			s.executeUpdate(sql);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
	}
	
	public void actualizarPlaniArrayIdProg(int idPlani, String ArrayIdProg ){
		String sql = "UPDATE "+ Constantes.getNombreBBDD()+".planificaciones SET ArrayIdProgramacionesTelecontrol='"+ArrayIdProg+
	  				"' WHERE idPlanificacion = '"+idPlani+"'";
		Statement s;
		try {
			s = conexion.createStatement();
			s.executeUpdate (sql);
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void actualizarPlanificadoObjeto(Integer idprogramacion, Integer planificado){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion_telecontrol SET planificado = '"+ planificado +"' WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
		   // TODO Auto-generated catch block
		   log.error(e.getMessage());
		  }
	}
	
	public void actualizarEnvioObjeto(Integer idprogramacion, Integer envio){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion_telecontrol SET enviado = '"+ envio +"' WHERE idprogramacion = '"+idprogramacion+"' and enviado < "+ envio +" ";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
	}
	
	public void actualizarResultadoObjeto(Integer idprogramacion, String resultado){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion_telecontrol SET resultado = concat(resultado,' <br> "+ resultado +"') WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
	}
	
	public void actualizarErrorObjeto(Integer idprogramacion, String error){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion_telecontrol SET error = '"+ error +"' WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
	}
	
	public String damePlanificadoObjetosTelecontrol(Integer idprogramacion){
		  String sql = "SELECT planificado  FROM "+ Constantes.getNombreBBDD() + ".programacion_telecontrol WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  String planificado = new String();
		  try {
			   s = conexion.createStatement();
			   ResultSet rs = s.executeQuery(sql);
			   while (rs.next()) {
				   	  planificado= rs.getString("planificado");			  
			   }
			   
		  }catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
		  return planificado;
	}
	
	public void actualizarErrorProgramacion (Integer idprogramacion, String error){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion SET error = '"+error+"' WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
		   // TODO Auto-generated catch block
		   log.error(e.getMessage());
		  }
	}
	
	public void actualizarProgramacionValvulas(Integer idprogramacion){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion SET enviado = '1' WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			  s = conexion.createStatement();
			  s.executeUpdate(sql);
			  
		  }catch (SQLException e) {
			  // TODO Auto-generated catch block
			  log.error(e.getMessage());
		  }
	}
	
	public void actualizarResultadoProgramacion (Integer idprogramacion, String resultado){
		  String sql = "UPDATE "+ Constantes.getNombreBBDD() + ".programacion SET resultado = '"+resultado+"' WHERE idprogramacion = '"+idprogramacion+"'";
		  Statement s;
		  try {
			   s = conexion.createStatement();
			   s.executeUpdate(sql);
			   
		  }catch (SQLException e) {
			   // TODO Auto-generated catch block
			   log.error(e.getMessage());
		  }
	}
	///Fin Reubicar codigo hasta
}

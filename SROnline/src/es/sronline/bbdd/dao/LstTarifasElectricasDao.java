
package es.sronline.bbdd.dao;

import java.util.Date;

import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;

public interface LstTarifasElectricasDao
{
	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria ''.
	 */
	public LstTarifasElectricas[] findAll() throws LstTarifasElectricasDaoException;
	/** 
	 * Inserts a new row in the TarifasElectricas table.
	 */
	public LstTarifasElectricasPk insert(LstTarifasElectricas dto) throws LstTarifasElectricasDaoException;

	/** 
	 * Updates a single row in the TarifasElectricas table.
	 */
	public void update(LstTarifasElectricasPk pk, LstTarifasElectricas dto) throws LstTarifasElectricasDaoException;

	/** 
	 * Deletes a single row in the TarifasElectricas table.
	 */
	public void delete(LstTarifasElectricasPk pk) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns the rows from the TarifasElectricas table that matches the specified primary-key value.
	 */
	public LstTarifasElectricas findByPrimaryKey(LstTarifasElectricasPk pk) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Secuencia = :Secuencia'.
	 */
	public LstTarifasElectricas findByPrimaryKey(int Secuencia) throws LstTarifasElectricasDaoException;

	
	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Secuencia = :Secuencia'.
	 */
	public LstTarifasElectricas[] findWhereSecuenciaEquals(int Secuencia) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Fecha = :Fecha'.
	 */
	public LstTarifasElectricas[] findWhereFechaEquals(Date Fecha) throws LstTarifasElectricasDaoException;


	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'idTarifasElectricas = :idTarifasElectricas'.
	 */
	public LstTarifasElectricas[] findWhereIdTarifasElectricasEquals(int idTarifasElectricas) throws LstTarifasElectricasDaoException;
	
	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Descripcion = :Descripcion'.
	 */
	public LstTarifasElectricas[] findWhereDescripcionEquals(int Descripcion) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Hora_Inicio = :HoraInicio'.
	 */
	public LstTarifasElectricas[] findWhereHoraInicioEquals(Date HoraInicio) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'Hora_Fin = :HoraFin'.
	 */
	public LstTarifasElectricas[] findWhereHoraFinEquals(Date HoraFin) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the criteria 'tarifa = :tarifa'.
	 */
	public LstTarifasElectricas[] findWhereTarifaEquals(String tarifa) throws LstTarifasElectricasDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the TarifasElectricas table that match the specified arbitrary SQL statement
	 */
	public LstTarifasElectricas[] findByDynamicSelect(String sql, Object[] sqlParams) throws LstTarifasElectricasDaoException;

	/** 
	 * Returns all rows from the TarifasElectricas table that match the specified arbitrary SQL statement
	 */
	public LstTarifasElectricas[] findByDynamicWhere(String sql, Object[] sqlParams) throws LstTarifasElectricasDaoException;

}

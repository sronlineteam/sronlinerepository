package es.sronline.bbdd.dao;


import es.sronline.bbdd.dto.Abonos;
import es.sronline.bbdd.dto.AbonosPk;
import es.sronline.bbdd.exceptions.AbonosDaoException;

public interface AbonosDao
{
	/** 
	 * Inserts a new row in the Abonos table.
	 */
	public AbonosPk insert(Abonos dto) throws AbonosDaoException;

	/** 
	 * Updates a single row in the Abonos table.
	 */
	public void update(AbonosPk pk, Abonos dto) throws AbonosDaoException;

	/** 
	 * Deletes a single row in the Abonos table.
	 */
	public void delete(AbonosPk pk) throws AbonosDaoException;

	/** 
	 * Returns the rows from the Abonos table that matches the specified primary-key value.
	 */
	public Abonos findByPrimaryKey(AbonosPk pk) throws AbonosDaoException;

	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idAbonos = :idAbonos'.
	 */
	public Abonos findByPrimaryKey(int idAbonos) throws AbonosDaoException;

	/** 
	 * Returns all rows from the Abonos table that match the criteria ''.
	 */
	public Abonos[] findAll() throws AbonosDaoException;

	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idAbonos = :idAbonos'.
	 */
	public Abonos[] findWhereIdAbonosEquals(int idAbonos) throws AbonosDaoException;

	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idprogramacion_postura = :idprogramacion_postura'.
	 */
	public Abonos[] findWhereIdPosturasEquals(int idposturas) throws AbonosDaoException;

	//findWhereIdPosturasIdClaseAbonoEquals
	/** 
	 * Returns all rows from the Abonos table that match the criteria 'idprogramacion_postura = :idprogramacion_postura'.
	 */
	public Abonos[] findWhereIdPosturasIdClaseAbonoEquals(int idPostura, int idClaseAbono) throws AbonosDaoException;
	
	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Abonos table that match the specified arbitrary SQL statement
	 */
	public Abonos[] findByDynamicSelect(String sql, Object[] sqlParams) throws AbonosDaoException;

	/** 
	 * Returns all rows from the Abonos table that match the specified arbitrary SQL statement
	 */
	public Abonos[] findByDynamicWhere(String sql, Object[] sqlParams) throws AbonosDaoException;

}

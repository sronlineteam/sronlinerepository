
package es.sronline.bbdd.dao;

import es.sronline.bbdd.dto.Sectores;
import es.sronline.bbdd.dto.SectoresPk;
import es.sronline.bbdd.exceptions.SectoresDaoException;

public interface SectoresDao
{
	/** 
	 * Inserts a new row in the Sectores table.
	 */
	public SectoresPk insert(Sectores dto) throws SectoresDaoException;

	/** 
	 * Updates a single row in the Sectores table.
	 */
	public void update(SectoresPk pk, Sectores dto) throws SectoresDaoException;

	/** 
	 * Deletes a single row in the Sectores table.
	 */
	public void delete(SectoresPk pk) throws SectoresDaoException;

	/** 
	 * Returns the rows from the Sectores table that matches the specified primary-key value.
	 */
	public Sectores findByPrimaryKey(SectoresPk pk) throws SectoresDaoException;

	/** 
	 * Returns all rows from the Sectores table that match the criteria 'idSector = :idSector'.
	 */
	public Sectores findByPrimaryKey(int idSector) throws SectoresDaoException;

	/** 
	 * Returns all rows from the programacion table that match the criteria ''.
	 */
	public Sectores[] findAll() throws SectoresDaoException;

	/** 
	 * Returns all rows from the Sectores table that match the criteria 'idSector = :idSector'.
	 */
	public Sectores[] findWhereIdSectorEquals(int idSector) throws SectoresDaoException;

	
	/** 
	 * Returns all rows from the Sectores table that match the criteria 'trama = :trama'.
	 */
	public Sectores[] findWhereNombreEquals(String Nombre) throws SectoresDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Sectores table that match the specified arbitrary SQL statement
	 */
	public Sectores[] findByDynamicSelect(String sql, Object[] sqlParams) throws SectoresDaoException;

	/** 
	 * Returns all rows from the Sectores table that match the specified arbitrary SQL statement
	 */
	public Sectores[] findByDynamicWhere(String sql, Object[] sqlParams) throws SectoresDaoException;

}

package es.sronline.bbdd.dao;


import es.sronline.bbdd.dto.Posturas;
import es.sronline.bbdd.dto.PosturasPk;
import es.sronline.bbdd.exceptions.PosturasDaoException;

public interface PosturasDao
{
	/** 
	 * Inserts a new row in the Posturas table.
	 */
	public PosturasPk insert(Posturas dto) throws PosturasDaoException;

	/** 
	 * Updates a single row in the Posturas table.
	 */
	public void update(PosturasPk pk, Posturas dto) throws PosturasDaoException;

	/** 
	 * Deletes a single row in the Posturas table.
	 */
	public void delete(PosturasPk pk) throws PosturasDaoException;

	/** 
	 * Returns the rows from the Posturas table that matches the specified primary-key value.
	 */
	public Posturas findByPrimaryKey(PosturasPk pk) throws PosturasDaoException;

	/** 
	 * Returns all rows from the Posturas table that match the criteria 'idposturas = :idposturas'.
	 */
	public Posturas findByPrimaryKey(int idposturas) throws PosturasDaoException;

	/** 
	 * Returns all rows from the Posturas table that match the criteria ''.
	 */
	public Posturas[] findAll() throws PosturasDaoException;

	/** 
	 * Returns all rows from the Posturas table that match the criteria 'idposturas = :idposturas'.
	 */
	public Posturas[] findWhereIdPosturasEquals(int idposturas) throws PosturasDaoException;

	/** 
	 * Returns all rows from the Posturas table that match the criteria 'idprogramacion_postura = :idprogramacion_postura'.
	 */
	public Posturas[] findWhereIdprogramacion_posturaEquals(int idprogramacion_postura) throws PosturasDaoException;

	
	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Posturas table that match the specified arbitrary SQL statement
	 */
	public Posturas[] findByDynamicSelect(String sql, Object[] sqlParams) throws PosturasDaoException;

	/** 
	 * Returns all rows from the Posturas table that match the specified arbitrary SQL statement
	 */
	public Posturas[] findByDynamicWhere(String sql, Object[] sqlParams) throws PosturasDaoException;

	/** 
	 * Returns count from the Posturas table that match the criteria 'idprogramacion_postura = :idprogramacion_postura'.
	 */
	public int countByIdprogramacion_posturaEquals(int idprogramacion_postura) throws PosturasDaoException;

}


package es.sronline.bbdd.dao;

import es.sronline.bbdd.dto.ClasesAbonos;
import es.sronline.bbdd.dto.ClasesAbonosPk;
import es.sronline.bbdd.exceptions.ClasesAbonosDaoException;

public interface ClasesAbonosDao
{
	/** 
	 * Inserts a new row in the ClasesAbonos table.
	 */
	public ClasesAbonosPk insert(ClasesAbonos dto) throws ClasesAbonosDaoException;

	/** 
	 * Updates a single row in the ClasesAbonos table.
	 */
	public void update(ClasesAbonosPk pk, ClasesAbonos dto) throws ClasesAbonosDaoException;

	/** 
	 * Deletes a single row in the ClasesAbonos table.
	 */
	public void delete(ClasesAbonosPk pk) throws ClasesAbonosDaoException;

	/** 
	 * Returns the rows from the ClasesAbonos table that matches the specified primary-key value.
	 */
	public ClasesAbonos findByPrimaryKey(ClasesAbonosPk pk) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'idClasesAbonos = :idClasesAbonos'.
	 */
	public ClasesAbonos findByPrimaryKey(int idClasesAbonos) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria ''.
	 */
	public ClasesAbonos[] findAll() throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'idClasesAbonos = :idClasesAbonos'.
	 */
	public ClasesAbonos[] findWhereIdClasesAbonosEquals(int idClasesAbonos) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'abonos_idabonos = :abonos_idabonos'.
	 */
	public ClasesAbonos[] findWhereAbonos_idabonosEquals(int abonos_idabonos) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'tipo_abono = :tipo_abono'.
	 */
	public ClasesAbonos[] findWhereTipo_abonoEquals(int tipo_abono) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'inicio = :inicio'.
	 */
	public ClasesAbonos[] findWhereInicioEquals(int inicio) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the criteria 'duracion = :duracion'.
	 */
	public ClasesAbonos[] findWhereDuracionEquals(int duracion) throws ClasesAbonosDaoException;

	

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the ClasesAbonos table that match the specified arbitrary SQL statement
	 */
	public ClasesAbonos[] findByDynamicSelect(String sql, Object[] sqlParams) throws ClasesAbonosDaoException;

	/** 
	 * Returns all rows from the ClasesAbonos table that match the specified arbitrary SQL statement
	 */
	public ClasesAbonos[] findByDynamicWhere(String sql, Object[] sqlParams) throws ClasesAbonosDaoException;

}

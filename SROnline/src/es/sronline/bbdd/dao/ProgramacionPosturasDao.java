/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package es.sronline.bbdd.dao;

import java.util.Date;

import es.sronline.bbdd.dto.*;
import es.sronline.bbdd.exceptions.*;

public interface ProgramacionPosturasDao
{
	/** 
	 * Inserts a new row in the programacionPosturas table.
	 */
	public ProgramacionPosturasPk insert(ProgramacionPosturas dto) throws ProgramacionPosturasDaoException;

	/** 
	 * Updates a single row in the programacionPosturas table.
	 */
	public void update(ProgramacionPosturasPk pk, ProgramacionPosturas dto) throws ProgramacionPosturasDaoException;

	/** 
	 * Deletes a single row in the programacionPosturas table.
	 */
	public void delete(ProgramacionPosturasPk pk) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns the rows from the programacionPosturas table that matches the specified primary-key value.
	 */
	public ProgramacionPosturas findByPrimaryKey(ProgramacionPosturasPk pk) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria 'idprogramacion = :idprogramacion'.
	 */
	public ProgramacionPosturas findByPrimaryKey(int idprogramacion_Posturas) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria ''.
	 */
	public ProgramacionPosturas[] findAll() throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria 'idprogramacion_Posturas = :idprogramacion_Posturas'.
	 */
	public ProgramacionPosturas[] findWhereIdprogramacionPosturasEquals(int idprogramacion_Posturas) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria 'idSector = :idSector'.
	 */
	public ProgramacionPosturas[] findWhereIdSectorEquals(int idSector) throws ProgramacionPosturasDaoException;

	
	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria 'fecha = :fecha'.
	 */
	public ProgramacionPosturas[] findWhereFechaEquals(Date fecha) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacionPosturas table that match the criteria 'Minutos = :Minutos'.
	 */
	public ProgramacionPosturas[] findWhereMinutosEquals(int Minutos) throws ProgramacionPosturasDaoException;

	
	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the programacionPosturas table that match the specified arbitrary SQL statement
	 */
	public ProgramacionPosturas[] findByDynamicSelect(String sql, Object[] sqlParams) throws ProgramacionPosturasDaoException;

	/** 
	 * Returns all rows from the programacion table that match the specified arbitrary SQL statement
	 */
	public ProgramacionPosturas[] findByDynamicWhere(String sql, Object[] sqlParams) throws ProgramacionPosturasDaoException;

}

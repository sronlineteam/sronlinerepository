
package es.sronline.bbdd.dao;

import es.sronline.bbdd.dto.Parcelas;
import es.sronline.bbdd.dto.ParcelasPk;
import es.sronline.bbdd.exceptions.ParcelasDaoException;

public interface ParcelasDao
{
	/** 
	 * Inserts a new row in the Parcelas table.
	 */
	public ParcelasPk insert(Parcelas dto) throws ParcelasDaoException;

	/** 
	 * Updates a single row in the Parcelas table.
	 */
	public void update(ParcelasPk pk, Parcelas dto) throws ParcelasDaoException;

	/** 
	 * Deletes a single row in the Parcelas table.
	 */
	public void delete(ParcelasPk pk) throws ParcelasDaoException;

	/** 
	 * Returns the rows from the Parcelas table that matches the specified primary-key value.
	 */
	public Parcelas findByPrimaryKey(ParcelasPk pk) throws ParcelasDaoException;

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idParcelas = :idParcelas'.
	 */
	public Parcelas findByPrimaryKey(int idParcelas) throws ParcelasDaoException;

	/** 
	 * Returns all rows from the Parcelas table that match the criteria ''.
	 */
	public Parcelas[] findAll() throws ParcelasDaoException;

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idParcelas = :idParcelas'.
	 */
	public Parcelas[] findWhereIdParcelasEquals(int idParcelas) throws ParcelasDaoException;

	/** 
	 * Returns all rows from the Parcelas table that match the criteria 'idParcelas = :idParcelas'.
	 */
	public Parcelas[] findWhereIdSectorEquals(int idSector) throws ParcelasDaoException;
	

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the Parcelas table that match the specified arbitrary SQL statement
	 */
	public Parcelas[] findByDynamicSelect(String sql, Object[] sqlParams) throws ParcelasDaoException;

	/** 
	 * Returns all rows from the Parcelas table that match the specified arbitrary SQL statement
	 */
	public Parcelas[] findByDynamicWhere(String sql, Object[] sqlParams) throws ParcelasDaoException;

}

package es.sronline.bbdd.dto;

import java.io.Serializable;

public class Abonos implements Serializable
{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idabonos in the abonos table.
	 */
	protected int idAbonos;
	/** 
	 * This attribute maps to the column idAbonos in the abonos table.
	 */
	protected int idPosturas;
	/** 
	 * This attribute maps to the column idclases_abonos in the abonos table.
	 */
	protected int idClasesAabonos;
	/** 
	 * This attribute maps to the column duracion in the abonos table.
	 */
	protected int duracion;
	/** 
	 * This attribute maps to the column inicio in the abonos table.
	 */
	protected int inicio;
	/**
	 * Method 'Abonos'
	 * 
	 */
	public Abonos()
	{
	}
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Abonos)) {
			return false;
		}
		
		final Abonos _cast = (Abonos) _other;
		if (idAbonos != _cast.idAbonos) {
			return false;
		}
		
		if (idPosturas != _cast.idPosturas) {
			return false;
		}		
		
		if (idClasesAabonos != _cast.idClasesAabonos) {
			return false;
		}		
		if (duracion != _cast.duracion) {
			return false;
		}
		
		if (inicio != _cast.inicio) {
			return false;
		}		

		
		

		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idAbonos;
		_hashCode = 29 * _hashCode + idPosturas;
		_hashCode = 29 * _hashCode + idClasesAabonos;
		_hashCode = 29 * _hashCode + duracion;
		_hashCode = 29 * _hashCode + inicio;
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return abonosPk
	 */
	public AbonosPk createPk()
	{
		return new AbonosPk(idAbonos);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.abonos: " );
		ret.append( "idAbonos=" + idAbonos);
		ret.append( ", idPosturas=" + idPosturas );
		ret.append( ", idClasesAbonos=" + idClasesAabonos );
		ret.append( ", duracion=" + duracion);
		ret.append( ", inicio=" + inicio);
		return ret.toString();
	}
	public int getIdAbonos() {
		return idAbonos;
	}
	public void setIdAbonos(int idAbonos) {
		this.idAbonos = idAbonos;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public int getIdPosturas() {
		return idPosturas;
	}
	public void setIdPosturas(int idPosturas) {
		this.idPosturas = idPosturas;
	}
	public int getIdClasesAabonos() {
		return idClasesAabonos;
	}
	public void setIdClasesAabonos(int idClasesAabonos) {
		this.idClasesAabonos = idClasesAabonos;
	}
	public int getInicio() {
		return inicio;
	}
	public void setInicio(int inicio) {
		this.inicio = inicio;
	}

}

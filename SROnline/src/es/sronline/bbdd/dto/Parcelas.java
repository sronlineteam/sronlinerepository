package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.util.*;
import java.util.Date;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;

public class Parcelas implements Serializable
{
	/** 
	 * This attribute maps to the column idparcela in the parcelas table.
	 */
	protected int idparcela;

	/** 
	 * This attribute maps to the column idparcela in the parcelas table.
	 */
	protected String descripcion;

	/** 
	 * This attribute maps to the column idSector in the parcelas table.
	 */
	protected int idSector;

		
	/**
	 * Method 'Parcelas'
	 * 
	 */
	public Parcelas()
	{
	}


	public int getIdparcela() {
		return idparcela;
	}


	public void setIdparcela(int idparcela) {
		this.idparcela = idparcela;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public int getIdSector() {
		return idSector;
	}


	public void setIdSector(int idSector) {
		this.idSector = idSector;
	}


	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Parcelas)) {
			return false;
		}
		
		final Parcelas _cast = (Parcelas) _other;
		if (idparcela != _cast.idparcela) {
			return false;
		}
		
		
		if (descripcion != _cast.descripcion) {
			return false;
		}
				
		if (idSector != _cast.idSector) {
			return false;
		}
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idparcela;
		if (descripcion != null) {
			_hashCode = 29 * _hashCode + descripcion.hashCode();
		}

		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return ParcelasPk
	 */
	public ParcelasPk createPk()
	{
		return new ParcelasPk(idparcela);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.Parcelas: " );
		ret.append( "idparcela=" + idparcela );
		ret.append( ", descripcion=" + descripcion );
		ret.append( ", idsector=" + idSector );
		return ret.toString();
	}

}

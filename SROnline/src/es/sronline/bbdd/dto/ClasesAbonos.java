package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.util.*;
import java.util.Date;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;

public class ClasesAbonos implements Serializable
{
	/** 
	 * This attribute maps to the column idclases_abonos in the clases_abonos table.
	 */
	protected int idclases_abonos;

	/** 
	 * This attribute maps to the column tipo_abono in the clases_abonos table.
	 */
	protected String tipo_abono;

	/** 
	 * This attribute maps to the column inicio in the clases_abonos table.
	 */
	protected int inicio;

	/** 
	 * This attribute maps to the column duracion in the clases_abonos table.
	 */
	protected int duracion;

	
	/**
	 * Method 'ClasesAbonos'
	 * 
	 */
	public ClasesAbonos()
	{
	}

	public int getIdclases_abonos() {
		return idclases_abonos;
	}

	public void setIdclases_abonos(int idclases_abonos) {
		this.idclases_abonos = idclases_abonos;
	}

	public String getTipo_abono() {
		return tipo_abono;
	}

	public void setTipo_abono(String tipo_abono) {
		this.tipo_abono = tipo_abono;
	}

	public int getInicio() {
		return inicio;
	}

	public void setInicio(int inicio) {
		this.inicio = inicio;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ClasesAbonos)) {
			return false;
		}
		
		final ClasesAbonos _cast = (ClasesAbonos) _other;
		if (idclases_abonos != _cast.idclases_abonos) {
			return false;
		}
		
		
		if (tipo_abono != _cast.tipo_abono) {
			return false;
		}
		
		if (inicio !=  _cast.inicio) {
			return false;
		}
		
		if (duracion != _cast.duracion) {
			return false;
		}
		
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idclases_abonos;
		if (tipo_abono != null) {
			_hashCode = 29 * _hashCode + tipo_abono.hashCode();
		}
		_hashCode = 29 * _hashCode + inicio;
		_hashCode = 29 * _hashCode + duracion;		
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return ClasesAbonosPk
	 */
	public ClasesAbonosPk createPk()
	{
		return new ClasesAbonosPk(idclases_abonos);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ClasesAbonos: " );
		ret.append( "idclases_abonos=" + idclases_abonos );
		ret.append( ", tipo_abono=" + tipo_abono );
		ret.append( ", inicio=" + inicio );
		ret.append( ", duracion=" + duracion );
		return ret.toString();
	}

}

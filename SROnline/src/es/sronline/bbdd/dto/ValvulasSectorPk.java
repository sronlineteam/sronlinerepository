
package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the ValvulasSector table.
 */
public class ValvulasSectorPk implements Serializable
{
	protected int IdRemota;

	/** 
	 * This attribute represents whether the primitive attribute IdSector is null.
	 */
	protected boolean IdRemotaNull;

	/** 
	 * Sets the value of IdRemota
	 */
	public void setIdRemota(int IdRemota)
	{
		this.IdRemota = IdRemota;
	}

	/** 
	 * Gets the value of IdSector
	 */
	public int getIdRemota()
	{
		return IdRemota;
	}

	/**
	 * Method 'ValvulasSectorPk'
	 * 
	 */
	public ValvulasSectorPk()
	{
	}

	/**
	 * Method 'ValvulasSectorPk'
	 * 
	 * @param IdRemota
	 */
	public ValvulasSectorPk(final int IdRemota)
	{
		this.IdRemota = IdRemota;
	}

	/** 
	 * Sets the value of IdSectorNull
	 */
	public void setIdRemotaNull(boolean IdRemotaNull)
	{
		this.IdRemotaNull = IdRemotaNull;
	}

	/** 
	 * Gets the value of IdRemotaNull
	 */
	public boolean isIdRemotaNull()
	{
		return IdRemotaNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ValvulasSectorPk)) {
			return false;
		}
		
		final ValvulasSectorPk _cast = (ValvulasSectorPk) _other;
		if (IdRemota != _cast.IdRemota) {
			return false;
		}
		
		if (IdRemotaNull != _cast.IdRemotaNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + IdRemota;
		_hashCode = 29 * _hashCode + (IdRemotaNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ValvulasSectorPk: " );
		ret.append( "IdRemota=" + IdRemota );
		return ret.toString();
	}

}

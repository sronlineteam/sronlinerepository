
package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the Sectores table.
 */
public class ObjetosTelecontrolPk implements Serializable
{
	protected int idObjeto;

	/** 
	 * This attribute represents whether the primitive attribute idObjeto is null.
	 */
	protected boolean idObjetoNull;

	/** 
	 * Sets the value of idObjeto
	 */
	public void setidObjeto(int idObjeto)
	{
		this.idObjeto = idObjeto;
	}

	/** 
	 * Gets the value of idObjeto
	 */
	public int getidObjeto()
	{
		return idObjeto;
	}

	/**
	 * Method 'idObjetoPk'
	 * 
	 */
	public ObjetosTelecontrolPk()
	{
	}

	/**
	 * Method 'SectoresPk'
	 * 
	 * @param idObjeto
	 */
	public ObjetosTelecontrolPk(final int idObjeto)
	{
		this.idObjeto = idObjeto;
	}


	/** 
	 * Sets the value of idObjetoNull
	 */
	public void setidObjetoNull(boolean idObjetoNull)
	{
		this.idObjetoNull = idObjetoNull;
	}

	/** 
	 * Gets the value of idObjetoNull
	 */
	public boolean isidObjetoNull()
	{
		return idObjetoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ObjetosTelecontrolPk)) {
			return false;
		}
		
		final ObjetosTelecontrolPk _cast = (ObjetosTelecontrolPk) _other;
		if (idObjeto != _cast.idObjeto) {
			return false;
		}
		
		if (idObjetoNull != _cast.idObjetoNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idObjeto;
		_hashCode = 29 * _hashCode + (idObjetoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ObjetosTelecontrolPk: " );
		ret.append( "idObjeto=" + idObjeto );
		return ret.toString();
	}

}


package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the Sectores table.
 */
public class SectoresPk implements Serializable
{
	protected int IdSector;

	/** 
	 * This attribute represents whether the primitive attribute IdSector is null.
	 */
	protected boolean IdSectorNull;

	/** 
	 * Sets the value of IdSector
	 */
	public void setIdSector(int IdSector)
	{
		this.IdSector = IdSector;
	}

	/** 
	 * Gets the value of IdSector
	 */
	public int getIdSector()
	{
		return IdSector;
	}

	/**
	 * Method 'IdSectorPk'
	 * 
	 */
	public SectoresPk()
	{
	}

	/**
	 * Method 'SectoresPk'
	 * 
	 * @param IdSector
	 */
	public SectoresPk(final int IdSector)
	{
		this.IdSector = IdSector;
	}

	/** 
	 * Sets the value of IdSectorNull
	 */
	public void setIdSectorNull(boolean IdSectorNull)
	{
		this.IdSectorNull = IdSectorNull;
	}

	/** 
	 * Gets the value of IdSectorNull
	 */
	public boolean isIdSectorNull()
	{
		return IdSectorNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof SectoresPk)) {
			return false;
		}
		
		final SectoresPk _cast = (SectoresPk) _other;
		if (IdSector != _cast.IdSector) {
			return false;
		}
		
		if (IdSectorNull != _cast.IdSectorNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + IdSector;
		_hashCode = 29 * _hashCode + (IdSectorNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.SectoresPk: " );
		ret.append( "IdSector=" + IdSector );
		return ret.toString();
	}

}

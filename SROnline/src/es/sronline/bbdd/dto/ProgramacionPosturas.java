package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.util.Date;

public class ProgramacionPosturas implements Serializable 
{
	/** 
	 * This attribute maps to the column Idprogramacion_Postura in the programacion_posturas table.
	 */
	protected int Idprogramacion_Postura;

	/** 
	 * This attribute maps to the column id_sector in the programacion_posturas table.
	 */
	protected int idsector;

	

	/** 
	 * This attribute maps to the column fecha in the programacion_posturas table.
	 */
	protected Date fecha;

	/** 
	 * This attribute maps to the column des_accion in the programacion_posturas table.
	 */
	protected int minutos;

	
	/**
	 * Method 'ProgramacionPosturas'
	 * 
	 */
	public ProgramacionPosturas()
	{
	}

	/**
	 * Method 'getIdprogramacion_Postura'
	 * 
	 * @return int
	 */
	public int getIdprogramacion_Postura()
	{
		return Idprogramacion_Postura;
	}

	/**
	 * Method 'setIdprogramacion_Postura'
	 * 
	 * @param Idprogramacion_Postura
	 */
	public void setIdprogramacion_Postura(int Idprogramacion_Postura)
	{
		this.Idprogramacion_Postura = Idprogramacion_Postura;
	}

	/**
	 * Method 'getidsector'
	 * 
	 * @return int
	 */
	public int getidsector()
	{
		return idsector;
	}

	/**
	 * Method 'setidsector'
	 * 
	 * @param idsector
	 */
	public void setidsector(int idsector)
	{
		this.idsector = idsector;
	}

	

	/**
	 * Method 'getfecha'
	 * 
	 * @return Date
	 */
	public Date getfecha()
	{
		return fecha;
	}

	/**
	 * Method 'setfecha'
	 * 
	 * @param fecha
	 */
	public void setfecha(Date fecha)
	{
		this.fecha = fecha;
	}

	/**
	 * Method 'getminutos'
	 * 
	 * @return int
	 */
	public int getminutos()
	{
		return minutos;
	}

	/**
	 * Method 'setminutos'
	 * 
	 * @param minutos
	 */
	public void setminutos(int minutos)
	{
		this.minutos = minutos;
	}

	
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ProgramacionPosturas)) {
			return false;
		}
		
		final ProgramacionPosturas _cast = (ProgramacionPosturas) _other;
		if (Idprogramacion_Postura != _cast.Idprogramacion_Postura) {
			return false;
		}
		
		if (idsector != _cast.idsector) {
			return false;
		}
		
		
		
		if (fecha == null ? _cast.fecha != fecha : !fecha.equals( _cast.fecha )) {
			return false;
		}
		
		if (minutos != _cast.minutos) {
			return false;
		}
		
				
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + Idprogramacion_Postura;
		_hashCode = 29 * _hashCode + idsector;
		if (fecha != null) {
			_hashCode = 29 * _hashCode + fecha.hashCode();
		}
		
		_hashCode = 29 * _hashCode + minutos;
		
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return ProgramacionPk
	 */
	public ProgramacionPosturasPk createPk()
	{
		return new ProgramacionPosturasPk(Idprogramacion_Postura);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ProgramacionPostura: " );
		ret.append( "Idprogramacion_Postura=" + Idprogramacion_Postura );
		ret.append( ", idsector=" + idsector );
		ret.append( ", fecha=" + fecha );
		ret.append( ", minutos=" + minutos );
		return ret.toString();
	}

}


package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.util.*;
import java.util.Date;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;

public class ValvulasSector implements Serializable
{
	/** 
	 * This attribute maps to the column id_remota in the ValvulasSector table.
	 */
	protected int idRemota;

	/** 
	 * This attribute maps to the column id_valvula in the ValvulasSector table.
	 */
	protected int idValvula;

	/** 
	 * This attribute maps to the column sectores_idsector in the ValvulasSector table.
	 */
	protected int sectores_idsector;

	/**
	 * Method 'ValvulasSector'
	 * 
	 */
	public ValvulasSector()
	{
	}

	/**
	 * Method 'getidremota'
	 * 
	 * @return int
	 */
	public int getidRemota()
	{
		return idRemota;
	}

	/**
	 * Method 'setidremota'
	 * 
	 * @param idremota
	 */
	public void setidRremota(int idRemota)
	{
		this.idRemota = idRemota;
	}

	
	/**
	 * Method 'getIdValvula'
	 * 
	 * @return int
	 */
	public int getIdValvula()
	{
		return idValvula;
	}

	/**
	 * Method 'setIdValvula'
	 * 
	 * @param idValvula
	 */
	public void setIdValvula(int idValvula)
	{
		this.idValvula = idValvula;
	}

	/**
	 * Method 'getsectores_idsector'
	 * 
	 * @return int
	 */
	public int getsectores_idsector()
	{
		return sectores_idsector;
	}

	/**
	 * Method 'setsectores_idsector'
	 * 
	 * @param sectores_idsector
	 */
	public void setsectores_idsector(int sectores_idsector)
	{
		this.sectores_idsector = sectores_idsector;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ValvulasSector)) {
			return false;
		}
		
		final ValvulasSector _cast = (ValvulasSector) _other;
		if (idRemota != _cast.idRemota) {
			return false;
		}
		
		if (idValvula != _cast.idValvula) {
			return false;
		}
				
		if (sectores_idsector != _cast.sectores_idsector) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idRemota;
		_hashCode = 29 * _hashCode + idValvula;		
		_hashCode = 29 * _hashCode + sectores_idsector;
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return sectores_idsectorPk
	 */
	public ValvulasSectorPk createPk()
	{
		return new ValvulasSectorPk(idRemota);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.sectores_idsector: " );
		ret.append( "idremota=" + idRemota );
		ret.append( ", idValvula=" + idValvula );
		ret.append( ", sectores_idsector=" + sectores_idsector );
		return ret.toString();
	}

}

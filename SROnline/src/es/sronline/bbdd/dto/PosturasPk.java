package es.sronline.bbdd.dto;

public class PosturasPk {
	protected int idposturas;

	/** 
	 * This attribute represents whether the primitive attribute idposturas is null.
	 */
	protected boolean idposturasNull;

	/** 
	 * Sets the value of idposturas
	 */
	public void setIdposturas(int idposturas)
	{
		this.idposturas = idposturas;
	}

	/** 
	 * Gets the value of idposturas
	 */
	public int getIdposturas()
	{
		return idposturas;
	}

	/**
	 * Method 'posturasPk'
	 * 
	 */
	public PosturasPk()
	{
	}

	/**
	 * Method 'posturasPk'
	 * 
	 * @param idposturas
	 */
	public PosturasPk(final int idposturas)
	{
		this.idposturas = idposturas;
	}

	/** 
	 * Sets the value of idposturasNull
	 */
	public void setIdposturasNull(boolean idposturasNull)
	{
		this.idposturasNull = idposturasNull;
	}

	/** 
	 * Gets the value of idposturasNull
	 */
	public boolean isIdposturasNull()
	{
		return idposturasNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof PosturasPk)) {
			return false;
		}
		
		final PosturasPk _cast = (PosturasPk) _other;
		if (idposturas != _cast.idposturas) {
			return false;
		}
		
		if (idposturasNull != _cast.idposturasNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idposturas;
		_hashCode = 29 * _hashCode + (idposturasNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.posturasPk: " );
		ret.append( "idposturas=" + idposturas );
		return ret.toString();
	}

}

package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the Parcelas table.
 */
public class ParcelasPk implements Serializable
{
	protected int idparcela;

	/** 
	 * This attribute represents whether the primitive attribute idparcelas is null.
	 */
	protected boolean idparcelasNull;

	/** 
	 * Sets the value of idparcelas
	 */
	public void setIdParcela(int idparcelas)
	{
		this.idparcela = idparcelas;
	}

	/** 
	 * Gets the value of idparcelas
	 */
	public int getIdParcela()
	{
		return idparcela;
	}

	/**
	 * Method 'ParcelasPk'
	 * 
	 */
	public ParcelasPk()
	{
	}

	/**
	 * Method 'ParcelasPk'
	 * 
	 * @param idparcelas
	 */
	public ParcelasPk(final int idparcelas)
	{
		this.idparcela = idparcelas;
	}

	/** 
	 * Sets the value of idparcelasNull
	 */
	public void setidparcelasNull(boolean idparcelasNull)
	{
		this.idparcelasNull = idparcelasNull;
	}

	/** 
	 * Gets the value of idparcelasNull
	 */
	public boolean isidparcelasNull()
	{
		return idparcelasNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ParcelasPk)) {
			return false;
		}
		
		final ParcelasPk _cast = (ParcelasPk) _other;
		if (idparcela != _cast.idparcela) {
			return false;
		}
		
		if (idparcelasNull != _cast.idparcelasNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idparcela;
		_hashCode = 29 * _hashCode + (idparcelasNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ParcelasPk: " );
		ret.append( "idparcelas=" + idparcela );
		return ret.toString();
	}

}

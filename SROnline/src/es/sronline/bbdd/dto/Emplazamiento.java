package es.sronline.bbdd.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Emplazamiento implements Serializable{
	protected String emplazamiento;
	protected String posicion;
	
	protected int idEmplazamiento;
	protected int idEmplazamientoPadre;
	
	public String getEmplazamiento() {
		return emplazamiento;
	}
	public void setEmplazamiento(String emplazamiento) {
		this.emplazamiento = emplazamiento;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public int getIdEmplazamiento() {
		return idEmplazamiento;
	}
	public void setIdEmplazamiento(int idEmplazamiento) {
		this.idEmplazamiento = idEmplazamiento;
	}
	public int getIdEmplazamientoPadre() {
		return idEmplazamientoPadre;
	}
	public void setIdEmplazamientoPadre(int idEmplazamientoPadre) {
		this.idEmplazamientoPadre = idEmplazamientoPadre;
	}
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Emplazamiento)) {
			return false;
		}
		
		final Emplazamiento _cast = (Emplazamiento) _other;
		
		
		if (emplazamiento == null ? _cast.emplazamiento != emplazamiento: !emplazamiento.equals( _cast.emplazamiento )) {
			return false;
		}
		
		
	
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return ObjetosTelecontrolPk
	 */
	public EmplazamientoPk createPk()
	{
		return new EmplazamientoPk(idEmplazamiento);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.Emplazamiento: " );
		ret.append( "idEmplazamiento=" + idEmplazamiento );
		ret.append( "emplazamiento=" + emplazamiento );
		ret.append( ", posicion=" + posicion );
		ret.append( "idEmplazamientoPadre=" + idEmplazamientoPadre);
		return ret.toString();
	}

}

package es.sronline.bbdd.dto;

import java.io.Serializable;

public class TipoObjetoTelecontrol implements Serializable
{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * This attribute maps to the column idTipoObjetoTelecontrol in the tipo_obj_telecontrol table.
	 */
	protected int idTipoObjetoTelecontrol;
	/** 
	 * This attribute maps to the column tipo_obj_telecontrol in the tipo_obj_telecontrol table.
	 */
	protected String tipoObjetoTelecontrol;
	/** 
	 * This attribute maps to the column descripcion in the tipo_obj_telecontrol table.
	 */
	protected String descripcion;
	/** 
	 * This attribute maps to the column tiempo_respuesta in the tipo_obj_telecontrol table.
	 */
	protected int tiempoRespuesta;

	public TipoObjetoTelecontrol()
	{
	}
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof TipoObjetoTelecontrol)) {
			return false;
		}
		
		final TipoObjetoTelecontrol _cast = (TipoObjetoTelecontrol) _other;
		if (idTipoObjetoTelecontrol != _cast.idTipoObjetoTelecontrol) {
			return false;
		}
		
		if (tipoObjetoTelecontrol != _cast.tipoObjetoTelecontrol) {
			return false;
		}		
		
		if (descripcion != _cast.descripcion) {
			return false;
		}		


		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idTipoObjetoTelecontrol;
		if (tipoObjetoTelecontrol != null) {
			_hashCode = 29 * _hashCode + tipoObjetoTelecontrol.hashCode();
		}
		if (descripcion != null) {
			_hashCode = 29 * _hashCode + descripcion.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return TiposObjetoTelecontrolPk
	 */
	public TipoObjetoTelecontrolPk createPk()
	{
		return new TipoObjetoTelecontrolPk(idTipoObjetoTelecontrol);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.abonos: " );
		ret.append( "idTipoObjetoTelecontrol=" + idTipoObjetoTelecontrol);
		ret.append( ", TipoObjetoTelecontrol=" + tipoObjetoTelecontrol );
		ret.append( ", descripcion=" + descripcion );
		ret.append( ", tiempoRespuesta=" + tiempoRespuesta );
		return ret.toString();
	}

	
	public int getIdTipoObjetoTelecontrol() {
		return idTipoObjetoTelecontrol;
	}
	public void setIdTipoObjetoTelecontrol(int idTipoObjetoTelecontrol) {
		this.idTipoObjetoTelecontrol = idTipoObjetoTelecontrol;
	}
	public String getTipoObjetoTelecontrol() {
		return tipoObjetoTelecontrol;
	}
	public void setTipoObjetoTelecontrol(String TipoObjetoTelecontrol) {
		this.tipoObjetoTelecontrol = TipoObjetoTelecontrol;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getTiempoRespuesta() {
		return tiempoRespuesta;
	}
	public void setTiempoRespuesta(int tiempoRespuesta) {
		this.tiempoRespuesta = tiempoRespuesta;
	}

}

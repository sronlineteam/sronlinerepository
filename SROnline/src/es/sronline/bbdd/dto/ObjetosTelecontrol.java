/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package es.sronline.bbdd.dto;

import java.io.Serializable;

public class ObjetosTelecontrol implements Serializable
{

	/**
	 * Method 'LstValvulas'
	 * 
	 */
	public ObjetosTelecontrol()
	{
	}


	protected int idObjeto;
	protected String nombre;
	protected String conexion;
	protected String tipoConexion;
	protected String direccionHex;
	protected String estado;
	protected String posicion;
	protected String esclavo;
	protected int controFlujo;
	protected String controlTarifa;
	protected int orden;
	protected int idEmplazamiento;
	protected int idTipoObjetoTelecontrol;


	/**
	 * Method 'getIdRemota'
	 * 
	 * @return int
	 */
	public int getIdObjeto() {
		return idObjeto;
	}
	public void setIdObjeto(int idObjeto) {
		this.idObjeto = idObjeto;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoConexion() {
		return tipoConexion;
	}
	public void setTipoConexion(String tipoConexion) {
		this.tipoConexion = tipoConexion;
	}
	public String getConexion() {
		return conexion;
	}
	public void setConexion(String conexion) {
		this.conexion = conexion;
	}
	public String getDireccionHex() {
		return direccionHex;
	}
	public void setDireccionHex(String direccionHex) {
		this.direccionHex = direccionHex;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public String getEsclavo() {
		return esclavo;
	}
	public void setEsclavo(String esclavo) {
		this.esclavo = esclavo;
	}
	
	public int getControFlujo() {
		return controFlujo;
	}
	public void setControFlujo(int controFlujo) {
		this.controFlujo = controFlujo;
	}

	public String getControlTarifa() {
		return controlTarifa;
	}
	public void setControlTarifa(String controlTarifa) {
		this.controlTarifa = controlTarifa;
	}
	
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getIdEmplazamiento() {
		return idEmplazamiento;
	}
	public int getIdTipoObjetoTelecontrol() {
		return idTipoObjetoTelecontrol;
	}
	public void setIdTipoObjetoTelecontrol(int idTipoObjetoTelecontrol) {
		this.idTipoObjetoTelecontrol = idTipoObjetoTelecontrol;
	}
	public void setIdEmplazamiento(int idEmplazamiento) {
		this.idEmplazamiento = idEmplazamiento;
	}
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ObjetosTelecontrol)) {
			return false;
		}
		
		final ObjetosTelecontrol _cast = (ObjetosTelecontrol) _other;
		
		
		if (nombre == null ? _cast.nombre != nombre : !nombre.equals( _cast.nombre )) {
			return false;
		}
		
		if (estado == null ? _cast.estado != estado : !estado.equals( _cast.estado )) {
			return false;
		}
		
	
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		return _hashCode;
	}
	/**
	 * Method 'createPk'
	 * 
	 * @return ObjetosTelecontrolPk
	 */
	public ObjetosTelecontrolPk createPk()
	{
		return new ObjetosTelecontrolPk(idObjeto);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ObjetosTelecontrol: " );
		ret.append( "nombre=" + nombre );
		ret.append( ", conexion=" + conexion );
		ret.append( ", direccionHex=" + direccionHex );
		ret.append( ", estado=" + estado );
		ret.append( ", posicion=" + posicion );
		ret.append( ", esclavo=" + esclavo);
		ret.append( ", controlTarifa=" + controlTarifa);
		ret.append( ", orden=" + orden);
		ret.append( ", idEmplazamiento=" + idEmplazamiento);
		ret.append( ", idTipoObjetoTelecontro=" + idTipoObjetoTelecontrol);
		return ret.toString();
	}

}

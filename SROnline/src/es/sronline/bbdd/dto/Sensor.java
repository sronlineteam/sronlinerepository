/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.util.*;
import java.util.Date;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.exceptions.*;
import es.sronline.bbdd.factory.*;

public class Sensor implements Serializable
{
	/** 
	 * This attribute maps to the column idSensores in the Sensores table.
	 */
	protected int idSensor;

	/** 
	 * This attribute maps to the column id_remota in the Sensores table.
	 */
	protected int idObjeto;

	/** 
	 * This attribute maps to the column posicion in the Sensores table.
	 */
	protected int posicion;

	/** 
	 * This attribute maps to the column dir_inicial in the Sensores table.
	 */
	protected String dirInicial;

	/** 
	 * This attribute maps to the column num_salidas in the Sensores table.
	 */
	protected int numSalidas;
	
	/**
	 * Method 'Sensores'
	 * 
	 */
	public Sensor()
	{
	}

	/**
	 * Method 'getIdSensores'
	 * 
	 * @return int
	 */
	public int getIdSensor()
	{
		return idSensor;
	}

	/**
	 * Method 'setIdSensores'
	 * 
	 * @param idSensores
	 */
	public void setIdSensor(int idSensor)
	{
		this.idSensor = idSensor;
	}

	/**
	 * Method 'getIdObjeto'
	 * 
	 * @return int
	 */
	public int getIdObjeto()
	{
		return idObjeto;
	}

	/**
	 * Method 'setIdObjeto'
	 * 
	 * @param idObjeto
	 */
	public void setIdObjeto(int idObjeto)
	{
		this.idObjeto = idObjeto;
	}
	/**
	 * Method 'getPosicion'
	 * 
	 * @return int
	 */
	public int getPosicion()
	{
		return posicion;
	}

	/**
	 * Method 'setPosicion'
	 * 
	 * @param Posicion
	 */
	public void setPosicion(int Posicion)
	{
		this.posicion = posicion;
	}

	public String getDirInicial() {
		return dirInicial;
	}

	public void setDirInicial(String dirInicial) {
		this.dirInicial = dirInicial;
	}

	public int getNumSalidas() {
		return numSalidas;
	}

	public void setNumSalidas(int numSalidas) {
		this.numSalidas = numSalidas;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof Sensor)) {
			return false;
		}
		
		final Sensor _cast = (Sensor) _other;
		if (idSensor != _cast.idSensor) {
			return false;
		}
		if (idObjeto != _cast.idObjeto) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idSensor;
		_hashCode = 29 * _hashCode + idObjeto;
		_hashCode = 29 * _hashCode + posicion;
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return SensoresPk
	 */
	public SensorPk createPk()
	{
		return new SensorPk(idSensor);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.Sensores: " );
		ret.append( "idSensores=" + idSensor );
		ret.append( ", idObjeto=" + idObjeto );
		ret.append( ", posicion=" + posicion );
		return ret.toString();
	}

}

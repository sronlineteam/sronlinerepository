package es.sronline.bbdd.dto;

public class AccionesTelecontrol {
	protected int idTipoObjetoTelecontrol;
	protected int idAccion;
	protected String comando; 
	protected String accion;
	protected String byte6;
	protected int tieneCRC;
	protected String respuestaOK;
	
	public int getIdTipoObjetoTelecontrol() {
		return idTipoObjetoTelecontrol;
	}
	public void setIdTipoObjetoTelecontrol(int idTipoObjetoTelecontrol) {
		this.idTipoObjetoTelecontrol = idTipoObjetoTelecontrol;
	}
	public int getIdAccion() {
		return idAccion;
	}
	public void setIdAccion(int idAccion) {
		this.idAccion = idAccion;
	}
	public String getComando() {
		return comando;
	}
	public void setComando(String comando) {
		this.comando = comando;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getByte6() {
		return byte6;
	}
	public void setByte6(String byte6) {
		this.byte6 = byte6;
	}
	public int getTieneCRC() {
		return tieneCRC;
	}
	public void setTieneCRC(int tieneCRC) {
		this.tieneCRC = tieneCRC;
	}
	public String getRespuestaOK() {
		return respuestaOK;
	}
	public void setRespuestaOK(String respuestaOK) {
		this.respuestaOK = respuestaOK;
	}
	public AccionesTelecontrolPk createPk(){
	  return new AccionesTelecontrolPk(idTipoObjetoTelecontrol, idAccion);
	}
	
}

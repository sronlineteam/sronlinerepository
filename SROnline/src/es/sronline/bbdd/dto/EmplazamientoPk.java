package es.sronline.bbdd.dto;

import java.io.Serializable;

public class EmplazamientoPk implements Serializable {
	protected int idEmplazamiento;

	/** 
	 * This attribute represents whether the primitive attribute idEmplazamiento is null.
	 */
	protected boolean idEmplazamientoNull;

	/** 
	 * Sets the value of idEmplazamiento
	 */
	public void setidEmplazamiento(int idEmplazamiento)
	{
		this.idEmplazamiento = idEmplazamiento;
	}

	/** 
	 * Gets the value of idEmplazamiento
	 */
	public int getidEmplazamiento()
	{
		return idEmplazamiento;
	}

	/**
	 * Method 'idEmplazamientoPk'
	 * 
	 */
	public EmplazamientoPk()
	{
	}

	/**
	 * Method 'SectoresPk'
	 * 
	 * @param idEmplazamiento
	 */
	public EmplazamientoPk(final int idEmplazamiento)
	{
		this.idEmplazamiento = idEmplazamiento;
	}


	/** 
	 * Sets the value of idEmplazamientoNull
	 */
	public void setidEmplazamientoNull(boolean idEmplazamientoNull)
	{
		this.idEmplazamientoNull = idEmplazamientoNull;
	}

	/** 
	 * Gets the value of idEmplazamientoNull
	 */
	public boolean isidEmplazamientoNull()
	{
		return idEmplazamientoNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof EmplazamientoPk)) {
			return false;
		}
		
		final EmplazamientoPk _cast = (EmplazamientoPk) _other;
		if (idEmplazamiento != _cast.idEmplazamiento) {
			return false;
		}
		
		if (idEmplazamientoNull != _cast.idEmplazamientoNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idEmplazamiento;
		_hashCode = 29 * _hashCode + (idEmplazamientoNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ObjetosTelecontrolPk: " );
		ret.append( "idEmplazamiento=" + idEmplazamiento );
		return ret.toString();
	}

}

/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the Programadores table.
 */
public class ProgramadoresPk implements Serializable
{
	protected int idProgramador;

	/** 
	 * This attribute represents whether the primitive attribute idProgramador is null.
	 */
	protected boolean idProgramadorNull;

	/** 
	 * Sets the value of idProgramador
	 */
	public void setIdProgramador(int idProgramador)
	{
		this.idProgramador = idProgramador;
	}

	/** 
	 * Gets the value of idProgramador
	 */
	public int getIdProgramador()
	{
		return idProgramador;
	}

	/**
	 * Method 'ProgramadoresPk'
	 * 
	 */
	public ProgramadoresPk()
	{
	}

	/**
	 * Method 'ProgramadoresPk'
	 * 
	 * @param idProgramador
	 */
	public ProgramadoresPk(final int idProgramador)
	{
		this.idProgramador = idProgramador;
	}

	/** 
	 * Sets the value of idProgramadorNull
	 */
	public void setIdProgramadorNull(boolean idProgramadorNull)
	{
		this.idProgramadorNull = idProgramadorNull;
	}

	/** 
	 * Gets the value of idProgramadorNull
	 */
	public boolean isIdProgramadorNull()
	{
		return idProgramadorNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ProgramadoresPk)) {
			return false;
		}
		
		final ProgramadoresPk _cast = (ProgramadoresPk) _other;
		if (idProgramador != _cast.idProgramador) {
			return false;
		}
		
		if (idProgramadorNull != _cast.idProgramadorNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idProgramador;
		_hashCode = 29 * _hashCode + (idProgramadorNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ProgramadoresPk: " );
		ret.append( "idProgramador=" + idProgramador );
		return ret.toString();
	}

}

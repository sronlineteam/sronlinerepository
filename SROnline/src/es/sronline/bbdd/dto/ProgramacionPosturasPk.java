package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the programacion table.
 */
public class ProgramacionPosturasPk implements Serializable 
{
	protected int idprogramacion_postura;

	/** 
	 * This attribute represents whether the primitive attribute idprogramacion_postura is null.
	 */
	protected boolean idprogramacion_posturaNull;

	/** 
	 * Sets the value of idprogramacion_postura
	 */
	public void setidprogramacion_postura(int idprogramacion_postura)
	{
		this.idprogramacion_postura = idprogramacion_postura;
	}

	/** 
	 * Gets the value of idprogramacion_postura
	 */
	public int getidprogramacion_postura()
	{
		return idprogramacion_postura;
	}

	/**
	 * Method 'ProgramacionPk'
	 * 
	 */
	public ProgramacionPosturasPk()
	{
	}

	/**
	 * Method 'ProgramacionPk'
	 * 
	 * @param idprogramacion_postura
	 */
	public ProgramacionPosturasPk(final int idprogramacion_postura)
	{
		this.idprogramacion_postura = idprogramacion_postura;
	}

	/** 
	 * Sets the value of idprogramacion_posturaNull
	 */
	public void setidprogramacion_posturaNull(boolean idprogramacion_posturaNull)
	{
		this.idprogramacion_posturaNull = idprogramacion_posturaNull;
	}

	/** 
	 * Gets the value of idprogramacion_posturaNull
	 */
	public boolean isidprogramacion_posturaNull()
	{
		return idprogramacion_posturaNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ProgramacionPosturasPk)) {
			return false;
		}
		
		final ProgramacionPosturasPk _cast = (ProgramacionPosturasPk) _other;
		if (idprogramacion_postura != _cast.idprogramacion_postura) {
			return false;
		}
		
		if (idprogramacion_posturaNull != _cast.idprogramacion_posturaNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idprogramacion_postura;
		_hashCode = 29 * _hashCode + (idprogramacion_posturaNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ProgramacionPosturasPk: " );
		ret.append( "idprogramacion_postura=" + idprogramacion_postura );
		return ret.toString();
	}
}

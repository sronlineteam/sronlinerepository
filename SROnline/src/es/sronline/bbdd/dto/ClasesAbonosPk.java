package es.sronline.bbdd.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the ClasesAbonos table.
 */
public class ClasesAbonosPk implements Serializable
{
	protected int idclases_abonos;

	/** 
	 * This attribute represents whether the primitive attribute idclases_abonos is null.
	 */
	protected boolean idclases_abonosNull;

	/** 
	 * Sets the value of idclases_abonos
	 */
	public void setidclases_abonos(int idclases_abonos)
	{
		this.idclases_abonos = idclases_abonos;
	}

	/** 
	 * Gets the value of idclases_abonos
	 */
	public int getidclases_abonos()
	{
		return idclases_abonos;
	}

	/**
	 * Method 'ClasesAbonosPk'
	 * 
	 */
	public ClasesAbonosPk()
	{
	}

	/**
	 * Method 'ClasesAbonosPk'
	 * 
	 * @param idclases_abonos
	 */
	public ClasesAbonosPk(final int idclases_abonos)
	{
		this.idclases_abonos = idclases_abonos;
	}

	/** 
	 * Sets the value of idclases_abonosNull
	 */
	public void setidclases_abonosNull(boolean idclases_abonosNull)
	{
		this.idclases_abonosNull = idclases_abonosNull;
	}

	/** 
	 * Gets the value of idclases_abonosNull
	 */
	public boolean isidclases_abonosNull()
	{
		return idclases_abonosNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof ClasesAbonosPk)) {
			return false;
		}
		
		final ClasesAbonosPk _cast = (ClasesAbonosPk) _other;
		if (idclases_abonos != _cast.idclases_abonos) {
			return false;
		}
		
		if (idclases_abonosNull != _cast.idclases_abonosNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + idclases_abonos;
		_hashCode = 29 * _hashCode + (idclases_abonosNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "es.sronline.bbdd.dto.ClasesAbonosPk: " );
		ret.append( "idclases_abonos=" + idclases_abonos );
		return ret.toString();
	}

}

package es.sronline.bbdd.dto;

public class ProgramacionTelecontrolHistorial extends ProgramacionTelecontrol {
	/** 
	 * This attribute maps to the column enviado in the programacion_telecontrol table.
	 */
	protected int enviado;

	/** 
	 * This attribute maps to the column error in the programacion_telecontrol table.
	 */
	protected int error;


	/** 
	 * This attribute maps to the column idobj_telecontrol in the resultado table.
	 */
	protected String resultado;
}

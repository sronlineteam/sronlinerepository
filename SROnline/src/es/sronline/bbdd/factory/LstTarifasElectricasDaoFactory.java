
package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.jdbc.*;

public class LstTarifasElectricasDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return TarifasElectricasDao
	 */
	public static LstTarifasElectricasDao create()
	{
		return new LstTarifasElectricasDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return TarifasElectricasDao
	 */
	public static LstTarifasElectricasDao create(Connection conn)
	{
		return new LstTarifasElectricasDaoImpl( conn );
	}

}

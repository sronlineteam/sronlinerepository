
package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.jdbc.*;

public class ValvulasSectorDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return ValvulasSectorDao
	 */
	public static ValvulasSectorDao create()
	{
		return new ValvulasSectorDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ValvulasSectorDao
	 */
	public static ValvulasSectorDao create(Connection conn)
	{
		return new ValvulasSectorDaoImpl( conn );
	}

}

package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.jdbc.*;

public class ClasesAbonosDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return ClasesAbonosDao
	 */
	public static ClasesAbonosDao create()
	{
		return new ClasesAbonosDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static ClasesAbonosDao create(Connection conn)
	{
		return new ClasesAbonosDaoImpl( conn );
	}

}

package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.jdbc.*;

public class ParcelasDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return ParcelasDao
	 */
	public static ParcelasDao create()
	{
		return new ParcelasDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static ParcelasDao create(Connection conn)
	{
		return new ParcelasDaoImpl( conn );
	}

}

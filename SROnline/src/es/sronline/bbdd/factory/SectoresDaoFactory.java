
package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.*;
import es.sronline.bbdd.jdbc.*;

public class SectoresDaoFactory
{
	/**
	 * Method 'create'
	 * 
	 * @return SectoresDao
	 */
	public static SectoresDao create()
	{
		return new SectoresDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return SectoresDao
	 */
	public static SectoresDao create(Connection conn)
	{
		return new SectoresDaoImpl( conn );
	}

}

package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.PosturasDao;
import es.sronline.bbdd.jdbc.PosturasDaoImpl;

public class PosturasDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static PosturasDao create()
	{
		return new PosturasDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static PosturasDao create(Connection conn)
	{
		return new PosturasDaoImpl( conn );
	}
}

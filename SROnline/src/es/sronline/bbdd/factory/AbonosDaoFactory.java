package es.sronline.bbdd.factory;

import java.sql.Connection;

import es.sronline.bbdd.dao.AbonosDao;
import es.sronline.bbdd.jdbc.AbonosDaoImpl;

public class AbonosDaoFactory {
	/**
	 * Method 'create'
	 * 
	 * @return ProgramacionDao
	 */
	public static AbonosDao create()
	{
		return new AbonosDaoImpl();
	}

	/**
	 * Method 'create'
	 * 
	 * @param conn
	 * @return ProgramacionDao
	 */
	public static AbonosDao create(Connection conn)
	{
		return new AbonosDaoImpl( conn );
	}
}

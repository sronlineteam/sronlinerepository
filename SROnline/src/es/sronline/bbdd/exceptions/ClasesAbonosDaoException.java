package es.sronline.bbdd.exceptions;

public class ClasesAbonosDaoException extends Exception {
	/**
	 * Method 'ClasesAbonosDaoException'
	 * 
	 * @param message
	 */
	public ClasesAbonosDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'ClasesAbonosDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public ClasesAbonosDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}

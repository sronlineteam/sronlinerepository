package es.sronline.bbdd.exceptions;

public class AbonosDaoException  extends Exception{
	/**
	 * Method 'AbonosDaoException'
	 * 
	 * @param message
	 * */
	 
	public AbonosDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'AbonosDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public AbonosDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
//	private static final long serialVersionUID = 1L;

}

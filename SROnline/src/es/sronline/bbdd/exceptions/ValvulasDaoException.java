/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package es.sronline.bbdd.exceptions;

public class ValvulasDaoException extends DaoException
{
	/**
	 * Method 'ValvulasDaoException'
	 * 
	 * @param message
	 */
	public ValvulasDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'ValvulasDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public ValvulasDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}

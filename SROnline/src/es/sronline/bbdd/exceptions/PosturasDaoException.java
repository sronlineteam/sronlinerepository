package es.sronline.bbdd.exceptions;

public class PosturasDaoException extends Exception {

	/**
	 * Method 'PosturasDaoException'
	 * 
	 * @param message
	 * */
	 
	public PosturasDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'PosturasDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public PosturasDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
//	private static final long serialVersionUID = 1L;

}


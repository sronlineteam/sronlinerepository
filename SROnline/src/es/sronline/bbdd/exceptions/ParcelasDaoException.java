package es.sronline.bbdd.exceptions;

public class ParcelasDaoException extends Exception {
	/**
	 * Method 'ParcelasDaoException'
	 * 
	 * @param message
	 */
	public ParcelasDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'ParcelasDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public ParcelasDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}

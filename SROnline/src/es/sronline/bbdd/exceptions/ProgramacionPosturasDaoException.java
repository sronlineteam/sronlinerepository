package es.sronline.bbdd.exceptions;

public class ProgramacionPosturasDaoException extends DaoException {
	/**
	 * Method 'ProgramacionPosturasDaoException'
	 * 
	 * @param message
	 */
	public ProgramacionPosturasDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'ProgramacionPosturasDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public ProgramacionPosturasDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

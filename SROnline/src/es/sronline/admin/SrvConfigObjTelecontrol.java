package es.sronline.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.EmplazamientoDao;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dao.ObjetosTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.Emplazamiento;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.dto.ObjetosTelecontrol;
import es.sronline.bbdd.dto.ObjetosTelecontrolPk;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.exceptions.EmplazamientoDaoException;
import es.sronline.bbdd.exceptions.ObjetosTelecontrolDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.EmplazamientoDaoFactory;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.ObjetosTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvObjTelecontrol
 */
@WebServlet("/SrvConfigObjTelecontrol")
public class SrvConfigObjTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvConfigObjTelecontrol() {
        super();
        log = Logger.getLogger(getClass().getName());

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	final int ADMIN_ACCION_BORRAR = 0;
    	final int ADMIN_ACCION_MOSTRAR = 1;
    	final int ADMIN_ACCION_ACTUALIZAR = 2;
    	final int ADMIN_ACCION_INSERTAR = 3;
    	
		log.debug("*********recibida peticion en SrvConfigObjTelecontrol*********");
		String paginaDestino = null;
			
		String idObjTeleControl = request.getParameter("idObjTeleControl");
		String accion = request.getParameter("accion");

		EmplazamientoDao daoEmplazamientos = EmplazamientoDaoFactory.create();
		TipoObjetoTelecontrolDao daoTipObjTC = TipoObjetoTelecontrolDaoFactory.create();

		try {
			request.setAttribute("lstEmplazamientos", daoEmplazamientos.findAll());
			request.setAttribute("lstTipoObjetoTelecontrol", daoTipObjTC.findAll());
			
		} catch (EmplazamientoDaoException | TipoObjetoTelecontrolDaoException e) {
			log.error(e.getMessage());
		}

		
		if (idObjTeleControl==null && accion==null ){
			
			request = AddLstObjetosTelecontrol(request, idObjTeleControl);
			paginaDestino = "/configListObjetosTelecontrol.jsp";

		
		}else{	

			int intAccion = Integer.parseInt(accion);
			ObjetosTelecontrolDao _dao1 = ObjetosTelecontrolDaoFactory.create();
			ObjetosTelecontrolPk  ObjTelPk;
			
			switch (intAccion) {
			case ADMIN_ACCION_BORRAR:
				
			    ObjTelPk = new ObjetosTelecontrolPk(Integer.parseInt(idObjTeleControl));

			    try {
			    	_dao1.delete(ObjTelPk);
			    } catch (ObjetosTelecontrolDaoException e1) {
			     // TODO Auto-generated catch block
			    	e1.printStackTrace();
			    }	
				
			    request = AddLstObjetosTelecontrol(request, idObjTeleControl);
				paginaDestino = "/configListObjetosTelecontrol.jsp";
		
				break;
			case ADMIN_ACCION_MOSTRAR:

				request.setAttribute("accion", accion);
				request.setAttribute("idObjTelecontrol", idObjTeleControl);
				request.setAttribute("nombreObjTeleControl", request.getParameter("nombreObjTeleControl"));
		
				
			    try{
			    	ObjetosTelecontrol[] objetosTelecontrol = _dao1.findByDynamicWhere(" idobj_telecontrol = ? ", new Object[] {idObjTeleControl});
			    	request.setAttribute("objeto_telecontrol", objetosTelecontrol[0]);
			    } catch (ObjetosTelecontrolDaoException e) {
			    	log.error(e.getMessage());
			    }
				
				paginaDestino = "/configObjetosTelecontrol.jsp";
				
				break;
			case ADMIN_ACCION_ACTUALIZAR:

				ObjTelPk = new ObjetosTelecontrolPk(Integer.parseInt(idObjTeleControl));
			    ObjetosTelecontrol dto1 = new ObjetosTelecontrol();
			    
			    dto1.setNombre(request.getParameter("nombre"));
			    dto1.setTipoConexion(request.getParameter("tipo_conexion"));
			    dto1.setConexion(request.getParameter("conexion"));
			    dto1.setDireccionHex(request.getParameter("direccionHex"));
			    dto1.setEstado(request.getParameter("estado"));
			    dto1.setPosicion(request.getParameter("posicion"));
			    dto1.setEsclavo(request.getParameter("esclavo"));
			    dto1.setControlTarifa(request.getParameter("controlTarifa"));
			    dto1.setOrden(Integer.parseInt(request.getParameter("orden")));
			    dto1.setIdEmplazamiento(Integer.parseInt(request.getParameter("idEmplazamiento")));
			    dto1.setIdTipoObjetoTelecontrol(Integer.parseInt(request.getParameter("idTipoObjetoTC")));
			    
			    try {
			    	_dao1.update(ObjTelPk, dto1);
			    } catch (ObjetosTelecontrolDaoException e) {
			    	// TODO Auto-generated catch block
			    	e.printStackTrace();
			    }
							
				request.setAttribute("accion", accion);
				request.setAttribute("idObjTelecontrol", idObjTeleControl);
				request = AddLstObjetosTelecontrol(request, idObjTeleControl);
				paginaDestino = "/configListObjetosTelecontrol.jsp";
				
				break;
			case ADMIN_ACCION_INSERTAR:

				dto1 = new ObjetosTelecontrol();
				dto1.setNombre(request.getParameter("nombre"));
				dto1.setTipoConexion(request.getParameter("tipo_conexion"));
				dto1.setConexion(request.getParameter("conexion"));
				dto1.setDireccionHex(request.getParameter("direccionHex"));
				dto1.setEstado(request.getParameter("estado"));
				dto1.setPosicion(request.getParameter("posicion"));
				dto1.setEsclavo(request.getParameter("esclavo"));
				dto1.setControlTarifa(request.getParameter("controlTarifa"));
				dto1.setOrden(Integer.parseInt(request.getParameter("orden")));
				dto1.setIdEmplazamiento(Integer.parseInt(request.getParameter("idEmplazamiento")));
				dto1.setIdTipoObjetoTelecontrol(Integer.parseInt(request.getParameter("idTipoObjetoTC")));
				
				try {
					_dao1.insert(dto1);
				} catch (ObjetosTelecontrolDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				request.setAttribute("objeto_telecontrol", dto1);
				request.setAttribute("idObjTelecontrol", String.valueOf(_dao1.getMaxRows()));
			
				paginaDestino = "/configObjetosTelecontrol.jsp";
				
			}
			
		}
		
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public HttpServletRequest AddLstObjetosTelecontrol (HttpServletRequest request, String idObjTeleControl) {
		LstObjetosTelecontrol[] lstObjTelecontrol = null;
//		ObjTelecontrol[] lstObjetos = null;

		try {
			
			LstObjetosTelecontrolDao _dao1 = LstObjetosTelecontrolDaoFactory.create();
			lstObjTelecontrol = _dao1.findAll("admin");

			LstObjetosTelecontrol auxLstObjTeleCtrl;
//			lstObjetos = new ObjTelecontrol[lstObjTelecontrol.length];
//			for (int i = 0; i < lstObjTelecontrol.length; i++) {
//				auxLstObjTeleCtrl = (LstObjetosTelecontrol) lstObjTelecontrol[i];
//				lstObjetos[i] = new ObjTelecontrol();
//				lstObjetos[i].setNombre(auxLstObjTeleCtrl.getNombre());
//				lstObjetos[i].setIdObjeto(auxLstObjTeleCtrl.getIdObjeto());
//				lstObjetos[i].setEstado(auxLstObjTeleCtrl.getEstado());
//				lstObjetos[i].setPosicion(auxLstObjTeleCtrl.getPosicion());
//				lstObjetos[i].setDireccionHex(auxLstObjTeleCtrl.getDireccionHex());
//				lstObjetos[i].setTipoConexion(auxLstObjTeleCtrl.getTipoConexion());
//				lstObjetos[i].setDatosConexion(auxLstObjTeleCtrl.getConexion());
//				lstObjetos[i].setControlTarifa(auxLstObjTeleCtrl.getControlTarifa());
//				lstObjetos[i].setOrden(auxLstObjTeleCtrl.getOrden());
//				lstObjetos[i].setIdEmplazamiento(auxLstObjTeleCtrl.getIdEmplazamiento());
//				lstObjetos[i].setIdTipoObjetoTelecontrol(auxLstObjTeleCtrl.getIdTipoObjetoTelecontrol());
//				
//				
//				if (auxLstObjTeleCtrl.getEsclavo() == null || auxLstObjTeleCtrl.getEsclavo().length() == 0)
//					lstObjetos[i].setEsclavo((String) Constantes.getDMBUS());
//				else
//					lstObjetos[i].setEsclavo(auxLstObjTeleCtrl.getEsclavo());
//			}
			request.setAttribute("idObjTeleControl", idObjTeleControl);
			request.setAttribute("lstObjTelecontrol", lstObjTelecontrol);
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		

		return request;
	}
}

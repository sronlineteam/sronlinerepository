package es.sronline.admin;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import es.sronline.bbdd.ConexionBBDD;
import es.sronline.bbdd.dao.LstObjetosTelecontrolDao;
import es.sronline.bbdd.dto.LstObjetosTelecontrol;
import es.sronline.bbdd.factory.LstObjetosTelecontrolDaoFactory;
import es.sronline.core.Programacion;
import es.sronline.core.ProgramacionTelecontrolFormateada;

/**
 * Servlet implementation class SrvHistoricoProgramaciones
 */
@WebServlet("/SrvHistoricoProgramaciones")
public class SrvHistoricoProgramaciones extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvHistoricoProgramaciones() {
        super();
        log = Logger.getLogger(getClass().getName());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		String usuario = userDetails.getUsername();
    	
		log.debug("*********recibida petici�n de " + usuario + " en SrvListadoProgramacionesTelecontrol*********");

		Date now = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		String fHoy =  sdf.format(now);
		String desde = fHoy;
		String hasta = fHoy;
		
		LstObjetosTelecontrol[] lstObjTelecontrol = null;
		String destino = "/historicoProgramaciones.jsp";
		
		if(request.getParameter("desde")!=null && request.getParameter("hasta")!=null){
			desde = (String) request.getParameter("desde");
			hasta = (String) request.getParameter("hasta");
		}
		
		
		List<ProgramacionTelecontrolFormateada> programaciones = cargarListaProgramacionesHistoricas(usuario, desde, hasta);
		request.setAttribute("lstProgramaciones", programaciones);
		getServletContext().getRequestDispatcher(destino).forward(request, response);

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	private List<ProgramacionTelecontrolFormateada> cargarListaProgramacionesHistoricas(String usuario, String desde, String hasta) {
		List<ProgramacionTelecontrolFormateada> lstObjTelecontrol = null;
		String[] desdeArray = desde.split("-");
		String[] hastaArray = hasta.split("-");
		String desdeFormateada;
		String hastaFormateada;
		StringBuffer auxFecha = new StringBuffer();
		
		auxFecha.append(desdeArray[2]);
		auxFecha.append("-");
		auxFecha.append(desdeArray[1]);
		auxFecha.append("-");
		auxFecha.append(desdeArray[0]);
		desdeFormateada = auxFecha.toString();
		
		auxFecha = new StringBuffer();
		auxFecha.append(hastaArray[2]);
		auxFecha.append("-");
		auxFecha.append(hastaArray[1]);
		auxFecha.append("-");
		auxFecha.append(hastaArray[0]);
		hastaFormateada = auxFecha.toString();
		String[] sqlParams = {desdeFormateada, hastaFormateada};
		
		ConexionBBDD con = new ConexionBBDD();
		lstObjTelecontrol = con.consultarProgramacionesTelecontrolHistoricas(usuario, desdeFormateada, hastaFormateada);
		con.cerrar();

		
		return lstObjTelecontrol;
	}

}

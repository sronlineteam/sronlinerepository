package es.sronline.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.EmplazamientoDao;
import es.sronline.bbdd.dto.Emplazamiento;
import es.sronline.bbdd.dto.EmplazamientoPk;
import es.sronline.bbdd.exceptions.EmplazamientoDaoException;
import es.sronline.bbdd.factory.EmplazamientoDaoFactory;
import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvConfigEmplazamientos
 */
@WebServlet("/SrvConfigEmplazamientos")
public class SrvConfigEmplazamientos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvConfigEmplazamientos() {
        super();
        log = Logger.getLogger(getClass().getName());

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("null")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	final int ADMIN_ACCION_BORRAR = 0;
    	final int ADMIN_ACCION_MOSTRAR = 1;
    	final int ADMIN_ACCION_ACTUALIZAR = 2;
    	final int ADMIN_ACCION_INSERTAR = 3;
   	
		log.debug("*********recibida peticion en SrvConfigObjTelecontrol*********");
		String paginaDestino = "/configListEmplazamientos.jsp";
			
		String idEmplazamiento = request.getParameter("idEmplazamiento");
		String accion = request.getParameter("accion");

		if (accion!=null ){
			if(idEmplazamiento!=null){
				int intAccion = Integer.parseInt(accion);
				EmplazamientoDao _dao1 = EmplazamientoDaoFactory.create();
			    EmplazamientoPk  emplaPk = new EmplazamientoPk(Integer.parseInt(idEmplazamiento));
			    Emplazamiento dto1;
			    
				switch (intAccion) {
				case ADMIN_ACCION_BORRAR:
				    
				    try {
				    	_dao1.delete(emplaPk);
				    } catch (EmplazamientoDaoException e1) {
				     // TODO Auto-generated catch block
				    	e1.printStackTrace();
				    }	
					break;
				case ADMIN_ACCION_MOSTRAR:
				case ADMIN_ACCION_ACTUALIZAR:

					String[] comandos = new String[] {"abrir","cerrar","consultar"};
					
				    dto1 = new Emplazamiento();
				    
				    dto1.setIdEmplazamiento(Integer.parseInt(idEmplazamiento));
				    dto1.setEmplazamiento(request.getParameter("nombre"));
				    dto1.setPosicion(request.getParameter("posicion"));
				    dto1.setIdEmplazamientoPadre(Integer.parseInt(request.getParameter("idEmplazamientoPadre")));
				    
				    try {
				    	_dao1.update(emplaPk, dto1);
					    } 
				    catch (EmplazamientoDaoException e) {
					    	// TODO Auto-generated catch block
					    	e.printStackTrace();
					    }
					break;
				case ADMIN_ACCION_INSERTAR:
					String emplazamiento = request.getParameter("nombre");
					String posicion = request.getParameter("posicion");
					int idEmplazamientoPadre = Integer.parseInt(request.getParameter("idEmplazamientoPadre"));
								
					dto1 = new Emplazamiento();
					dto1.setEmplazamiento(emplazamiento);
					dto1.setPosicion(posicion);
					dto1.setIdEmplazamientoPadre(idEmplazamientoPadre);
					try {
						_dao1.insert(dto1);
						} catch (EmplazamientoDaoException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
								
					
				}
			}
		}
			
		request = addLstEmplazamientos(request);
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public HttpServletRequest addLstEmplazamientos (HttpServletRequest request) {
		Emplazamiento[] lstEmplazamiento = null;

		try {
			
			EmplazamientoDao _dao1 = EmplazamientoDaoFactory.create();
			lstEmplazamiento = _dao1.findAll();
			request.setAttribute("lstEmplazamientos", lstEmplazamiento);
		
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		

		return request;
	}
}

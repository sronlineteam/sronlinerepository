package es.sronline.admin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.core.Constantes;

/**
 * Servlet implementation class SrvDatosLog
 */
@WebServlet("/SrvDatosLog")
public class SrvDatosLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvDatosLog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String ficheroLog = "logSROnline.json";
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		if(request.getParameter("ficheroLog") != null ){
			ficheroLog = (String) request.getParameter("ficheroLog");
		}

		StringBuffer log = new StringBuffer();
		log.append("{\n");
		log.append("  \"headers\": [\n");
		log.append("   [");
		log.append("      { \"text\": \"fecha\", \"class\": \"fname\", \"width\": \"20%\" },");
		log.append("      \"file\",");
		log.append("      \"tipo\",");
		log.append("      \"mensaje\"");
		log.append("    ]");
		log.append("  ],\n");
		log.append("  \"rows\": [\n");
		
		String cadena;
		try {
		      FileReader f = new FileReader(request.getContextPath() + "logs/" + ficheroLog);
		      BufferedReader b = new BufferedReader(f);
		      while((cadena = b.readLine())!=null){
		    	  log.append(cadena).append(',').append("\n");
		      }
		      b.close();			
		} catch (Exception e) {

			Logger.getLogger(getClass().getName()).error("Error al cargar el log " + e.getMessage());;

		}
		log.deleteCharAt(log.length()-1);
		log.deleteCharAt(log.length()-1);
		log.append("    ]");
		log.append("  }");
		response.setContentType("application/x-json;charset=UTF-8");
	    PrintWriter out = response.getWriter();
	    out.println(log);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

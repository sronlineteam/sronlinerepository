package es.sronline.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import es.sronline.bbdd.dao.AccionesTelecontrolDao;
import es.sronline.bbdd.dao.TipoObjetoTelecontrolDao;
import es.sronline.bbdd.dto.AccionesTelecontrol;
import es.sronline.bbdd.dto.AccionesTelecontrolPk;
import es.sronline.bbdd.dto.TipoObjetoTelecontrol;
import es.sronline.bbdd.dto.TipoObjetoTelecontrolPk;
import es.sronline.bbdd.exceptions.AccionesTelecontrolDaoException;
import es.sronline.bbdd.exceptions.TipoObjetoTelecontrolDaoException;
import es.sronline.bbdd.factory.AccionesTelecontrolDaoFactory;
import es.sronline.bbdd.factory.TipoObjetoTelecontrolDaoFactory;

/**
 * Servlet implementation class SrvGestTipoObjetosTelecontrol
 */
@WebServlet("/SrvGestTipoObjetosTelecontrol")
public class SrvGestTipoObjetosTelecontrol extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger log;  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvGestTipoObjetosTelecontrol() {
        super();
        log = Logger.getLogger(getClass().getName());
   }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	final int ADMIN_ACCION_BORRAR = 0;
    	final int ADMIN_ACCION_MOSTRAR = 1;
    	final int ADMIN_ACCION_ACTUALIZAR = 2;
    	final int ADMIN_ACCION_INSERTAR = 3;

		log.debug("*********recibida peticion en " + this.getServletName() + "*********");
		String paginaDestino = "/configListTiposObjetoTelecontrol.jsp";

		String idTipoObjTeleControl = request.getParameter("idTipoObjTeleControl");
		String accion = request.getParameter("accion");

		if (idTipoObjTeleControl==null && accion==null ){
			TipoObjetoTelecontrolDao daoTipObjTC = TipoObjetoTelecontrolDaoFactory.create();
			try {
				request.setAttribute("lstTipoObjetoTelecontrol", daoTipObjTC.findAll());
				
			} catch (TipoObjetoTelecontrolDaoException e) {
				log.error(e.getMessage());
			}
			
			paginaDestino = "/configListTiposObjetoTelecontrol.jsp";

		
		}else{
			int intAccion = Integer.parseInt(accion);
			AccionesTelecontrolDao _daoAccioneesTC = AccionesTelecontrolDaoFactory.create();
			TipoObjetoTelecontrolPk  TipoObjTelPk;
			TipoObjetoTelecontrolDao _daoTipo;
			TipoObjetoTelecontrol dtoTipoObjeto;
			
//			String idObjTC = request.getParameter("idObjetoTelecontrol");
			try {
				AccionesTelecontrol[] lstAccionesTelecontrolDao = _daoAccioneesTC.findByDynamicWhere(" idtipo_obj_telecontrol = ? ", new Object[] {idTipoObjTeleControl} );
				request.setAttribute("lstAccionesTelecontrolDao", lstAccionesTelecontrolDao);
				
			} catch (AccionesTelecontrolDaoException e) {
				log.error(e.getMessage());
			}

			
			
			
			switch (intAccion) {
			case ADMIN_ACCION_BORRAR:
			    AccionesTelecontrolDao _dao = AccionesTelecontrolDaoFactory.create();
			    TipoObjetoTelecontrolDao _dao1 = TipoObjetoTelecontrolDaoFactory.create();

				for (int i = 1; i < 4; i++) {
					//Borramos las 3 filas de la tabla acciones telecontrol pertenecientes al objeto telecontrol que queremos eliminar
					//Eliminamos primero de esta tabla porque es hija de la tabla objeto telecontrol
					AccionesTelecontrolPk AccTelPk = new AccionesTelecontrolPk(Integer.parseInt(idTipoObjTeleControl),i);
				    try {
					       _dao.delete(AccTelPk);
					    } catch (AccionesTelecontrolDaoException e) {
					       // TODO Auto-generated catch block
					       e.printStackTrace();
					    }
				}
				
			    TipoObjetoTelecontrolPk  ObjTelPk = new TipoObjetoTelecontrolPk(Integer.parseInt(idTipoObjTeleControl));

			    try {
			    	//Una vez hemos borrado las 3 filas de la tabla acciones telecontrol borramos la fila de la tabla obj telecontrol
			    	_dao1.delete(ObjTelPk);
			    } catch (TipoObjetoTelecontrolDaoException e1) {
			     // TODO Auto-generated catch block
			    	e1.printStackTrace();
			    }	
				
//			    request = AddLstObjetosTelecontrol(request, idTipoObjTeleControl);
				paginaDestino = "/configListTiposObjetoTelecontrol.jsp";

				
				break;
			case ADMIN_ACCION_MOSTRAR:
//				try {
//					AccionesTelecontrol[] lstAccionesTelecontrolDao = _daoAccioneesTC.findByDynamicWhere(" idobj_telecontrol = ? ", new Object[] {idObjTC} );
//					request.setAttribute("lstAccionesTelecontrolDao", lstAccionesTelecontrolDao);
//				} catch (AccionesTelecontrolDaoException e) {
//					log.error(e.getMessage());
//				}
				TipoObjTelPk = new TipoObjetoTelecontrolPk(Integer.parseInt(idTipoObjTeleControl));
			    _daoTipo = TipoObjetoTelecontrolDaoFactory.create();
			    try {
					dtoTipoObjeto = _daoTipo.findByPrimaryKey(Integer.valueOf(idTipoObjTeleControl));
				    request.setAttribute("objeto_telecontrol", dtoTipoObjeto);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (TipoObjetoTelecontrolDaoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				paginaDestino = "/configTipoObjetosTelecontrol.jsp";
				
				break;
			case ADMIN_ACCION_ACTUALIZAR:
				//Actualizaci�n de las acciones abrir, cerrar y consultar
				String[] comandos = new String[] {"abrir","cerrar","consultar"};
				String auxRespuestaOk = null;
				
				for (int i = 1; i < 4; i++) {
					//Definimos un array de strings para introducir los par�metros de una forma m�s robusta 
					//dado que la estructura de las variables nos permiten trabajar as�
					AccionesTelecontrolPk accTelPk = new AccionesTelecontrolPk(Integer.parseInt(idTipoObjTeleControl),i);
					AccionesTelecontrolDao _daoAcciones = AccionesTelecontrolDaoFactory.create();
					AccionesTelecontrol dto = new AccionesTelecontrol();
					dto.setIdTipoObjetoTelecontrol(Integer.parseInt(idTipoObjTeleControl));
					dto.setIdAccion(i);
					dto.setComando(request.getParameter(comandos[i-1]+"_comando"));
					dto.setAccion(request.getParameter(comandos[i-1]+"_accion"));
					dto.setByte6(request.getParameter(comandos[i-1]+"_byte6"));
					dto.setTieneCRC(Integer.parseInt(request.getParameter(comandos[i-1]+"_crc")));
					if(!request.getParameter(comandos[i-1]+"_respuestaOk").isEmpty()){
						auxRespuestaOk = request.getParameter(comandos[i-1]+"_respuestaOk");
						if (auxRespuestaOk.equalsIgnoreCase("null"))
							auxRespuestaOk = null;
					}
					dto.setRespuestaOK(auxRespuestaOk);  
				 		
					try {
						_daoAcciones.update(accTelPk,dto);
					} catch (AccionesTelecontrolDaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				TipoObjTelPk = new TipoObjetoTelecontrolPk(Integer.parseInt(idTipoObjTeleControl));
			    _daoTipo = TipoObjetoTelecontrolDaoFactory.create();
			    dtoTipoObjeto = new TipoObjetoTelecontrol();
			    
			    dtoTipoObjeto.setTipoObjetoTelecontrol(request.getParameter("tipoObjeto"));
			    dtoTipoObjeto.setDescripcion(request.getParameter("descripcion"));
			    dtoTipoObjeto.setIdTipoObjetoTelecontrol(Integer.parseInt(request.getParameter("idTipoObjTeleControl")));
			    dtoTipoObjeto.setTiempoRespuesta(Integer.parseInt(request.getParameter("tiempoRespuesta")));
			    
			    try {
			    	_daoTipo.update(TipoObjTelPk, dtoTipoObjeto);
					request.setAttribute("lstTipoObjetoTelecontrol", _daoTipo.findAll());

			    } catch (TipoObjetoTelecontrolDaoException e) {
			    	// TODO Auto-generated catch block
			    	e.printStackTrace();
			    }


				try {
					AccionesTelecontrol[] lstAccionesTelecontrolDao = _daoAccioneesTC.findByDynamicWhere(" idtipo_obj_telecontrol = ? ", new Object[] {idTipoObjTeleControl} );
					request.setAttribute("lstAccionesTelecontrolDao", lstAccionesTelecontrolDao);
				} catch (AccionesTelecontrolDaoException e) {
					log.error(e.getMessage());
				}
				request.setAttribute("accion", null);
				paginaDestino = "/configListTiposObjetoTelecontrol.jsp";

				break;
			case ADMIN_ACCION_INSERTAR:
				String tipoObjeto = request.getParameter("tipoObjeto");
				String descripcion = request.getParameter("descripcion");
				//int idTipoObjetoTelecontrol = Integer.parseInt(request.getParameter("idTipoObjTeleControl"));
				int tiempoRespuesta = Integer.parseInt(request.getParameter("tiempoRespuesta"));
							
				AccionesTelecontrolDao _daoAccion = AccionesTelecontrolDaoFactory.create();
				_daoTipo = TipoObjetoTelecontrolDaoFactory.create();
				dtoTipoObjeto = new TipoObjetoTelecontrol();
				dtoTipoObjeto.setTipoObjetoTelecontrol(tipoObjeto);
				dtoTipoObjeto.setDescripcion(descripcion);
				//dtoTipoObjeto.setIdTipoObjetoTelecontrol(idTipoObjetoTelecontrol);
				dtoTipoObjeto.setTiempoRespuesta(tiempoRespuesta);
				
				try {
					_daoTipo.insert(dtoTipoObjeto);
				} catch (TipoObjetoTelecontrolDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (int i = 1; i < 4; i++) {

					AccionesTelecontrol dto = new AccionesTelecontrol();
					dto.setIdTipoObjetoTelecontrol(_daoTipo.getMaxRows());;
					dto.setIdAccion(i);
					dto.setComando("00");
					dto.setAccion("00");
					dto.setByte6("00");
					dto.setTieneCRC(0);
					
					try {
						_daoAccion.insert(dto);
					} catch (AccionesTelecontrolDaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				request.setAttribute("objeto_telecontrol", dtoTipoObjeto);
				request.setAttribute("idObjTelecontrol", String.valueOf(_daoAccion.getMaxRows()));
				AccionesTelecontrolDao _dao5 = AccionesTelecontrolDaoFactory.create();
				try {
					AccionesTelecontrol[] lstAccionesTelecontrolDao = _dao5.findByDynamicWhere(" idobj_telecontrol = ? ", new Object[] {_daoAccion.getMaxRows()} );
					request.setAttribute("lstAccionesTelecontrolDao", lstAccionesTelecontrolDao);
				} catch (AccionesTelecontrolDaoException e) {
					log.error(e.getMessage());
				}
				
				paginaDestino = "/configListTiposObjetoTelecontrol.jsp";
				
				break;

			default:
				break;
			}

		}
		getServletContext().getRequestDispatcher(paginaDestino).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		log.debug("*********recibida peticion Post en " + this.getServletName() + "*********");
		doGet(request, response);
	}

}

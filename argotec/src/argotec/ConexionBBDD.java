package argotec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import argotec.Constantes;

public class ConexionBBDD {
	
	static Connection conexion = null;
	static Logger log;   

	
	public static void conectar(){
		try {
			conexion = java.sql.DriverManager.getConnection (Constantes.getConexionBBDD(),Constantes.getUsuarioBBDD(), Constantes.getPasswordBBDD());
			log = Logger.getLogger("Argotec");
	 
			//valoresDispositivos();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
	}
	
	
	
	public static void GuardarDatos ( Qbic_obj objeto){
		
		
		try {
			
			java.sql.Statement stmt1 = conexion.createStatement();
			
			stmt1.executeUpdate("INSERT INTO "+Constantes.getNombreBBDD()+".info_measured (alias_qbic, measured_time_comp, received_time_comp, "
			     + "name_group_qbic, alias_comp, value_comp) "
			     + "VALUES ('"+objeto.getAlias()+"', '"+objeto.getMeasured_time()+"', '"+objeto.getReceived_time()+"', '"+objeto.getName_group_qbic()+"', '"+objeto.getAlias_comp()+"', "
			     + "'"+objeto.getValue_comp()+"')");
			
			}catch (MySQLIntegrityConstraintViolationException d){
				log = Logger.getLogger("Entrada duplicada");
			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		
		
		
	}
	
	public static Qbic_obj SacarUltimoDato (String alias_qbic, String name_group_qbic, String alias_comp){
        Qbic_obj Objeto_1 = new Qbic_obj();

		

		try {
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet datos = stmt.executeQuery("SELECT * FROM "+Constantes.getNombreBBDD()+".info_measured WHERE alias_comp ='"+alias_comp+"' and alias_qbic = '"+alias_qbic+"' and name_group_qbic = '"+name_group_qbic+"' ORDER BY measured_time_comp DESC LIMIT 1");
			while (datos.next()){
		        Objeto_1.setAlias(datos.getString("alias_qbic"));
		        Objeto_1.setAlias_comp(datos.getString("alias_comp"));
		        Objeto_1.setMeasured_time(datos.getTimestamp("measured_time_comp"));
		        Objeto_1.setReceived_time(datos.getTimestamp("measured_time_comp"));
		        Objeto_1.setName_group_qbic(datos.getString("name_group_qbic"));
		        Objeto_1.setValue_comp(datos.getInt("value_comp"));
			}
		}
		catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }

		
		return Objeto_1;
	}
	
	public static List<Integer> ValoresDia (String alias_qbic, String name_group_qbic, String alias_comp, String dia){
		List<Integer> medidas = new ArrayList<Integer>();

		

		try {
			java.sql.Statement stmt = conexion.createStatement();
			ResultSet datos = stmt.executeQuery("SELECT value_comp FROM "+Constantes.getNombreBBDD()+".info_measured WHERE alias_comp ='"+alias_comp+"' and alias_qbic = '"+alias_qbic+"' and name_group_qbic = '"+name_group_qbic+"' and CAST(measured_time_comp AS DATE) = '"+dia+"'");
			while (datos.next()){
		        medidas.add(datos.getInt("value_comp"));
			}
		}
		catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
		return medidas;		
		
	}
	
		
	}
	


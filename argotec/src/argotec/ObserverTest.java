package argotec;





import es.argotec.netbowsqbicapi.Qbic;
import es.argotec.netbowsqbicapi.QbicObserver;
import es.argotec.nodes.Component;
import java.sql.Timestamp;

import org.apache.log4j.Logger;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author diego
 */
public class ObserverTest extends QbicObserver {
	static Logger log;   

    
    public void treatNewMessageEvent(Qbic qbic, Component component) {
		log = Logger.getLogger("Argotec");

    	if ((Integer)component.getState().getValue() != 0){
			log.debug("Event from qbic: "
    				+ qbic.getAlias() + " ("+ component.getAlias() +")"
    				+ " at group " + qbic.getQbicGroup().getName()
    				+ ": " + component.getState().getValue() + ", measured at: " + component.getState().getMeasureTime() 
    				+ " received at: " + component.getState().getReceivedTime());
        
    		Timestamp measured = new Timestamp (component.getState().getMeasureTime().getMillis());
    		Timestamp received = new Timestamp (component.getState().getReceivedTime().getMillis());
    		Qbic_obj Objeto_1 = new Qbic_obj();
    		Objeto_1.setAlias(qbic.getAlias());
    		Objeto_1.setAlias_comp(component.getAlias());
    		Objeto_1.setMeasured_time(measured);
    		Objeto_1.setReceived_time(received);
    		Objeto_1.setName_group_qbic(qbic.getQbicGroup().getName());
    		Objeto_1.setValue_comp((Integer)component.getState().getValue());
        
        	ConexionBBDD.GuardarDatos(Objeto_1);
        }
    } 
}

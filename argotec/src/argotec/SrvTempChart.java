package argotec;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class SrvTempChart
 */
@WebServlet("/SrvTempChart")
public class SrvTempChart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger log;   

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvTempChart() {
        super();
        log = Logger.getLogger("Argotec");

        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String load = null;
		String google_chart = null;
		
		//-Line:
	    //  load = "line";
	    //  google_chart = "charts.Line";
		
		
		//-Areachart:
	    //  load = "corechart";
	    //  google_chart = "visualization.AreaChart";
		
		//-AnnotationChart:
	    //  load = "annotationchart";
	    //  google_chart = "visualization.AnnotationChart";
		
		//-Calendar:
	      load = "calendar";
	      google_chart = "visualization.Calendar";
		
		//-Barras:
	    //  load = "corechart";
	    //  google_chart = "visualization.ColumnChart";



		
		String inputStr = "2016-06-30";
		String qbic = "Qbic-6";
		String group = "Group-2";
		String comp = "BATT";
		int num_day = 10;

		
		
		
		List<Integer> medidas_bbdd = new ArrayList<Integer>();
		ArrayList<String> dias = new ArrayList<String>();
		List<Integer> medidas = new ArrayList<Integer>();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date inputDate = null;
		try {
			inputDate = dateFormat.parse(inputStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int j = 0; j< num_day; j++){
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTime(inputDate); 
	      calendar.add(Calendar.DAY_OF_YEAR, j);  

	      String reportDate = dateFormat.format((Date) calendar.getTime());
	      dias.add(reportDate);
	      ConexionBBDD.conectar();
	      medidas_bbdd = ConexionBBDD.ValoresDia(qbic, group, comp, reportDate);
		
	      int sum = 0;
	      for (int i = 0; i < medidas_bbdd.size(); i++) {
	    	  sum += medidas_bbdd.get(i);
	      }
	      if (sum != 0){
	    	  int mean =  sum / medidas_bbdd.size();
	    	  medidas.add(mean);
	      }else{
	    	  int mean = 0;
	    	  medidas.add(mean);

	    	  
	      }
		}
	    
	    log.debug("la media es: " + medidas);
	    log.debug("los dias son: " + dias);
	    
		request.setAttribute("medidas", medidas);
		request.setAttribute("dias", dias);
		request.setAttribute("load", load);
		request.setAttribute("google_chart", google_chart);
		
		getServletContext().getRequestDispatcher("/temp_chart.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	


}

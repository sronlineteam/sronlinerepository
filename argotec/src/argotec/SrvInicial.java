package argotec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class SrvDisplay
 */
@WebServlet("/")
public class SrvInicial extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger log;   


    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvInicial() {
        super();
        // TODO Auto-generated constructor stub
        log = Logger.getLogger("Argotec");

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ConexionBBDD.conectar();
		Example.lanzar();
		getServletContext().getRequestDispatcher("/thermo.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		doGet(request, response);
	}

}

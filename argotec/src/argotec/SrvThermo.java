package argotec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;


/**
 * Servlet implementation class SrvThermo
 */
@WebServlet("/SrvThermo")
public class SrvThermo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger log;   

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SrvThermo() {
        super();
        log = Logger.getLogger("Argotec");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		ConexionBBDD.conectar();
		Qbic_obj qbic = ConexionBBDD.SacarUltimoDato("Qbic-6", "Group-2", "BATT");
		String valor = String.valueOf(qbic.getValue_comp());
		String nombre = qbic.getAlias_comp();
		String yellow_min = "50";
		String yellow_max = "75";
		String red_min = "75";
		String red_max = "120";
		String minor_ticks = "2";
		
	    list.add(valor);
	    list.add(nombre);
	    list.add(yellow_min);
	    list.add(yellow_max);
	    list.add(red_min);
	    list.add(red_max);
	    list.add(minor_ticks);
	    
	   
	    String json = new Gson().toJson(list);

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8"); 
		response.getWriter().write(json); 


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

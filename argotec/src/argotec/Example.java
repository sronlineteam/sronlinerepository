package argotec;



import org.apache.log4j.Logger;

import es.argotec.netbowsqbicapi.Qbic;
import es.argotec.netbowsqbicapi.QbicGroup;
import es.argotec.netbowsqbicapi.QbicNetwork;
import es.argotec.netbowsqbicapi.exceptions.NonEmptyGroupException;
import es.argotec.netbowsqbicapi.exceptions.NonUniqueNameException;
import es.argotec.netbowsqbicapi.exceptions.NotExistsException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author diego
 */
public class Example {    
	static Logger log;   

    public static  void lanzar() {
		log = Logger.getLogger("Argotec");

        QbicNetwork network = new QbicNetwork("dataloggers.argotec.es", 6556, 6560);
		//Código de ejemplo para ver el uso de la API
        ObserverTest observer = new ObserverTest();
        try {
        	log.debug("Deleting all nodes...");
            for (QbicGroup group: network.getQbicsGroups()){
                for (Qbic qbic: group.getQbics()){
                    network.removeQbic(group.getName(), qbic.getAlias());
                }
                network.deleteQbicsGroup(group.getName());                
            }
            log.debug("Deleted.");
            log.debug("Creating Qbips..");
            network.createQbicsGroup("Group-1", "First group");
            network.createQbicsGroup("Group-2", "Second group");
            
            log.debug("Created groups: " + network.getQbicGroupsAliases());
            log.debug("Adding qbics groups:");
            
            
            network.registerQbic("Qbic-4", "Group-1", "0405");
            network.registerQbic("Qbic-6", "Group-2", "0407");
			
            log.debug("Added.");
            // Configuring all qbics:
            log.debug("Configuring all qbics...");
			//Configuramos que todos los qbics midan cada 5 minutos. Ver documentación
            network.setQbicsSampleTime(1);
			//Enviamos cada 3 muestras, es decir, cada 15 minutos.
            network.setQbicsSyncCount(2);
            log.debug("Configured.");
            // Subscribe to all qbics
            log.debug("Starting to consume events...");
            network.addObserver(observer);           
            // Como no nos hacemos un network.stop(), el programa no se cierra.
        } catch (NotExistsException | NonEmptyGroupException | NonUniqueNameException  ex) {
        	log.debug(ex.getMessage());	
        	network.stop();
        }
    }
}

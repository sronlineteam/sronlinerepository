package argotec;

import java.sql.Timestamp;


public class Qbic_obj {

	
	private String alias = null;
	private Timestamp measured_time = null;
	private Timestamp received_time = null;
	private Integer value_comp = 0;
	private String alias_comp = null;
	private String name_group_qbic = null;
	
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}


	public Timestamp getMeasured_time() {
		return measured_time;
	}
	public void setMeasured_time(Timestamp measured) {
		this.measured_time = measured;
	}


	
	public Timestamp getReceived_time() {
		return received_time;
	}
	public void setReceived_time(Timestamp received_time) {
		this.received_time = received_time;
	}
	
	
	public String getAlias_comp() {
		return alias_comp;
	}
	public void setAlias_comp(String alias_comp) {
		this.alias_comp = alias_comp;
	}
	
	public String getName_group_qbic() {
		return name_group_qbic;
	}
	public void setName_group_qbic(String name_group_qbic) {
		this.name_group_qbic = name_group_qbic;
	}
	
	public Integer getValue_comp() {
		return value_comp;
	}
	public void setValue_comp(Integer object) {
		this.value_comp = object;
	}

	
	
}

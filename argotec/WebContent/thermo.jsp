<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.JSONObject" %>
    <%
	Integer valor = null;
	Object obj_datos = request.getAttribute("valor");
	if (obj_datos != null){
		valor = (Integer) obj_datos;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Temperatura Argotec</title>
</head>
<body>
  <head>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
   <script type="text/javascript">
   
	var valor_str;
	var valor_int;
	var nombre;
	var yellow_min;
	var yellow_max;
	var red_min;
	var red_max;
	var minor_ticks;
		
	$(document).ready(function() {
		$.get('SrvThermo',
		function termo (responseText) {
			valor  = responseText;
			valor_clean = valor.replace(/["']/g,"").replace("[","").replace("]","").split(",");
			valor_str = valor_clean[0];			
			nombre = valor_clean[1];
			red_min = parseInt(valor_clean[2]);
			red_max = parseInt(valor_clean[3]);
			yellow_min = parseInt(valor_clean[4]);
			yellow_max = parseInt(valor_clean[5]);
			minor_ticks = parseInt(valor_clean[6]);

			
		});
	});

	
	
	google.charts.load('current', {'packages':['gauge']});
   	google.charts.setOnLoadCallback(drawChart);
   	function drawChart() {

    	  
     	valor_int = parseInt(valor_str); 


     	
        var data = google.visualization.arrayToDataTable([
          	['Label', 'Value'],
          	[nombre, valor_int]

        ]);

     	var options = {
        	width: 400, height: 120,
          	redFrom: red_min, redTo: red_max,
          	yellowFrom:yellow_min, yellowTo: yellow_max,
          	minorTicks: minor_ticks
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);
        
        setInterval(function() {
        $(document).ready(function() {
    		$.get('SrvThermo',
    		function termo (responseText) {
    			valor  = responseText;
    			valor_clean = valor.replace(/["']/g,"").replace("[","").replace("]","").split(",");
    			valor_str = valor_clean[0];    			
    			nombre = valor_clean[1];
    			red_min = parseInt(valor_clean[2]);
    			red_max = parseInt(valor_clean[3]);
    			yellow_min = parseInt(valor_clean[4]);
    			yellow_max = parseInt(valor_clean[5]);
    			minor_ticks = parseInt(valor_clean[6]);

    			
    		});
    	}); 
    	  
         	valor_int = parseInt(valor_str); 

         	
         	data.setValue(0, 1, valor_int);
           	chart.draw(data, options);
           	
     	}, 5000);
        
        


	}
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 400px; height: 120px;"></div>

  </body>


</body>
</html>
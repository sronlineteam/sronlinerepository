<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.JSONObject" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Temperatura Argotec</title>
</head>
<body>
  <head>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
   <script type="text/javascript">
   
	<%
	List<Integer> medidas = null;
	Object obj_0 = request.getAttribute("medidas");
	if (obj_0 != null)
		medidas = (List<Integer>) obj_0;
	%>
	<%	
	ArrayList<String> dias = null;
	Object obj_1 = request.getAttribute("dias");
	if (obj_1 != null)
		dias = (ArrayList<String>) obj_1;

	
	%>
	
	<%
	String load = null;
	Object obj_2 = request.getAttribute("load");
	if (obj_2 != null)
		load = (String) obj_2;
	%>

	<%
	String google_chart = null;
	Object obj_3 = request.getAttribute("google_chart");
	if (obj_3 != null)
		google_chart = (String) obj_3;
	%>
	


   
   google.charts.load('current', {packages: ['<%=load%>']}); //LINEA 1 A MODIFICAR
   google.charts.setOnLoadCallback(drawMultSeries);
	
   var medidas = <%=medidas%>;
   

   function drawMultSeries() {

           var data = new google.visualization.DataTable();
           data.addColumn('date', 'Time of Day');
           data.addColumn('number', 'ºC');
		   var row =[];
           <% for (int i = 0; i < dias.size(); i++) {%>
            row = [];
      		row.push(new Date ("<%=dias.get(i)%>"));
      		row.push(<%=medidas.get(i)%>);
      		data.addRows([row]);
      	  <%}%>



           var options = {
             title: 'Temperature',
             width: 900,
             height: 500,
             hAxis: {
               format: 'MMM dd, yyyy',
               gridlines: {count: 15}
             },
             vAxis: {
               gridlines: {color: 'none'},
               minValue: 0
             }
           };


         var chart = new google.<%=google_chart%>(
           document.getElementById('chart_div')); //LINEA 2 A MODIFICAR

         chart.draw(data, options);
       }
   </script>
  </head>
  <body>
    <div id="chart_div" style="width: 400px; height: 120px;"></div>

  </body>


</body>
</html>